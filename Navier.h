#include "HEP.h"

class Navier : public HEP
{
      class EulPressureFunction;
      class NavierDotFromInterval;
      void Single_mesh_move (Mesh& mesh, PlateList& plate, Function& zeta, Function& zetaold, Function& varbottom, Function& varbottomold);

      int Single_run(char* dirname, bool resume);
};
