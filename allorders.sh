#!/bin/bash

source orders.sh

top=$PWD

rm -rf ffc.log
for dir in . plates
do
cd $top/$dir
for file in *.ufl
do
 echo " order checking $file"
 cp $file $top/tmp
 cd $top/tmp
 cp $file tmp.ufl
 for exchange in $EXCHANGES
 do
  sed -e "s/$exchange/g" tmp.ufl > tmp1.ufl
  mv tmp1.ufl tmp.ufl
 done;
 diff $top/$dir/$file.last tmp.ufl > /tmp/changed
 if [ ! $? -eq 0 -o ! -e $top/$dir/$file.h ]
 then
   mv tmp.ufl $file
   ffc -l dolfin $file > $top/ffc.log
   name=`echo $file | sed -e 's/\(.*\)\..*/\1/' -e 's/\.//' -`
   newh=$name.h
   oldh=$top/$dir/$name.h
   diff $newh $oldh > /tmp/changed
   if [ ! $? -eq 0 ]
   then
     echo " $file changed..."
     cp $newh $oldh
     cp $file $top/$dir/$file.last
   fi;
 fi;
 cd $top/$dir
done;
cd $top
done;

touch orders.lastbuild
