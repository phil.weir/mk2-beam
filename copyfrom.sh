#!/bin/bash
#Setup from arg1 to arg2
source outroot.sh
cd ..
./procsetup.sh $2
cp $outroot/output.$1/automesh.geo $outroot/output.$2
cp $outroot/output.$1/params/plate1.$1 $outroot/output.$2/params/plate1.$2
cp $outroot/output.$1/params/plate2.$1 $outroot/output.$2/params/plate2.$2
cp $outroot/output.$1/params/wavemaker.$1 $outroot/output.$2/params/wavemaker.$2
sed -e "s/\\.$1/.$2/" $outroot/output.$1/params/parameters.$1 > $outroot/output.$2/params/parameters.$2
echo $1 > $outroot/output.$2/copied_from
