%module mk2matrix

#define HAS_PETSC 1
#define HAS_MPI 1
%{
#include <dolfin.h>
#include <numpy/arrayobject.h>
%}
%import "std_string.i"
%import "std_map.i"
%import "dolfin/swig/shared_ptr_classes.i"
%import "dolfin/swig/typemaps/std_pair.i"
%import "dolfin/swig/typemaps/numpy.i"
%import "dolfin/swig/typemaps/array.i"
%import "dolfin/swig/typemaps/std_set.i"
%import <exception.i>
%include "dolfin/swig/exceptions.i"
%import <std_string.i>

%{
#include "matrix_solve.h"
%}

%include "matrix_solve.h"
