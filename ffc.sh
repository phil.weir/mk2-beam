#!/bin/bash

echo
echo "Building MK2"
echo "============"
echo

export GINAC_DIR=`pwd`

builddir=`pwd`/../build
mkdir -p $builddir
mkdir -p $builddir/scripts
mkdir -p $builddir/include

if [[ ! -e $builddir/scripts/outroot.sh || ! -e $builddir/include/outroot.h ]]
then
  echo -n "[MK2-BUILD] Enter output directory (omitting trailing slash): "
  read -e OUTROOT
  mkdir -p $OUTROOT
  OUTROOT2=$(echo $OUTROOT|sed -e 's/\//\\\//g')
  echo -n "[MK2-BUILD] Enter gmsh path: "
  read -e GMSH_PATH
  GMSH_PATH2=$(echo $GMSH_PATH|sed -e 's/\//\\\//g')
  sed -e "s/OUTROOT_INPUT/$OUTROOT2/g" outroot.sh.base > $builddir/scripts/outroot.sh
  sed -e "s/OUTROOT_INPUT/$OUTROOT2/g" -e "s/GMSH_PATH_INPUT/$GMSH_PATH2/g" outroot.h.base > $builddir/include/outroot.h
else
  source $builddir/scripts/outroot.sh
  echo "[MK2-BUILD] Output dir is : $outroot"
  echo "[MK2-BUILD] gmsh path is : $gmsh_path"
fi;

echo
echo "[MK2-BUILD] Running build.sh"
./build.sh $builddir
if [ "$?" -ne "0" ]
then exit 1
fi

#if [ orders.sh -nt orders.lastbuild ]
#then
#  echo "Orders need to be rebuilt..."
#  ./allorders.sh
#fi;
#
#rm -rf ffc.log
#top=$PWD
#for dir in . plates
#do
#cd $top/$dir
#for file in `ls *.ufl`
#do
#  name=`echo $file | sed -e 's/\(.*\)\..*/\1/' -e 's/\.//' -`
#  if [ $name.h -ot $name.ufl -o ! -e $name.h ]
#  then
#   echo "[Rebuild $name.ufl]"
#   if [ $1!='-T' ]
#   then
#     $top/anorder.sh $top $file
#     echo ' : done'
#   fi;
#   touch $name.h
#  fi;
#done;
#cd $top
#done;

sd=`pwd`
if [ $1!='-T' ]
then
 echo
 echo "[MK2-BUILD] Running CMake"
 #cp CMakeLists.txt $builddir
 cd $builddir
 cmake $sd
 if [ "$?" -ne "0" ]
 then
   exit 2
 fi

 echo
 echo "[MK2-BUILD] Running make"
 make
 if [ "$?" -ne "0" ]
 then
   exit 3
 fi
 #echo "Running make (mk2)"
 #make mk2
 #echo "Running make (beamrun)"
 #make beamrun
fi
cp $sd/procsetup.sh .
cp $sd/beamprocsetup.sh .
cp $sd/reffc.sh .
cp $sd/pyreffc.sh .

mv $sd/mk2beam.py python
mv $sd/_mk2beam.so python

mkdir -p python
cp -R $sd/python/* python
cp $sd/*.sh $sd/*.py scripts
rm scripts/reffc.sh scripts/ffc.sh scripts/procsetup.sh scripts/beamprocsetup.sh scripts/pyreffc.sh

echo "SOURCE_DIR=$sd" > source_dir.sh

cd $sd
tar -zcf $builddir/source.tgz *
