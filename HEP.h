#ifndef HEP_H
#define HEP_H

#include <dolfin.h>
#include <iostream>
#include <ginac/ginac.h>
#include "FluidMesh.h"
#include "DoubleTop.h"
#include "DoubleFluidMesh.h"
#include "AltUnitInterval.h"
#include "DoubleAltUnitInterval.h"
#include "Top.h"
#include "plateproblem.h"
#include "logfile.h"
#include "WaveMaker.h"

// Use our own eps, above the compiler's zero!
#define EPS 1e-18

using namespace dolfin;
class HEP
{
public:
      class DzFunction;
      class SlopeFunction;
      class BernoulliPressure;
      class Pdtn1Function;
      class Boundary;
      class MeshFromPLine;
      class PlateMask;
      class FSLineFromMesh;
      class Single_ZetaInitialConditions;
      class WavedNeumann;
      class WaveddNeumann;
      class WdotNeumann;
      class WddotNeumann;
      class VelNeumann;
      class Beach_nuFunction;
      class Single_FreeSurfaceFlat;
      class Single_TopSubDomain;
      class Single_FreeSurface;
      class Single_Plates;
      class Single_FreeSurfaceIns;
      //class Single_NearFreeSurface;
      class ApproxzFunction;
      class WaveVelDBC;
      class WaveAccDBC;
      class SquareFunction;
      //void Single_mesh_move (Mesh& mesh, Function& W, Function& Wold, Function& zeta, Function& zetaold, Function& varbottom, Function& varbottomold);
      virtual void Single_mesh_move (Mesh& mesh, Mesh& mesh0, PlateList& plate, Function& zeta, Function& zetaold, Function& varbottom, Function& varbottomold);
      virtual int Single_run(char* dirname, bool resume);
      int Platetest_run(char* dirname);

      class Multiple_ZetaInitialConditions;
      class Multiple_LastCoordFunction;
      class Multiple_FreeSurfaceFlat;
      class Multiple_TopSubDomain;
      class Multiple_FreeSurface;
      class Multiple_PlateDomain;
      void Multiple_mesh_move (Mesh& mesh, MeshFunction<uint>& map, Function& W, Function& Wold, Function& Y, Function& Yold, Function& zeta, Function& zetaold);
      int Multiple_run();

    class PointFunction;
    class GiNaCFunction;
    class Constantws;
    class MeshFromInterval;
    class PressureFunction;
    class AlphaFunction;
    class LukeBoundary;

  public:
    int run (int argc, char* argv[]);
    HEP ();
    static WaveMaker* wavemaker;

    double set_parameters ( char* paraloc );
    void predict (Vector& r, double Dt, Vector& av, Vector& bv, Vector& cv, Vector& dv, Vector& ev);
    void correct (Vector& r, double Dt, Vector& av, Vector& bv, Vector& cv, Vector& dv, Vector& ev);
    void mesh_vector_inject (FunctionSpace& V, Mesh& mesha, MeshFunction<uint>& mapa, Mesh& meshb, MeshFunction<uint>& mapb, Vector& v1, Vector& v2);
    void mesh_vector_inject (FunctionSpace& V, Mesh& mesh1, MeshFunction<uint>& map, Vector& v1, Vector& v2);
    void combine_disps (Vector& disp, Mesh& mesh1, MeshFunction<uint>& map1, Vector& v1, Mesh& mesh2, MeshFunction<uint>& map2, Vector& v2);
    void parse_args(int argc, char* argv[], char* paramloc, char* dirname, char* mkoutdir, char* outdir, char* logstr, std::ostream& logfile, bool& resume);
    void dz ( Mesh& mesh, FunctionSpace& V, Vector& v, Function& f );
    double get_volume_norm ( Function& f );
    double get_interval_norm ( Function& f );
//    double approx_z ( const Function& f, double x, bool linearize = false, bool test=false );
  /* Parameters */
    uint N, M; // # facets along x, y boundaries resp.
    double Dt; // Timestep
    double rhow; // Density of the water
    double mup; // Plate viscosity
    double rhop; // Density of the plate
    double T; // Final time
    double lscale; // Length scaling
    double musurf; // Artificial surface viscosity
    double g; // Acc. due to gravity
    double E, h, nu, beta; // Plate parameters
    double alpha; // Weighting for Wddt correction
    double tolexp; // Exponent for our tolerance in the predictor-corrector step
    int horizfm;
    static double length; // Domain dimensions
    int clmax; // Maximum number of predictor-corrector iterations before breaking
    int maxfs;
    double k_guess; // Estimate of the wave-number - only used to output approx wave celerity under plate (Cs)
    char icmode;
    static bool nlfluid, tilted_fluid;
    int forcingmode;
    double f_wd_h, f_wd_x, f_wd_m, f_wde_t; GiNaC::ex f_ex_ex;
    char expstr[500], buffer[500], texstr[100], runname[40];
    bool default_args, pid_args, dir_args, plate_output, ciout, fout, aout, euler;
    int ostartframe; double ostarttime;
    int outfreq;
    bool platetest;
    FILE* forcingfile;
    FILE* paramfile;
    std::ofstream texfile;
    int pid;
    int argl;
    static bool twoplates;
    static double LEFT1, RIGHT1, fl, fr, draft;
    std::list<PlateDescription*> platedescriptions;
    bool automesh;
    GiNaC::ex depth, falsebtm;
    static double LEFT2, RIGHT2;
    char forcingstr[500];
    GiNaC::symbol x, pi, symt;
    GiNaC::symtab tab;
    char* fce;
    static double mcl[4];
    double mrat;
    GiNaC::ex ic, icpi;
    static double t;
    bool mode3d;

    void eval_luke (double& luke, FunctionSpace& V, Function& phi, Function& lukebdy, MeshFunction<uint>& fs);
    void eval_pressure (Function& pressure, FunctionSpace& pV, Constant& rhowc, Function& phidt, Function& gsphi);
    void eval_fscond (File& file, Function& pdt, Function& zdt, FunctionSpace& dpV, FunctionSpace& dzV, Constant& gc, Function& zeta, Function& gsphi,
     Function& phinonz, Function& beach_nu, Function& cphi, Constant& muc, Constant& lc);
    void eval_predict (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, FunctionSpace& dpV, Constant& Dtc,
     Function& zetan, Function& zdtn, Function& zdtnm1, Function& zdtnm2, Function& zdtnm3, 
     Function& phitopn, Function& pdtn, Function& pdtnm1, Function& pdtnm2, Function& pdtnm3, Constant& lc, Constant& muc);
    void eval_correct (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, Constant& Dtc,
     Function& zetan, Function& zdtn1, Function& zdtn, Function& zdtnm1, Function& zdtnm2, 
     Function& phitopn, Function& pdtn1, Function& pdtn, Function& pdtnm1, Function& pdtnm2 );
    void eval_alpha (Function& Wddtn1, FunctionSpace& pV, Constant& alphac, Function& Wddtkm1);
    void eval_dx(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc);
    void eval_dx1(Function& zetax, FunctionSpace& fsV, Function& zetan1);
    void eval_dx2(Function& zetax, FunctionSpace& fsV, Function& zetan1);
    void eval_phinonz(Function& phinonz, FunctionSpace& V, Expression& zetax, Function& phi, Expression& cphix);
    void eval_accpot (Function& pdt, FunctionSpace& V, Expression& pdttop, DirichletBC& bcdt, DirichletBC& bcwdt, Expression& Wddt, Expression& wave, Function& gsphin1, MeshFunction<uint>& p1dommf, Expression& pdtbc, Constant& lc, bool nlfluid);
    void eval_velpot (Function& phi, FunctionSpace& V, Expression& phitop, DirichletBC& bc, DirichletBC& bcw, Expression& Wdt, Expression& wave, MeshFunction<uint>& p1dommf, Constant& lc);
    void eval_gradsq (Function& gsphin1, FunctionSpace& V, Function& phin1, Constant& lc);
    void eval_gradsqfs ( Function& gsphifs, FunctionSpace& fsV, Function& phinonz, Function& phitop, Function& zeta, DirichletBC& bgsfs );
    void eval_phiz ( Function& phiz, FunctionSpace& V, Function& phi, MeshFunction<uint>& p1dommf );
    void eval_phix ( Function& phix, FunctionSpace& V, Function& phi, Constant& lc );
    void eval_flatten ( Function& zetaic, FunctionSpace& V, Expression& zetaicpre );
    void eval_luke_3d (double& luke, FunctionSpace& V, Function& phi, Function& lukebdy, MeshFunction<uint>& fs);
    void eval_pressure_3d (Function& pressure, FunctionSpace& pV, Constant& rhowc, Function& phidt, Function& gsphi);
    void eval_fscond_3d (File& file, Function& pdt, Function& zdt, FunctionSpace& dpV, FunctionSpace& dzV, Constant& gc, Function& zeta, Function& gsphi,
     Function& phinonz, Function& beach_nu, Function& cphi, Constant& muc, Constant& lc);
    void eval_predict_3d (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, FunctionSpace& dpV, Constant& Dtc,
     Function& zetan, Function& zdtn, Function& zdtnm1, Function& zdtnm2, Function& zdtnm3, 
     Function& phitopn, Function& pdtn, Function& pdtnm1, Function& pdtnm2, Function& pdtnm3, Constant& lc, Constant& muc);
    void eval_correct_3d (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, Constant& Dtc,
     Function& zetan, Function& zdtn1, Function& zdtn, Function& zdtnm1, Function& zdtnm2, 
     Function& phitopn, Function& pdtn1, Function& pdtn, Function& pdtnm1, Function& pdtnm2 );
    void eval_alpha_3d (Function& Wddtn1, FunctionSpace& pV, Constant& alphac, Function& Wddtkm1);
    void eval_dx_3d(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc);
    void eval_dx1_3d(Function& zetax, FunctionSpace& fsV, Function& zetan1);
    void eval_dx2_3d(Function& zetax, FunctionSpace& fsV, Function& zetan1);
    void eval_phinonz_3d(Function& phinonz, FunctionSpace& V, Expression& zetax, Function& phi, Expression& cphix);
    void eval_accpot_3d (Function& pdt, FunctionSpace& V, Expression& pdttop, DirichletBC& bcdt, DirichletBC& bcwdt, Expression& Wddt, Expression& wave, Expression& gsphin1, MeshFunction<uint>& p1dommf, Expression& pdtbc, Constant& lc);
    void eval_velpot_3d (Function& phi, FunctionSpace& V, Expression& phitop, DirichletBC& bc, DirichletBC& bcw, Expression& Wdt, Expression& wave, MeshFunction<uint>& p1dommf, Constant& lc);
    void eval_gradsq_3d (Function& gsphin1, FunctionSpace& V, Function& phin1, Constant& lc);
    void eval_gradsqfs_3d ( Function& gsphifs, FunctionSpace& fsV, Function& phinonz, Function& phitop, Function& zeta, DirichletBC& bgsfs );
    void eval_phiz_3d ( Function& phiz, FunctionSpace& V, Function& phi, MeshFunction<uint>& p1dommf );
    void eval_phix_3d ( Function& phix, FunctionSpace& V, Function& phi, Constant& lc );
    void eval_flatten_3d ( Function& zetaic, FunctionSpace& V, Expression& zetaicpre );
};

class HEP::PointFunction : public Expression
{
  public:
    PointFunction (double mt, double xit ) : m(mt), xi(xit) {}
    double m, xi;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      if (fabs(x[0] - xi) <= EPS) values[0] = m;
      else values[0] = 0;
//      std::cout << xi << " " << values[0] << std::endl;
    }
};

class HEP::GiNaCFunction : public Expression
{
  public:
     GiNaCFunction(GiNaC::symbol xsymt, GiNaC::ex et) : xsym(xsymt), e(et) {}
     GiNaC::symbol xsym;
     GiNaC::ex e;
     void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
     {
       values[0] = GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==x[0])).to_double();
     }
};

class HEP::Constantws : public Expression
{
  public:
    Constantws(double constantt) : constant(constantt) {}
    double constant;
  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    values[0] = constant;
  }
};

class HEP::Single_ZetaInitialConditions : public Expression
{
 public: // Thanks to GiNaC Tutorial here
    Single_ZetaInitialConditions(GiNaC::symbol xsymt, GiNaC::ex et, PlateList& platelistt, char icmodet, bool platet) :
      xsym(xsymt), e(et), icmode(icmodet), plate(platet), platelist(platelistt) {}
    GiNaC::symbol xsym;
    GiNaC::ex e;
    char icmode;
    bool plate;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
// values[0] = -6*(dolfin::pow(x[0],3.0)/3 - 15*dolfin::pow(x[0],2.0)/2)/dolfin::pow(15.0,3);
// return;//RMV
      Plate* platel = Plate::find_plate(platelist, false, x[0], plate, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], plate, length);
      bool underplate = (platel==plater)&&platel;
      if (underplate) {
        Plate* plate = platel;
        double zl = (plate->left>=fl&&plate->left<=fr) ? GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==plate->left)).to_double() : 0.0;
        double zr = (plate->right>=fl&&plate->right<=fr) ? GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==plate->right)).to_double() : 0.0;
        double tmp = icmode=='A'?zl + (zr - zl)*(x[0] - plate->left)/(plate->right - plate->left):GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==x[0])).to_double();
	values[0] = tmp; // RMV SOMETHING WRONG HERE
      } else
        values[0] = (x[0]>=fl&&x[0]<=fr) ? GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==x[0])).to_double() : 0.0;
    }
};

#endif
