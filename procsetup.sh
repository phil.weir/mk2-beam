#!/bin/bash
#root="/media/Phil Weir/Photos-old/Mathematics"
#root=/tmp
source scripts/outroot.sh

mkdir -p "$outroot/output.$1"
mkdir -p "$outroot/output.$1/source"
mkdir -p "$outroot/output.$1/disp"
mkdir -p "$outroot/output.$1/phi"
mkdir -p "$outroot/output.$1/pressure"
mkdir -p "$outroot/output.$1/plate"
mkdir -p "$outroot/output.$1/mesh"
mkdir -p "$outroot/output.$1/params"
mkdir -p "$outroot/output.$1/add/acc"
mkdir -p "$outroot/output.$1/add/vel"
mkdir -p "$outroot/output.$1/add/gsp"
mkdir -p "$outroot/output.$1/add/phidt"
mkdir -p "$outroot/output.$1/add/phinonz"
mkdir -p "$outroot/output.$1/add/btm"
mkdir -p "$outroot/output.$1/add/lap"
mkdir -p "$outroot/output.$1/add/misc"

