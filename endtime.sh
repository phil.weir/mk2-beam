source outroot.sh
files=`ls $outroot | grep "$1"`
for i in $files
do
	file $outroot/$i/mk2*log >/dev/null 2>&1
	if [ $? != 0 ]; then
		echo $i : NOT STARTED
	else
		runname=`echo $i|sed 's/.*\.//g'`
		running=`pgrep -lf "mk2 .*$runname"`
		j=`tail -n 10 $outroot/output.$runname/mk2.$runname.log 2>&1 | grep Time | tail -n 1`
		if [ "$running" != "" ]; then
			j="$j [*]"
		fi
		echo $i : $j
	fi
done

