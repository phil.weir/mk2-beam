#include "plateproblem.h"
#include <ginac/ginac.h>

class GeoMaker {
  public:
   Mesh& simplePlateDescriptionList (double left, double right, double depth, std::list<PlateDescription*>& platelist, double mcl[], double rat,
    GiNaC::ex& ic, GiNaC::symbol& xsym, bool extrude);
   Mesh& generateGMSHMeshFromFile(char* filename, double rat, bool mode3d);
};
