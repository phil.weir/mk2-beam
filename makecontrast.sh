source outroot.sh
for file in {left,right,plate1}
do
 padn=`printf %06d $3`
 padm=`printf %06d $4`
 rm $file-1.csv $file-2.csv
 cp $outroot/output.$1/$file-$padn.csv $file-1.csv
 cp $outroot/output.$2/$file-$padm.csv $file-2.csv
 #sed '1d' $file-1-1.csv > $file-1.csv
 #sed '1d' $file-2-1.csv > $file-2.csv
done;
cp $outroot/output.$1/parameters parameters-1.tmp
cp $outroot/output.$2/parameters parameters-2.tmp
python test.py -S -D $5 $3 $4
rsvg -w 1200 test.svg test.png
rsvg -w 4000 test.svg printable.png
#octave ctest.m > fftlog
#mv octout.png ctst_$1_$2_$3_$4.png
cp test.png ctst_$1_$2_$3_$4.png
display test.png
