source outroot.sh
cd $outroot
outfile=new
files=`find output.$1 -newermt "$2" | grep 'left-\|right-\|plate1-\|rms-\|mbm-\|leftp-'`
if [ -n "$files" ]; then
  head -n 30 output.$1/mk2.$1.log > output.$1/log-end
  tail -n 30 output.$1/mk2.$1.log >> output.$1/log-end
  echo $files | xargs tar -cf $outfile.tar
  tar -rf $outfile.tar output.$1/log-end
  rm $outfile.tar.gz
  gzip -q $outfile.tar
  echo $outroot/$outfile.tar.gz
fi
