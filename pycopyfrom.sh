#!/bin/bash
#Setup from arg1 to arg2
source outroot.sh
mkdir $outroot/output.$2
cp -R $outroot/output.$1/params $outroot/output.$2
cp $outroot/output.$1/automesh.*.geo $outroot/output.$2
echo $1 > $outroot/output.$2/copied_from
