source outroot.sh
sed '1d' $outroot/output.$1/rms-0.csv > rmsa.csv
sed '1d' $outroot/output.$2/rms-0.csv > rmsb.csv
octave rmsctst.m > fftlog
cp test.png rms-$1-$2-0.png
display test.png
