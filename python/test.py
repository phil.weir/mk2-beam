from solve import Solver
import shutil
import math
import time
from subprocess import Popen
import mk2beam
import sys
import os
import argparse
from numpy import *
from dolfin import *
from multiprocessing import Process, Queue

class Timing :
    last_to_time = 0
    from_time = 0
    times = None
    current_times = None

    def __init__(self) :
        self.times = {}
        self.current_times = (self.times, None, None)

    def click(self, interval_name) :
        from_time = time.time()
        self.current_name = interval_name
        if interval_name not in self.current_times[0] :
            self.current_times[0][interval_name] = \
                [{}, [], self.current_times, from_time]
        self.current_times[0][interval_name][3] = from_time
        self.current_times = self.current_times[0][interval_name]

    def clock(self) :
        to_time = time.time()
        from_time = self.current_times[3]
        self.current_times[1].append(to_time-from_time)
        self.current_times = self.current_times[2]

    def _get_stats(self, times=None) :
        if times is None :
            return None
        return dict((k,\
            {'subs' : self._get_stats(v[0]),
             'occurs' : len(v[1]), 'ttot' : sum(v[1]),
             'tavg' : sum(v[1])/len(v[1])}) \
            for k, v in times.items())

    def _print_sub_stats(self, stats_dict, prefix='') :
        outstr = ''
        for k in stats_dict :
            stats = stats_dict[k]
            outstr += prefix + '{0:20} {1:3d} {2:3.3f} {3:3.3f}\n'.\
                format(str(k), stats['occurs'], stats['ttot'], stats['tavg'])
            outstr += self._print_sub_stats(stats['subs'], prefix+' ')
        return outstr

    def print_stats(self) :
        stats_dict = self._get_stats(self.times)
        outstr = self._print_sub_stats(stats_dict)
        return outstr

class NormalDerivative(Expression) :
    def __init__(self, phi, mesh, line_cell_to_mesh_cell) :
        self.mesh = mesh
        self.grad_phi = project(grad(phi), phi.function_space())
        self.cell_map = line_cell_to_mesh_cell
    def eval_cell(self, values, x, ufc_cell) :
        cell = Cell(self.mesh, line_cell_to_mesh_cell[ufc_cell.index])
        print cell.coordinates()

class free_surface_under_beam_cl(SubDomain) :
    def __init__(self, beam) :
        SubDomain.__init__(self)
        self.beam = beam
        self.pv = zeros(1)
        self.zv = zeros(1)
    def inside(self, x, on_boundary) :
        self.beam.p.eval(self.pv, x)
        self.beam.zetan1.eval(self.zv, x)
        #return x[0] < 15.
        #print self.pv[0] - 9.81*self.beam.rhow*(self.zv[0]-initial_draft)
        return self.pv[0] - 9.81*self.beam.rhow*(self.zv[0]-initial_draft) < 1.e-1
        #return self.zv[0] < self.pv[0]

class W_to_zeta(Expression) :
    def __init__(self, p, W, zeta, zeta_old, fs_subdom) :
        Expression.__init__(self)
        self.p = p
        self.W = W
        self.zeta = zeta
        self.zeta_old = zeta_old
        #self.g = g
        self.pv = zeros(1)
        self.zov = zeros(1)
        self.fs_subdom = fs_subdom
    def eval(self, values, x) :
        #gamma = 1.
        pv = zeros(1)
        self.zeta.eval(pv, x)
        self.W.eval(values, x)
        if pv[0]<values[0] and self.fs_subdom.inside(x, False) :
            #self.zeta.eval(values, x)
            values[0] = pv[0]
        #else :
        #    self.W.eval(values, x)
        #self.p.eval(self.pv, x)
        #self.W.eval(values, x)
        #self.zeta_old.eval(self.zov, x)
        #self.zeta.eval(self.pv, x)

        ##values[0] = gamma * (self.pv[0]/(beam.rhow*self.g) + initial_draft) +\
        ##            (1-gamma)*self.zov[0]
        ##if self.pv[0] - self.g*beam.rhow*(values[0]-initial_draft) < 1e-8 :
        #if self.pv[0] < values[0] :
        #    values[0] = self.pv[0]
        #values[0] = gamma * values[0] + (1-gamma)*self.zov[0]

class fs_beam(mk2beam.Plate) :
    zeta_on = False
    def __init__(self, beamline, beam_desc, dt, rhow, g, beamer, name, sbmg, zeta_on):
        mk2beam.Plate.__init__(self, beamline, beam_desc, dt, rhow, g, beamer, name,
            sbmg)
        self.Dt = dt
        self.zeta_on = zeta_on
        self.beta = beam_desc.beta
        self.g = g

        V = self.Wn1.function_space()
        self.zetan1 = Function(V)
        self.zetan1x = Function(V)
        self.zetan1xx = Function(V)
        self.zetan1_old = Function(V)
        self.zetadtn1 = Function(V)
        self.zetaddtn1 = Function(V)
        self.zetan = Function(V)
        self.zetadotn = Function(V)
        self.zetaddotn = Function(V)
        self.zetaddtkm1 = Function(V)
    def step(self, fs_subdom, zeta=None, do_adj=True) :
        #mk2beam.Plate.step(self)

        if False and self.zeta_on and do_adj:
            Dt = self.Dt
            beta = self.beta
            new_zeta = project(W_to_zeta(self.p, self.Wn1, self.zetan1, self.zetan1_old, fs_subdom), self.Wn1.function_space())
            self.zetan1.interpolate(new_zeta)
            #Neumann beta (should match beam)
            self.zetaddtn1.interpolate(project(\
                1/(beta*Dt*Dt)*(self.zetan1 - self.zetan - \
                Dt*self.zetadotn - (0.5-beta)*Dt*Dt*self.zetaddotn)))
            self.zetadtn1.interpolate(project(\
                self.zetadotn + (Dt/2)*self.zetaddotn + (Dt/2)*self.zetaddtn1))
        else :
            self.zetan1.interpolate(self.Wn1)
            self.zetadtn1.interpolate(self.Wdtn1)
            self.zetaddtn1.interpolate(self.Wddtn1)
    def nonstep(self) :
        mk2beam.Plate.nonstep(self)
        self.zetaddtn1.interpolate(self.zetaddotn)
        self.zetadtn1.interpolate(self.zetadotn)
        self.zetan1.interpolate(self.zetan)
    def clstep(self) :
        mk2beam.Plate.clstep(self)
        self.zetan1_old.interpolate(self.zetan1)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
    def adjacc(self, alpha) :
        mk2beam.Plate.adjacc(self, alpha)
        f = project(alpha * self.zetaddtn1 + (1-alpha) * self.zetaddtkm1)
        self.zetaddtn1.interpolate(f)
    def timestep(self) :
        mk2beam.Plate.timestep(self)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
        self.zetaddotn.interpolate(self.zetaddtn1)
        self.zetadotn.interpolate(self.zetadtn1)
        self.zetan.interpolate(self.zetan1)
    def load_function_vectors(self) :
        mk2beam.Plate.load_function_vectors(self)
        self.zetan1.interpolate(self.Wn1)
        self.zetadtn1.interpolate(self.Wdtn1)
        self.zetaddtn1.interpolate(self.Wddtn1)
        self.zetan1_old.interpolate(self.Wn1old)
        self.zetaddtkm1.interpolate(self.Wddtkm1)
        self.zetaddotn.interpolate(self.Wddotn)
        self.zetadotn.interpolate(self.Wdotn)
        self.zetan.interpolate(self.Wn)

def update_free_surface(phi, cphi_new, cphi_m1, cphi, zeta, zeta_old, bmesh_to_mesh, top_to_bmesh) :
    cphi.interpolate(cphi_new)
    zeta_old.interpolate(zeta)

    num_top_points = len(cphi_new.function_space().mesh().coordinates())
    cphi_a = zeros(num_top_points)
    phi.vector().get_local(cphi_a, array(map(lambda i :
        bmesh_to_mesh[top_to_bmesh[i]],
        range(0, num_top_points)), dtype="I"))
    cphi_new.vector().set_local(cphi_a)
    zetaf = (1/(9.81*dt)) * (cphi_m1 - cphi_new)
    zeta.interpolate(project(zetaf))

def do_skew(mesh, bmesh, depth, zeta, draft, moving_index, fixed_point, horiz_cutoff,
            vert_cutoff, boundary, nudgable=False) :
        closest_facet = bmesh.closest_cell(Point(*fixed_point))
        nearby_points = list(bmesh.topology()(1,0)(closest_facet))
        moving_top = bmesh.coordinates()[moving_index]
        nearby_points.sort(lambda i, j : \
            cmp((bmesh.coordinates()[i])[1], (bmesh.coordinates()[j])[1]))
        if abs(bmesh.coordinates()[nearby_points[0]][1]-fixed_point[1]) < \
           abs(bmesh.coordinates()[nearby_points[1]][1]-fixed_point[1]) :
            chosen_point = nearby_points[0]
        else :
            chosen_point = nearby_points[1]

        delta_mp = 0.
        skew = False
        deskew = {}
        if chosen_point == moving_index :
            if abs(moving_top[1]-fixed_point[1]) < vert_cutoff and nudgable :
                move_from = bmesh.coordinates()[chosen_point][1]
                skew = True
                for i in range(0, len(mesh.coordinates())) :
                    # Don't skew horiz neighbours in this case, as we should be
                    # close enough anyhow and we want to avoid using zeta
                    if abs(mesh.coordinates()[i][0]-boundary) < 1e-4 :
                        cur = mesh.coordinates()[i]
                        deskew[i] = cur[1]
                        (mesh.coordinates()[i])[1] += \
                            (cur[0]-horiz_cutoff)*(fixed_point[1]-move_from)*(cur[1]+depth)/((depth+move_from)*(boundary-horiz_cutoff))
                return (skew, deskew, fixed_point[1], delta_mp)
            chosen_point = nearby_points[0]

        if abs(bmesh.coordinates()[chosen_point][1]-fixed_point[1]) > DOLFIN_EPS :
            move_from = bmesh.coordinates()[chosen_point][1]
            skew = True
            z = zeros(1)
            x = zeros(1)
            for i in range(0, len(mesh.coordinates())) :
                if abs(mesh.coordinates()[i][0]-boundary) < abs(horiz_cutoff-boundary) :
                    cur = mesh.coordinates()[i]
                    deskew[i] = cur[1]
                    x[0] = cur[0]
                    zeta.eval(z, x)
                    z[0] -= draft
                    skew_from =\
                        (z[0]+depth)*(move_from+depth)/(moving_top[1]+depth)-depth
                    skew_fixed =\
                        (z[0]+depth)*(fixed_point[1]+depth)/(moving_top[1]+depth)-depth
                    fm = cur[1]
                    if abs(fm-z[0]) < DOLFIN_EPS :
                        delta = 0.
                    elif fm > skew_from + DOLFIN_EPS :
                        delta = (skew_fixed-skew_from)*(z[0]-fm)/(z[0]-skew_from)
                    elif fm < skew_from - DOLFIN_EPS :
                        delta = (skew_fixed-skew_from)*(depth+fm)/(depth+skew_from)
                    else :
                        delta = skew_fixed-skew_from

                    if abs(cur[0] - boundary) < DOLFIN_EPS :
                        (mesh.coordinates()[i])[1] += delta
                    else :
                        (mesh.coordinates()[i])[1] += (cur[0]-horiz_cutoff)*delta/(boundary-horiz_cutoff)
                    #(mesh.coordinates()[i])[1] += \
                    #    (cur[0]-horiz_cutoff)*(skew_fixed-skew_from)*(z[0]-cur[1])*(cur[1]+depth)/((z[0]-skew_from)*(depth+skew_from)*(boundary-horiz_cutoff))
        return (skew, deskew, fixed_point[1], delta_mp)

# Define function for projecting a 1D mesh embedded in 2D
# onto the x-axis
def flatten(mesh) :
    num_points = len(mesh.coordinates())
    conn = mesh.topology()(1,0)
    line = Mesh()
    editor_flat = MeshEditor()
    editor_flat.open(line, "interval", 1, 1)
    editor_flat.init_vertices(num_points)
    editor_flat.init_cells(num_points-1)
    for i in range(0, num_points) :
        editor_flat.add_vertex(i, mesh.coordinates()[i][0])
    for i in range(0, num_points-1) :
        editor_flat.add_cell(i, *conn(i))
    editor_flat.close()
    return line

# Define function for moving the mesh top from zeta_old to zeta
def mesh_move(mesh, zeta_old, zeta, depth, draft = 0.) :
    zold = zeros(1)
    znew = zeros(1)
    x = zeros(1)
    coords = mesh.coordinates()
    for i in range(0, len(coords)) :
        x[0] = coords[i][0]
        zeta_old.eval(zold, x)
        zo = zold[0]
        zeta.eval(znew, x)
        zn = znew[0]

        if abs(zo-coords[i][1]-draft) < 1e-8 :
            coords[i][1] = zn-draft
        else :
            coords[i][1] += (zn-zo)*(coords[i][1]+depth)/(zo-draft+depth)
    mesh.intersection_operator().clear()

# Define function to update the boundary mesh coordinates to
# match a full mesh
def update_boundary_mesh(mesh, bmesh) :
    vertex_map = bmesh.vertex_map()
    for i in range(0, len(bmesh.coordinates())) :
        bmesh.coordinates()[i] = mesh.coordinates()[vertex_map[i]]
    bmesh.intersection_operator().clear()

if __name__ == '__main__' :
    cvgce_old = 0
    timing = Timing()
    
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-r', '--runname')
    argparser.add_argument('-l', '--load', action='store_true')
    argparser.add_argument('-N', '--no-save', action='store_true')
    argparser.add_argument('--stop-after')
    argparser.add_argument('--output-ci', action='store_true')
    args = argparser.parse_args()
    
    if args.runname is None :
        print "Please supply runname : -r RUN"
        sys.exit(3)

    stop_after = None
    if args.stop_after is not None :
        print "Stopping at iteration " + args.stop_after
        stop_after = int(args.stop_after)
    
    resume = args.load
    no_save = args.no_save
    runname =  args.runname
    output_ci = args.output_ci
    
    builddir = "/maybehome/pweir/Work/mk2-matrix/build"

    try :
        with open("scripts/outroot.sh", 'r') as f :
            outroot = f.readlines()[0]
            outroot = outroot.split('=')[1].strip()
            outdir = outroot + '/output.' + runname
    except :
        print "Could not establish outroot; are you definitely running from build root?"
    
    if not os.path.exists(outdir) :
        try :
            os.mkdir(outdir)
            os.makedirs(outdir+'/phi')
            os.makedirs(outdir+'/add/phidt')
            os.makedirs(outdir+'/add/mesh')
            os.makedirs(outdir+'/source')
        except error :
            print str(error)
            sys.exit(5)

    for source_file in os.listdir(builddir+'/python') :
        shutil.copyfile(builddir+'/python/'+source_file, outdir+'/source')

    os.chdir(outdir)
    
    al_dbc = 1e3
    
    print "MK3 - hydroelasticity simulation"
    print "="*40
    
    source = Expression("0")
    
    # Settings
    try :
        pf = File("params/parameters.xml")
        p = Parameters("mk3")
        pf >> p
    except :
        print "Could not open params/parameters.xml in output directory"
        sys.exit(4)
    
    T = p.final_time
    dt = p.timestep
    depth = p.depth
    initial_draft = p.initial_draft
    domain_left = p.domain_left
    left = p.left
    right = p.right
    domain_right = p.domain_right
    refinement_level = p.mesh_refinement_level
    vertical_cells = p.mesh_vertical_cells
    horiz_cell_density = p.mesh_horizontal_cell_density
    rat = p.mesh_rat
    rhow = p.density_fluid
    ic_left = p.ic_left
    ic_width = p.ic_width
    ic_amp = p.ic_amp
    ic_offset = p.ic_offset
    ic_vert_offset = p.ic_vert_offset

    E = p.beam_youngs

    ci_lim = 1000
    mu = 0.00

    skew_cutoff = 1.e0
    skew_vert_cutoff = .001
    
    # DOLFIN settings
    set_log_level(100)
    
    # Open output files
    asdf1 = File("asdf1.pvd")
    asdf2 = File("asdf2.pvd")
    file_phi1 = File("phi/model-"+runname+"-phi1_.pvd")
    file_phi2 = File("phi/model-"+runname+"-phi2_.pvd")
    file_phi3 = File("phi/model-"+runname+"-phi3_.pvd")
    file_phidt1 = File("add/phidt/model-"+runname+"-phidt1_.pvd")
    file_phidt2 = File("add/phidt/model-"+runname+"-phidt2_.pvd")
    file_phidt3 = File("add/phidt/model-"+runname+"-phidt3_.pvd")
    file_mesh1 = File("add/mesh/model-"+runname+"-mesh1_.pvd")
    file_mesh2 = File("add/mesh/model-"+runname+"-mesh2_.pvd")
    file_mesh3 = File("add/mesh/model-"+runname+"-mesh3_.pvd")
    
    # Create mesh
    n = horiz_cell_density
    #mesh1 = Rectangle(domain_left, -depth, left, 0., int((left-domain_left)*n), vertical_cells)
    #mesh2 = Rectangle(left, -depth, 1., -initial_draft, int((right-left)*n), vertical_cells)
    #mesh3 = Rectangle(right, -depth, domain_right, 0., int((domain_right-right)*n), vertical_cells)
    
    def make_mesh(i) :
        all_paths_exist = os.path.exists("tmp.automesh."+str(i)) or \
                os.path.getmtime("tmp.automesh."+str(i)) < \
                os.path.getmtime("automesh."+str(i)+".geo")
        paths_newer = os.stat("tmp.automesh."+str(i)).st_mtime > \
                os.stat("automesh."+str(i)+".geo").st_mtime

        if not all_paths_exist or not paths_newer :
            f = open("tmp.automesh."+str(i), 'w')
            p = Popen(["gmsh", "-2", "automesh."+str(i)+".geo", "-algo", "del2d"], stdout=f)
            Popen.wait(p)
            p = Popen(["dolfin-convert", "automesh."+str(i)+".msh", "automesh."+str(i)+".xml"], stdout=f)
            Popen.wait(p)
            p = Popen(["dolfin-order", "automesh."+str(i)+".xml"], stdout=f)
            Popen.wait(p)
            f.close()
    
        mesh = Mesh("automesh."+str(i)+".xml")
        for j in range(0, len(mesh.coordinates())) :
            mesh.coordinates()[j][0] *= rat
    
        return mesh
    
    try :
        mesh1 = make_mesh(1)
        mesh2 = make_mesh(2)
        mesh3 = make_mesh(3)
    except e :
        print str(e)
        quit(6)

    mesh1 = refine(mesh1)
    mesh2 = refine(mesh2)
    mesh3 = refine(mesh3)
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    h = 1./n
    
    # Define intersection top variable
    gamma_top_1 = -initial_draft
    gamma_top_3 = -initial_draft
    
    # Define crucial points
    ulcrnr1 = Point(domain_left, 0)
    urcrnr1 = Point(left, 0)
    llcrnr2 = Point(left, gamma_top_1)
    lrcrnr2 = Point(right, gamma_top_3)
    ulcrnr3 = Point(right, 0)
    urcrnr3 = Point(domain_right, 0)
    
    # Refine near corners
    class near_1_top_corners_cl(SubDomain) :
        def __init__(self, dist) :
            SubDomain.__init__(self)
            self.dist = dist
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(ulcrnr1) < self.dist or \
                   Point(*x).distance(urcrnr1) < self.dist or \
                   Point(*x).distance(llcrnr2) < self.dist or \
                   x[1] > ulcrnr1[1]-self.dist - DOLFIN_EPS
    class near_beam_corners_cl(SubDomain) :
        def __init__(self, dist, bar = True) :
            SubDomain.__init__(self)
            self.dist = dist
            self.bar = bar
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(llcrnr2) < self.dist or \
                   Point(*x).distance(lrcrnr2) < self.dist or \
                   (self.bar and x[1] > -initial_draft-self.dist - DOLFIN_EPS)
    class near_3_top_corners_cl(SubDomain) :
        def __init__(self, dist) :
            SubDomain.__init__(self)
            self.dist = dist
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(ulcrnr3) < self.dist or \
                   Point(*x).distance(urcrnr3) < self.dist or \
                   Point(*x).distance(lrcrnr2) < self.dist or \
                   x[1] > ulcrnr3[1]-self.dist - DOLFIN_EPS
    
    def refine_mesh(subdom_cl, mesh, dist, **argv) :
        nearcornersd = subdom_cl(dist, **argv)
        nearcorner = MeshFunction("bool", mesh, mesh.topology().dim())
        nearcorner.set_all(False)
        nearcornersd.mark(nearcorner, True)
        return refine(mesh, nearcorner)
    
    #for i in range(0, refinement_level) :
    #    mesh1 = refine_mesh(near_1_top_corners_cl, mesh1, depth*1.5**(-(i-1)/2))
    #    mesh2 = refine_mesh(near_beam_corners_cl, mesh2, depth*1.5**(-(i-1)/2))
    #    mesh3 = refine_mesh(near_3_top_corners_cl, mesh3, depth*1.5**(-(i-1)/2))
    
    #for i in range(0, 2*refinement_level) :
    #    mesh1 = refine_mesh(near_beam_corners_cl, mesh1, depth*2.**(-.5*(i-1)))
    #    mesh3 = refine_mesh(near_beam_corners_cl, mesh3, depth*2.**(-.5*(i-1)))
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    
    # Define 1D subdomains
    # NB: The SubDomains and classes, except for inner_boundary_cl, will be
    # invalidated by movement of the mesh, so stick to index lists
    class top_cl(SubDomain) :
        def __init__(self, draft=0.) :
            SubDomain.__init__(self)
            self.draft = draft
        def inside(self, x, on_boundary) :
            return x[1] > -self.draft - DOLFIN_EPS
    topsd = top_cl()
    def get_top_indices(mesh, draft=0.) :
        top_indices = MeshFunction("uint", mesh, mesh.topology().dim()-1)
        top_indices.set_all(0)
        topsd = top_cl(draft)
        topsd.mark(top_indices, 1)
        top_indices = filter(lambda t : top_indices[t]==1, range(0, top_indices.size()))
        return top_indices
    top_indices_1 = get_top_indices(mesh1)
    top_indices_2 = get_top_indices(mesh2, initial_draft)
    top_indices_3 = get_top_indices(mesh3)
    
    class beam_cl(SubDomain) :
        def inside(self, x, on_boundary) :
            return x[1] > - initial_draft - DOLFIN_EPS
    beamsd = beam_cl()
    
    # Define internal boundary
    class inner_boundary_cl(SubDomain):
        def __init__(self, line, gamma_top) :
            SubDomain.__init__(self)
            self.line = line
            self.gamma_top = gamma_top
        def inside(self, x, on_boundary) :
            #if abs(x[0] - 10.) < DOLFIN_EPS :
            #    print (x[1], x[1]-self.gamma_top-DOLFIN_EPS)
            return abs(x[0] - self.line) < DOLFIN_EPS and x[1] < self.gamma_top + DOLFIN_EPS
    
    # Defining the boundary meshes saves us working on the full mesh quite so often
    def create_boundary(mesh, sd) :
        bmesh = BoundaryMesh(mesh)
        bmconn = bmesh.topology()(1,0)
        top = SubMesh(bmesh, sd)
        bmesh_to_mesh = bmesh.vertex_map()
        top_to_bmesh = top.data().mesh_function("parent_vertex_indices")
        num_top_points = len(top.coordinates())
        return bmesh, num_top_points, bmesh_to_mesh, top_to_bmesh, bmconn, top
    
    bmesh1, num_top_points_1, bmesh1_to_mesh1, top_to_bmesh1, bm1conn, top_1 =  \
        create_boundary(mesh1, topsd)
    
    #bmesh2 = BoundaryMesh(mesh2)
    #bm2conn = bmesh2.topology()(1,0)
    #beam_mesh = SubMesh(bmesh2, beamsd)
    #
    #beam_to_bmesh2 = beam_mesh.data().mesh_function("parent_vertex_indices")
    #num_beam_points = len(beam_mesh.coordinates())
    bmesh2, num_beam_points, bmesh2_to_mesh2, beam_to_bmesh2, bm2conn, beam_mesh = \
        create_boundary(mesh2, beamsd)
    
    bmesh3, num_top_points_3, bmesh3_to_mesh3, top_to_bmesh3, bm3conn, top_3 = \
        create_boundary(mesh3, topsd)
    
    # Build flat meshes
    # NB : this is a vertical projection of the top, so integrating over it is
    # different (i.e. don't)!
    topline_1 = flatten(top_1)
    V_topline_1 = FunctionSpace(topline_1, "Lagrange", 1)
            
    beamline = flatten(beam_mesh)
    V_beamline = FunctionSpace(beamline, "Lagrange", 1)
    
    # depends on beamline having same numbering as beam_mesh
    bmesh2_to_mesh2_cell = bmesh2.cell_map()
    V2 = FunctionSpace(mesh2, "Lagrange", 2)
    V_beam_mesh = FunctionSpace(beam_mesh, "Lagrange", 2)
    beam_conn = beam_mesh.topology()(1,0)
    mesh_conn = mesh2.topology()(1,2)
    bmesh2.init(0,1)
    bmesh2_conn = bmesh2.topology()(0,1)
    mesh_vertices = []
    vertices = {}
    g_to_m = {}
    k = 0
    beamline_cell_to_mesh2_cell = MeshFunction("uint", beamline, 1)
    for i in range(0, beam_mesh.num_cells()) :
        v1, v2 = beam_conn(i)
        cell_on_boundary =\
            list(set(bmesh2_conn(beam_to_bmesh2[v1])).intersection(set(bmesh2_conn(beam_to_bmesh2[v2]))))[0]
        beamline_cell_to_mesh2_cell[i] = mesh_conn(bmesh2_to_mesh2_cell[cell_on_boundary])[0]
    
    topline_3 = flatten(top_3)
    V_topline_3 = FunctionSpace(topline_3, "Lagrange", 1)
            
    # Build beam base
    beam_desc = mk2beam._PlateDescription()
    beam_desc.name = "beam"
    beam_desc.left = left
    beam_desc.right = right
    beam_desc.draft = initial_draft
    beam_desc.E = E
    beam_desc.nu = 0.3
    beam_desc.beta = 0.25
    beam_desc.rhop = 917.
    beam_desc.mup = 0.
    beam_desc.h = beam_desc.draft * rhow / beam_desc.rhop
    alpha = .001
    beam_type = "Euler-Bernoulli"
    beamer = mk2beam.Plate.get_platermap()
    beamer = mk2beam.Plate.get_plater(beam_type)
    #beam = mk2beam.Plate(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True);
    beam = fs_beam(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True,
        zeta_on=True)
    
    # Identify indices of the right end of the free surface and left end of the beam
    # with respect to the boundary meshes
    top_right_index = (sorted(range(0, num_top_points_1),
        lambda c,d : cmp(top_1.coordinates()[d][0],top_1.coordinates()[c][0])))[0]
    top_right_index = top_to_bmesh1[top_right_index]
    beam_left_index = (sorted(range(0, num_beam_points),
        lambda c,d : cmp(beam_mesh.coordinates()[c][0],beam_mesh.coordinates()[d][0])))[0]
    beam_left_index = beam_to_bmesh2[beam_left_index]
    beam_right_index = (sorted(range(0, num_beam_points),
        lambda c,d : cmp(beam_mesh.coordinates()[d][0],beam_mesh.coordinates()[c][0])))[0]
    beam_right_index = beam_to_bmesh2[beam_right_index]
    top_left_index = (sorted(range(0, num_top_points_3),
        lambda c,d : cmp(top_3.coordinates()[c][0],top_3.coordinates()[d][0])))[0]
    top_left_index = top_to_bmesh3[top_left_index]
    
    class ic_cl(Expression) :
        def eval(self, values, x) :
            if (x[0] < ic_left) :
                values[0] =\
                ic_amp*.5*(cos(pi*(ic_offset)/ic_width)+1)+ic_vert_offset
            elif (x[0] < ic_left+ic_width) :
                values[0] =\
                ic_amp*.5*(cos(pi*(x[0]-ic_left+ic_offset)/ic_width)+1)+ic_vert_offset
            else :
                values[0] =\
                ic_amp*.5*(cos(pi*(ic_width+ic_offset)/ic_width)+1)+ic_vert_offset
    ice = ic_cl()
    
    # Define functions on (time-independent) 1D meshes
    def make_functions(V_topline, ic = True) :
        #   Primary functions
        cphi = Function(V_topline)
        cphi_m1 = Function(V_topline)
        cphi_new = Function(V_topline)
        zeta = Function(V_topline)
        zeta_m1 = Function(V_topline)
        zeta_old = Function(V_topline)
        #   Time derivatives
        cphidt = Function(V_topline)
        zetadt = Function(V_topline)
        #   Auxiliary functions
        dirichlet = Function(V_topline)
        
        # Set initial conditions
        cphi_new.interpolate(Constant(0.))
        cphi_m1.interpolate(Constant(0.))
        zeta.interpolate(Constant(0.))
        zeta_m1.interpolate(zeta)
        zeta_old.interpolate(zeta)
        if ic :
            zeta.assign(ice)
            zeta_m1.assign(ice)
            zeta_old.assign(ice)
        zetax = project(zeta.dx(0))
        zetaxx = project(zetax.dx(0))
            #cphi.interpolate(Constant(1.))
    
        return cphi, cphi_new, cphi_m1, \
                zeta, zeta_m1, zeta_old, zetax, \
               zetaxx, cphidt, zetadt, dirichlet
    
    # Load functions for top meshes
    def load_functions(V_topline, num) :
    
        cphi, cphi_new, cphi_m1, zeta, zeta_m1, zeta_old,\
        zetax, zetaxx, cphidt, zetadt,\
        dirichlet =\
            make_functions(V_topline, ic=False)
        
        #   Primary functions
        File("cphi.%d.xml" % num) >> cphi.vector()
        File("cphi_new.%d.xml" % num) >> cphi_new.vector()
        File("cphi_m1.%d.xml" % num) >> cphi_m1.vector()
        File("zeta.%d.xml" % num) >> zeta.vector()
        File("zeta_m1.%d.xml" % num) >> zeta_m1.vector()
        File("zetax.%d.xml" % num) >> zetax.vector()
        File("zetaxx.%d.xml" % num) >> zetaxx.vector()
        File("cphidt.%d.xml" % num) >> cphidt.vector()
        File("zetadt.%d.xml" % num) >> zetadt.vector()

        zeta_old.interpolate(zeta_m1)

        return cphi, cphi_new, cphi_m1, \
                zeta, zeta_m1, zeta_old, zetax, \
                zetaxx, cphidt, zetadt, dirichlet

    def load_state() :
        t = 0.
        titeration = 0
        cl_last = 0

        f = open('state.st', 'r')
        line = f.readline()
        t = float(line.split(':')[-1])
        line = f.readline()
        titeration = int(line.split(':')[-1])
        line = f.readline()
        cl_last = int(line.split(':')[-1])

        return t, titeration, cl_last

    def save_state(t, titeration, cl_last) :
        f = open('state.st', 'w')
        f.write("t:%lf\n" % t)
        f.write("titeration:%d\n" % titeration)
        f.write("cl_last:%d\n" % cl_last)

    if resume :
        # Try and reduce likelihood of us overwriting the copy we want
        if not no_save :
            n = 0
            for f in os.listdir('.') :
                if f.startswith('state.st') :
                    n += 1

            shutil.copyfile('state.st', 'state.st.'+str(n))
            backup_functions(1, n)
            beam.backup_function_vectors()
            backup_functions(3, n)

        cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
            zetax_1, zetaxx_1, cphidt_1, zetadt_1,\
            dirichlet_1 =\
                load_functions(V_topline_1, 1)

        beam.load_function_vectors()
    
        cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
            zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
            dirichlet_3 =\
                load_functions(V_topline_3, 3)
    else :
        cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
            zetax_1, zetaxx_1, cphidt_1, zetadt_1,\
            dirichlet_1 =\
                make_functions(V_topline_1)
    
        cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
            zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
            dirichlet_3 =\
                make_functions(V_topline_3)
    
    beam.nonstep()
    beam.setup_diagnostics("w")
    
    neumann_2 = Function(V_beamline)
    neumann_2.assign(Constant(0.))
    
    cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
        zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
        dirichlet_2 =\
            make_functions(V_beamline)
    zeta_2_m1.interpolate(beam.zetan1_old)
    
    # Move mesh to starting position
    mesh_move(mesh1, Constant(0.), zeta_1_m1, depth)
    update_boundary_mesh(mesh1, bmesh1)
    mesh_move(mesh2, Constant(0.), beam.zetan1_old, depth, initial_draft)
    update_boundary_mesh(mesh2, bmesh2)
    mesh_move(mesh3, Constant(0.), zeta_3_m1, depth)
    update_boundary_mesh(mesh3, bmesh3)
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    
    log_now = output_ci
    quit_for_timing = False

    chg = 0.
    chg_old = 0.
    
    # Time loop
    if resume :
        t, tn, ci = load_state()
    else :
        t = 0
        tn = 0
        ci = 0
    fn = 0
    while t < T and not quit_for_timing :
    
        #beam.output_diagnostics(t)
    
        keep_correcting = True
        while keep_correcting and not quit_for_timing :

            if log_now :
                fn += 1

            if log_now :
                #   Output moved mesh
                file_mesh1 << mesh1
                file_mesh2 << mesh2
                file_mesh3 << mesh3
    
            # CI step
            beam.clstep()

            free_surface_under_beam = free_surface_under_beam_cl(beam)
    
            # Build solver
            inner_boundary_1 = inner_boundary_cl(left, gamma_top_1)
            inner_boundary_3 = inner_boundary_cl(right, gamma_top_3)
            timing.click("solver init")
            solver = Solver(mesh1, mesh2, mesh3,
                inner_boundary_1, top_indices_1,
                top_indices_2, free_surface_under_beam,
                inner_boundary_3, top_indices_3,
                timing)
            timing.clock()
    
            # Solve
            xepr = Expression("x")
            cphi_1_m1.interpolate(Constant(1.))
            dirichlet_1 = project(cphi_1_m1-9.81*dt*(zeta_1_m1 + mu*dt*zetaxx_1))
            bdy = zeros(1)
            bdy[0] = 10.
            bv = zeros(1)
            dirichlet_2 = project(cphi_2_m1-9.81*dt*(beam.zetan + mu*dt*beam.zetan1xx))
            neumann_2.interpolate(project((1+Wn1x**2)**.5*Wdtn1))
            dirichlet_3 = project(cphi_3_m1-9.81*dt*(zeta_3_m1 + mu*dt*zetaxx_3))
            #asdf1 << dirichlet_3
            dn_c = 9.81*dt*dt
            #dn_c = 0.
            timing.click("solve_poisson")
            phi_1, phi_2, phi_3 = solver.solve_poisson(h, source, 1e-9,
                zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
                beam.zetan1x, dirichlet_2, neumann_2, beam_to_bmesh2, bmesh2_to_mesh2,
                zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
                dn_c=dn_c)
            timing.clock()
    
            if log_now :
                # Output phi
                file_phi1 << phi_1
                file_phi2 << phi_2
                file_phi3 << phi_3
    
        t += dt
        tn += 1
    
    beam.shutdown_diagnostics()
    
    print "Done"
    
    timing_file = open("timing.out", "w")
    timing_file.write(timing.print_stats())
    timing_file.close()
