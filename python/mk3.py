from solve import Solver
import shutil
import math
import time
from subprocess import Popen
import mk2beam
import sys
import os
import argparse
from numpy import *
from dolfin import *
from multiprocessing import Process, Queue

res = 1e-9

print "YOU MEAN mk3s.py DON'T YOU?"
exit(1000)

def skew() :
       # Deal with mesh
       #   Move linearly
       timing.click("mesh move")

       mesh_move(mesh1, zeta_1_old, zeta_1, depth)
       update_boundary_mesh(mesh1, bmesh1)

       mesh_move(mesh2, zeta_2_old, zeta_2, depth, beam.draft)
       update_boundary_mesh(mesh2, bmesh2)

       mesh_move(mesh3, zeta_3_old, zeta_3, depth)
       update_boundary_mesh(mesh3, bmesh3)

       if log_now :
           #   Output moved mesh
           file_mesh1 << mesh1
           file_mesh2 << mesh2
           file_mesh3 << mesh3

       #   Skew for corners
       skew_1 = 0
       skew_3 = 0
       top_right_height = bmesh1.coordinates()[top_right_index][1]
       beam_left_height = bmesh2.coordinates()[beam_left_index][1]
       beam_right_height = bmesh2.coordinates()[beam_right_index][1]
       top_left_height = bmesh3.coordinates()[top_left_index][1]
       
       if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
           av =bdy_average(zeta_2, beam_left_index_b,
                            zeta_1, top_right_index_t, left, beam.draft,
                            do=True)
           bdy_average(zeta_2_m1, beam_left_index_b,
                            zeta_1_m1, top_right_index_t, left, beam.draft)
           mesh1.coordinates()[bmesh1_to_mesh1[top_right_index]][1] = av
           mesh2.coordinates()[bmesh2_to_mesh2[beam_left_index]][1] = av
           beam_left_height = av
           top_right_height = av
           bdy_average(cphi_2, beam_left_index_b,
                            cphi_1, top_right_index_t, left)
           bdy_average(cphi_2_m1, beam_left_index_b,
                       cphi_1_m1, top_right_index_t, left)
           bdy_average(cpdt_2, beam_left_index_b,
                       cpdt_1, top_right_index_t, left)
           bdy_average(cpdt_2_m1, beam_left_index_b,
                       cpdt_1_m1, top_right_index_t, left)
           bdy_average(cpdt_2_m2, beam_left_index_b,
                       cpdt_1_m2, top_right_index_t, left)
           bdy_average(cpdt_2_m3, beam_left_index_b,
                       cpdt_1_m3, top_right_index_t, left)
           bdy_average(cpdt_2_m4, beam_left_index_b,
                       cpdt_1_m4, top_right_index_t, left)
       if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
           av = bdy_average(zeta_2, beam_right_index_b,
                            zeta_3, top_left_index_t, right, beam.draft,
                            do=True)
           bdy_average(zeta_2_m1, beam_right_index_b,
                       zeta_3_m1, top_left_index_t, right, beam.draft)
           #bdy_average(zeta_2_m2, beam_right_index_b,
           #            zeta_3_m2, top_left_index_t, right, beam.draft)
           #bdy_average(zeta_2_m3, beam_right_index_b,
           #            zeta_3_m3, top_left_index_t, right, beam.draft)
           #bdy_average(zeta_2_m4, beam_right_index_b,
           #            zeta_3_m4, top_left_index_t, right, beam.draft)
           mesh3.coordinates()[bmesh3_to_mesh3[top_left_index]][1] = av
           mesh2.coordinates()[bmesh2_to_mesh2[beam_right_index]][1] = av
           bdy_average(cphi_2, beam_left_index_b,
                       cphi_3, top_right_index_t, right)
           bdy_average(cphi_2, beam_right_index_b,
                       cphi_3, top_left_index_t, right)
           bdy_average(cpdt_2, beam_right_index_b,
                       cpdt_3, top_left_index_t, right)
           bdy_average(cpdt_2_m1, beam_right_index_b,
                       cpdt_3_m1, top_left_index_t, right)
           bdy_average(cpdt_2_m2, beam_right_index_b,
                       cpdt_3_m2, top_left_index_t, right)
           bdy_average(cpdt_2_m3, beam_right_index_b,
                       cpdt_3_m3, top_left_index_t, right)
           bdy_average(cpdt_2_m4, beam_right_index_b,
                       cpdt_3_m4, top_left_index_t, right)
           beam_right_height = av
           top_left_height = av

       mesh1.intersection_operator().clear()
       update_boundary_mesh(mesh1, bmesh1)
       mesh2.intersection_operator().clear()
       update_boundary_mesh(mesh2, bmesh2)
       mesh3.intersection_operator().clear()
       update_boundary_mesh(mesh3, bmesh3)

       zeta_1_old.interpolate(zeta_1)
       beam.clstep()
       zeta_2_old.interpolate(zeta_2)
       zeta_3_old.interpolate(zeta_3)

       if log_now :
           #   Output moved mesh
           file_mesh1 << mesh1
           file_mesh2 << mesh2
           file_mesh3 << mesh3

       #       1
       if top_right_height >= beam_left_height :
           skew_1, deskew_1, gamma_top_1, delta_mp = \
               do_skew(mesh1, bmesh1, depth, zeta_1, 0., top_right_index,
               bmesh2.coordinates()[beam_left_index], left-skew_cutoff,
               skew_vert_cutoff, left, nudgable=True)
           skew_1 = 1 if skew_1 else 0
           mesh1.intersection_operator().clear()
           update_boundary_mesh(mesh1, bmesh1)
       elif top_right_height < beam_left_height :
           skew_1, deskew_1, gamma_top_1, delta_mp = \
               do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_left_index,
               bmesh1.coordinates()[top_right_index], left+skew_cutoff,
               skew_vert_cutoff, left, nudgable=True)
           #do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_left_index,
           skew_1 = 2 if skew_1 else 0
           mesh2.intersection_operator().clear()
           update_boundary_mesh(mesh2, bmesh2)
       else :
           gamma_top_1 = top_right_height

       #       3
       if top_left_height >= beam_right_height :
           skew_3, deskew_3, gamma_top_3, delta_mp = \
               do_skew(mesh3, bmesh3, depth, zeta_3, 0., top_left_index,
               bmesh2.coordinates()[beam_right_index], right+skew_cutoff,
               skew_vert_cutoff, right, nudgable=True)
           skew_3 = 1 if skew_3 else 0
           mesh3.intersection_operator().clear()
           update_boundary_mesh(mesh3, bmesh3)
       elif top_left_height < beam_right_height :
           skew_3, deskew_3, gamma_top_3, delta_mp = \
               do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_right_index,
               bmesh3.coordinates()[top_left_index], right-skew_cutoff,
               skew_vert_cutoff, right, nudgable=True)
           skew_3 = 2 if skew_3 else 0
           mesh2.intersection_operator().clear()
           update_boundary_mesh(mesh2, bmesh2)
       else :
           gamma_top_3 = top_left_height
       timing.clock()

       return skew_1, deskew_1, gamma_top_1, skew_3, deskew_3, gamma_top_3
            

def bdy_average(dirichlet_2, beam_left_index_b, dirichlet_1, top_right_index_t,
                x=10, draft=0., do=True) :
    xp = zeros(1)
    p1 = zeros(1)
    p2 = zeros(1)
    xp[0] = x
    dirichlet_1.eval(p1, xp)
    dirichlet_2.eval(p2, xp)

    av = (p1[0]+p2[0]-draft)/2

    if do :
        dirichlet_2.vector()[beam_left_index_b] = av+draft

    dirichlet_1.vector()[top_right_index_t] = av

    return av

def straighten(skew_1, deskew_1, mesh1, bmesh1,
                       mesh2, bmesh2,
                       skew_3, deskew_3, mesh3, bmesh3) :
    if skew_1 == 1 :
        for i in range(0, len(mesh1.coordinates())) :
            if i in deskew_1 :
                (mesh1.coordinates()[i])[1] = deskew_1[i]
        mesh1.intersection_operator().clear()
        update_boundary_mesh(mesh1, bmesh1)
    elif skew_1 == 2 :
        for i in range(0, len(mesh2.coordinates())) :
            if i in deskew_1 :
                (mesh2.coordinates()[i])[1] = deskew_1[i]
        mesh2.intersection_operator().clear()
        update_boundary_mesh(mesh2, bmesh2)
    if skew_3 == 1 :
        for i in range(0, len(mesh3.coordinates())) :
            if i in deskew_3 :
                (mesh3.coordinates()[i])[1] = deskew_3[i]
        mesh3.intersection_operator().clear()
        update_boundary_mesh(mesh3, bmesh3)
    elif skew_3 == 2 :
        for i in range(0, len(mesh2.coordinates())) :
            if i in deskew_3 :
                (mesh2.coordinates()[i])[1] = deskew_3[i]
        mesh2.intersection_operator().clear()
        update_boundary_mesh(mesh2, bmesh2)

class Timing :
    last_to_time = 0
    from_time = 0
    times = None
    current_times = None

    def __init__(self) :
        self.times = {}
        self.current_times = (self.times, None, None)

    def click(self, interval_name) :
        from_time = time.time()
        self.current_name = interval_name
        if interval_name not in self.current_times[0] :
            self.current_times[0][interval_name] = \
                [{}, [], self.current_times, from_time]
        self.current_times[0][interval_name][3] = from_time
        self.current_times = self.current_times[0][interval_name]

    def clock(self) :
        to_time = time.time()
        from_time = self.current_times[3]
        self.current_times[1].append(to_time-from_time)
        self.current_times = self.current_times[2]

    def _get_stats(self, times=None) :
        if times is None :
            return None
        return dict((k,\
            {'subs' : self._get_stats(v[0]),
             'occurs' : len(v[1]), 'ttot' : sum(v[1]),
             'tavg' : sum(v[1])/len(v[1])}) \
            for k, v in times.items())

    def _print_sub_stats(self, stats_dict, prefix='') :
        outstr = ''
        for k in stats_dict :
            stats = stats_dict[k]
            outstr += prefix + '{0:20} {1:3d} {2:3.3f} {3:3.3f}\n'.\
                format(str(k), stats['occurs'], stats['ttot'], stats['tavg'])
            outstr += self._print_sub_stats(stats['subs'], prefix+' ')
        return outstr

    def print_stats(self) :
        stats_dict = self._get_stats(self.times)
        outstr = self._print_sub_stats(stats_dict)
        return outstr

class NormalDerivative(Expression) :
    def __init__(self, phi, zetas, line_cell_to_mesh_cell,
                line_cell_to_mesh_line, draft) :
        self.mesh = phi.function_space().mesh()
        self.phi = phi
        self.gpfs = VectorFunctionSpace(self.mesh, "Lagrange", 1)
        self.grad_phi = project(grad(phi), self.gpfs)
        self.cell_map = line_cell_to_mesh_cell
        self.facet_map = line_cell_to_mesh_line
        self.zeta, self.zetax = zetas
        self.draft = draft
    def eval_cell(self, values, x, ufc_cell) :
        cell = Cell(self.mesh, self.cell_map[ufc_cell.index])
        mesh_conn = self.mesh.topology()(2,1)
        m = list(mesh_conn(self.cell_map[ufc_cell.index])).index(\
            self.facet_map[ufc_cell.index])
        n = cell.normal(m)
        gp = zeros(2)
        xv = zeros(2)
        x2 = zeros(1)
        z = zeros(1)
        #if x[0] < 10 :
        #    dz = 1e-5
        #    dx = 1e-5

        #    if x[0] + 0.5*dx > 10. :
        #        o = -0.5*dx
        #    if x[0] + 0.5*dx < 10. :
        #        o =  0.5*dx

        #    l = x[0] - 0.5*dx + o
        #    r = x[0] + 0.5*dx + o
        #    x2[0] = l
        #    self.zeta.eval(z, x2)
        #    zl = z[0]
        #    x2[0] = r
        #    self.zeta.eval(z, x2)
        #    zr = z[0]

        #    zetax = (zr-zl)/dx
        #    
        #    xv[0] = x[0]
        #    z = zeros(1)
        #    self.zeta.eval(z, x)
        #    xv[1] = z[0]-1e-6-self.draft

        #    self.phi.eval(z, xv)
        #    v0 = z[0]
        #    xv[0] -=  -dz*zetax/sqrt(1+zetax*zetax)
        #    xv[1] -=  dz/sqrt(1+zetax*zetax)
        #    if xv[0] < 0. :
        #        xv[0] = 1e-15
        #    elif xv[0] > 10. :
        #        xv[0] = 10.-1e-14
        #    self.phi.eval(z, xv)
        #    v1 = z[0]

        #    values[0] = sqrt(1+zetax*zetax)*(v0-v1)/dz;

        #else :
        xv[0] = x[0]
        self.zeta.eval(z, x)
        xv[1] = z[0]-1e-9-self.draft
        try: 
            self.grad_phi.eval(gp, xv)
        except :
            print xv
            print "COULDN'T SURVIVE GRAD_PHI"
            quit()
        self.zetax.eval(z, x)
        values[0] = n.dot(Point(*gp))*sqrt(1+z[0]*z[0])
        #values[0] = gp[1]
        #values[0] = n[0]
        #mesh_conn = self.mesh.topology()(1,0)
        #values[0] = Facet(self.mesh, self.facet_map[ufc_cell.index]).midpoint().x()
        #values[0] = Cell(self.zeta.function_space().mesh(),ufc_cell.index).midpoint().x()
        #print "%lf,%lf,%lf" % (x[0], values[0], ufc_cell.index)
        #values[0] = ufc_cell.index
        #values[0] = self.mesh.geometry().point(mesh_conn(self.facet_map[ufc_cell.index])[1]).y()
        #values[0] = z[0]
        #values[0] =\
        #    self.mesh.coordinates()[mesh_conn(self.cell_map[ufc_cell.index])[0]][1]

not_first = True
class free_surface_under_beam_cl(SubDomain) :
    def __init__(self, beam, fszeta, not_first) :
        self.not_first = not_first
        SubDomain.__init__(self)
        self.beam = beam
        self.fszeta = fszeta
        self.pv = zeros(1)
        self.zv = zeros(1)
    def inside(self, x, on_boundary) :
        return False #RMV
        if self.fszeta is None :
            return False

        self.fszeta.eval(self.pv, x)
        self.beam.Wn1.eval(self.zv, x)
        return self.pv[0] + DOLFIN_EPS < self.zv[0]

class free_surface_under_beam_zero_cl(SubDomain) :
    def inside(self, x, on_boundary) :
        return True

class W_to_zeta(Expression) :
    def __init__(self, p, W, zeta, zeta_old, fs_subdom) :
        Expression.__init__(self)
        self.p = p
        self.W = W
        self.zeta = zeta
        self.zeta_old = zeta_old
        self.pv = zeros(1)
        self.zov = zeros(1)
        self.fs_subdom = fs_subdom
    def eval(self, values, x) :
        pv = zeros(1)
        self.zeta.eval(pv, x)
        self.W.eval(values, x)

        if pv[0] < values[0] and self.fs_subdom.inside(x, False) :
            values[0] = pv[0]

class fs_beam(mk2beam.Plate) :
    zeta_on = False
    def __init__(self, beamline, beam_desc, dt, rhow, g, beamer, name, sbmg, zeta_on):
        mk2beam.Plate.__init__(self, beamline, beam_desc, dt, rhow, g, beamer, name,
            sbmg)
        self.Dt = dt
        self.zeta_on = zeta_on
        self.beta = beam_desc.beta
        self.g = g

        V = self.Wn1.function_space()
        self.zetan1 = Function(V)
        self.zetan1x = Function(V)
        self.zetan1xx = Function(V)
        self.zetan1_old = Function(V)
        self.zetadtn1 = Function(V)
        self.zetaddtn1 = Function(V)
        self.zetan = Function(V)
        self.zetadotn = Function(V)
        self.zetaddotn = Function(V)
        self.zetaddtkm1 = Function(V)

        self.zetan1.interpolate(self.Wn1)
        self.zetadtn1.interpolate(self.Wdtn1)
        self.zetaddtn1.interpolate(self.Wddtn1)
        self.zetan.interpolate(self.Wn)
        self.zetadotn.interpolate(self.Wdotn)
        self.zetaddotn.interpolate(self.Wddotn)
    def step(self, W_to_zeta, zeta=None, do_adj=True) :
        mk2beam.Plate.step(self)

        if self.zeta_on and do_adj:
            Dt = self.Dt
            beta = self.beta
            #print '-'*20
            new_zeta = W_to_zeta
            #print '='*20
            self.zetan1.interpolate(new_zeta)
            #Neumann beta (should match beam)
            self.zetaddtn1.interpolate(project(\
                1/(beta*Dt*Dt)*(self.zetan1 - self.zetan - \
                Dt*self.zetadotn - (0.5-beta)*Dt*Dt*self.zetaddotn)))
            self.zetadtn1.interpolate(project(\
                self.zetadotn + (Dt/2)*self.zetaddotn + (Dt/2)*self.zetaddtn1))
        else :
            self.zetan1.interpolate(self.Wn1)
            self.zetadtn1.interpolate(self.Wdtn1)
            self.zetaddtn1.interpolate(self.Wddtn1)
    def nonstep(self) :
        mk2beam.Plate.nonstep(self)
        self.zetaddtn1.interpolate(self.zetaddotn)
        self.zetadtn1.interpolate(self.zetadotn)
        self.zetan1.interpolate(self.zetan)
    def clstep(self) :
        mk2beam.Plate.clstep(self)
        self.zetan1_old.interpolate(self.zetan1)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
    def adjacc(self, alpha) :
        mk2beam.Plate.adjacc(self, alpha)
        f = project(alpha * self.zetaddtn1 + (1-alpha) * self.zetaddtkm1)
        self.zetaddtn1.interpolate(f)
    def timestep(self) :
        mk2beam.Plate.timestep(self)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
        self.zetaddotn.interpolate(self.zetaddtn1)
        self.zetadotn.interpolate(self.zetadtn1)
        self.zetan.interpolate(self.zetan1)
    def save_function_vectors(self) :
        mk2beam.Plate.save_function_vectors(self)
        File(self.name + "_zetan1.xml") << self.zetan1.vector()
        File(self.name + "_zetan1x.xml") << self.zetan1x.vector()
        File(self.name + "_zetan1xx.xml") << self.zetan1xx.vector()
        File(self.name + "_zetadtn1.xml") << self.zetadtn1.vector()
        File(self.name + "_zetaddtn1.xml") << self.zetaddtn1.vector()
        File(self.name + "_zetan1_old.xml") << self.zetan1_old.vector()
        File(self.name + "_zetaddtkm1.xml") << self.zetaddtkm1.vector()
        File(self.name + "_zetaddotn.xml") << self.zetaddotn.vector()
        File(self.name + "_zetadotn.xml") << self.zetadotn.vector()
        File(self.name + "_zetan.xml") << self.zetan.vector()
    def load_function_vectors(self) :
        mk2beam.Plate.load_function_vectors(self)

        if os.path.exists(self.name + "_zetan1.xml") :
            File(self.name + "_zetan1.xml") >> self.zetan1.vector()
            File(self.name + "_zetan1x.xml") >> self.zetan1x.vector()
            File(self.name + "_zetan1xx.xml") >> self.zetan1xx.vector()
            File(self.name + "_zetadtn1.xml") >> self.zetadtn1.vector()
            File(self.name + "_zetaddtn1.xml") >> self.zetaddtn1.vector()
            File(self.name + "_zetan1_old.xml") >> self.zetan1_old.vector()
            File(self.name + "_zetaddtkm1.xml") >> self.zetaddtkm1.vector()
            File(self.name + "_zetaddotn.xml") >> self.zetaddotn.vector()
            File(self.name + "_zetadotn.xml") >> self.zetadotn.vector()
            File(self.name + "_zetan.xml") >> self.zetan.vector()
        else :
            self.zetan1.interpolate(self.Wn1)
            Wn1x = project(Dx(Function(beam.Wn1.function_space(), beam.Wn1), 0))
            self.zetan1x.interpolate(Wn1x)
            Wn1xx = project(Dx(Dx(Function(beam.Wn1.function_space(), beam.Wn1),
                0), 0))
            self.zetan1xx.interpolate(Wn1xx)
            self.zetadtn1.interpolate(self.Wdtn1)
            self.zetaddtn1.interpolate(self.Wddtn1)
            self.zetan1_old.interpolate(self.Wn1old)
            self.zetaddtkm1.interpolate(self.Wddtkm1)
            self.zetaddotn.interpolate(self.Wddotn)
            self.zetadotn.interpolate(self.Wdotn)
            self.zetan.interpolate(self.Wn)
    def backup_functions(to) :
        filenames = []
        filenames.append(self.name + "_zetan1.xml")
        filenames.append(self.name + "_zetan1x.xml")
        filenames.append(self.name + "_zetan1xx.xml")
        filenames.append(self.name + "_zetadtn1.xml")
        filenames.append(self.name + "_zetaddtn1.xml") 
        filenames.append(self.name + "_zetan1_old.xml")
        filenames.append(self.name + "_zetaddtkm1.xml")
        filenames.append(self.name + "_zetaddotn.xml") 
        filenames.append(self.name + "_zetadotn.xml")
        filenames.append(self.name + "_zetan.xml")

        for filename in filenames :
            shutil.copyfile(filename, filename+'.'+str(to))


def update_free_surface_b(phi, cphi_new, cphi_m1, cphi, zeta, zeta_old,
            bmesh_to_mesh, top_to_bmesh, zeta_m1, zdt, zdtm1, zdtm2, zdtm3):
    cphi.interpolate(cphi_new)
    zeta_old.interpolate(zeta)

    num_top_points = len(cphi_new.function_space().mesh().coordinates())
    cphi_a = zeros(num_top_points)
    phi.vector().get_local(cphi_a, array(map(lambda i :
        bmesh_to_mesh[top_to_bmesh[i]],
        range(0, num_top_points)), dtype="I"))
    cphi_new.vector().set_local(cphi_a)

    zetaf = zeta_m1 + (dt/24.)*(9*zdt + 19*zdtm1 - 5*zdtm2 + zdtm3)
    zeta.interpolate(project(zetaf))

def update_free_surface(phi, cphi_new, cphi_m1, cphi, zeta, zeta_old,
            bmesh_to_mesh, top_to_bmesh, draft=0., do_init=False, zetan=None) :
    cphi.interpolate(cphi_new)
    zeta_old.interpolate(zeta)

    num_top_points = len(cphi_new.function_space().mesh().coordinates())
    cphi_a = zeros(num_top_points)
    phi.vector().get_local(cphi_a, array(map(lambda i :
        bmesh_to_mesh[top_to_bmesh[i]],
        range(0, num_top_points)), dtype="I"))
    cphi_new.vector().set_local(cphi_a)

    zetaf = (1/(9.81*dt)) * (cphi_m1 - cphi_new) + draft
    zeta.interpolate(project(zetaf))

def do_skew(mesh, bmesh, depth, zeta, draft, moving_index, fixed_point, horiz_cutoff,
            vert_cutoff, boundary, nudgable=False) :
        closest_facet = bmesh.closest_cell(Point(*fixed_point))
        nearby_points = list(bmesh.topology()(1,0)(closest_facet))
        moving_top = bmesh.coordinates()[moving_index]
        nearby_points.sort(lambda i, j : \
            cmp((bmesh.coordinates()[i])[1], (bmesh.coordinates()[j])[1]))
        if abs(bmesh.coordinates()[nearby_points[0]][1]-fixed_point[1]) < \
           abs(bmesh.coordinates()[nearby_points[1]][1]-fixed_point[1]) :
            chosen_point = nearby_points[0]
        else :
            chosen_point = nearby_points[1]

        delta_mp = 0.
        skew = False
        deskew = {}
        if chosen_point == moving_index :
            if abs(moving_top[1]-fixed_point[1]) < vert_cutoff and nudgable :
                move_from = bmesh.coordinates()[chosen_point][1]
                skew = True
                for i in range(0, len(mesh.coordinates())) :
                    # Don't skew horiz neighbours in this case, as we should be
                    # close enough anyhow and we want to avoid using zeta
                    if abs(mesh.coordinates()[i][0]-boundary) < 1e-4 :
                        cur = mesh.coordinates()[i]
                        deskew[i] = cur[1]
                        (mesh.coordinates()[i])[1] += \
                            (cur[0]-horiz_cutoff)*(fixed_point[1]-move_from)*(cur[1]+depth)/((depth+move_from)*(boundary-horiz_cutoff))
                return (skew, deskew, fixed_point[1], delta_mp)
            chosen_point = nearby_points[0]

        if abs(bmesh.coordinates()[chosen_point][1]-fixed_point[1]) > DOLFIN_EPS :
            move_from = bmesh.coordinates()[chosen_point][1]
            skew = True
            z = zeros(1)
            x = zeros(1)
            for i in range(0, len(mesh.coordinates())) :
                if abs(mesh.coordinates()[i][0]-boundary) < abs(horiz_cutoff-boundary) :
                    cur = mesh.coordinates()[i]
                    deskew[i] = cur[1]
                    x[0] = cur[0]
                    zeta.eval(z, x)
                    z[0] -= draft
                    skew_from =\
                        (z[0]+depth)*(move_from+depth)/(moving_top[1]+depth)-depth
                    skew_fixed =\
                        (z[0]+depth)*(fixed_point[1]+depth)/(moving_top[1]+depth)-depth
                    fm = cur[1]
                    if abs(fm-z[0]) < DOLFIN_EPS :
                        delta = 0.
                    elif fm > skew_from + DOLFIN_EPS :
                        delta = (skew_fixed-skew_from)*(z[0]-fm)/(z[0]-skew_from)
                    elif fm < skew_from - DOLFIN_EPS :
                        delta = (skew_fixed-skew_from)*(depth+fm)/(depth+skew_from)
                    else :
                        delta = skew_fixed-skew_from

                    if abs(cur[0] - boundary) < DOLFIN_EPS :
                        if abs(fm - skew_from) < DOLFIN_EPS :
                            (mesh.coordinates()[i])[1] = skew_fixed
                        else :
                            (mesh.coordinates()[i])[1] += delta
                    else :
                        (mesh.coordinates()[i])[1] += (cur[0]-horiz_cutoff)*delta/(boundary-horiz_cutoff)

        return (skew, deskew, fixed_point[1], delta_mp)

# Define function for projecting a 1D mesh embedded in 2D
# onto the x-axis
def flatten(mesh) :
    mesh.order()
    num_points = len(mesh.coordinates())
    mesh.init(0,1)
    conn = mesh.topology()(0,1)
    line = Mesh()
    editor_flat = MeshEditor()
    editor_flat.open(line, "interval", 1, 1)
    editor_flat.init_vertices(num_points)
    editor_flat.init_cells(num_points-1)
    order = []
    i = 0
    for vit in vertices(mesh) :
        order.append((mesh.geometry().point(vit.index()).x(), i))
        i += 1
    print '#'*10
    order.sort(lambda c,d : cmp(c[0], d[0]))
    mp = {}
    rmp = {}
    for i in range(0, num_points) :
        mp[i] = order[i][1]
        rmp[order[i][1]] = i
    for i in range(0, num_points) :
        editor_flat.add_vertex(i, order[i][0])
    for i in range(0, num_points-1) :
        editor_flat.add_cell(i, i, i+1)
    editor_flat.close()
    return line, mp, rmp

# Define function for moving the mesh top from zeta_old to zeta
def mesh_move(mesh, zeta_old, zeta, depth, draft = 0.) :
    zold = zeros(1)
    znew = zeros(1)
    x = zeros(1)
    coords = mesh.coordinates()
    for i in range(0, len(coords)) :
        x[0] = coords[i][0]
        zeta_old.eval(zold, x)
        zo = zold[0]
        zeta.eval(znew, x)
        zn = znew[0]

        if abs(zo-coords[i][1]-draft) < 1e-8 :
            coords[i][1] = zn-draft
        else :
            coords[i][1] += (zn-zo)*(coords[i][1]+depth)/(zo-draft+depth)
    mesh.intersection_operator().clear()

def generate_mfs (bmesh, topline, mesh, top_to_bmesh) :
    bmesh_to_mesh_cell = bmesh.cell_map()
    top_conn = topline.topology()(1,0)
    mesh_conn = mesh.topology()(1,2)
    mesh_conn_rev = mesh.topology()(2,1)
    bmesh.init(0,1)
    bmesh_conn = bmesh.topology()(0,1)
    k = 0
    topline_cell_to_mesh_cell = MeshFunction("uint", topline, 1)
    topline_cell_to_mesh_line = MeshFunction("uint", topline, 1)
    for i in range(0, topline.num_cells()) :
        v1, v2 = top_conn(i)
        cell_on_boundary =\
            list(set(bmesh_conn(top_to_bmesh[v1])).intersection(set(bmesh_conn(top_to_bmesh[v2]))))[0]
        topline_cell_to_mesh_cell[i] = mesh_conn(bmesh_to_mesh_cell[cell_on_boundary])[0]
        topline_cell_to_mesh_line[i] =\
            bmesh_to_mesh_cell[cell_on_boundary]
    return topline_cell_to_mesh_cell, topline_cell_to_mesh_line

# Define function to update the boundary mesh coordinates to
# match a full mesh
def update_boundary_mesh(mesh, bmesh) :
    vertex_map = bmesh.vertex_map()
    for i in range(0, len(bmesh.coordinates())) :
        bmesh.coordinates()[i] = mesh.coordinates()[vertex_map[i]]
    bmesh.intersection_operator().clear()

if __name__ == '__main__' :
    cvgce_old = 0
    timing = Timing()
    
    # Obtain parameters

    argparser = argparse.ArgumentParser()
    argparser.add_argument('-r', '--runname')
    argparser.add_argument('-l', '--load', action='store_true')
    argparser.add_argument('-n', '--no-save', action='store_true')
    argparser.add_argument('-s', '--silent', action='store_true')
    argparser.add_argument('-f', '--frequency')
    argparser.add_argument('--stop-after')
    argparser.add_argument('--output-ci', action='store_true')
    args = argparser.parse_args()
    
    if args.runname is None :
        print "Please supply runname : -r RUN"
        sys.exit(3)

    stop_after = None
    if args.stop_after is not None :
        print "Stopping at iteration " + args.stop_after
        stop_after = int(args.stop_after)

    frame_frequency = 1
    if args.frequency is not None :
        frame_frequency = int(args.frequency)
    
    resume = args.load
    no_save = args.no_save
    runname =  args.runname
    output_ci = args.output_ci
    silent = args.silent
    
    builddir = "/maybehome/pweir/Work/mk2-matrix/build"

    try :
        with open("scripts/outroot.sh", 'r') as f :
            outroot = f.readlines()[0]
            outroot = outroot.split('=')[1].strip()
            outdir = outroot + '/output.' + runname
    except :
        print "Could not establish outroot; are you definitely running from build root?"
    
    try :
        if not os.path.exists(outdir) :
            os.mkdir(outdir)
        for path in ('/phi', '/adda/phidt', '/add/mesh', '/source') :
            if not os.path.exists(outdir+path) :
                os.makedirs(outdir+path)
    except error :
        print str(error)
        sys.exit(5)

    for source_file in os.listdir(builddir+'/python') :
        try :
            shutil.copyfile(builddir+'/python/'+source_file,
                outdir+'/source/'+source_file)
        except :
            pass

    os.chdir(outdir)
    
    al_dbc = 1e3
    
    if not silent :
        print "MK3 - hydroelasticity simulation"
        print "="*40
    
    source = Expression("0")
    
    # Settings
    try :
        pf = File("params/parameters.xml")
        p = Parameters("mk3")
        pf >> p
    except :
        print "Could not open params/parameters.xml in output directory"
        sys.exit(4)
    
    T = p.final_time
    dt = p.timestep
    depth = p.depth
    initial_draft = p.initial_draft
    domain_left = p.domain_left
    left = p.left
    right = p.right
    domain_right = p.domain_right
    refinement_level = p.mesh_refinement_level
    vertical_cells = p.mesh_vertical_cells
    horiz_cell_density = p.mesh_horizontal_cell_density
    rat = p.mesh_rat
    rhow = p.density_fluid
    ic_left = p.ic_left
    ic_width = p.ic_width
    ic_amp = p.ic_amp

    if p.has_key('ic_offset') :
        ic_offset = p.ic_offset
    else :
        ic_offset = 0.

    if p.has_key('ic_vert_offset') :
        ic_vert_offset = p.ic_vert_offset
    else :
        ic_vert_offset = 0.
    
    if p.has_key('ic_symmetric') :
        ic_symmetric = p.ic_symmetric
    else :
        ic_symmetric = False

    if p.has_key('alpha') :
        alpha = p.alpha
    else :
        alpha = .001

    if p.has_key('beam_youngs') :
        E = p.beam_youngs
    else :
        E = 8e9

    if p.has_key('beam_type') :
        beam_type = p.beam_type
    else :
        beam_type = "Euler-Bernoulli"

    if p.has_key('ci_min') :
        ci_min = p.ci_min
    else :
        ci_min = 20

    if p.has_key('ci_lim') :
        ci_lim = p.ci_lim
    else :
        ci_lim = 100

    if p.has_key('mu') :
        mu = p.mu
    else :
        mu = 0.00

    skew_cutoff = 1.e0
    skew_vert_cutoff = .01
    
    # DOLFIN settings
    set_log_level(100)
    
    # Open output files
    asdf1 = File("asdf1.pvd")
    asdf2 = File("asdf2.pvd")
    file_phi1 = File("phi/model-"+runname+"-phi1_.pvd")
    file_phi2 = File("phi/model-"+runname+"-phi2_.pvd")
    file_phi3 = File("phi/model-"+runname+"-phi3_.pvd")
    file_phidt1 = File("add/phidt/model-"+runname+"-phidt1_.pvd")
    file_phidt2 = File("add/phidt/model-"+runname+"-phidt2_.pvd")
    file_phidt3 = File("add/phidt/model-"+runname+"-phidt3_.pvd")
    file_phidt_la12 = File("add/phidt/model-"+runname+"-phidtla12_.pvd")
    file_phidt_la23 = File("add/phidt/model-"+runname+"-phidtla23_.pvd")
    file_mesh1 = File("add/mesh/model-"+runname+"-mesh1_.pvd")
    file_mesh2 = File("add/mesh/model-"+runname+"-mesh2_.pvd")
    file_mesh3 = File("add/mesh/model-"+runname+"-mesh3_.pvd")
    phinonz_file = File("phinonz-"+runname+"-1_.pvd")
    logfile = open("mk2." + runname + ".log", "w")
    
    logfile.write(p.str(True))
    logfile.write("\n")

    # Create mesh
    n = horiz_cell_density
    #mesh1 = Rectangle(domain_left, -depth, left, 0., int((left-domain_left)*n), vertical_cells)
    #mesh2 = Rectangle(left, -depth, 1., -initial_draft, int((right-left)*n), vertical_cells)
    #mesh3 = Rectangle(right, -depth, domain_right, 0., int((domain_right-right)*n), vertical_cells)
    
    def make_mesh(i) :
        all_paths_exist = os.path.exists("tmp.automesh."+str(i)) and \
                os.path.getmtime("tmp.automesh."+str(i)) > \
                os.path.getmtime("automesh."+str(i)+".geo")

        if not all_paths_exist :
            f = open("tmp.automesh."+str(i), 'w')
            p = Popen(["gmsh", "-2", "automesh."+str(i)+".geo", "-algo", "del2d"], stdout=f)
            Popen.wait(p)
            p = Popen(["dolfin-convert", "automesh."+str(i)+".msh", "automesh."+str(i)+".xml"], stdout=f)
            Popen.wait(p)
            p = Popen(["dolfin-order", "automesh."+str(i)+".xml"], stdout=f)
            Popen.wait(p)
            f.close()
    
        mesh = Mesh("automesh."+str(i)+".xml")
        for j in range(0, len(mesh.coordinates())) :
            mesh.coordinates()[j][0] *= rat
    
        return mesh
    
    try :
        mesh1 = make_mesh(1)
        mesh2 = make_mesh(2)
        mesh3 = make_mesh(3)
    except e :
        print str(e)
        quit(6)

    #mesh1 = refine(mesh1)
    #mesh2 = refine(mesh2)
    #mesh3 = refine(mesh3)
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    h = 1./n
    
    # Define intersection top variable
    gamma_top_1 = -initial_draft
    gamma_top_3 = -initial_draft
    
    # Define crucial points
    ulcrnr1 = Point(domain_left, 0)
    urcrnr1 = Point(left, 0)
    llcrnr2 = Point(left, gamma_top_1)
    lrcrnr2 = Point(right, gamma_top_3)
    ulcrnr3 = Point(right, 0)
    urcrnr3 = Point(domain_right, 0)
    
    # Refine near corners
    class near_1_top_corners_cl(SubDomain) :
        def __init__(self, dist) :
            SubDomain.__init__(self)
            self.dist = dist
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(ulcrnr1) < self.dist or \
                   Point(*x).distance(urcrnr1) < self.dist or \
                   Point(*x).distance(llcrnr2) < self.dist or \
                   x[1] > ulcrnr1[1]-self.dist - DOLFIN_EPS
    class near_beam_corners_cl(SubDomain) :
        def __init__(self, dist, bar = True) :
            SubDomain.__init__(self)
            self.dist = dist
            self.bar = bar
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(llcrnr2) < self.dist or \
                   Point(*x).distance(lrcrnr2) < self.dist or \
                   (self.bar and x[1] > -initial_draft-self.dist - DOLFIN_EPS)
    class near_3_top_corners_cl(SubDomain) :
        def __init__(self, dist) :
            SubDomain.__init__(self)
            self.dist = dist
    
        def inside(self, x, on_boundary) :
            return Point(*x).distance(ulcrnr3) < self.dist or \
                   Point(*x).distance(urcrnr3) < self.dist or \
                   Point(*x).distance(lrcrnr2) < self.dist or \
                   x[1] > ulcrnr3[1]-self.dist - DOLFIN_EPS
    
    def refine_mesh(subdom_cl, mesh, dist, **argv) :
        nearcornersd = subdom_cl(dist, **argv)
        nearcorner = MeshFunction("bool", mesh, mesh.topology().dim())
        nearcorner.set_all(False)
        nearcornersd.mark(nearcorner, True)
        return refine(mesh, nearcorner)
    
    for i in range(0, refinement_level) :
        mesh1 = refine(mesh1)
        mesh2 = refine(mesh2)
        mesh3 = refine(mesh3)
    #    mesh1 = refine_mesh(near_1_top_corners_cl, mesh1, depth*1.5**(-(i-1)/2))
    #    mesh2 = refine_mesh(near_beam_corners_cl, mesh2, depth*1.5**(-(i-1)/2))
    #    mesh3 = refine_mesh(near_3_top_corners_cl, mesh3, depth*1.5**(-(i-1)/2))
    
    #for i in range(0, 2*refinement_level) :
    #    mesh1 = refine_mesh(near_beam_corners_cl, mesh1, depth*2.**(-.5*(i-1)))
    #    mesh3 = refine_mesh(near_beam_corners_cl, mesh3, depth*2.**(-.5*(i-1)))
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    
    # Define 1D subdomains
    # NB: The SubDomains and classes, except for inner_boundary_cl, will be
    # invalidated by movement of the mesh, so stick to index lists
    class top_cl(SubDomain) :
        def __init__(self, draft=0.) :
            SubDomain.__init__(self)
            self.draft = draft
        def inside(self, x, on_boundary) :
            return x[1] > -self.draft - DOLFIN_EPS
    topsd = top_cl()
    def get_top_indices(mesh, draft=0.) :
        top_indices = MeshFunction("uint", mesh, mesh.topology().dim()-1)
        top_indices.set_all(0)
        topsd = top_cl(draft)
        topsd.mark(top_indices, 1)
        top_indices = filter(lambda t : top_indices[t]==1, range(0, top_indices.size()))
        return top_indices
    top_indices_1 = get_top_indices(mesh1)
    top_indices_2 = get_top_indices(mesh2, initial_draft)
    top_indices_3 = get_top_indices(mesh3)
    
    class beam_cl(SubDomain) :
        def inside(self, x, on_boundary) :
            return x[1] > - initial_draft - DOLFIN_EPS
    beamsd = beam_cl()
    
    # Define internal boundary
    class inner_boundary_cl(SubDomain):
        def __init__(self, line, gamma_top) :
            SubDomain.__init__(self)
            self.line = line
            self.gamma_top = gamma_top
        def inside(self, x, on_boundary) :
            return abs(x[0] - self.line) < DOLFIN_EPS and x[1] < self.gamma_top + DOLFIN_EPS
    
    # Defining the boundary meshes saves us working on the full mesh quite so often
    def create_boundary(mesh, sd) :
        bmesh = BoundaryMesh(mesh)
        bmconn = bmesh.topology()(1,0)
        top = SubMesh(bmesh, sd)
        bmesh_to_mesh = bmesh.vertex_map()
        top_to_bmesh = top.data().mesh_function("parent_vertex_indices")
        num_top_points = len(top.coordinates())
        return bmesh, num_top_points, bmesh_to_mesh, top_to_bmesh, bmconn, top
    
    bmesh1, num_top_points_1, bmesh1_to_mesh1, top_to_bmesh1t, bm1conn, top_1 =  \
        create_boundary(mesh1, topsd)
    
    #bmesh2 = BoundaryMesh(mesh2)
    #bm2conn = bmesh2.topology()(1,0)
    #beam_mesh = SubMesh(bmesh2, beamsd)
    #
    #beam_to_bmesh2 = beam_mesh.data().mesh_function("parent_vertex_indices")
    #num_beam_points = len(beam_mesh.coordinates())
    bmesh2, num_beam_points, bmesh2_to_mesh2, beam_to_bmesh2t, bm2conn, beam_mesh = \
        create_boundary(mesh2, beamsd)
    
    bmesh3, num_top_points_3, bmesh3_to_mesh3, top_to_bmesh3t, bm3conn, top_3 = \
        create_boundary(mesh3, topsd)
    
    # Build flat meshes
    # NB : this is a vertical projection of the top, so integrating over it is
    # different (i.e. don't)!
    topline_1, mp, rmp = flatten(top_1)
    top_to_bmesh1 = MeshFunction("uint", topline_1, 0)
    for i in range(0, len(topline_1.coordinates())) :
        top_to_bmesh1[i] = top_to_bmesh1t[mp[i]]
    V_topline_1 = FunctionSpace(topline_1, "Lagrange", 1)
            
    beamline, mp, rmp = flatten(beam_mesh)
    beam_to_bmesh2 = MeshFunction("uint", beamline, 0)
    for i in range(0, len(beamline.coordinates())) :
        beam_to_bmesh2[i] = beam_to_bmesh2t[mp[i]]
    V_beamline = FunctionSpace(beamline, "Lagrange", 1)
    
    topline_3, mp, rmp = flatten(top_3)
    top_to_bmesh3 = MeshFunction("uint", topline_3, 0)
    for i in range(0, len(topline_3.coordinates())) :
        top_to_bmesh3[i] = top_to_bmesh3t[mp[i]]
    V_topline_3 = FunctionSpace(topline_3, "Lagrange", 1)

    topline_cell_to_mesh1_cell, topline_cell_to_mesh1_line = generate_mfs(
        bmesh1, topline_1, mesh1, top_to_bmesh1)
        #    mesh_conn_rev(topline_cell_to_mesh1_cell[i]).index(bmesh1_to_mesh1_cell[cell_on_boundary])
            
    beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line = generate_mfs(
        bmesh2, beamline, mesh2, beam_to_bmesh2)

    topline_cell_to_mesh3_cell, topline_cell_to_mesh3_line = generate_mfs(
        bmesh3, topline_3, mesh3, top_to_bmesh3)

    # Build beam base
    beam_desc = mk2beam._PlateDescription()
    beam_desc.name = "beam"
    beam_desc.left = left
    beam_desc.right = right
    beam_desc.draft = initial_draft
    beam_desc.E = E
    beam_desc.nu = 0.3
    beam_desc.beta = 0.25
    beam_desc.rhop = 917.
    beam_desc.mup = 0.
    beam_desc.h = beam_desc.draft * rhow / beam_desc.rhop
    beamer = mk2beam.Plate.get_platermap()
    beamer = mk2beam.Plate.get_plater(beam_type)
    #beam = mk2beam.Plate(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True);
    beam = fs_beam(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True,
        zeta_on=True)
    
    # Identify indices of the right end of the free surface and left end of the beam
    # with respect to the boundary meshes
    top_left_corner_index_t = (sorted(range(0, num_top_points_1),
        lambda c,d : cmp(topline_1.coordinates()[c][0],topline_1.coordinates()[d][0])))[0]
    top_right_index_t = (sorted(range(0, num_top_points_1),
        lambda c,d : cmp(topline_1.coordinates()[d][0],topline_1.coordinates()[c][0])))[0]
    top_right_index = top_to_bmesh1[top_right_index_t]
    beam_left_index_b = (sorted(range(0, num_beam_points),
        lambda c,d : cmp(beamline.coordinates()[c][0],beamline.coordinates()[d][0])))[0]
    beam_left_index = beam_to_bmesh2[beam_left_index_b]
    beam_right_index_b = (sorted(range(0, num_beam_points),
        lambda c,d : cmp(beamline.coordinates()[d][0],beamline.coordinates()[c][0])))[0]
    beam_right_index = beam_to_bmesh2[beam_right_index_b]
    top_left_index_t = (sorted(range(0, num_top_points_3),
        lambda c,d : cmp(topline_3.coordinates()[c][0],topline_3.coordinates()[d][0])))[0]
    top_left_index = top_to_bmesh3[top_left_index_t]
    coords =beam.zetan1.function_space().mesh().coordinates()
    beam_V_left_index_b = (sorted(range(0, len(coords)),
        lambda c,d : cmp(coords[c][0],coords[d][0])))[0]
    beam_V_right_index_b = (sorted(range(0, len(coords)),
        lambda c,d : cmp(coords[d][0],coords[c][0])))[0]
    
    class ic_cl(Expression) :
        def eval(self, values, x) :
            if ic_symmetric :
                x = (left+right)*.5 - abs(x[0] - (left+right)*.5)
            else :
                x = x[0]

            if (x < ic_left) :
                values[0] =\
                ic_amp*.5*(cos(pi*(ic_offset)/ic_width)+1)+ic_vert_offset
            elif (x < ic_left+ic_width) :
                values[0] =\
                ic_amp*.5*(cos(pi*(x-ic_left+ic_offset)/ic_width)+1)+ic_vert_offset
            else :
                values[0] =\
                ic_amp*.5*(cos(pi*(ic_width+ic_offset)/ic_width)+1)+ic_vert_offset
    ice = ic_cl()
    
    # Define functions on (time-independent) 1D meshes
    def make_functions(V_topline, ic = True) :
        #   Primary functions
        cphi = Function(V_topline)
        cphi_m1 = Function(V_topline)
        cphi_new = Function(V_topline)
        zeta = Function(V_topline)
        zeta_m1 = Function(V_topline)
        zeta_old = Function(V_topline)
        #   Time derivatives
        cphidt = Function(V_topline)
        zetadt = Function(V_topline)
        #   Auxiliary functions
        dirichlet = Function(V_topline)
        
        # Set initial conditions
        cphi_new.interpolate(Constant(0.))
        cphi_m1.interpolate(Constant(0.))
        zeta.interpolate(Constant(0.))
        zeta_m1.interpolate(zeta)
        zeta_old.interpolate(zeta)
        if ic :
            zeta.assign(ice)
            zeta_m1.assign(ice)
            zeta_old.assign(ice)
        zetax = project(zeta.dx(0))
        zetaxx = project(zetax.dx(0))
            #cphi.interpolate(Constant(1.))
    
        return cphi, cphi_new, cphi_m1, \
                zeta, zeta_m1, zeta_old, zetax, \
               zetaxx, cphidt, zetadt, dirichlet
    
    # Load functions for top meshes
    def load_functions(V_topline, num) :
    
        cphi, cphi_new, cphi_m1, zeta, zeta_m1, zeta_old,\
        zetax, zetaxx, cphidt, zetadt,\
        dirichlet =\
            make_functions(V_topline, ic=False)
        
        #   Primary functions
        File("cphi.%d.xml" % num) >> cphi.vector()
        File("cphi_new.%d.xml" % num) >> cphi_new.vector()
        File("cphi_m1.%d.xml" % num) >> cphi_m1.vector()
        File("zeta.%d.xml" % num) >> zeta.vector()
        File("zeta_m1.%d.xml" % num) >> zeta_m1.vector()
        File("zeta_old.%d.xml" % num) >> zeta_old.vector()
        File("zetax.%d.xml" % num) >> zetax.vector()
        File("zetaxx.%d.xml" % num) >> zetaxx.vector()
        File("cphidt.%d.xml" % num) >> cphidt.vector()
        File("zetadt.%d.xml" % num) >> zetadt.vector()

        return cphi, cphi_new, cphi_m1, \
                zeta, zeta_m1, zeta_old, zetax, \
                zetaxx, cphidt, zetadt, dirichlet
    
    # Load functions for top meshes
    def save_functions(V_topline, num, cphi, cphi_new, cphi_m1,
               zeta, zeta_m1, zeta_old, zetax, zetaxx,
               cphidt, zetadt, dirichlet) :
        
        #   Primary functions
        File("cphi.%d.xml" % num) << cphi.vector()
        File("cphi_new.%d.xml" % num) << cphi_new.vector()
        File("cphi_m1.%d.xml" % num) << cphi_m1.vector()
        File("zeta.%d.xml" % num) << zeta.vector()
        File("zeta_m1.%d.xml" % num) << zeta_m1.vector()
        File("zeta_old.%d.xml" % num) << zeta_old.vector()
        File("zetax.%d.xml" % num) << zetax.vector()
        File("zetaxx.%d.xml" % num) << zetaxx.vector()
        File("cphidt.%d.xml" % num) << cphidt.vector()
        File("zetadt.%d.xml" % num) << zetadt.vector()
    
    def backup_functions(num, to) :
        filenames = []
        filenames.append("cphi.%d.xml" % num)
        filenames.append("cphi_new.%d.xml" % num)
        filenames.append("cphi_m1.%d.xml" % num)
        filenames.append("zeta.%d.xml" % num)
        filenames.append("zeta_m1.%d.xml" % num)
        filenames.append("zeta_old.%d.xml" % num)
        filenames.append("zetax.%d.xml" % num)
        filenames.append("zetaxx.%d.xml" % num)
        filenames.append("cphidt.%d.xml" % num)
        filenames.append("zetadt.%d.xml" % num)

        for filename in filenames :
            shutil.copyfile(filename, filename+'.'+str(to))

    def load_state() :
        t = 0.
        titeration = 0
        cl_last = 0

        f = open('state.st', 'r')
        line = f.readline()
        t = float(line.split(':')[-1])
        line = f.readline()
        titeration = int(line.split(':')[-1])
        line = f.readline()
        cl_last = int(line.split(':')[-1])

        return t, titeration, cl_last

    def save_state(t, titeration, cl_last) :
        f = open('state.st', 'w')
        f.write("t:%lf\n" % t)
        f.write("titeration:%d\n" % titeration)
        f.write("cl_last:%d\n" % cl_last)

    if resume :
        # Try and reduce likelihood of us overwriting the copy we want
        if not no_save :
            n = 0
            for f in os.listdir('.') :
                if f.startswith('state.st') :
                    n += 1

            shutil.copyfile('state.st', 'state.st.'+str(n))
            backup_functions(1, n)
            beam.backup_function_vectors(str(n))
            backup_functions(3, n)

        cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
            zetax_1, zetaxx_1, cphidt_1, zdt_1,\
            dirichlet_1 =\
                load_functions(V_topline_1, 1)

    
        cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
            zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
            dirichlet_2 =\
                make_functions(V_beamline)
        #cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
        #    zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
        #    dirichlet_2 =\
        #        load_functions(V_beamline, 2)

        beam.load_function_vectors()
    
        cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
            zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
            dirichlet_3 =\
                load_functions(V_topline_3, 3)
    else :
        cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
            zetax_1, zetaxx_1, cphidt_1, zdt_1,\
            dirichlet_1 =\
                make_functions(V_topline_1)
    
        cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
            zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
            dirichlet_2 =\
                make_functions(V_beamline)

        cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
            zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
            dirichlet_3 =\
                make_functions(V_topline_3)
    #print zeta_1.vector()[top_right_index_t]
    zeta_2.interpolate(beam.zetan1)
    zeta_2_m1.interpolate(beam.zetan)
    zeta_2_old.interpolate(beam.zetan1_old)

    zdt_1 = Function(V_topline_1)
    zdt_1.vector().zero()
    zdt_1_m1 = project(zdt_1)
    zdt_1_m2 = project(zdt_1)
    zdt_1_m3 = project(zdt_1)
    zdt_1_m4 = project(zdt_1)
    zdt_2 = Function(V_beamline)
    zdt_2.vector().zero()
    zdt_2_m1 = project(zdt_2)
    zdt_2_m2 = project(zdt_2)
    zdt_2_m3 = project(zdt_2)
    zdt_2_m4 = project(zdt_2)
    zdt_3 = Function(V_topline_3)
    zdt_3.vector().zero()
    zdt_3_m1 = project(zdt_3)
    zdt_3_m2 = project(zdt_3)
    zdt_3_m3 = project(zdt_3)
    zdt_3_m4 = project(zdt_3)
    cpdt_1 = Function(V_topline_1)
    cpdt_1_m1 = project(cpdt_1)
    cpdt_1_m2 = project(cpdt_1)
    cpdt_1_m3 = project(cpdt_1)
    cpdt_1_m4 = project(cpdt_1)
    cpdt_2 = Function(V_beamline)
    cpdt_2_m1 = project(cpdt_2)
    cpdt_2_m2 = project(cpdt_2)
    cpdt_2_m3 = project(cpdt_2)
    cpdt_2_m4 = project(cpdt_2)
    cpdt_3 = Function(V_topline_3)
    cpdt_3_m1 = project(cpdt_3)
    cpdt_3_m2 = project(cpdt_3)
    cpdt_3_m3 = project(cpdt_3)
    cpdt_3_m4 = project(cpdt_3)
    
    beam.nonstep()
    beam.setup_diagnostics("w")
    
    neumann_2 = Function(V_beamline)
    neumann_2.assign(Constant(0.))
    
    # Move mesh to starting position
    mesh_move(mesh1, Constant(0.), zeta_1_old, depth)
    update_boundary_mesh(mesh1, bmesh1)
    mesh_move(mesh2, Constant(0.), zeta_2_old, depth, initial_draft)
    update_boundary_mesh(mesh2, bmesh2)
    mesh_move(mesh3, Constant(0.), zeta_3_old, depth)
    update_boundary_mesh(mesh3, bmesh3)
    
    file_mesh1 << mesh1
    file_mesh2 << mesh2
    file_mesh3 << mesh3
    
    log_now = output_ci
    final_loop = False
    quit_for_timing = False

    chg = 0.
    chg_old = 0.

    # Time loop
    if resume :
        t, tn, ci = load_state()
        logfile.write(\
            "Resuming from t = %lf (%d); correction # %d\n" % (t, tn, ci))
    else :
        t = 0
        tn = 0
        ci = 0
    fn = 0
    start = t
    new_zeta = Function(V_beamline)
    old_zeta = Function(V_beamline)

    beam.zetan.interpolate(beam.zetan1)
    free_surface_under_beam = free_surface_under_beam_cl(beam, None, not_first)
    free_surface_under_beam_zero = free_surface_under_beam_zero_cl()
    constraint_2 = MeshFunction("uint", mesh2, mesh2.topology().dim()-1)
    constraint_2.set_all(2)
    free_surface_under_beam.mark(constraint_2, 3)
    
    while t < T and not quit_for_timing :
    
        beam.output_diagnostics(t)
    
        keep_correcting = True

        zeta_1.interpolate(project(zeta_1_m1 + (dt/24.)*(55*zdt_1_m1 - 59*zdt_1_m2 + 37*zdt_1_m3 - 9*zdt_1_m4)))
        zetax_1 = project(zeta_1.dx(0))
        zetaxx_1 = project(zetax_1.dx(0))
        zeta_2.interpolate(project(zeta_2_m1 + (dt/24.)*(55*zdt_2_m1 - 59*zdt_2_m2 + 37*zdt_2_m3 - 9*zdt_2_m4)))
        zetax_2 = project(zeta_2.dx(0))
        zetaxx_2 = project(zetax_2.dx(0))
        zeta_3.interpolate(project(zeta_3_m1 + (dt/24.)*(55*zdt_3_m1 - 59*zdt_3_m2 + 37*zdt_3_m3 - 9*zdt_3_m4)))
        zetax_3 = project(zeta_3.dx(0))
        zetaxx_3 = project(zetax_3.dx(0))

        while keep_correcting and not quit_for_timing :

            if log_now :
                fn += 1

            skew_1, deskew_1, gamma_top_1, skew_3, deskew_3, gamma_top_3 = \
                skew()

            top_right_height = bmesh1.coordinates()[top_right_index][1]
            beam_left_height = bmesh2.coordinates()[beam_left_index][1]
            beam_right_height = bmesh2.coordinates()[beam_right_index][1]
            top_left_height = bmesh3.coordinates()[top_left_index][1]
            #print (t, top_right_height, beam_left_height)

            if log_now :
                #   Output moved mesh
                file_mesh1 << mesh1
                file_mesh2 << mesh2
                file_mesh3 << mesh3
    
            # CI step

            # Build solver
            inner_boundary_1 = inner_boundary_cl(left, gamma_top_1)
            inner_boundary_3 = inner_boundary_cl(right, gamma_top_3)
            timing.click("solver init")
            solver = Solver(mesh1, mesh2, mesh3,
                inner_boundary_1, top_indices_1,
                top_indices_2, constraint_2,
                inner_boundary_3, top_indices_3,
                timing)
            timing.clock()
    
            # Solve
            Wn1x = Dx(Function(beam.Wn1.function_space(), beam.Wn1), 0)
            Wdtn1 = Function(beam.Wdtn1.function_space(), beam.Wdtn1)
            Wddtn1 = Function(beam.Wddtn1.function_space(), beam.Wddtn1)

            cpdt_1 = project(-9.81*zeta_1)
            if t < DOLFIN_EPS :
                cpdt_1_m1.interpolate(cpdt_1)
                cpdt_1_m2.interpolate(cpdt_1)
                cpdt_1_m3.interpolate(cpdt_1)
                cpdt_1_m4.interpolate(cpdt_1)

            cphi_1_new.interpolate(project(cphi_1_m1 + (dt/24.)*(9*cpdt_1 + 19*cpdt_1_m1 - 5*cpdt_1_m2 + cpdt_1_m3)))
            dirichlet_1.interpolate(cphi_1_new)

            cpdt_2 = project(-9.81*(zeta_2-beam.draft))
            cphi_2_new.interpolate(project(cphi_2_m1 + (dt/24.)*(9*cpdt_2 + 19*cpdt_2_m1 - 5*cpdt_2_m2 + cpdt_2_m3)))
            dirichlet_2.interpolate(cphi_2_new)

            #if abs(beam_left_height - top_right_height) < DOLFIN_EPS :
            if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
                av = bdy_average(dirichlet_2, beam_left_index_b,
                                 dirichlet_1, top_right_index_t, left)
            #if abs(beam_right_height - top_left_height) < DOLFIN_EPS :
            if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
                av = bdy_average(dirichlet_2, beam_right_index_b,
                                 dirichlet_3, top_left_index_t, right)

            neumann_2.interpolate(project((1+Wn1x**2)**.5*Wdtn1))
            #print neumann_2.vector().array()

            cphi_3_new.interpolate(project(cphi_3_m1 + (dt/24.)*(9*cpdt_3 + 19*cpdt_3_m1 - 5*cpdt_3_m2 + cpdt_3_m3)))
            dirichlet_3.interpolate(cphi_3_new)

            #asdf1 << dirichlet_3
            #if cpdt_1 is None :
            #    dn_c = 9.81*dt*dt
            #else :
            #    dn_c = 0.

            #dn_c = 0.
            timing.click("solve_poisson")
            phi_1, phi_2, phi_3, phi_la12, phi_la23 = solver.solve_poisson(h,
                source, res,
                zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
                beam.zetan1x, dirichlet_2, neumann_2, beam_to_bmesh2, bmesh2_to_mesh2,
                zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
                dn_c=0.)
            timing.clock()

            #RUNAGAIN FOR FS COMP
            solver = Solver(mesh1, mesh2, mesh3,
                inner_boundary_1, top_indices_1,
                top_indices_2, None,
                inner_boundary_3, top_indices_3,
                timing)
            fsphi_1, fsphi_2, fsphi_3, fsphi_la12, fsphi_la23 = solver.solve_poisson(h,
                source, res,
                zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
                beam.zetan1x, dirichlet_2, None, beam_to_bmesh2, bmesh2_to_mesh2,
                zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
                dn_c=0.)

            if log_now :
                # Output phi
                file_phi1 << phi_1
                file_phi2 << phi_2
                file_phi3 << phi_3
    
            #RMV COMMENT
            #straighten(skew_1, deskew_1, mesh1, bmesh1,
            #           mesh2, bmesh2,
            #           skew_3, deskew_3, mesh3, bmesh3)

            #if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            #    av =bdy_average(zeta_2, beam_left_index_b,
            #                     zeta_1, top_right_index_t, left, beam.draft)
            #    av =bdy_average(zeta_2_m1, beam_left_index_b,
            #                     zeta_1_m1, top_right_index_t, left, beam.draft)
            #if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            #    av = bdy_average(zeta_2, beam_right_index_b,
            #                     zeta_3, top_left_index_t, right, beam.draft)
            #    av = bdy_average(zeta_2_m1, beam_right_index_b,
            #                     zeta_3_m1, top_left_index_t, right, beam.draft)

            #skew_1, deskew_1, gamma_top_1, skew_3, deskew_3, gamma_top_3 = \
            #    skew()

            #top_right_height = bmesh1.coordinates()[top_right_index][1]
            #beam_left_height = bmesh2.coordinates()[beam_left_index][1]
            #beam_right_height = bmesh2.coordinates()[beam_right_index][1]
            #top_left_height = bmesh3.coordinates()[top_left_index][1]

            #if log_now :
            #    #   Output moved mesh
            #    file_mesh1 << mesh1
            #    file_mesh2 << mesh2
            #    file_mesh3 << mesh3
    
            ## CI step

            ##free_surface_under_beam = free_surface_under_beam_cl(beam, not_first)
    
            ## Build solver
            #inner_boundary_1 = inner_boundary_cl(left, gamma_top_1)
            #inner_boundary_3 = inner_boundary_cl(right, gamma_top_3)
            #solver = Solver(mesh1, mesh2, mesh3,
            #    inner_boundary_1, top_indices_1,
            #    top_indices_2, constraint_2,
            #    inner_boundary_3, top_indices_3,
            #    timing)

            # Calc phidt
            dirichlet_1 = project(-9.81*zeta_1)
            dirichlet_2 = project(-9.81*(zeta_2-beam.draft), V_beamline)

            neumann_2 = Function(V_beamline);
            neumann_2.interpolate(project((1+Wn1x**2)**.5*Wddtn1))
            #dirichlet_3 = project((cphi_3_new-cphi_3_m1)/dt)
            dirichlet_3 = project(-9.81*zeta_3)
            dn_c = 0.#9.81*dt*dt

            if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
                av = bdy_average(dirichlet_2, beam_left_index_b,
                                 dirichlet_1, top_right_index_t, left)
            if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
                av = bdy_average(dirichlet_2, beam_right_index_b,
                                 dirichlet_3, top_left_index_t, right)

            #dn_c = 0.
            timing.click("solve_poisson")
            phidt_1, phidt_2, phidt_3, phidt_la12, phidt_la23 = \
                solver.solve_poisson(h, source, res,
                zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
                beam.zetan1x, dirichlet_2, neumann_2, beam_to_bmesh2, bmesh2_to_mesh2,
                zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
                dn_c=dn_c)
            timing.clock()

            #zdt_1 = project(NormalDerivative(phi_1, zeta_1,
            #    topline_cell_to_mesh1_cell, topline_cell_to_mesh1_line,0)*\
            #    (1+zetax_1*zetax_1)**0.5,
            #    V_topline_1)
            zdt_1.interpolate(NormalDerivative(phi_1, (zeta_1, zetax_1),
                topline_cell_to_mesh1_cell, topline_cell_to_mesh1_line,0))
            phinonz_file << project(grad(phi_1)[1])
            #if t < DOLFIN_EPS :
            #    zdt_1_m1.interpolate(zdt_1)
            #    zdt_1_m2.interpolate(zdt_1)
            #    zdt_1_m3.interpolate(zdt_1)
            #print zdt_1.vector().array()
            #phinonz_file << project(grad(phi_1))
    
            # Update FS
            timing.click("update fs")
            update_free_surface_b(phi_1,
                cphi_1_new, cphi_1_m1, cphi_1,
                zeta_1, zeta_1_old,
                bmesh1_to_mesh1, top_to_bmesh1,
                zeta_1_m1, zdt_1, zdt_1_m1, zdt_1_m2, zdt_1_m3)
    
            zetax_1 = project(zeta_1.dx(0))
            zetaxx_1 = project(zetax_1.dx(0))
    
            #print zeta_2.vector().array()
            zdt_2 = project(NormalDerivative(phi_2, (zeta_2, zetax_2),
                beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line, beam.draft)*\
                (1+zetax_2*zetax_2)**0.5,
                V_beamline)
            update_free_surface_b(phi_2,
                cphi_2_new, cphi_2_m1, cphi_2,
                zeta_2, zeta_2_old,
                bmesh2_to_mesh2, beam_to_bmesh2,
                zeta_2_m1, zdt_2, zdt_2_m1, zdt_2_m2, zdt_2_m3)
            fszdt_2 = project(NormalDerivative(fsphi_2, (zeta_2_old, zetax_2),
                beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line,
                beam.draft)*\
                (1+zetax_2*zetax_2)**0.5,
                V_beamline)
            fszeta_2 = project(zeta_2_m1 + (dt/24.)*(9*fszdt_2 + 19*zdt_2_m1 - 5*zdt_2_m2 + zdt_2_m3))
            #print '8'*3
            #print zeta_2.vector().array()
    
            #print beam.zetan1.vector().array()
            #if do_init :
            #    cphi_2.interpolate(cphi_2_new)

            #RMV - surely unnecessary?
            if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
                bdy_average(cphi_2_new, beam_left_index_b,
                                 cphi_1_new, top_right_index_t, left)
                bdy_average(cphi_2, beam_left_index_b,
                                 cphi_1, top_right_index_t, left)
                bdy_average(cphi_2_m1, beam_left_index_b,
                                 cphi_1_m1, top_right_index_t, left)
            if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
                bdy_average(cphi_2_new, beam_right_index_b,
                                 cphi_3_new, top_left_index_t, right)
                bdy_average(cphi_2, beam_left_index_b,
                                 cphi_3, top_right_index_t, right)
                bdy_average(cphi_2_m1, beam_right_index_b,
                                 cphi_3_m1, top_left_index_t, right)

            new_zeta = W_to_zeta(beam.p, beam.Wn1, zeta_2,
                zeta_2_old, free_surface_under_beam)
            zeta_2.interpolate(project(new_zeta, V_beamline))

            #zeta_2_m1.interpolate()
            #beam.zetan1.interpolate(new_zeta)
            #beam.zetan.interpolate(old_zeta)
            beam.zetan1x.interpolate(project(beam.zetan1.dx(0)))
            # NOT QUITE CORRECT RMV
            beam.zetan1xx.interpolate(\
                project(Function(beam.Wn1.function_space(),
                    beam.Wn1).dx(0).dx(0)))
            #print cphi_2.vector().array()
    
            zdt_3.interpolate(NormalDerivative(phi_3, (zeta_3, zetax_3),
                topline_cell_to_mesh3_cell, topline_cell_to_mesh3_line,0))
            update_free_surface_b(phi_3,
                cphi_3_new, cphi_3_m1, cphi_3,
                zeta_3, zeta_3_old,
                bmesh3_to_mesh3, top_to_bmesh3,
                zeta_3_m1, zdt_3, zdt_3_m1, zdt_3_m2, zdt_3_m3)
    
            zetax_3 = project(zeta_3.dx(0))
            zetaxx_3 = project(zetax_3.dx(0))
            timing.clock()
    
            #cphidt_new = Function(V_beamline)
            #n_top_points = len(cphidt_new.function_space().mesh().coordinates())
            #cphidt_a = zeros(n_top_points)
            #arr = array(map(lambda i :
            #    bmesh2_to_mesh2[beam_to_bmesh2[i]], range(0, n_top_points)), dtype="I")
            #print arr
            #phidt_2.vector().get_local(cphidt_a, arr)
            #print arr
            #cphidt_new.vector().set_local(cphidt_a)
            #print arr
            #asdf = project(zeta_2-beam.draft, V_beamline)
            #v= cphidt_new.vector()+9.81*asdf.vector()
            #print v.array()
            #for i in range(0, n_top_points) :
            #    print (mesh2.coordinates()[arr[i]][0],asdf.vector()[i]- mesh2.coordinates()[arr[i]][1])
            #xv = zeros(1)
            #v = zeros(1)
            #xv[0] = 10.
            #asdf.eval(v, xv)
            #print (bmesh2.coordinates()[beam_left_index],v[0],gamma_top_1)
            if log_now :
                # Output phidt
                file_phidt1 << phidt_1
                file_phidt2 << phidt_2
                file_phidt3 << phidt_3
                file_phidt_la12 << phidt_la12
                file_phidt_la23 << phidt_la23

            # Beam response
            pressuref = - beam.rhow * phidt_2
            pf = project(pressuref, phidt_2.function_space())
    
            timing.click("beam motion")
            pfl = Function(V_beamline)
            pfl.assign(Constant(0.))
            #vm = beam_mesh.data().mesh_function("parent_vertex_indices")
            for i in range(0, beam_to_bmesh2.size()) :
                 pfl.vector()[i] = pf.vector()[bmesh2.vertex_map()[beam_to_bmesh2[i]]]
            beam.p.interpolate(pfl)
            beam.step(new_zeta)
            free_surface_under_beam = free_surface_under_beam_cl(beam, fszeta_2, not_first)
            constraint_2.set_all(2)
            free_surface_under_beam.mark(constraint_2, 3)
            #beam.zetan.interpolate(old_zeta)
            #fn2 = Function(V_beamline)
            #fn2.interpolate(beam.p)
            ##asdf1 << fn2
            #f2 = fn2.vector()
            #zv = project(zeta_2, V_beamline)
            #z2 = zv.vector()
            #z2 *= 9.81*beam.rhow
            ##asdf1 << zv
            #f2 -= zv.vector()
            ##asdf1 << fn2
    
            #print beam.zetan1.vector().array()
            if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
                #print new_zeta.vector()[beam_left_index_b]
                #print zeta_1.vector()[top_right_index_t]
                #print beam_V_left_index_b
                #print coords[beam_V_left_index_b]
                #xv = zeros(1)
                #pv = zeros(1)
                #xv[0] = 10.

                #beam.zetan1.eval(pv, xv)
                #print pv[0]-beam.draft
                #zeta_1.eval(pv, xv)
                #print pv[0]
                av =bdy_average(zeta_2, beam_left_index_b,
                                 zeta_1, top_right_index_t, left, beam.draft)
                #zeta_2.eval(pv, xv)
                #print pv[0]-beam.draft
                #zeta_1.eval(pv, xv)
                #print pv[0]
                #zeta_2_m1.eval(pv, xv)
                #print pv[0]-beam.draft
                #zeta_1_m1.eval(pv, xv)
                #print pv[0]
                av =bdy_average(zeta_2_m1, beam_left_index_b,
                                 zeta_1_m1, top_right_index_t, left, beam.draft)
                av =bdy_average(zdt_2, beam_left_index_b,
                                zdt_1, top_right_index_t, left, beam.draft)
                av =bdy_average(zdt_2_m1, beam_left_index_b,
                                zdt_1_m1, top_right_index_t, left, beam.draft)
                av =bdy_average(zdt_2_m2, beam_left_index_b,
                                zdt_1_m2, top_right_index_t, left, beam.draft)
                av =bdy_average(zdt_2_m3, beam_left_index_b,
                                zdt_1_m3, top_right_index_t, left, beam.draft)
                av =bdy_average(zdt_2_m4, beam_left_index_b,
                                zdt_1_m4, top_right_index_t, left, beam.draft)
                #zeta_2_m1.eval(pv, xv)
                #print pv[0]-beam.draft
                #zeta_1_m1.eval(pv, xv)
                #print pv[0]
                #av =bdy_average(beam.zetan1_old, beam_V_left_index_b,
                #                 zeta_1_old, top_right_index_t, left, beam.draft)
            if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
                av = bdy_average(zeta_2, beam_right_index_b,
                                 zeta_3, top_left_index_t, right, beam.draft)

                av = bdy_average(zeta_2_m1, beam_right_index_b,
                                 zeta_3_m1, top_left_index_t, right, beam.draft)
                av = bdy_average(zdt_2, beam_right_index_b,
                                 zdt_3, top_left_index_t, right, beam.draft)
                av = bdy_average(zdt_2_m1, beam_right_index_b,
                                 zdt_3_m1, top_left_index_t, right, beam.draft)
                av = bdy_average(zdt_2_m2, beam_right_index_b,
                                 zdt_3_m2, top_left_index_t, right, beam.draft)
                av = bdy_average(zdt_2_m3, beam_right_index_b,
                                 zdt_3_m3, top_left_index_t, right, beam.draft)
                av = bdy_average(zdt_2_m4, beam_right_index_b,
                                 zdt_3_m4, top_left_index_t, right, beam.draft)
                #av = bdy_average(beam.zetan1_old, beam_V_right_index_b,
                #                 zeta_3_old, top_left_index_t, right, beam.draft)
            #print new_zeta.vector().array()
    
            chg_old = chg
            chg = (beam.Wddtn1.vector() - beam.Wddtkm1.vector()).norm("l2")
            if not silent and ci % 100 == 0 and ci > 0:
                print chg
            #chg += (beam.zetaddtn1.vector() - beam.zetaddtn1.vector()).norm("l2")

            beam.adjacc(alpha)
            timing.clock()

            if log_now :
                # Output beam
                mk2beam.write_function(beam.p, "W_p")
                #p_real = beam.p-beam.rhow*9.81*(beam.zetan1-initial_draft)
                #mk2beam.write_function(p_real, "W_p_real")
                mk2beam.write_function(beam.Wn1, "W_Wn1")
                mk2beam.write_function(beam.zetan1, "W_zetan1")
                mk2beam.write_function(zeta_2, "W_zetan1_lin")
                mk2beam.write_function(zeta_2_m1, "W_zetan_lin")
                mk2beam.write_function(beam.Wddtn1, "W_Wddtn1")
                mk2beam.write_function(beam.zetaddtn1, "W_zetaddtn1")
    
            straighten(skew_1, deskew_1, mesh1, bmesh1,
                       mesh2, bmesh2,
                       skew_3, deskew_3, mesh3, bmesh3)

            #logfile.write('.')
            #logfile.flush()

            #cvgce = math.floor(math.log10(chg))
            #if cvgce < cvgce_old :
            #    logfile.write('|')
            #    logfile.flush()
            #elif cvgce > cvgce_old :
            #    logfile.write('!')
            #    logfile.flush()
            #cvgce_old = cvgce
    
            if ci > ci_lim :
                logfile.write("Correction limit reached (%d)\n" % ci_lim)

            if (chg < 2.e0 and ci > ci_min) or ci > ci_lim :
                if final_loop :
                    log_now = output_ci
                    final_loop = False
                    keep_correcting = False
                else :
                    final_loop = True
                    log_now = (tn % frame_frequency == 0)

            quit_for_timing = (stop_after is not None and not keep_correcting \
                 and tn == stop_after)
            if not no_save and (log_now or quit_for_timing) :
                save_functions(V_topline_1, 1, cphi_1, cphi_1_new, cphi_1_m1,
                       zeta_1, zeta_1_m1, zeta_1_old, zetax_1,
                       zetaxx_1, cphidt_1, zdt_1, dirichlet_1)
                beam.save_function_vectors()
                save_functions(V_topline_3, 3, cphi_3, cphi_3_new, cphi_3_m1,
                       zeta_3, zeta_3_m1, zeta_3_old, zetax_3,
                       zetaxx_3, cphidt_3, zetadt_3, dirichlet_3)
                save_state(t, tn, ci)

            if not silent : sys.stdout.write('\b \b'*len(str(ci)))
            ci += 1
            if not silent : sys.stdout.write('.'+str(ci))
            sys.stdout.flush()

            not_first = True

        if not silent :
            for i in range(0, ci+1) :
                sys.stdout.write('\b \b')
        sys.stdout.flush()
        ci_last = ci
        ci = 0

        # Timestep
        beam.timestep()
        cphi_1_m1.interpolate(cphi_1_new)
        cphi_2_m1.interpolate(cphi_2_new)
        cphi_3_m1.interpolate(cphi_3_new)
        zeta_1_p1 = project(2*zeta_1-zeta_1_m1)
        zeta_2_p1 = project(2*zeta_2-zeta_2_m1)
        zeta_3_p1 = project(2*zeta_3-zeta_3_m1)
        zeta_1_m1.interpolate(zeta_1)
        zeta_2_m1.interpolate(zeta_2)
        zeta_3_m1.interpolate(zeta_3)
        zeta_1.interpolate(zeta_1_p1)
        zeta_2.interpolate(zeta_2_p1)
        zeta_3.interpolate(zeta_3_p1)

        zdt_1_m4.interpolate(zdt_1_m3)
        zdt_1_m3.interpolate(zdt_1_m2)
        zdt_1_m2.interpolate(zdt_1_m1)
        zdt_1_m1.interpolate(zdt_1)
        cpdt_1_m4.interpolate(cpdt_1_m3)
        cpdt_1_m3.interpolate(cpdt_1_m2)
        cpdt_1_m2.interpolate(cpdt_1_m1)
        cpdt_1_m1.interpolate(cpdt_1)
        zdt_2_m4.interpolate(zdt_2_m3)
        zdt_2_m3.interpolate(zdt_2_m2)
        zdt_2_m2.interpolate(zdt_2_m1)
        zdt_2_m1.interpolate(zdt_2)
        cpdt_2_m4.interpolate(cpdt_2_m3)
        cpdt_2_m3.interpolate(cpdt_2_m2)
        cpdt_2_m2.interpolate(cpdt_2_m1)
        cpdt_2_m1.interpolate(cpdt_2)
    
        #free_surface_under_beam = free_surface_under_beam_cl(beam, not_first)
                
        # Logging
        logfile.write(\
            "Time: %lf (%d) #CI:%d Ddisp:%lf Ddisp(k-1):%lf [Frame %d]\n" \
                % (t, tn, ci_last, chg, chg_old, fn-1))
    
        t += dt
        tn += 1
        logfile.flush()
    
    beam.shutdown_diagnostics()
    
    logfile.write("Done\n")
    if not silent :
        print "Done"
    
    timing_file = open("timing.out", "w")
    timing_file.write(timing.print_stats())
    timing_file.close()
