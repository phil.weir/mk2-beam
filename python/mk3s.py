import solve as solve_mod
import shutil
import math
import time
from subprocess import Popen
import mk2beam
import sys
import os
import argparse
from numpy import *
from dolfin import *
from multiprocessing import Process, Queue
from routines import skew, bdy_average, straighten, NormalDerivative,\
    NormalDerivative2,linearize, FSLineFromMesh,\
    free_surface_under_beam_cl, free_surface_under_beam_zero_cl, W_to_zeta,\
    fs_beam, update_free_surface_b, update_free_surface, do_skew, flatten,\
    mesh_move, generate_mfs, update_boundary_mesh, \
    ic_cl, make_functions, load_functions, save_functions, \
    backup_functions, load_state, save_state, get_args, setup_dir,\
    get_params, open_files, make_mesh, set_cpdt,\
    near_1_top_corners_cl, near_beam_corners_cl, near_3_top_corners_cl,\
    refine_mesh, top_cl, get_top_indices, beam_cl, inner_boundary_cl,\
    create_boundary, get_beam_desc, get_dirmost, make_1Ds, \
    predict, correct

import routines
import timing

res = 1e-12

not_first = True

cvgce_old = 0
timing = timing.Timing()
routines.timing = timing

# Obtain parameters
arg_str, stop_after, resume, no_save, runname, output_ci, silent, frame_frequency = get_args()
    
builddir = "/maybehome/pweir/Work/mk2-matrix/build"

setup_dir(runname, builddir)

logfile = open("mk2." + runname + ".log", "w")
logfile.write(arg_str+"\n")

if not silent :
    print "MK3 - hydroelasticity simulation"
    print "="*40

source = Expression("0")

skew_cutoff = 1.e0
skew_vert_cutoff = .001

T, dt, depth, initial_draft, domain_left, left, right, domain_right,\
refinement_level, vertical_cells, horiz_cell_density, rat, rhow, ic_left,\
ic_width, ic_amp, ic_offset, ic_vert_offset, ic_symmetric, alpha, E, beam_type,\
ci_min, ci_lim, mu = get_params(runname, logfile)

# DOLFIN settings
set_log_level(100)

# Open output files
file_phi1, file_phi2, file_phi3,\
file_phidt1, file_phidt2, file_phidt3,\
file_phidt_la12, file_phidt_la23,\
file_mesh1, file_mesh2, file_mesh3,\
phinonz_file, phinonz2_file = open_files(runname)

# Create mesh
n = horiz_cell_density

try :
    mesh1 = make_mesh(1, rat)
    mesh2 = make_mesh(2, rat)
    mesh3 = make_mesh(3, rat)
except e :
    print str(e)
    quit(6)

file_mesh1 << mesh1
file_mesh2 << mesh2
file_mesh3 << mesh3
h = 1./n

# Define intersection top variable
gamma_top_1 = -initial_draft
gamma_top_3 = -initial_draft

# Define crucial points
ulcrnr1 = Point(domain_left, 0)
urcrnr1 = Point(left, 0)
llcrnr2 = Point(left, gamma_top_1)
lrcrnr2 = Point(right, gamma_top_3)
ulcrnr3 = Point(right, 0)
urcrnr3 = Point(domain_right, 0)

for i in range(0, refinement_level) :
    mesh1 = refine(mesh1)
    mesh2 = refine(mesh2)
    mesh3 = refine(mesh3)

file_mesh1 << mesh1
file_mesh2 << mesh2
file_mesh3 << mesh3

topsd = top_cl()
top_indices_1 = get_top_indices(mesh1)
top_indices_2 = get_top_indices(mesh2, initial_draft)
top_indices_3 = get_top_indices(mesh3)
beamsd = beam_cl(initial_draft)

bmesh1, num_top_points_1, bmesh1_to_mesh1, top_to_bmesh1t, bm1conn, top_1 =  \
    create_boundary(mesh1, topsd)

bmesh2, num_beam_points, bmesh2_to_mesh2, beam_to_bmesh2t, bm2conn, beam_mesh = \
    create_boundary(mesh2, beamsd)

bmesh3, num_top_points_3, bmesh3_to_mesh3, top_to_bmesh3t, bm3conn, top_3 = \
    create_boundary(mesh3, topsd)

topline_1, top_to_bmesh1, V_topline_1 = flatten(top_1, top_to_bmesh1t)
beamline, beam_to_bmesh2, V_beamline = flatten(beam_mesh, beam_to_bmesh2t)
topline_3, top_to_bmesh3, V_topline_3 = flatten(top_3, top_to_bmesh3t)

topline_cell_to_mesh1_cell, topline_cell_to_mesh1_line = generate_mfs(
    bmesh1, topline_1, mesh1, top_to_bmesh1)
        
beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line = generate_mfs(
    bmesh2, beamline, mesh2, beam_to_bmesh2)

topline_cell_to_mesh3_cell, topline_cell_to_mesh3_line = generate_mfs(
    bmesh3, topline_3, mesh3, top_to_bmesh3)

# Build beam base
beam_desc = get_beam_desc(left, right, initial_draft, E, rhow)
beamer = mk2beam.Plate.get_platermap()
beamer = mk2beam.Plate.get_plater(beam_type)
beam = fs_beam(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True,
    zeta_on=True)
solve_mod.beam = beam

# Identify indices of the right end of the free surface and left end of the beam
# with respect to the boundary meshes
top_right_index_t = get_dirmost(1, topline_1)
top_right_index = top_to_bmesh1[top_right_index_t]

beam_left_index_b = get_dirmost(-1, beamline)
beam_left_index = beam_to_bmesh2[beam_left_index_b]
beam_right_index_b = get_dirmost(1, beamline)
beam_right_index = beam_to_bmesh2[beam_right_index_b]

top_left_index_t = get_dirmost(-1, topline_3)
top_left_index = top_to_bmesh3[top_left_index_t]

coords =beam.zetan1.function_space().mesh().coordinates()
beam_V_left_index_b = (sorted(range(0, len(coords)),
    lambda c,d : cmp(coords[c][0],coords[d][0])))[0]
beam_V_right_index_b = (sorted(range(0, len(coords)),
    lambda c,d : cmp(coords[d][0],coords[c][0])))[0]

ice = ic_cl({'symmetric':ic_symmetric,
             'left':ic_left,
             'amp':ic_amp,
             'offset':ic_offset,
             'vert_offset':ic_vert_offset,
             'width':ic_width},
             left,right)

if resume :
    # Try and reduce likelihood of us overwriting the copy we want
    if not no_save :
        n = 0
        for f in os.listdir('.') :
            if f.startswith('state.st') :
                n += 1

        shutil.copyfile('state.st', 'state.st.'+str(n))
        backup_functions(1, n)
        beam.backup_function_vectors(str(n))
        backup_functions(3, n)

    cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
        zetax_1, zetaxx_1, cphidt_1, zdt_1,\
        dirichlet_1 =\
            load_functions(V_topline_1, 1)


    cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
        zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
        dirichlet_2 =\
            make_functions(V_beamline, ice)
    #cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
    #    zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
    #    dirichlet_2 =\
    #        load_functions(V_beamline, 2)

    beam.load_function_vectors()

    cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
        zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
        dirichlet_3 =\
            load_functions(V_topline_3, 3)
else :
    cphi_1, cphi_1_new, cphi_1_m1, zeta_1, zeta_1_m1, zeta_1_old,\
        zetax_1, zetaxx_1, cphidt_1, zdt_1,\
        dirichlet_1 =\
            make_functions(V_topline_1, ice)

    cphi_2, cphi_2_new, cphi_2_m1, zeta_2, zeta_2_m1, zeta_2_old,\
        zetax_2, zetaxx_2, cphidt_2, zetadt_2,\
        dirichlet_2 =\
            make_functions(V_beamline, ice)

    cphi_3, cphi_3_new, cphi_3_m1, zeta_3, zeta_3_m1, zeta_3_old,\
        zetax_3, zetaxx_3, cphidt_3, zetadt_3,\
        dirichlet_3 =\
            make_functions(V_topline_3, ice)
#print zeta_1.vector()[top_right_index_t]
zeta_2.interpolate(beam.zetan1)
zeta_2_m1.interpolate(beam.zetan)
zeta_2_old.interpolate(beam.zetan1_old)

zdt_1, zdt_1_m1, zdt_1_m2, zdt_1_m3, zdt_1_m4,\
cpdt_1, cpdt_1_m1, cpdt_1_m2, cpdt_1_m3, cpdt_1_m4 = make_1Ds(V_topline_1)
zdt_2, zdt_2_m1, zdt_2_m2, zdt_2_m3, zdt_2_m4,\
cpdt_2, cpdt_2_m1, cpdt_2_m2, cpdt_2_m3, cpdt_2_m4 = make_1Ds(V_beamline)
zdt_3, zdt_3_m1, zdt_3_m2, zdt_3_m3, zdt_3_m4,\
cpdt_3, cpdt_3_m1, cpdt_3_m2, cpdt_3_m3, cpdt_3_m4 = make_1Ds(V_topline_3)

beam.nonstep()
zeta_2.interpolate(beam.Wn1)
beam.setup_diagnostics("w")

neumann_2 = Function(V_beamline)
neumann_2.assign(Constant(0.))

# Move mesh to starting position
mesh_move(mesh1, Constant(0.), zeta_1_old, depth)
update_boundary_mesh(mesh1, bmesh1)
mesh_move(mesh2, Constant(0.), zeta_2_old, depth, initial_draft)
update_boundary_mesh(mesh2, bmesh2)
mesh_move(mesh3, Constant(0.), zeta_3_old, depth)
update_boundary_mesh(mesh3, bmesh3)

file_mesh1 << mesh1
file_mesh2 << mesh2
file_mesh3 << mesh3

log_now = output_ci
final_loop = False
quit_for_timing = False

chg = 0.
chg_old = 0.

# Time loop
if resume :
    t, tn, ci = load_state()
    logfile.write(\
        "Resuming from t = %lf (%d); correction # %d\n" % (t, tn, ci))
else :
    t = 0
    tn = 0
    ci = 0
fn = 0
start = t
new_zeta = Function(V_beamline)
old_zeta = Function(V_beamline)

beam.zetan.interpolate(beam.zetan1)
free_surface_under_beam = free_surface_under_beam_cl(beam, None, not_first)
free_surface_under_beam_zero = free_surface_under_beam_zero_cl()
constraint_2 = MeshFunction("uint", mesh2, mesh2.topology().dim()-1)
constraint_2.set_all(2)
free_surface_under_beam.mark(constraint_2, 3)

while t < T and not quit_for_timing :

    beam.output_diagnostics(t)

    keep_correcting = True

    predict(dt, zeta_1, zetax_1, zetaxx_1, zeta_1_m1, zdt_1_m1, zdt_1_m2, zdt_1_m3, zdt_1_m4)
    set_cpdt(cpdt_1, zeta_1)
    correct(dt, cphi_1_new, cphi_1_m1, cpdt_1, cpdt_1_m1, cpdt_1_m2, cpdt_1_m3)
    predict(dt, zeta_2, zetax_2, zetaxx_2, zeta_2_m1, zdt_2_m1, zdt_2_m2, zdt_2_m3, zdt_2_m4)
    zeta_2.interpolate(beam.Wn1)
    set_cpdt(cpdt_2, zeta_2, beam.draft)
    correct(dt, cphi_2_new, cphi_2_m1, cpdt_2, cpdt_2_m1, cpdt_2_m2, cpdt_2_m3)
    predict(dt, zeta_3, zetax_3, zetaxx_3, zeta_3_m1, zdt_3_m1, zdt_3_m2, zdt_3_m3, zdt_3_m4)
    set_cpdt(cpdt_3, zeta_3)
    correct(dt, cphi_3_new, cphi_3_m1, cpdt_3, cpdt_3_m1, cpdt_3_m2, cpdt_3_m3)

    #print '-'*10
    while keep_correcting and not quit_for_timing :

        if log_now :
            fn += 1

        top_right_height = bmesh1.coordinates()[top_right_index][1]
        beam_left_height = bmesh2.coordinates()[beam_left_index][1]
        beam_right_height = bmesh2.coordinates()[beam_right_index][1]
        top_left_height = bmesh3.coordinates()[top_left_index][1]
        if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            av =bdy_average(zeta_2, beam_left_index_b,
                             zeta_1, top_right_index_t, left, beam.draft,
                             do=True)
            bdy_average(zeta_2_m1, beam_left_index_b,
                             zeta_1_m1, top_right_index_t, left, beam.draft)
            mesh1.coordinates()[bmesh1_to_mesh1[top_right_index]][1] = av
            mesh2.coordinates()[bmesh2_to_mesh2[beam_left_index]][1] = av
            beam_left_height = av
            top_right_height = av
            bdy_average(cphi_2, beam_left_index_b,
                             cphi_1, top_right_index_t, left)
            bdy_average(cphi_2_m1, beam_left_index_b,
                        cphi_1_m1, top_right_index_t, left)
            bdy_average(cpdt_2, beam_left_index_b,
                        cpdt_1, top_right_index_t, left)
            bdy_average(cpdt_2_m1, beam_left_index_b,
                        cpdt_1_m1, top_right_index_t, left)
            bdy_average(cpdt_2_m2, beam_left_index_b,
                        cpdt_1_m2, top_right_index_t, left)
            bdy_average(cpdt_2_m3, beam_left_index_b,
                        cpdt_1_m3, top_right_index_t, left)
            bdy_average(cpdt_2_m4, beam_left_index_b,
                        cpdt_1_m4, top_right_index_t, left)
        if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            av = bdy_average(zeta_2, beam_right_index_b,
                             zeta_3, top_left_index_t, right, beam.draft,
                             do=True)
            bdy_average(zeta_2_m1, beam_right_index_b,
                        zeta_3_m1, top_left_index_t, right, beam.draft)
            #bdy_average(zeta_2_m2, beam_right_index_b,
            #            zeta_3_m2, top_left_index_t, right, beam.draft)
            #bdy_average(zeta_2_m3, beam_right_index_b,
            #            zeta_3_m3, top_left_index_t, right, beam.draft)
            #bdy_average(zeta_2_m4, beam_right_index_b,
            #            zeta_3_m4, top_left_index_t, right, beam.draft)
            mesh3.coordinates()[bmesh3_to_mesh3[top_left_index]][1] = av
            mesh2.coordinates()[bmesh2_to_mesh2[beam_right_index]][1] = av
            bdy_average(cphi_2, beam_left_index_b,
                        cphi_3, top_right_index_t, right)
            bdy_average(cphi_2, beam_right_index_b,
                        cphi_3, top_left_index_t, right)
            bdy_average(cpdt_2, beam_right_index_b,
                        cpdt_3, top_left_index_t, right)
            bdy_average(cpdt_2_m1, beam_right_index_b,
                        cpdt_3_m1, top_left_index_t, right)
            bdy_average(cpdt_2_m2, beam_right_index_b,
                        cpdt_3_m2, top_left_index_t, right)
            bdy_average(cpdt_2_m3, beam_right_index_b,
                        cpdt_3_m3, top_left_index_t, right)
            bdy_average(cpdt_2_m4, beam_right_index_b,
                        cpdt_3_m4, top_left_index_t, right)
            beam_right_height = av
            top_left_height = av


        skew_1, deskew_1, gamma_top_1, skew_3, deskew_3, gamma_top_3 = \
            skew(mesh1, bmesh1, zeta_1_old, zeta_1,
                 mesh2, bmesh2, zeta_2_old, zeta_2,
                 mesh3, bmesh3, zeta_3_old, zeta_3,
                 depth, beam, log_now,
                 top_right_index, beam_left_index, beam_right_index, top_left_index,
                 skew_cutoff, skew_vert_cutoff, left, right,
                 file_mesh1, file_mesh2, file_mesh3,
                 beam_left_index_b, beam_right_index_b,
                 top_left_index_t, top_right_index_t
                )
        phinonz_file << zeta_2

        top_right_height = bmesh1.coordinates()[top_right_index][1]
        beam_left_height = bmesh2.coordinates()[beam_left_index][1]
        beam_right_height = bmesh2.coordinates()[beam_right_index][1]
        top_left_height = bmesh3.coordinates()[top_left_index][1]

        if log_now :
            file_mesh1 << mesh1
            file_mesh2 << mesh2
            file_mesh3 << mesh3

        # Build solver
        inner_boundary_1 = inner_boundary_cl(left, gamma_top_1)
        inner_boundary_3 = inner_boundary_cl(right, gamma_top_3)
        timing.click("solver init")
        solver = solve_mod.Solver(mesh1, mesh2, mesh3,
            inner_boundary_1, top_indices_1,
            top_indices_2, constraint_2,
            inner_boundary_3, top_indices_3,
            timing)
        timing.clock()

        dirichlet_1.interpolate(cphi_1_new)

        dirichlet_2.interpolate(cphi_2_new)
        Wn1x = Dx(Function(beam.Wn1.function_space(), beam.Wn1), 0)
        Wdtn1 = Function(beam.Wdtn1.function_space(), beam.Wdtn1)
        Wddtn1 = Function(beam.Wddtn1.function_space(), beam.Wddtn1)
        neumann_2.interpolate(beam.Wdtn1)

        dirichlet_3.interpolate(cphi_3_new)

        #  join edges
        if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            av = bdy_average(dirichlet_2, beam_left_index_b,
                             dirichlet_1, top_right_index_t, left)
        if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            av = bdy_average(dirichlet_2, beam_right_index_b,
                             dirichlet_3, top_left_index_t, right)

        #  solve for phi
        timing.click("solve_poisson")
        phi_1, phi_2, phi_3, phi_la12, phi_la23 = solver.solve_poisson(h,
            source, res,
            zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
            beam.zetan1x, dirichlet_2, beam.Wdtn1, beam_to_bmesh2, bmesh2_to_mesh2,
            zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
            dn_c=0.)
        phinonz2_file << project(grad(phi_1))
        timing.clock()

        #RUNAGAIN FOR FS COMP
        solver = solve_mod.Solver(mesh1, mesh2, mesh3,
            inner_boundary_1, top_indices_1,
            top_indices_2, None,
            inner_boundary_3, top_indices_3,
            timing)
        fsphi_1, fsphi_2, fsphi_3, fsphi_la12, fsphi_la23 = solver.solve_poisson(h,
            source, res,
            zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
            beam.zetan1x, dirichlet_2, None, beam_to_bmesh2, bmesh2_to_mesh2,
            zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
            dn_c=0.)

        if log_now :
            # Output phi
            file_phi1 << phi_1
            file_phi2 << phi_2
            file_phi3 << phi_3

        # Calc phidt

        #  fs left
        set_cpdt(cpdt_1, zeta_1)
        dirichlet_1.interpolate(cpdt_1) #project(-9.81*zeta_1, V_topline_1)

        #  beam
        set_cpdt(cpdt_2, zeta_2, beam.draft)
        dirichlet_2.interpolate(cpdt_2) #project(-9.81*(zeta_2-beam.draft), V_beamline)
        neumann_2 = Function(V_beamline)
        neumann_2.interpolate(beam.Wddtn1)
        #neumann_2.interpolate(project((1+Wn1x**2)**.5*Wddtn1))

        #  fs right
        set_cpdt(cpdt_3, zeta_3)
        dirichlet_3.interpolate(cpdt_3) #project(-9.81*zeta_3, V_topline_3)

        #  join edges
        if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            av = bdy_average(dirichlet_2, beam_left_index_b,
                             dirichlet_1, top_right_index_t, left)
        if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            av = bdy_average(dirichlet_2, beam_right_index_b,
                             dirichlet_3, top_left_index_t, right)

        solver = solve_mod.Solver(mesh1, mesh2, mesh3,
            inner_boundary_1, top_indices_1,
            top_indices_2, constraint_2,
            inner_boundary_3, top_indices_3,
            timing)
        phinonz_file << beam.Wddtn1
        timing.click("solve_poisson")
        phidt_1, phidt_2, phidt_3, phidt_la12, phidt_la23 = \
            solver.solve_poisson(h, source, res,
            zetax_1, dirichlet_1, top_to_bmesh1, bmesh1_to_mesh1,
            beam.zetan1x, dirichlet_2, beam.Wddtn1, beam_to_bmesh2, bmesh2_to_mesh2,
            zetax_3, dirichlet_3, top_to_bmesh3, bmesh3_to_mesh3,
            dn_c=0.)
        phinonz_file << phidt_2
        timing.clock()

        # Beam response
        pressuref = - beam.rhow * phidt_2
        pf = project(pressuref, phidt_2.function_space())
        timing.click("beam motion")
        pfl = Function(V_beamline)
        pfl.assign(Constant(0.))
        for i in range(0, beam_to_bmesh2.size()) :
             pfl.vector()[i] = pf.vector()[bmesh2.vertex_map()[beam_to_bmesh2[i]]]

        #beam.p.interpolate(pfl)
        beam.p.interpolate(FSLineFromMesh(phidt_2, zeta_2, beam.draft))
        for i in range(0, len(beam.p.vector())) :
            beam.p.vector()[i] *= -beam.rhow
        phinonz_file << beam.p
        beam.step(zeta_2)
        beam.adjacc(alpha)

        # Update FS

        #  fs left
        #zdt_1.interpolate(NormalDerivative(phi_1, (zeta_1, zetax_1),
        #    topline_cell_to_mesh1_cell, topline_cell_to_mesh1_line,0))
        #zdt_1.interpolate(NormalDerivative2(phi_1, zeta_1,
        #    cphi_1_new, beam, domain_right))

        timing.click("update fs")
        #zdt_1.interpolate(NormalDerivative2(phi_1, zeta_1,
        #    cphi_1_new, beam, domain_right))
        update_free_surface_b(phi_1,
            cphi_1_new, cphi_1_m1, cphi_1,
            zeta_1, zeta_1_old,
            bmesh1_to_mesh1, top_to_bmesh1,
            zeta_1_m1, zdt_1, zdt_1_m1, zdt_1_m2, zdt_1_m3, dt,
            cpdt_1, cpdt_1_m1, cpdt_1_m2, cpdt_1_m3,
            beam, domain_right, phinonz_file=phinonz_file)

        zetax_1 = project(zeta_1.dx(0), zetax_1.function_space())
        zetaxx_1 = project(zetax_1.dx(0), zetaxx_1.function_space())

        #  beam
        zdt_2.interpolate(project(NormalDerivative(phi_2, (zeta_2, zetax_2),
            beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line, beam.draft)*\
            (1+zetax_2*zetax_2)**0.5, V_beamline))
        #update_free_surface_b(phi_2,
        #    cphi_2_new, cphi_2_m1, cphi_2,
        #    zeta_2, zeta_2_old,
        #    bmesh2_to_mesh2, beam_to_bmesh2,
        #    zeta_2_m1, zdt_2, zdt_2_m1, zdt_2_m2, zdt_2_m3, dt,
        #    cpdt_2, cpdt_2_m1, cpdt_2_m2, cpdt_2_m3,
        #    beam, domain_right,
        #    beam.draft)
        fszdt_2 = project(NormalDerivative(fsphi_2, (zeta_2_old, zetax_2),
            beamline_cell_to_mesh2_cell, beamline_cell_to_mesh2_line,
            beam.draft)*\
            (1+zetax_2*zetax_2)**0.5,
            V_beamline)
        fszeta_2 = project(zeta_2_m1 + (dt/24.)*(9*fszdt_2 + 19*zdt_2_m1 - 5*zdt_2_m2 + zdt_2_m3), V_beamline)

        #RMV - surely unnecessary?
        if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            bdy_average(cphi_2_new, beam_left_index_b,
                             cphi_1_new, top_right_index_t, left)
            bdy_average(cphi_2, beam_left_index_b,
                             cphi_1, top_right_index_t, left)
            bdy_average(cphi_2_m1, beam_left_index_b,
                             cphi_1_m1, top_right_index_t, left)
        if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            bdy_average(cphi_2_new, beam_right_index_b,
                             cphi_3_new, top_left_index_t, right)
            bdy_average(cphi_2, beam_left_index_b,
                             cphi_3, top_right_index_t, right)
            bdy_average(cphi_2_m1, beam_right_index_b,
                             cphi_3_m1, top_left_index_t, right)

        # old beam.p...
        new_zeta.interpolate(W_to_zeta(beam.p, beam.Wn1, zeta_2,
            zeta_2_old, free_surface_under_beam))
        zeta_2.interpolate(new_zeta)#project(new_zeta, V_beamline))

        beam.zetan1x.interpolate(project(beam.zetan1.dx(0), beam.zetan1.function_space()))
        # NOT QUITE CORRECT RMV
        beam.zetan1xx.interpolate(\
            project(Function(beam.Wn1.function_space(),
                beam.Wn1).dx(0).dx(0), beam.Wn1.function_space()))

        #  fs right
        #zdt_3.interpolate(NormalDerivative2(phi_3, zeta_3,
        #    cphi_3_new, beam, domain_right))
        #zdt_3.interpolate(NormalDerivative(phi_3, (zeta_3, zetax_3),
        #    topline_cell_to_mesh3_cell, topline_cell_to_mesh3_line,0))
        update_free_surface_b(phi_3,
            cphi_3_new, cphi_3_m1, cphi_3,
            zeta_3, zeta_3_old,
            bmesh3_to_mesh3, top_to_bmesh3,
            zeta_3_m1, zdt_3, zdt_3_m1, zdt_3_m2, zdt_3_m3, dt,
            cpdt_3, cpdt_3_m1, cpdt_3_m2, cpdt_3_m3,
            beam, domain_right)

        zetax_3 = project(zeta_3.dx(0), zetax_3.function_space())
        zetaxx_3 = project(zetax_3.dx(0), zetaxx_3.function_space())
        timing.clock()

        if log_now :
            # Output phidt
            file_phidt1 << phidt_1
            file_phidt2 << phidt_2
            file_phidt3 << phidt_3
            file_phidt_la12 << phidt_la12
            file_phidt_la23 << phidt_la23

        free_surface_under_beam = free_surface_under_beam_cl(beam, fszeta_2, not_first)
        constraint_2.set_all(2)
        free_surface_under_beam.mark(constraint_2, 3)

        if abs(beam_left_height - top_right_height) < skew_vert_cutoff :
            av =bdy_average(zeta_2, beam_left_index_b,
                             zeta_1, top_right_index_t, left, beam.draft)
            av =bdy_average(zeta_2_m1, beam_left_index_b,
                             zeta_1_m1, top_right_index_t, left, beam.draft)
            av =bdy_average(zdt_2, beam_left_index_b,
                            zdt_1, top_right_index_t, left, beam.draft)
            av =bdy_average(zdt_2_m1, beam_left_index_b,
                            zdt_1_m1, top_right_index_t, left, beam.draft)
            av =bdy_average(zdt_2_m2, beam_left_index_b,
                            zdt_1_m2, top_right_index_t, left, beam.draft)
            av =bdy_average(zdt_2_m3, beam_left_index_b,
                            zdt_1_m3, top_right_index_t, left, beam.draft)
            av =bdy_average(zdt_2_m4, beam_left_index_b,
                            zdt_1_m4, top_right_index_t, left, beam.draft)
        if abs(beam_right_height - top_left_height) < skew_vert_cutoff :
            av = bdy_average(zeta_2, beam_right_index_b,
                             zeta_3, top_left_index_t, right, beam.draft)

            av = bdy_average(zeta_2_m1, beam_right_index_b,
                             zeta_3_m1, top_left_index_t, right, beam.draft)
            av = bdy_average(zdt_2, beam_right_index_b,
                             zdt_3, top_left_index_t, right, beam.draft)
            av = bdy_average(zdt_2_m1, beam_right_index_b,
                             zdt_3_m1, top_left_index_t, right, beam.draft)
            av = bdy_average(zdt_2_m2, beam_right_index_b,
                             zdt_3_m2, top_left_index_t, right, beam.draft)
            av = bdy_average(zdt_2_m3, beam_right_index_b,
                             zdt_3_m3, top_left_index_t, right, beam.draft)
            av = bdy_average(zdt_2_m4, beam_right_index_b,
                             zdt_3_m4, top_left_index_t, right, beam.draft)

        chg_old = chg
        chg = (beam.Wddtn1.vector() - beam.Wddtkm1.vector()).norm("l2")
        if not silent and ci % 100 == 0 and ci > 0:
            print chg

        timing.clock()

        #phinonz_file << zeta_2
        if log_now :
            # Output beam
            mk2beam.write_function(beam.p, "W_p")
            mk2beam.write_function(beam.Wn1, "W_Wn1")
            mk2beam.write_function(beam.zetan1, "W_zetan1")
            mk2beam.write_function(zeta_2, "W_zetan1_lin")
            mk2beam.write_function(zeta_2_m1, "W_zetan_lin")
            mk2beam.write_function(beam.Wddtn1, "W_Wddtn1")
            mk2beam.write_function(beam.zetaddtn1, "W_zetaddtn1")

        straighten(skew_1, deskew_1, mesh1, bmesh1,
                   mesh2, bmesh2,
                   skew_3, deskew_3, mesh3, bmesh3)

        if ci > ci_lim :
            logfile.write("Correction limit reached (%d)\n" % ci_lim)

        if (chg < 2.e0 and ci > ci_min) or ci > ci_lim :
            if final_loop :
                log_now = output_ci
                final_loop = False
                keep_correcting = False
            else :
                final_loop = True
                log_now = (tn % frame_frequency == 0)

        quit_for_timing = (stop_after is not None and not keep_correcting \
             and tn == stop_after)
        if not no_save and (log_now or quit_for_timing) :
            save_functions(V_topline_1, 1, cphi_1, cphi_1_new, cphi_1_m1,
                   zeta_1, zeta_1_m1, zeta_1_old, zetax_1,
                   zetaxx_1, cphidt_1, zdt_1, dirichlet_1)
            beam.save_function_vectors()
            save_functions(V_topline_3, 3, cphi_3, cphi_3_new, cphi_3_m1,
                   zeta_3, zeta_3_m1, zeta_3_old, zetax_3,
                   zetaxx_3, cphidt_3, zetadt_3, dirichlet_3)
            save_state(t, tn, ci)

        if not silent : sys.stdout.write('\b \b'*len(str(ci)))
        ci += 1
        if not silent : sys.stdout.write('.'+str(ci))
        sys.stdout.flush()

        not_first = True
        #phinonz_file << project(grad(phi_2))

    if not silent :
        for i in range(0, ci+1) :
            sys.stdout.write('\b \b')
    sys.stdout.flush()
    ci_last = ci
    ci = 0

    # Timestep
    beam.timestep()
    zeta_2.interpolate(beam.Wn1)
    cphi_1_m1.interpolate(cphi_1_new)
    cphi_2_m1.interpolate(cphi_2_new)
    cphi_3_m1.interpolate(cphi_3_new)
    zeta_1_m1.interpolate(zeta_1)
    zeta_2_m1.interpolate(zeta_2)
    zeta_3_m1.interpolate(zeta_3)

    zdt_1_m4.interpolate(zdt_1_m3)
    zdt_1_m3.interpolate(zdt_1_m2)
    zdt_1_m2.interpolate(zdt_1_m1)
    zdt_1_m1.interpolate(zdt_1)
    cpdt_1_m4.interpolate(cpdt_1_m3)
    cpdt_1_m3.interpolate(cpdt_1_m2)
    cpdt_1_m2.interpolate(cpdt_1_m1)
    cpdt_1_m1.interpolate(cpdt_1)
    zdt_2_m4.interpolate(zdt_2_m3)
    zdt_2_m3.interpolate(zdt_2_m2)
    zdt_2_m2.interpolate(zdt_2_m1)
    zdt_2_m1.interpolate(zdt_2)
    cpdt_2_m4.interpolate(cpdt_2_m3)
    cpdt_2_m3.interpolate(cpdt_2_m2)
    cpdt_2_m2.interpolate(cpdt_2_m1)
    cpdt_2_m1.interpolate(cpdt_2)
    zdt_3_m4.interpolate(zdt_3_m3)
    zdt_3_m3.interpolate(zdt_3_m2)
    zdt_3_m2.interpolate(zdt_3_m1)
    zdt_3_m1.interpolate(zdt_3)
    cpdt_3_m4.interpolate(cpdt_3_m3)
    cpdt_3_m3.interpolate(cpdt_3_m2)
    cpdt_3_m2.interpolate(cpdt_3_m1)
    cpdt_3_m1.interpolate(cpdt_3)

    #free_surface_under_beam = free_surface_under_beam_cl(beam, not_first)
            
    # Logging
    logfile.write(\
        "Time: %lf (%d) #CI:%d Ddisp:%lf Ddisp(k-1):%lf [Frame %d]\n" \
            % (t, tn, ci_last, chg, chg_old, fn-1))

    t += dt
    tn += 1
    logfile.flush()

beam.shutdown_diagnostics()

logfile.write("Done\n")
if not silent :
    print "Done"

timing_file = open("timing.out", "w")
timing_file.write(timing.print_stats())
timing_file.close()
