"""
Based on Anders Logg's LGPL Poisson demo.
"""

from dolfin import *
from block import *
from multiprocessing import Process, Queue
import mk2matrix
from block.iterative import MinRes, ConjGrad, LGMRES, Richardson
from numpy import matrix, array, uint, set_printoptions, identity, zeros
set_printoptions(linewidth=300, suppress=True, precision=4, threshold=2000)
from Schur import SchurComplement, DOLFINSolveMat

parameters["linear_algebra_backend"] = "PETSc"

al = 2e5

al_dbc = 2e5

import _cpp

beam = None

vA = zeros(1)
xA = zeros(1)
def eval_z(f, x) :
    xA[0] = x
    f.eval(vA, xA)
    return vA[0]

class MeshFromInterval(Expression) :
    def __init__(self, fn, under_plate=False) :
        Expression.__init__(self)
        self.fn = fn
        self.under_plate = under_plate
    def eval_cell(self, values, x, ufc_cell) :
        if not self.under_plate and (beam.left < x[0] and x[0] < beam.right) :
            xt = x[0]
            l = beam.left
            r = beam.right
            values[0] = ((xt-r)*eval_z(self.fn, l) + (l-xt)*eval_z(self.fn,r)) / (l-r)
        else :
            values[0] = eval_z(self.fn, x[0])

        if x[1] < -.5 :
            values[0] = 0.

def run_build_interior_problem(q, V, Vl, intersection, h, f, g, k, vm, bm, dn_c, timing) :
    A1, L1 = build_interior_problem(V, V1,
            intersection, h, f, g, k, vm, bm, dn_c, timing)
    q.put(A1, L1)

# Function to create a new PETScMatrix
def new_PETScMatrix(r, c) :
    umat = PETScMatrix()
    umat.resize(r, c)
    umat.zero()
    #rows = array(range(0, r), dtype="I")
    #cols = array(range(0, c), dtype="I")
    #umat.set(array(mat.tolist()), rows, cols)
    #umat.apply("insert")
    return umat

# Function to convert a numpy matrix to a uBLASDenseMatrix
def npm_to_uBLAS(mat) :
    umat = PETScMatrix()
    umat.resize(*mat.shape)
    rows = array(range(0, mat.shape[0]), dtype="I")
    cols = array(range(0, mat.shape[1]), dtype="I")
    umat.set(array(mat.tolist()), rows, cols)
    umat.apply("insert")
    return umat

# Function to convert an nx1 numpy matrix to a uBLASVector
def npm_to_uBLAS_vec(vec) :
    vec = array(vec)
    umat = uBLASVector(len(vec))
    umat.set_local(vec)
    return umat

# Define a lambda function to sort our vertex triples
vert_sort = lambda x, y : cmp(x[0][1], y[0][1])

# Function to create an inner boundary mesh from the vertices
# of the m/p boundary meshes
def build_gamma(v_m, v_p) :
    #print v_m, v_p
    # Combine the vertices from either side of Gamma and sort them vertically
    # These triples indicate which mesh each of the vertices originated in.
    v_int = v_m + v_p
    v_int.sort(vert_sort)
    
    # Remove duplicate points
    vold = None
    i = 0
    
    while (i < len(v_int)) :
        vnew = v_int[i]
        v = vnew[0]
        
        # If two consecutive points in the same place, remove the latter and note
        # duality in the former
        if vold is not None and sqrt((v[0]-vold[0][0])**2+(v[1]-vold[0][1])**2) < 1e-4:
            inds = [0,0]
            inds[vold[1]-1] = vold[2]
            inds[vnew[1]-1] = vnew[2]
            v_int[i-1] = (vold[0], 3, tuple(inds))
            del v_int[i]
            #elif vold is not None and sqrt((v[0]-vold[0][0])**2+(v[1]-vold[0][1])**2) < 1e-4 :
            #    print v
            #    print vold
            #    print sqrt((v[0]-vold[0][0])**2+(v[1]-vold[0][1])**2)
        else :
            i += 1
        vold = v_int[i-1]
    
    # Build gamma from the ordered list of points
    #print v_int
    gamma = Mesh()
    editor = MeshEditor()
    editor.open(gamma, "interval", 1, 2)
    editor.init_vertices(len(v_int))
    editor.init_cells(len(v_int)-1)
    for i in range(0, len(v_int)) :
        editor.add_vertex(i, *v_int[i][0])
        if i > 0 :
            editor.add_cell(i-1, i-1, i)
        i += 1
    editor.close()
    
    # Define a function space over gamma
    M = FunctionSpace(gamma, "Lagrange", 1)
    
    # Dictionaries for moving from +- gammas to gamma
    gm_to_g = {}
    gp_to_g = {}
    for i in range(0, len(v_int)) :
        if v_int[i][1] == 1 :
            gm_to_g[v_int[i][2]] = i
        if v_int[i][1] == 2 :
            gp_to_g[v_int[i][2]] = i
        if v_int[i][1] == 3 :
            gm_to_g[v_int[i][2][0]] = i
            gp_to_g[v_int[i][2][1]] = i
    
    # Build a horizontal gamma (rot -pi/2) to allow
    # us to build forms
    gamma_flat = Mesh()
    editor_flat = MeshEditor()
    editor_flat.open(gamma_flat, "interval", 1, 1)
    editor_flat.init_vertices(len(gamma.coordinates()))
    editor_flat.init_cells(len(gamma.coordinates())-1)
    for i in range(0, len(v_int)) :
        editor_flat.add_vertex(i, gamma.coordinates()[i][1])
        if i > 0 :
            editor_flat.add_cell(i-1, i-1, i)
        i += 1
    editor_flat.close()
    #print v_int
    
    M_flat = FunctionSpace(gamma_flat, "Lagrange", 1)

    return  gamma, gamma_flat, M_flat,\
            gm_to_g, gp_to_g

# From a mesh and subdomain, find mappings to gamma
def prep_sub_domain(num, mesh, inner_boundary, dirichlet, constraint) :
    # Define internal boundary mesh and function space
    bmesh = BoundaryMesh(mesh)
    cm = bmesh.cell_map()

    intersection = MeshFunction("uint", mesh, mesh.topology().dim()-1)
    intersection.set_all(2)

    #if constraint is not None :
    #    constraint.mark(intersection, 3)

    inner_boundary.mark(intersection, 1)

    if dirichlet is not None :
        for i in dirichlet :
            if constraint is None or constraint[i] != 3 :
                intersection[i] = 2
            else :
                intersection[i] = 4

    bintersection = MeshFunction("uint", bmesh, bmesh.topology().dim())
    bintersection.set_all(0)
    inner_boundary.mark(bintersection, 1)
    gamma_bar_i_coarse = SubMesh(bmesh, inner_boundary)
    #print gamma_bar_i_coarse.coordinates()[0][0]
    #print sorted([c[1] for c in filter(lambda c : abs(c[0] - 10.) < 1e-12,bmesh.coordinates())])
    #print sorted([c[1] for c in gamma_bar_i_coarse.coordinates()])
    V_coarse = FunctionSpace(gamma_bar_i_coarse, "Lagrange", 2)
    gamma_bar_i = Mesh()
    editor_fine = MeshEditor()
    editor_fine.open(gamma_bar_i, "interval", 1, 2)
    num_points = V_coarse.dofmap().global_dimension()
    V = FunctionSpace(mesh, "Lagrange", 2)
    bmesh_to_mesh_cell = bmesh.cell_map()
    editor_fine.init_vertices(num_points)
    editor_fine.init_cells(num_points-1)
    conn = gamma_bar_i_coarse.topology()(1,0)
    vconn = mesh.topology()(1,2)
    bmesh.init(0,1)
    rconn = bmesh.topology()(0,1)
    mesh_vertices = []
    vertices = {}
    gvi = gamma_bar_i_coarse.data().mesh_function("parent_vertex_indices")
    g_to_m = {}
    k = 0
    for i in range(0, gamma_bar_i_coarse.num_cells()) :
        c1, c2, mid = V_coarse.dofmap().cell_dofs(i)
        v1, v2 = conn(i)
        cell_on_boundary =\
            list(set(rconn(gvi[v1])).intersection(set(rconn(gvi[v2]))))[0]
        cell_on_mesh = vconn(cm[cell_on_boundary])[0]
        Vdofs = V.dofmap().cell_dofs(cell_on_mesh)
        Vdofs_c = V.dofmap().tabulate_coordinates(Cell(mesh, cell_on_mesh))
        ks = k
        for j in range(0, len(Vdofs)) :
            c = Vdofs_c[j]
            if inner_boundary.inside(c, on_boundary=True) :
                if Vdofs[j] not in mesh_vertices :
                    mesh_vertices.append(Vdofs[j])
                    vertices[k] = (Vdofs[j], Vdofs_c[j])
                    k += 1
    sorted_vertices = sorted(vertices.keys(),
        lambda c, d: cmp(vertices[c][1][1], vertices[d][1][1]))
    editor_fine.add_vertex(0, *vertices[sorted_vertices[0]][1])
    g_to_m[0] = vertices[sorted_vertices[0]][0]
    for i in range(0, len(sorted_vertices)-1) :
        g_to_m[i+1] = vertices[sorted_vertices[i+1]][0]
        editor_fine.add_vertex(i+1, *vertices[sorted_vertices[i+1]][1])
        editor_fine.add_cell(i, sorted_vertices[i], sorted_vertices[i+1])
    editor_fine.close()

    c = len(gamma_bar_i.coordinates())
    v_i = zip(gamma_bar_i.coordinates(), [num]*c, range(0, c))
    v_i.sort(vert_sort)

    return (intersection, gamma_bar_i, v_i, g_to_m)

# Define matrices to get from meshes to gamma
def build_transfer_matrix(mesh, gamma, gamma_bar, gb_to_m, gb_to_g) :
    Xm = zeros((len(gamma_bar.coordinates()), len(gamma.coordinates())))
    Ym = zeros((len(gamma.coordinates()), len(gamma_bar.coordinates())))

    for i in range(0, len(gamma_bar.coordinates())) :
       Xm[i, gb_to_g[i]] = 1.

    # Invert dictionary
    g_to_gb = dict([(gb_to_g[k],k) for k in gb_to_g])
    for i in range(0, len(gamma.coordinates())) :
        # If this point on gamma is in the (m/p) mesh, note it
        # and the subsequent overlapping point for interpolating
        # any following points that aren't in the (m/p) mesh
        if i in g_to_gb :
            Ym[i, g_to_gb[i]] = 1.
            if i+1 == len(gamma.coordinates()) :
                continue
            j = g_to_gb[i]
            jc = gamma.coordinates()[i][1]
            jn = j+1
            jnc = gamma.coordinates()[gb_to_g[jn]][1]
        else :
            ic = gamma.coordinates()[i][1]
            Ym[i, j] = (ic - jnc)/(jc - jnc)
            Ym[i, jn] = (jc - ic)/(jc - jnc)
    return Xm, Ym

# Build the interior forms on a section
def build_interior_problem(V, Vl, intersection, h, f, g, k, vm, bm, zx, dn_c,
timing, do_sign=0) :
    u = TrialFunction(V)
    v = TestFunction(V)

    # Necessary normal derivatives
    n = FacetNormal(V.mesh())
    vn = inner(grad(v), n)
    un = inner(grad(u), n)

    timing.click("m assembly")
    A = PETScMatrix()

    a_form = inner(grad(u), grad(v))*dx \
                + (2*al_dbc/h)**2*v*u*ds(4)
    if vm is not None :
        if zx is not None :
            #zxl = Function(Vl)
            #zxl.assign(Constant(0.))
            #for i in range(0, vm.size()) :
            #    zxl.vector()[bm[vm[i]]] = zx.vector()[i]
            #zx = project(zxl, V)
            zx = MeshFromInterval(zx)
            #a_form += (2*al_dbc/h)**2*v*(dn_c*un/(1+zx*zx)**.5)*ds(4)
    assemble(a_form,
            cell_domains=None,
            exterior_facet_domains=intersection,
            interior_facet_domains=None,
            tensor=A)
    timing.clock()

    # The RHS is just the same as for the unbroken problem
    ff = Function(V)
    ff.assign(f)

    l_form = v*ff*dx

    timing.click("building bcs")
    if vm is not None :
        if g is not None :
            #gfl = Function(Vl)
            #gfl.assign(Constant(0.))
            #for i in range(0, vm.size()) :
            #    gfl.vector()[bm[vm[i]]] = g.vector()[i]
            #gf = project(gfl, V)
            gf = MeshFromInterval(g, under_plate=True)
            l_form += v*gf*ds(2)
        if k is not None :
            #kfl = Function(Vl)
            #kfl.assign(Constant(0.))
            #for i in range(0, vm.size()) :
            #    kfl.vector()[bm[vm[i]]] = k.vector()[i]
            #kf = project(kfl, V)
            kf = MeshFromInterval(k)
            l_form += (2*al_dbc/h)**2*kf*v*ds(4)
    timing.clock()

    timing.click("v assembly")
    L = PETScVector()
    assemble(l_form,
        cell_domains=None,
        exterior_facet_domains=intersection,
        interior_facet_domains=None,
        tensor=L)
    timing.clock()

    return A, L

# Take an (m/p) mesh, function space and transfer matrices and
# build all of the appropriate indexed matrices for the full block
# matrix problem
def build_variational_problem(V, Vl, intersection, T, X, Y, M_flat, h, f,
        dn_c, gb_to_m, timing) :
    timing.click("build v p")
    # These test and trial functions will be needed to generate L2 inner product matrices
    u = TrialFunction(V)
    v = TestFunction(V)
    la_f = TrialFunction(M_flat)
    mu_f = TestFunction(M_flat)

    # Necessary normal derivatives
    n = FacetNormal(V.mesh())
    vn = inner(grad(v), n)
    un = inner(grad(u), n)

    cols = array(map(lambda i : gb_to_m[i], range(0, X.shape[0])), dtype="I")
    if abs(2*al/h) > 0 :
        #A_al = assemble( \
        #    (2*al/h)*u*v*ds(1),
        #    cell_domains=None,
        #    exterior_facet_domains=intersection,
        #    interior_facet_domains=None)
        A_a = assemble(mu_f*la_f*dx,
            cell_domains=None,
            exterior_facet_domains=None,
            interior_facet_domains=None)
        A_al = PETScMatrix()
        A_al.resize(V.dim(), V.dim())
        A_al.set((2*al/h)*matrix(Y).transpose()*matrix(A_a.array())*matrix(Y),
            cols, cols)
        A_al.apply("insert")
        #print A_al.array()
    else :
        A_al = 0

    # Generate the matrices to inner product functions defined on the (m/p) mesh
    # with la and mu defined on gamma. Here u represents (trans_mat * la)
    B = PETScMatrix()
    assemble(vn*u*ds(1),
        cell_domains=None,
        exterior_facet_domains=intersection,
        interior_facet_domains=None, tensor=B)
    B.zero()
        
    rows = array(range(0, B.size(0)), dtype="I")
    Bm = zeros((B.size(0), X.shape[0]))
    B.get(Bm, rows, cols)
    B = npm_to_uBLAS(Bm * matrix(X))
    Bt = npm_to_uBLAS(matrix(X).transpose() * Bm.transpose())
    if abs(2*al/h) > 0 :
        B_a = assemble(mu_f*la_f*dx,
            cell_domains=None,
            exterior_facet_domains=None,
            interior_facet_domains=None)
        #TYt = T*matrix(Y).transpose()
        B_al = PETScMatrix()
        B_al.resize(B.size(0), B_a.size(1))
        B_al.set(-(2*al/h)*matrix(Y).transpose()*matrix(B_a.array()),
            cols, array(range(0, B_a.size(1)), dtype="I"))
        B_al.apply("insert")
        Bt_al = PETScMatrix()
        Bt_al.resize(B_a.size(0), B.size(0))
        # expects B_a to be symm
        Bt_al.set(-(2*al/h)*matrix(B_a.array())*matrix(Y),
            array(range(0, B_a.size(0)), dtype="I"), cols)
        Bt_al.apply("insert")
    else :
        B_al = 0
        Bt_al = 0
    #print B_al.array()

    # One remaining term, representing the inner product of la and mu
    # on gamma
    C = 0
    if abs(2*al/h) > 0 :
        C_al = PETScMatrix()
        assemble((2*al/h)*(la_f*mu_f*dx),
                cell_domains=None,
                exterior_facet_domains=None,
                interior_facet_domains=None,
                tensor=C_al)
    else :
        C_al = 0
    #print C_al.array()

    G = PETScVector(C_al.size(0))
    G.zero()

    timing.clock()
    return B, Bt, C, G, A_al, B_al, Bt_al, C_al

class bds2(SubDomain) :
    def inside (self, x, on_boundary) :
        return abs(x[1]+1) < 1e-2 or abs(x[1]+0.5) < 1e-2
class Solver :
    timing = None

    def __init__(self, mesh1, mesh2, mesh3,
            inner_boundary_1, dirichlet_1,
            dirichlet_2, constraint_2, 
            inner_boundary_3, dirichlet_3, timing) :

        self.timing = timing
        # Define function spaces
        self.V1 = FunctionSpace(mesh1, "Lagrange", 2)
        self.V2 = FunctionSpace(mesh2, "Lagrange", 2)
        self.V3 = FunctionSpace(mesh3, "Lagrange", 2)
        self.V1l = FunctionSpace(mesh1, "Lagrange", 1)
        self.V2l = FunctionSpace(mesh2, "Lagrange", 1)
        self.V3l = FunctionSpace(mesh3, "Lagrange", 1)

        self.intersection_1 = MeshFunction("uint", mesh1, mesh1.topology().dim()-1)
        self.intersection_1.set_all(5)
        self.intersection_2 = MeshFunction("uint", mesh2, mesh2.topology().dim()-1)
        self.intersection_2.set_all(5)
        self.intersection_3 = MeshFunction("uint", mesh3, mesh3.topology().dim()-1)
        self.intersection_3.set_all(5)

        for i in dirichlet_1 :
              self.intersection_1[i] = 4
        #constraint_2.mark(self.intersection_constraint_2, 3)
        for i in dirichlet_2 :
            if constraint_2 is not None and constraint_2[i] == 3 :
                self.intersection_2[i] = 4
            else :
                self.intersection_2[i] = 2
                
        #for i in range(0, len(self.intersection_2.array())) :
        #    if self.intersection_2[i] == 3 :
        #        self.intersection_2[i] = 4
        for i in dirichlet_3 :
              self.intersection_3[i] = 4

        inner_boundary_1.mark(self.intersection_1, 1)
        inner_boundary_1.mark(self.intersection_2, 1)
        inner_boundary_3.mark(self.intersection_2, 1)
        inner_boundary_3.mark(self.intersection_3, 1)

        #asdf = Function(self.V2l)
        #vconn = mesh2.topology()(1,0)
        #mesh2.init(0,1)
        #rconn = mesh2.topology()(0,1)
        #for i in range(0, len(self.intersection_2.array())) :
        #    for j in vconn(i) :
        #        asdf.vector()[j] += self.intersection_2[i]/float(len(rconn(j)))
        #File("asdf.pvd") << asdf

        # Generate gamma_bar's and necessary maps
        self.intersection_1_m, gamma_bar_1_m, v_1_m, gm_to_m1 = prep_sub_domain(1,
            mesh1,
            inner_boundary_1, dirichlet_1, None)
        self.intersection_1_p, gamma_bar_1_p, v_1_p, gp_to_m2 = prep_sub_domain(2,
            mesh2,
            inner_boundary_1, dirichlet_2, constraint_2)
        
        self.intersection_3_m, gamma_bar_3_m, v_3_m, gm_to_m2 = prep_sub_domain(1,
            mesh2,
            inner_boundary_3, dirichlet_2, constraint_2)
        self.intersection_3_p, gamma_bar_3_p, v_3_p, gp_to_m3 = prep_sub_domain(2,
            mesh3,
            inner_boundary_3, dirichlet_3, None)

        gamma_1, gamma_flat_1, self.M_flat_1, \
            gm_to_g_1, gp_to_g_1 = build_gamma(v_1_m, v_1_p)

        gamma_3, gamma_flat_3, self.M_flat_3, \
            gm_to_g_3, gp_to_g_3 = build_gamma(v_3_m, v_3_p)

        # Turn the geometric description into transfer matrices
        X_1_m, Y_1_m = build_transfer_matrix(mesh1, gamma_1, gamma_bar_1_m, gm_to_m1,
                       gm_to_g_1)
        X_1_p, Y_1_p = build_transfer_matrix(mesh2, gamma_1, gamma_bar_1_p, gp_to_m2,
                       gp_to_g_1)

        X_3_m, Y_3_m = build_transfer_matrix(mesh3, gamma_3, gamma_bar_3_m, gm_to_m2,
                       gm_to_g_3)
        X_3_p, Y_3_p = build_transfer_matrix(mesh2, gamma_3, gamma_bar_3_p, gp_to_m3,
                       gp_to_g_3)

        self.gm_to_m1 = gm_to_m1
        self.gp_to_m2 = gp_to_m2
        self.gm_to_m2 = gm_to_m2
        self.gp_to_m3 = gp_to_m3

        self.m_mats = ((X_1_m, Y_1_m), (X_3_m, Y_3_m))
        self.p_mats = ((X_1_p, Y_1_p), (X_3_p, Y_3_p))
        
    def solve_poisson(self, h, f, res,
        zx_1, k_1, vm_1, bm_1,
        zx_2, k_2, g_2, vm_2, bm_2,
        zx_3, k_3, vm_3, bm_3,
        dn_c) : 

        self.timing.click("matrix assembly")
        # From the function space and transfer matrices for the (m/p) mesh generate the
        # indexed entries for the big block matrix
        A1, L1 = build_interior_problem(self.V1, self.V1l,
            self.intersection_1, h, f, None, k_1, vm_1, bm_1, zx_1, dn_c, self.timing)

        B1_m, B1t_m, C1_m, G1_m, \
        A_al1_m, B_al1_m, Bt_al1_m, C_al1_m = \
                build_variational_problem(self.V1, self.V1l,
                self.intersection_1_m, None,
                self.m_mats[0][0], self.m_mats[0][1],
                self.M_flat_1, h, f, dn_c, self.gm_to_m1, self.timing)

        A_al1 = A_al1_m

        B2_p, B2t_p, C2_p, G2_p, \
        A_al2_p, B_al2_p, Bt_al2_p, C_al2_p = \
                build_variational_problem(self.V2, self.V2l,
                self.intersection_1_p, None,
                self.p_mats[0][0], self.p_mats[0][1],
                self.M_flat_1, h, f, dn_c, self.gp_to_m2, self.timing)

        A2, L2 = build_interior_problem(self.V2, self.V2l,
            self.intersection_2, h, f, g_2, k_2, vm_2, bm_2, zx_2, dn_c,
            self.timing)

        B2_m, B2t_m, C2_m, G2_m, \
        A_al2_m, B_al2_m, Bt_al2_m, C_al2_m = \
                build_variational_problem(self.V2, self.V2l,
                self.intersection_3_m, None,
                self.m_mats[1][0], self.m_mats[1][1],
                self.M_flat_3, h, f, dn_c, self.gm_to_m2, self.timing)

        A_al2 = A_al2_m + A_al2_p

        B3_p, B3t_p, C3_p, G3_p, \
        A_al3_p, B_al3_p, Bt_al3_p, C_al3_p = \
                build_variational_problem(self.V3, self.V3l,
                self.intersection_3_p, None,
                self.p_mats[1][0], self.p_mats[1][1],
                self.M_flat_3, h, f, dn_c, self.gp_to_m3, self.timing)

        A3, L3 = build_interior_problem(self.V3, self.V3l,
            self.intersection_3, h, f, None, k_3, vm_3, bm_3, zx_3, dn_c, self.timing)

        A_al3 = A_al3_p
        self.timing.clock()

        C_1 = new_PETScMatrix(C_al1_m.size(0), C_al1_m.size(1))
        C_3 = new_PETScMatrix(C_al3_p.size(0), C_al3_p.size(1))

        self.timing.click("matrix combination")

        if C1_m != 0: C_1.axpy(1., C1_m, False)
        if C2_p != 0: C_1.axpy(1., C2_p, False)
        if C2_m != 0: C_3.axpy(1., C2_m, False)
        if C3_p != 0: C_3.axpy(1., C3_p, False)
        if abs(2*al/h) > 0 :
            C_1.axpy(1., C_al1_m, False)
            C_1.axpy(1., C_al2_p, False)
            C_3.axpy(1., C_al2_m, False)
            C_3.axpy(1., C_al3_p, False)
            A1.axpy(1., A_al1, False)
            A2.axpy(1., A_al2, False)
            A3.axpy(1., A_al3, False)
            B1_m.axpy(1., B_al1_m, False)
            B2_p.axpy(1., B_al2_p, False)
            B2_m.axpy(1., B_al2_m, False)
            B3_p.axpy(1., B_al3_p, False)
            B1t_m.axpy(1., Bt_al1_m, False)
            B2t_p.axpy(1., Bt_al2_p, False)
            B2t_m.axpy(1., Bt_al2_m, False)
            B3t_p.axpy(1., Bt_al3_p, False)
        #A1I = DOLFINSolveMat(A1)
        #A2I = DOLFINSolveMat(A2)
        #A3I = DOLFINSolveMat(A3)
        #S = C - (B1t*(A1I*B1)) - (B2t*(A2I*B2)) - (B2t*(A2I*B2)) - (B3t*(A3I*B3))
        #G = - (B1t*(A1I*L1)) - (B2t*(A2I*L2)) - (B3t*(A3I*L3))
        #LA = ConjGrad(S, maxiter=1000, tolerance=res)*G

        #m = 2
        #N = 2**(m+1)-1
        #d = [[0]*(N+2) for i in range(0,N+2)]
        #dI = [[0]*(N+2) for i in range(0,N+2)]
        #e = [[0]*(N+2) for i in range(0,N+2)]
        #f = [[0]*(N+2) for i in range(0,N+2)]
        #v = [[0]*(N+2) for i in range(0,N+2)]
        #x = [[0]*(N+2) for i in range(0,N+2)]

        #d[1][1] = A1
        #dI[1][1] = DOLFINSolveMat(A1)
        #f[1][1] = B1_m

        #e[2][1] = B1t_m
        #d[2][1] = C_1
        #dI[2][1] = DOLFINSolveMat(C_1)
        #f[2][1] = B2t_p

        #e[3][1] = B2_p
        #d[3][1] = A2
        #dI[3][1] = DOLFINSolveMat(A2)
        #f[3][1] = B2_m

        #e[4][1] = B2t_m
        #d[4][1] = C_3
        #dI[4][1] = DOLFINSolveMat(C_3)
        #f[4][1] = B3t_p

        #e[5][1] = B3_p
        #d[5][1] = A3
        #dI[5][1] = DOLFINSolveMat(A3)

        #v[1][1] = L1
        #v[2][1] = G1_m+G2_p
        #v[3][1] = L2
        #v[4][1] = G2_m+G3_p
        #v[5][1] = L3

        self.timing.clock()

        self.timing.click("matrix_solve")
        U1 = PETScVector()
        U1.resize(A1.size(0))
        U2 = PETScVector()
        U2.resize(A2.size(0))
        U3 = PETScVector()
        U3.resize(A3.size(0))
        LA12 = PETScVector()
        LA12.resize(C_1.size(0))
        LA23 = PETScVector()
        LA23.resize(C_3.size(0))
        mk2matrix.matrix_solve(
            A1, B1_m, C_1, B2_p,
            A2, B2_m, C_3, B3_p,
            A3,
            U1, LA12, U2, LA23, U3,
            L1, G1_m, G2_p, L2, G2_m, G3_p, L3
        )
        #mk2matrix.matrix_solve(
        #    A3, B3_p, C_3, B2_m,
        #    A2, B2_p, C_1, B1_m,
        #    A1,
        #    U3, LA23, U2, LA12, U1,
        #    L3, G3_p, G2_m, L2, G2_p, G1_m, L1, res
        #)
        self.timing.clock()
        #print U1.array()


        #d[1][2] = d[2][1] - e[2][1] * dI[1][1] * f[1][1] - f[2][1] * dI[3][1] * e[3][1]
        ##dI[1][2] = DOLFINSolveMat(d[1][2])
        #dI[1][2] = ConjGrad(d[1][2])
        #f[1][2] = (-1) * f[2][1] * dI[3][1] * f[3][1]
        #v[1][2] = v[2][1] - e[2][1] * dI[1][1] * v[1][1] - f[2][1] * dI[3][1] * v[3][1]

        #e[2][2] = (-1) * e[4][1] * dI[3][1] * e[3][1]
        #d[2][2] = d[4][1] - e[4][1] * dI[3][1] * f[3][1] - f[4][1] * dI[5][1] * e[5][1]

        ##dI[2][2] = DOLFINSolveMat(d[2][2])
        #dI[2][2] = ConjGrad(d[2][2])
        #f[2][2] = (-1) * f[4][1] * dI[5][1] * f[5][1]
        #v[2][2] = v[4][1] - e[4][1] * dI[3][1] * v[3][1] - f[4][1] * dI[5][1] * v[5][1]

        #d[1][3] = d[2][2] - e[2][2] * dI[1][2] * f[1][2]
        #dI[1][3] = ConjGrad(d[1][3])
        #v[1][3] = v[2][2] - e[2][2] * dI[1][2] * v[1][2]

        #x[1][3] = dI[1][3] * v[1][3]
        ##out = x[1][3]
        ##print out.array()
        ##print (out-U1).array()
        ##print "==>"+str(U1.size(0))
        ##dI[1][3] = DOLFINSolveMat(d[1][3])
        ##print v[1][3].array()
        #x[1][3] = LA23

        #x[2][2] = x[1][3]
        #x[1][2] = dI[1][2] * (v[1][2] - f[1][2]*x[2][2])

        #x[2][1] = x[1][2]
        #x[4][1] = x[2][2]
        #x[1][1] = dI[1][1] * (v[1][1] - f[1][1]*x[2][1])
        #x[3][1] = dI[3][1] * (v[3][1] - e[3][1]*x[2][1] - f[3][1]*x[4][1])
        #x[5][1] = dI[5][1] * (v[5][1] - e[5][1]*x[4][1])

        ##print U1.array()
        ##print (U1-x[1][1]).array()
        #U1 = x[1][1]
        #U2 = x[3][1]
        #U3 = x[5][1]

        #for i in range(1, N) :
        #    for j in range(1, 2**(m-i+1)) :
        #        dIm = DOLFINSolveMat(d[2*j-1][i])
        #        dIp = DOLFINSolveMat(d[2*j+1][i])
        #        e[j][i+1] = (-1) * e[2*j][i] * dIm * e[2*j-1][i]
        #        d[j][i+1] = d[2*j][i] - e[2*j][i] * dIm * f[2*j-1][i] - f[2*j][i] * dIp * e[2*j+1][i]
        #        f[j][i+1] = (-1) * f[2*j][i] * dIp * f[2*j+1][i]
        #        print i,j
        #        print f[2*j][i]
        #        print dIp
        #        print dIp * v[2*j+1][i]
        #        v[j][i+1] = v[2*j][i] - e[2*j][i] * dIm * v[2*j-1][i] - f[2*j][i] * dIp * v[2*j+1][i]

        ##c = Function(self.M_flat)
        ##c.assign(Constant(1.))
        ##d = c.vector()
        #print (S*d).array()
        #U1 = Function(self.V1)
        #U1.assign(Constant(1.))
        #print (A1*U1.vector()).array()
        #print (B1*d).array()
        #U2 = Function(self.V2)
        #U2.assign(Constant(1.))
        #print (A2*U2.vector()).array()
        #print L1.array()
        ##quit()

        #U1 = A1I*(L1 - B1*LA)
        #U2 = A2I*(L2 - B2*LA)
        #U3 = A3I*(L3 - B3*LA)
        ##print (A1I*(L1 - B1*d)).array()
        #print (A2I*(L2 - B2*d)).array()
        ##quit()
        
        # Turn the numpy arrays into DOLFIN functions
        self.U1 = U1
        self.U2 = U2
        self.U3 = U3
        self.LA12 = LA12
        self.LA23 = LA23
        self.u1 = Function(self.V1, self.U1)
        self.u2 = Function(self.V2, self.U2)
        self.u3 = Function(self.V3, self.U3)
        self.la12 = Function(self.M_flat_1, self.LA12)
        self.la23 = Function(self.M_flat_3, self.LA23)

        return self.u1, self.u2, self.u3, self.la12, self.la23
