import argparse
from scipy.interpolate import interp1d
from scipy.integrate import quad
import numpy

argparser = argparse.ArgumentParser()
argparser.add_argument('diff1')
argparser.add_argument('diff2')
#argparser.add_argument('--timestep', type=float)
argparser.add_argument('--compare-until', type=float, default=7.)

args = argparser.parse_args()
diff1 = args.diff1
diff2 = args.diff2
#ts = args.timestep
end = args.compare_until

#if ts is None :
#    print 'Must supply timestep'
#    quit(1)

print 'Comparing %s and %s' % (diff1, diff2)


try :
    with open("scripts/outroot.sh", 'r') as f :
        outroot = f.readlines()[0]
        outroot = outroot.split('=')[1].strip()
        outdir1 = outroot + '/output.' + diff1
        outdir2 = outroot + '/output.' + diff2
except :
    print "Could not establish outroot; are you definitely running from build root?"

try :
    rms1 = outdir1+'/rms-%s_plate_1.csv' % diff1
    file1 = numpy.loadtxt(rms1, skiprows=1, delimiter=',')
except :
    rms1 = outdir1+'/rms-plate_1.csv'
    file1 = numpy.loadtxt(rms1, skiprows=1, delimiter=',')
try :
    rms2 = outdir2+'/rms-%s_plate_1.csv' % diff2
    file2 = numpy.loadtxt(rms2, skiprows=1, delimiter=',')
except :
    rms2 = outdir2+'/rms-plate_1.csv'
    file2 = numpy.loadtxt(rms2, skiprows=1, delimiter=',')


file1 = zip(*file1)
file2 = zip(*file2)
file1 = map(list, file1)
file2 = map(list, file2)

m = max(file1[1])
print "Using %s as ref" % diff1

if file1[0][0] != 0 :
    print "1 Starts at %lf, adding 0" % file1[0][0]
    file1[0].insert(0, 0.)
    file1[1].insert(0, 0.)
if file2[0][0] != 0 :
    print "2 Starts at %lf, adding 0" % file2[0][0]
    file2[0].insert(0, 0.)
    file2[1].insert(0, 0.)
#n = round(end/ts)
#x = numpy.linspace(0, end, n)

f1 = interp1d(file1[0], file1[1])
f2 = interp1d(file2[0], file2[1])

#if len(f1) > len(f2) :
#    t = f1
#else :
#    t = f2
#
#N = 0
#for i in range(x) :
#    

ret = quad(lambda t : (f1(t)-f2(t))**2, 0.1, end, limit=200)
N = ret[0]**.5
print "From 0 to %lf : %lf (%lf%%) [int err: %lf]" % (end, N, N*100/m, ret[1])
