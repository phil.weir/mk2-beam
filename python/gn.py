"""This demo program solves Poisson's equation

    - div grad u(x, y) = f(x, y)

on the unit square with source f given by

    f(x, y) = 10*exp(-((x - 0.5)^2 + (y - 0.5)^2) / 0.02)

and boundary conditions given by

    u(x, y) = 0        for x = 0 or x = 1
du/dn(x, y) = sin(5*x) for y = 0 or y = 1
"""

__author__ = "Anders Logg (logg@simula.no)"
__date__ = "2007-08-16 -- 2010-03-05"
__copyright__ = "Copyright (C) 2007-2009 Anders Logg"
__license__  = "GNU LGPL Version 2.1"

# Begin demo

from dolfin import *
import mk2beam

class Left(SubDomain) :
    def inside(self, x, on_boundary) :
        return x[0] <= .25+DOLFIN_EPS
class Right(SubDomain) :
    def inside(self, x, on_boundary) :
        return x[0] >= .75-DOLFIN_EPS
def predict(dt, zeta, zeta_m1, zdt_m1, zdt_m2, zdt_m3, zdt_m4) :
    #zeta.interpolate(project(zeta_m1 + (dt/24.)*(55*zdt_m1 - 59*zdt_m2 + 37*zdt_m3 - 9*zdt_m4), zeta_m1.function_space()))
    for i in range(0, len(zeta.vector())) :
        zeta_m1v = zeta_m1.vector()[i]
        zdt_m1v = zdt_m1.vector()[i]
        zdt_m2v = zdt_m2.vector()[i]
        zdt_m3v = zdt_m3.vector()[i]
        zdt_m4v = zdt_m4.vector()[i]
        zeta.vector()[i] = zeta_m1v + (dt/24.)*(55*zdt_m1v - 59*zdt_m2v + 37*zdt_m3v - 9*zdt_m4v)
    #zeta.interpolate(project(zeta_m1 + (dt/24.)*(55*zdt_m1 - 59*zdt_m2 + 37*zdt_m3 - 9*zdt_m4), zeta_m1.function_space()))


def correct(dt, cphi_new, cphi_m1, cpdt, cpdt_m1, cpdt_m2, cpdt_m3) :
    #cphi_new.interpolate(project(cphi_m1 + (dt/24.)*(9*cpdt + 19*cpdt_m1 - 5*cpdt_m2 + cpdt_m3), cphi_new.function_space()))
    for i in range(0, len(cphi_new.vector())) :
        cphi_m1v = cphi_m1.vector()[i]
        cpdtv = cpdt.vector()[i]
        cpdt_m1v = cpdt_m1.vector()[i]
        cpdt_m2v = cpdt_m2.vector()[i]
        cpdt_m3v = cpdt_m3.vector()[i]
        cphi_new.vector()[i] = cphi_m1v + (dt/24.)*(9*cpdtv + 19*cpdt_m1v - 5*cpdt_m2v + cpdt_m3v)

def make_dd(dd, dt, d, d1, dd1) :
    for i in range(0, len(dd.vector())) :
        dd.vector()[i] = (2./dt)*(d.vector()[i]-d1.vector()[i]) - dd1.vector()[i]
def sub_hydrostatic(p) :
    for i in range(0, len(p.vector())) :
        p.vector()[i] -= rhow*initial_draft

left = 10
right = 20

initial_draft = 0.3
E = 8*10**9
beam_type = "Euler-Bernoulli"

rhow = 1027.

ic_width = 5
ic_offset = 0
ic_vert_offset = 0
ic_amp = 0.2
ic_symmetric = False
ic_left = 0

class BeamLeft(SubDomain) :
    def inside(self, x, on_boundary) :
        return abs(x[0] - left) <= DOLFIN_EPS

class BeamEnds(SubDomain) :
    def inside(self, x, on_boundary) :
        return abs(x[0] - left) <= DOLFIN_EPS or abs(x[0] - right) <= DOLFIN_EPS

class Beam(SubDomain) :
    def inside(self, x, on_boundary) :
        return x[0] >= left-DOLFIN_EPS and x[0] <= right+DOLFIN_EPS

class ic_cl(Expression) :
    def eval(self, values, x) :
        if ic_symmetric :
            x = (left+right)*.5 - abs(x[0] - (left+right)*.5)
        else :
            x = x[0]

        if (x < ic_left) :
            values[0] =\
            ic_amp*.5*(cos(pi*(ic_offset)/ic_width)+1)+ic_vert_offset
        elif (x < ic_left+ic_width) :
            values[0] =\
            ic_amp*.5*(cos(pi*(x-ic_left+ic_offset)/ic_width)+1)+ic_vert_offset
        else :
            values[0] =\
            ic_amp*.5*(cos(pi*(ic_width+ic_offset)/ic_width)+1)+ic_vert_offset

class Combine(Expression) :
    def __init__(self, fs, beam) :
        self.fs = fs
        self.beam = beam
        Expression.__init__(self)
    def eval_cell(self, value, x, ufc_cell) :
        cell = MeshEntity(interval, 1, ufc_cell.index)
        if (left < cell.midpoint()[0]) and (right > cell.midpoint()[0]) :
            self.beam.eval_cell(value, x, ufc_cell)
        else :
            self.fs.eval_cell(value, x, ufc_cell)

interval = UnitInterval(80)
for x in interval.coordinates() :
    x[0] *= 40

depth = 1.0
class Depth(Expression) :
    def eval_cell(self, value, x, ufc_cell) :
        cell = MeshEntity(interval, 1, ufc_cell.index)
        if (left < cell.midpoint()[0]) and (right > cell.midpoint()[0]) :
            value[0] = depth-initial_draft
        else :
            value[0] = depth

#left = SubMesh(interval, Left())
#left = Left()
#beam = SubMesh(interval, Beam())
#right = SubMesh(interval, Right())

dt = 0.01

Vzeta = FunctionSpace(interval, "Discontinuous Lagrange", 5)
VU = FunctionSpace(interval, "Discontinuous Lagrange", 5)
Vp = FunctionSpace(interval, "Discontinuous Lagrange", 5)
V = MixedFunctionSpace((VU, Vzeta, Vp))
Udf, zdf, pf = TrialFunctions(V)
v, xi, q = TestFunctions(V)
uv = Function(V)

U = Function(VU)
U_1 = Function(VU)
U_d = Function(VU)
U_d1 = Function(VU)
U_d2 = Function(VU)
U_d3 = Function(VU)
U_d4 = Function(VU) 

zeta = Function(Vzeta)
zeta_1 = Function(Vzeta)
zeta_d = Function(Vzeta)
zeta_dd = Function(Vzeta)
zeta_dd1 = Function(Vzeta)
zeta_dd_old = Function(Vzeta)
zeta_d1 = Function(Vzeta)
zeta_d2 = Function(Vzeta)
zeta_d3 = Function(Vzeta)
zeta_d4 = Function(Vzeta) 

p = Function(Vp)

init_zeta = ic_cl()

zeta.assign(init_zeta)
zeta_1.assign(init_zeta)

beamends = BeamEnds()
beam_s = Beam()
beamleft = BeamLeft()

beamline = SubMesh(interval, beam_s)
beam_desc = mk2beam._PlateDescription()
beam_desc.name = "beam"
beam_desc.left = left
beam_desc.right = right
beam_desc.draft = initial_draft
beam_desc.E = E
beam_desc.nu = 0.3
beam_desc.beta = 0.25
beam_desc.rhop = 917.
beam_desc.mup = 0.
beam_desc.h = beam_desc.draft * rhow / beam_desc.rhop
beamer = mk2beam.Plate.get_platermap()
beamer = mk2beam.Plate.get_plater(beam_type)
#beam = mk2beam.Plate(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True);
beam = mk2beam.Plate(beamline, beam_desc, dt, rhow, 9.81, beamer, "beam", True)

imf = MeshFunction("uint", interval, 0)
imf.set_all(0)
beamends.mark(imf, 1)
rmf = MeshFunction("uint", interval, 0)
rmf.set_all(0)
beamleft.mark(rmf, 1)
lmf = MeshFunction("uint", interval, 0)
lmf.set_all(0)
jmf = MeshFunction("uint", interval, 1)
jmf.set_all(0)
beam_s.mark(jmf, 1)
#left.mark(lmf, 1)

dx = Measure("dx")[jmf]
ds = Measure("ds")[lmf]
dS = Measure("dS")[imf]

#d = 1.0
de = Depth()
d = Function(Vzeta)
d.assign(de)

test =Function(VU)
set_log_level(100)

zeta_dd_old.assign(Constant(1.))

while True :

    eps = 1
    while eps > 1e-20 :
        beam.clstep()
        n = FacetNormal(V.mesh())
        phi = d+zeta
        w = zeta_d+U*Dx(zeta,0)
        psi = .5*(zeta-d)
        phit = zeta_d
        Pa = pf*(zeta+d)
        Pb = 1/6. * rhow*(d+zeta)**2*(2*zeta_dd+3*9.81)
        Ufa =       (dt/24.)*(9*Udf                          )
        Ufb = U_1 + (dt/24.)*(        19*U_d1 - 5*U_d2 + U_d3)
        Uda = Udf
        Udb = U*Dx(zeta,0)
        av = v*Uda*dx(0)+avg(Dx(v,0))*jump(Udf)*dS(0)+avg(v)*jump(Dx(Udf,0))*dS(0)+v*Uda*n*ds(0) \
             + v*Dx((d+zeta)*Ufa,0)*dx(1)+avg(v)*rhow*jump(phi*Udf+zdf*U)*dS(1)\
             +avg(v)*rhow*jump(U*Dx(zeta,0)*Ufa)*dS(1)
        axi = xi*zdf*dx(0)+avg(xi)*jump(Dx(zdf,0))*dS(0)+avg(Dx(xi,0))*jump(zdf)*dS(0)\
             +xi*Dx(zdf,0)*n*ds(0)\
             + xi*zdf*dx(1)
        aq = q*pf*dx(0)+avg(Dx(q,0))*jump(pf)*dS(0)+avg(q)*jump(Dx(pf,0))*dS(0) \
             + q*Uda*dx(1) \
             + q*Dx(pf,0)/rhow*dx(1) \
             - avg(q)*jump(Pa/phi)*dS(1)\
             - avg(q)*jump(rhow*U*Ufa)*dS(1)
        a = av+axi+aq

        #     + avg(v)*jump((1/12.)*rho*psi*U*w)*dS(1) \
        #     + avg(xi)*jump(rho*psi*U)*dS(1) \
        #     + avg(Dx(xi,0))*jump(rho*psi*U*U)*dS(1)
        Lv = -v*Udb*dx(0) - v*Udb*n*ds(0) \
             + v*(-9.81*Dx(zeta,0))*dx(0)\
             - v*Dx((d+zeta)*Ufb,0)*dx(1)\
             - v*zeta_d*dx(1)\
             -avg(v)*rhow*jump(U*Dx(zeta,0)*Ufb)*dS(1)
        Lxi = -xi*Dx((d+zeta)*U,0)*dx(0)
        Lq = q*(-9.81*Dx(zeta,0))*dx(1)\
             - q*Udb*dx(1)\
             + avg(q)*jump(rhow*(U*Ufb))*dS(1)\
             #- avg(q)*phit('+')*jump((rhow/3)*w)*dS(1)
        L = Lv+Lxi+Lq
        
        solve(a==L, uv)
        (U_dn, zeta_dn, p_dn) = uv.split()
        U_d.interpolate(U_dn)
        zeta_d.interpolate(zeta_dn)
        p.interpolate(p_dn)
        #sub_hydrostatic(p)
        make_dd(zeta_dd, dt, zeta_d, zeta_d1, zeta_dd1)
        test.interpolate(project(U_d))
        plot(p)#, vmin=-.0003, vmax=.0003, interactive=True)
        plot(U)#, vmin=-3e-15, vmax=3e-15)#, interactive=True)
        plot(zeta_d) #, vmin=-3e-18, vmax=3e-18)#, interactive=True)
        #interactive()
        #plot(test, vmin=-1e-8, vmax=1e-8)
        L = Lv+Lxi+Lq
        
        beam.p.interpolate(p)

        #beam.step()

        #plot(U, vmin=-1,vmax=1)
        #plot(p)#, vmin=-.3, vmax=.3)
        #interactive()

        correct(dt, U, U_1, U_d, U_d1, U_d2, U_d3)
        correct(dt, zeta, zeta_1, zeta_d, zeta_d1, zeta_d2, zeta_d3)

        z_combine = Combine(zeta, beam.Wn1)
        zd_combine = Combine(zeta_d, beam.Wdtn1)
        zdd_combine = Combine(zeta_dd, beam.Wddtn1)

        p.assign(z_combine)
        zeta.interpolate(p)
        p.assign(zd_combine)
        zeta_d.interpolate(p)
        p.assign(zdd_combine)
        zeta_dd.interpolate(p)

        M = (zeta_dd-zeta_dd_old)**2*dx(0)+(zeta_dd-zeta_dd_old)**2*dx(1)
        M = assemble(M)
        if M > 0 :
            eps = sqrt(M)
        else :
            eps = 0
        print eps

        zeta_dd_old.interpolate(zeta_dd)

    U_d4.interpolate(U_d3)
    U_d3.interpolate(U_d2)
    U_d2.interpolate(U_d1)
    U_d1.interpolate(U_d)
    U_1.interpolate(U)

    zeta_dd1.interpolate(zeta_dd)
    zeta_d4.interpolate(zeta_d3)
    zeta_d3.interpolate(zeta_d2)
    zeta_d2.interpolate(zeta_d1)
    zeta_d1.interpolate(zeta_d)
    zeta_1.interpolate(zeta)
    zeta_dd_old.interpolate(zeta_dd)
    beam.timestep()

    predict(dt, U, U_1, U_d1, U_d2, U_d3, U_d4)
    predict(dt, zeta, zeta_1, zeta_d1, zeta_d2, zeta_d3, zeta_d4)
