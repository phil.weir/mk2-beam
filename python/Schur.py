from block import *
import dolfin
from dolfin import *
from mpmath import *
class SchurComplement :
    def __init__(self, A1, A2, B1, B2, B1t, B2t, C, L1, L2) :
        self.A1 = A1
        self.A2 = A2
        self.B1 = B1
        self.B2 = B2
        self.B1t = B1t
        self.B2t =B2t
        self.C = C
        self.L1 = L1
        self.L2 = L2
        self.A1iB1x = uBLASVector(B1.size(0))
        self.A2iB2x = uBLASVector(B2.size(0))
        self.A1iL1 = uBLASVector(B1.size(0))
        self.A2iL2 = uBLASVector(B2.size(0))

    res = 1e-8
    def solve(self, x) :
        la = get_log_level()
        set_log_level(100)

        b = self.get_b()
        d = self.diag()
        for i in range(0, len(b)) :
            if abs(d[i]) > DOLFIN_EPS :
                x[i] = b[i]/d[i]
            else :
                x[i] = 0.

        r = ((- self.mult(x)) + b)
        p = r.copy()
        k = 0
        res = r.norm("l2") ** 2
        while res > self.res :
            Ap = self.mult(p)
            al = res/(Ap * p).norm("l1")
            x += al*p
            r -= al*Ap
            res_new = r.norm("l2") ** 2
            beta = res_new / res
            p *= beta
            p += r
            res = res_new
            k += 1
            #if k > 40 : quit()

        set_log_level(la)
        return x

    def diag(self) :
        return [self.C[1].array()[i][i] for i in range(0, self.B1.size(1))]

    def mult(self, x) :
        A1 = self.A1
        A2 = self.A2
        B1 = self.B1
        B2 = self.B2
        B1t = self.B1t
        B2t = self.B2t
        C = self.C
        L1 = self.L1
        L2 = self.L2
        A1iB1x = self.A1iB1x
        A2iB2x = self.A2iB2x

        B1x = B1 * x
        B2x = B2 * x
        solve(A1, A1iB1x, B1x)
        solve(A2, A2iB2x, B2x)
        return (C*x - (B1t * (A1iB1x)) - (B2t * (A2iB2x)))
        #G = - (B1t * (A1.I * L1)) - (B2t * (A2.I * L2))

    def get_b(self) :
        A1 = self.A1
        A2 = self.A2
        B1 = self.B1
        B2 = self.B2
        B1t = self.B1t
        B2t = self.B2t
        C = self.C
        L1 = self.L1
        L2 = self.L2
        A1iL1 = self.A1iL1
        A2iL2 = self.A2iL2

        solve(A1, A1iL1, L1)
        solve(A2, A2iL2, L2)
        return - (B1t * (A1iL1)) - (B2t * (A2iL2))

class DOLFINSolveMat(block_mat):
    def __init__(self, A) :
        block_mat.__init__(self, [[A]])
        self.solver = LinearSolver()

    def matvec(self, x):
        from dolfin import GenericVector, GenericMatrix
        import numpy
        m,n = self.blocks.shape

        z = self[0,0].create_vec(dim=0)
        self.solver.solve(self[0,0], z, x)
        return z

    def transpmult(self, x):
        import numpy
        from dolfin import GenericVector

        z = self[0,0].create_vec(dim=0)
        self.solver.solve(self[0,0].transpose(), z, x)
        return z
