
from solve import Solver
import shutil
import math
import time
from subprocess import Popen
import mk2beam
import sys
import os
import argparse
from numpy import *
from dolfin import *
from multiprocessing import Process, Queue

class Timing :
    last_to_time = 0
    from_time = 0
    times = None
    current_times = None

    def __init__(self) :
        self.times = {}
        self.current_times = (self.times, None, None)

    def click(self, interval_name) :
        from_time = time.time()
        self.current_name = interval_name
        if interval_name not in self.current_times[0] :
            self.current_times[0][interval_name] = \
                [{}, [], self.current_times, from_time]
        self.current_times[0][interval_name][3] = from_time
        self.current_times = self.current_times[0][interval_name]

    def clock(self) :
        to_time = time.time()
        from_time = self.current_times[3]
        self.current_times[1].append(to_time-from_time)
        self.current_times = self.current_times[2]

    def _get_stats(self, times=None) :
        if times is None :
            return None
        return dict((k,\
            {'subs' : self._get_stats(v[0]),
             'occurs' : len(v[1]), 'ttot' : sum(v[1]),
             'tavg' : sum(v[1])/len(v[1])}) \
            for k, v in times.items())

    def _print_sub_stats(self, stats_dict, prefix='') :
        outstr = ''
        for k in stats_dict :
            stats = stats_dict[k]
            outstr += prefix + '{0:20} {1:3d} {2:3.3f} {3:3.3f}\n'.\
                format(str(k), stats['occurs'], stats['ttot'], stats['tavg'])
            outstr += self._print_sub_stats(stats['subs'], prefix+' ')
        return outstr

    def print_stats(self) :
        stats_dict = self._get_stats(self.times)
        outstr = self._print_sub_stats(stats_dict)
        return outstr

