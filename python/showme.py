import gtk
import os
import fnmatch
import string
import sys

try :
    with open("scripts/outroot.sh", 'r') as f :
        outroot = f.readlines()[0]
        outroot = outroot.split('=')[1].strip()
except :
    print "Could not establish outroot; are you definitely running from build root?"

win = gtk.Window()
frame = gtk.Frame()
hbox = gtk.HBox()
win.add(hbox)
runnames = gtk.ListStore(str)
tree_view = gtk.TreeView(runnames)
tvc = gtk.TreeViewColumn('Runname')
tree_view.append_column(tvc)
cell = gtk.CellRendererText()
tvc.pack_start(cell)
tvc.set_attributes(cell, text=0)
hbox.pack_start(tree_view, False)
hbox.pack_start(frame)

d = os.listdir(outroot)
for f in sorted(d) :
    if not os.path.basename(f).startswith('output.') :
        continue
    for arg in sys.argv[1:] :
        runname = f[7:]
        if fnmatch.fnmatch(runname, arg) :
            runnames.append([runname])
prev = {}

def get_val(line) :
    comm = line.split('#')
    if len(comm) > 0:
        line = comm[0].strip()
    if len(line) == 0 :
        return None, None
    eq = line.split('=')
    arr = eq[1].strip().split()
    return eq[0].strip(), arr[0]

def add_to_table(table, params, c) :
    i = 0
    for line in params :
        l, v = get_val(line)
        if l is not None :
            val = gtk.Label(v)
            if l in prev and prev[l] != v :
                val.modify_fg(gtk.STATE_NORMAL, gtk.gdk.Color('red'))
            else :
                val.modify_fg(gtk.STATE_NORMAL, gtk.gdk.Color('black'))
            prev[l] = v
            table.attach(gtk.Label(l), 2*c, 2*c+1, i, i+1)
            table.attach(val, 2*c+1, 2*c+2, i, i+1)
            i += 1

def set_runname(runname) :
    win.set_title(runname)
    if frame.get_child() is not None :
        frame.remove(frame.get_child())
    table = gtk.Table()
    frame.add(table)
    
    params = \
     open('%s/output.%s/params/parameters.%s'%(outroot,runname, runname))
    
    line = params.readline()
    while line[0] != '=' :
        line = params.readline()
    
    add_to_table(table, params, 0)
    
    beam = \
     open('%s/output.%s/params/plate1.%s'%(outroot,runname, runname))
    
    add_to_table(table, beam, 1)
    
    wavemaker = \
     open('%s/output.%s/params/wavemaker.%s'%(outroot,runname, runname))
    
    add_to_table(table, wavemaker, 2)
    frame.show_all()

def chg_by_cursor() :
    c = tree_view.get_cursor()
    n = c[0][0]
    set_runname(runnames[n][0])
def proc_key(e) :
    global n
    name = gtk.gdk.keyval_name(e.keyval)
    if name == 'Left' :
        n = max(0, n-1)
        set_runname(runnames[n][0])
    if name == 'Right' :
        n = min(len(runnames)-1, n+1)
        set_runname(runnames[n][0])

n = 0
set_runname(runnames[0][0])
win.show_all()
win.set_size_request(300, 300)
win.present()

win.connect('destroy', lambda w :gtk.main_quit())
win.connect('key-press-event', lambda w, e : proc_key(e))
tree_view.connect('cursor-changed', lambda t :\
    chg_by_cursor())
gtk.main()
