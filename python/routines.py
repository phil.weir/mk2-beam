from solve import Solver, eval_z
import shutil
import math
import time
from subprocess import Popen
import mk2beam
import sys
import os
import argparse
from numpy import *
from dolfin import *
from multiprocessing import Process, Queue

def linearize(fn, linear_V=None) :
    if linear_V is None :
        linear_V = FunctionSpace(fn.function_space().mesh(), "Lagrange", 1)
    fnl = Function(linear_V)
    fnl.interpolate(fn)
    return fnl

timing = None
def skew(mesh1, bmesh1, zeta_1_old, zeta_1,
         mesh2, bmesh2, zeta_2_old, zeta_2,
         mesh3, bmesh3, zeta_3_old, zeta_3,
         depth, beam, log_now,
         top_right_index, beam_left_index, beam_right_index, top_left_index,
         skew_cutoff, skew_vert_cutoff, left, right,
         file_mesh1, file_mesh2, file_mesh3,
         beam_left_index_b, beam_right_index_b,
         top_left_index_t, top_right_index_t
        ) :
       # Deal with mesh
       #   Move linearly
       timing.click("mesh move")

       mesh_move(mesh1, zeta_1_old, zeta_1, depth)
       update_boundary_mesh(mesh1, bmesh1)

       mesh_move(mesh2, zeta_2_old, zeta_2, depth, beam.draft)
       update_boundary_mesh(mesh2, bmesh2)

       mesh_move(mesh3, zeta_3_old, zeta_3, depth)
       update_boundary_mesh(mesh3, bmesh3)

       if log_now :
           #   Output moved mesh
           file_mesh1 << mesh1
           file_mesh2 << mesh2
           file_mesh3 << mesh3

       #   Skew for corners
       skew_1 = 0
       skew_3 = 0
       top_right_height = bmesh1.coordinates()[top_right_index][1]
       beam_left_height = bmesh2.coordinates()[beam_left_index][1]
       beam_right_height = bmesh2.coordinates()[beam_right_index][1]
       top_left_height = bmesh3.coordinates()[top_left_index][1]
       
       mesh1.intersection_operator().clear()
       update_boundary_mesh(mesh1, bmesh1)
       mesh2.intersection_operator().clear()
       update_boundary_mesh(mesh2, bmesh2)
       mesh3.intersection_operator().clear()
       update_boundary_mesh(mesh3, bmesh3)

       zeta_1_old.interpolate(zeta_1)
       beam.clstep()
       zeta_2_old.interpolate(zeta_2)
       zeta_3_old.interpolate(zeta_3)

       if log_now :
           #   Output moved mesh
           file_mesh1 << mesh1
           file_mesh2 << mesh2
           file_mesh3 << mesh3

       #       1
       if top_right_height >= beam_left_height :
           skew_1, deskew_1, gamma_top_1, delta_mp = \
               do_skew(mesh1, bmesh1, depth, zeta_1, 0., top_right_index,
               bmesh2.coordinates()[beam_left_index], left-skew_cutoff,
               skew_vert_cutoff, left, nudgable=True)
           skew_1 = 1 if skew_1 else 0
           mesh1.intersection_operator().clear()
           update_boundary_mesh(mesh1, bmesh1)
       elif top_right_height < beam_left_height :
           skew_1, deskew_1, gamma_top_1, delta_mp = \
               do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_left_index,
               bmesh1.coordinates()[top_right_index], left+skew_cutoff,
               skew_vert_cutoff, left, nudgable=True)
           #do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_left_index,
           skew_1 = 2 if skew_1 else 0
           mesh2.intersection_operator().clear()
           update_boundary_mesh(mesh2, bmesh2)
       else :
           gamma_top_1 = top_right_height

       #       3
       if top_left_height >= beam_right_height :
           skew_3, deskew_3, gamma_top_3, delta_mp = \
               do_skew(mesh3, bmesh3, depth, zeta_3, 0., top_left_index,
               bmesh2.coordinates()[beam_right_index], right+skew_cutoff,
               skew_vert_cutoff, right, nudgable=True)
           skew_3 = 1 if skew_3 else 0
           mesh3.intersection_operator().clear()
           update_boundary_mesh(mesh3, bmesh3)
       elif top_left_height < beam_right_height :
           skew_3, deskew_3, gamma_top_3, delta_mp = \
               do_skew(mesh2, bmesh2, depth, zeta_2, beam.draft, beam_right_index,
               bmesh3.coordinates()[top_left_index], right-skew_cutoff,
               skew_vert_cutoff, right, nudgable=True)
           skew_3 = 2 if skew_3 else 0
           mesh2.intersection_operator().clear()
           update_boundary_mesh(mesh2, bmesh2)
       else :
           gamma_top_3 = top_left_height
       timing.clock()

       return skew_1, deskew_1, gamma_top_1, skew_3, deskew_3, gamma_top_3

def bdy_average(dirichlet_2, beam_left_index_b, dirichlet_1, top_right_index_t,
                x=10, draft=0., do=True) :
    xp = zeros(1)
    p1 = zeros(1)
    p2 = zeros(1)
    xp[0] = x
    dirichlet_1.eval(p1, xp)
    dirichlet_2.eval(p2, xp)

    av = (p1[0]+p2[0]-draft)/2

    if do :
        dirichlet_2.vector()[beam_left_index_b] = av+draft

    dirichlet_1.vector()[top_right_index_t] = av

    return av

def straighten(skew_1, deskew_1, mesh1, bmesh1,
                       mesh2, bmesh2,
                       skew_3, deskew_3, mesh3, bmesh3) :
    if skew_1 == 1 :
        for i in range(0, len(mesh1.coordinates())) :
            if i in deskew_1 :
                (mesh1.coordinates()[i])[1] = deskew_1[i]
        mesh1.intersection_operator().clear()
        update_boundary_mesh(mesh1, bmesh1)
    elif skew_1 == 2 :
        for i in range(0, len(mesh2.coordinates())) :
            if i in deskew_1 :
                (mesh2.coordinates()[i])[1] = deskew_1[i]
        mesh2.intersection_operator().clear()
        update_boundary_mesh(mesh2, bmesh2)
    if skew_3 == 1 :
        for i in range(0, len(mesh3.coordinates())) :
            if i in deskew_3 :
                (mesh3.coordinates()[i])[1] = deskew_3[i]
        mesh3.intersection_operator().clear()
        update_boundary_mesh(mesh3, bmesh3)
    elif skew_3 == 2 :
        for i in range(0, len(mesh2.coordinates())) :
            if i in deskew_3 :
                (mesh2.coordinates()[i])[1] = deskew_3[i]
        mesh2.intersection_operator().clear()
        update_boundary_mesh(mesh2, bmesh2)

class NormalDerivative(Expression) :
    def __init__(self, phi, zetas, line_cell_to_mesh_cell,
                line_cell_to_mesh_line, draft) :
        self.mesh = phi.function_space().mesh()
        self.phi = phi
        self.gpfs = VectorFunctionSpace(self.mesh, "Lagrange", 1)
        self.grad_phi = project(grad(phi), self.gpfs)
        self.cell_map = line_cell_to_mesh_cell
        self.facet_map = line_cell_to_mesh_line
        self.zeta, self.zetax = zetas
        self.zeta = linearize(self.zeta)
        self.draft = draft
    def eval_cell(self, values, x, ufc_cell) :
        cell = Cell(self.mesh, self.cell_map[ufc_cell.index])
        mesh_conn = self.mesh.topology()(2,1)
        m = list(mesh_conn(self.cell_map[ufc_cell.index])).index(\
            self.facet_map[ufc_cell.index])
        n = cell.normal(m)
        gp = zeros(2)
        xv = zeros(2)
        x2 = zeros(1)
        z = zeros(1)
        #if x[0] < 10 :
        #    dz = 1e-5
        #    dx = 1e-5

        #    if x[0] + 0.5*dx > 10. :
        #        o = -0.5*dx
        #    if x[0] + 0.5*dx < 10. :
        #        o =  0.5*dx

        #    l = x[0] - 0.5*dx + o
        #    r = x[0] + 0.5*dx + o
        #    x2[0] = l
        #    self.zeta.eval(z, x2)
        #    zl = z[0]
        #    x2[0] = r
        #    self.zeta.eval(z, x2)
        #    zr = z[0]

        #    zetax = (zr-zl)/dx
        #    
        #    xv[0] = x[0]
        #    z = zeros(1)
        #    self.zeta.eval(z, x)
        #    xv[1] = z[0]-1e-6-self.draft

        #    self.phi.eval(z, xv)
        #    v0 = z[0]
        #    xv[0] -=  -dz*zetax/sqrt(1+zetax*zetax)
        #    xv[1] -=  dz/sqrt(1+zetax*zetax)
        #    if xv[0] < 0. :
        #        xv[0] = 1e-15
        #    elif xv[0] > 10. :
        #        xv[0] = 10.-1e-14
        #    self.phi.eval(z, xv)
        #    v1 = z[0]

        #    values[0] = sqrt(1+zetax*zetax)*(v0-v1)/dz;

        #else :
        xv[0] = x[0]
        self.zeta.eval(z, x)
        xv[1] = z[0]-1e-9-self.draft
        try: 
            self.grad_phi.eval(gp, xv)
        except :
            print xv
            print "COULDN'T SURVIVE GRAD_PHI"
            quit()
        self.zetax.eval(z, x)
        values[0] = n.dot(Point(*gp))*sqrt(1+z[0]*z[0])
        #values[0] = gp[1]
        #values[0] = n[0]
        #mesh_conn = self.mesh.topology()(1,0)
        #values[0] = Facet(self.mesh, self.facet_map[ufc_cell.index]).midpoint().x()
        #values[0] = Cell(self.zeta.function_space().mesh(),ufc_cell.index).midpoint().x()
        #print "%lf,%lf,%lf" % (x[0], values[0], ufc_cell.index)
        #values[0] = ufc_cell.index
        #values[0] = self.mesh.geometry().point(mesh_conn(self.facet_map[ufc_cell.index])[1]).y()
        #values[0] = z[0]
        #values[0] =\
        #    self.mesh.coordinates()[mesh_conn(self.cell_map[ufc_cell.index])[0]][1]

EPS = 1e-12
DZ = 0.00005
#FIX FOR UNDERBEAM
class NormalDerivative2(Expression) :
    def __init__(self, phi, zeta, cphi, plate, length) :
        self.phi = phi
        self.zeta = zeta
        self.zeta_linear = linearize(zeta)
        self.cphi = cphi
        self.plate = plate
        self.length = length
    def eval_cell(self, values, x, ufc_cell) :
        p1 = zeros(2)
        p2 = zeros(2)
        z = zeros(1)
        z2 = zeros(1)
        x2 = zeros(1)
        phi = self.phi
        p1[0] = x[0]
        p2[0] = x[0]
        dz = DZ
        dx = dz
        self.zeta.eval(z, x)
        self.zeta_linear.eval(z, x)
        p1[1] = z[0] - dz*1e-2
        p2[1] = z[0] - dz*1e-2 - dz

        o = 0

        if x[0] < 0.5*dx or (x[0]-.5*dx >= self.plate.left and x[0]-.5*dx <= self.plate.right) :
            o = 0.5*dx;
        
        if x[0] > self.length-0.5*dx or (x[0]+.5*dx >= self.plate.left and x[0]+.5*dx <= self.plate.right) :
            o = -0.5*dx;

        x2[0] = x[0]+.5*dx+o
        self.zeta.eval(z, x2)
        x2[0] = x[0]-.5*dx+o
        self.zeta.eval(z2, x2)
        zetax = (z[0] - z2[0])/dx

        x2[0] = x[0]+.5*dx+o
        self.cphi.eval(z, x2)
        x2[0] = x[0]-.5*dx+o
        self.cphi.eval(z2, x2)
        cphix = (z[0] - z2[0])/dx

        v1 = zeros(1)
        v2 = zeros(1)

        self.phi.eval(v1, p1)

        n = zeros(2)
        n[0] = -zetax / sqrt(1+zetax*zetax)
        n[1] = 1/sqrt(1+zetax*zetax)

        p2[0] = p1[0] - dz*n[0]
        p2[1] = p1[1] - dz*n[1]
            
        underplate = (p2[0] >= self.plate.left - EPS and p2[0] <= self.plate.right + EPS)

        if p2[0] <= 0 or underplate or p2[0] >= self.length :
            p2[0] = p1[0]
            p2[1] = p1[1] - dz/sqrt(1+zetax*zetax)

            phi.eval(v2, p2)

            underplate = False
            phiz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz
            values[0] = (1+zetax*zetax)*phiz - cphix*zetax

        else :
            phi.eval(v2, p2)
            values[0] = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz
            #values[0] = v1[0]

class free_surface_under_beam_cl(SubDomain) :
    def __init__(self, beam, fszeta, not_first) :
        self.not_first = not_first
        SubDomain.__init__(self)
        self.beam = beam
        self.fszeta = fszeta
        self.pv = zeros(1)
        self.zv = zeros(1)
    def inside(self, x, on_boundary) :
        return False #RMV
        if self.fszeta is None :
            return False

        self.fszeta.eval(self.pv, x)
        self.beam.Wn1.eval(self.zv, x)
        return self.pv[0] + EPS < self.zv[0]

class free_surface_under_beam_zero_cl(SubDomain) :
    def inside(self, x, on_boundary) :
        return True

class W_to_zeta(Expression) :
    def __init__(self, p, W, zeta, zeta_old, fs_subdom) :
        Expression.__init__(self)
        self.p = p
        self.W = W
        self.zeta = zeta
        self.zeta_old = zeta_old
        self.pv = zeros(1)
        self.zov = zeros(1)
        self.fs_subdom = fs_subdom
    def eval(self, values, x) :
        pv = zeros(1)
        self.zeta.eval(pv, x)
        self.W.eval(values, x)

        if pv[0] < values[0] and self.fs_subdom.inside(x, False) :
            values[0] = pv[0]

class fs_beam(mk2beam.Plate) :
    zeta_on = False
    def __init__(self, beamline, beam_desc, dt, rhow, g, beamer, name, sbmg, zeta_on):
        mk2beam.Plate.__init__(self, beamline, beam_desc, dt, rhow, g, beamer, name,
            sbmg)
        self.Dt = dt
        self.zeta_on = zeta_on
        self.beta = beam_desc.beta
        self.g = g

        V = self.Wn1.function_space()
        self.zetan1 = Function(V)
        self.zetan1x = Function(V)
        self.zetan1xx = Function(V)
        self.zetan1_old = Function(V)
        self.zetadtn1 = Function(V)
        self.zetaddtn1 = Function(V)
        self.zetan = Function(V)
        self.zetadotn = Function(V)
        self.zetaddotn = Function(V)
        self.zetaddtkm1 = Function(V)

        self.zetan1.interpolate(self.Wn1)
        self.zetadtn1.interpolate(self.Wdtn1)
        self.zetaddtn1.interpolate(self.Wddtn1)
        self.zetan.interpolate(self.Wn)
        self.zetadotn.interpolate(self.Wdotn)
        self.zetaddotn.interpolate(self.Wddotn)
    def step(self, W_to_zeta, zeta=None, do_adj=True) :
        mk2beam.Plate.step(self)

        if self.zeta_on and do_adj:
            Dt = self.Dt
            beta = self.beta
            #print '-'*20
            new_zeta = W_to_zeta
            #print '='*20
            self.zetan1.interpolate(new_zeta)
            #Neumann beta (should match beam)
            self.zetaddtn1.interpolate(project(\
                1/(beta*Dt*Dt)*(self.zetan1 - self.zetan - \
                Dt*self.zetadotn - (0.5-beta)*Dt*Dt*self.zetaddotn),
                self.zetaddtn1.function_space()))
            self.zetadtn1.interpolate(project(\
                self.zetadotn + (Dt/2)*self.zetaddotn + (Dt/2)*self.zetaddtn1,
                self.zetadtn1.function_space()))
        else :
            self.zetan1.interpolate(self.Wn1)
            self.zetadtn1.interpolate(self.Wdtn1)
            self.zetaddtn1.interpolate(self.Wddtn1)
    def nonstep(self) :
        mk2beam.Plate.nonstep(self)
        self.zetaddtn1.interpolate(self.zetaddotn)
        self.zetadtn1.interpolate(self.zetadotn)
        self.zetan1.interpolate(self.zetan)
    def clstep(self) :
        mk2beam.Plate.clstep(self)
        self.zetan1_old.interpolate(self.zetan1)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
    def adjacc(self, alpha) :
        mk2beam.Plate.adjacc(self, alpha)
        f = project(alpha * self.zetaddtn1 + (1-alpha) * self.zetaddtkm1, self.zetaddtn1.function_space())
        self.zetaddtn1.interpolate(f)
    def timestep(self) :
        mk2beam.Plate.timestep(self)
        self.zetaddtkm1.interpolate(self.zetaddtn1)
        self.zetaddotn.interpolate(self.zetaddtn1)
        self.zetadotn.interpolate(self.zetadtn1)
        self.zetan.interpolate(self.zetan1)
    def save_function_vectors(self) :
        mk2beam.Plate.save_function_vectors(self)
        File(self.name + "_zetan1.xml") << self.zetan1.vector()
        File(self.name + "_zetan1x.xml") << self.zetan1x.vector()
        File(self.name + "_zetan1xx.xml") << self.zetan1xx.vector()
        File(self.name + "_zetadtn1.xml") << self.zetadtn1.vector()
        File(self.name + "_zetaddtn1.xml") << self.zetaddtn1.vector()
        File(self.name + "_zetan1_old.xml") << self.zetan1_old.vector()
        File(self.name + "_zetaddtkm1.xml") << self.zetaddtkm1.vector()
        File(self.name + "_zetaddotn.xml") << self.zetaddotn.vector()
        File(self.name + "_zetadotn.xml") << self.zetadotn.vector()
        File(self.name + "_zetan.xml") << self.zetan.vector()
    def load_function_vectors(self) :
        mk2beam.Plate.load_function_vectors(self)

        if os.path.exists(self.name + "_zetan1.xml") :
            File(self.name + "_zetan1.xml") >> self.zetan1.vector()
            File(self.name + "_zetan1x.xml") >> self.zetan1x.vector()
            File(self.name + "_zetan1xx.xml") >> self.zetan1xx.vector()
            File(self.name + "_zetadtn1.xml") >> self.zetadtn1.vector()
            File(self.name + "_zetaddtn1.xml") >> self.zetaddtn1.vector()
            File(self.name + "_zetan1_old.xml") >> self.zetan1_old.vector()
            File(self.name + "_zetaddtkm1.xml") >> self.zetaddtkm1.vector()
            File(self.name + "_zetaddotn.xml") >> self.zetaddotn.vector()
            File(self.name + "_zetadotn.xml") >> self.zetadotn.vector()
            File(self.name + "_zetan.xml") >> self.zetan.vector()
        else :
            self.zetan1.interpolate(self.Wn1)
            Wn1x = project(Dx(Function(beam.Wn1.function_space(), beam.Wn1), 0))
            self.zetan1x.interpolate(Wn1x)
            Wn1xx = project(Dx(Dx(Function(beam.Wn1.function_space(), beam.Wn1),
                0), 0))
            self.zetan1xx.interpolate(Wn1xx)
            self.zetadtn1.interpolate(self.Wdtn1)
            self.zetaddtn1.interpolate(self.Wddtn1)
            self.zetan1_old.interpolate(self.Wn1old)
            self.zetaddtkm1.interpolate(self.Wddtkm1)
            self.zetaddotn.interpolate(self.Wddotn)
            self.zetadotn.interpolate(self.Wdotn)
            self.zetan.interpolate(self.Wn)
    def backup_functions(to) :
        filenames = []
        filenames.append(self.name + "_zetan1.xml")
        filenames.append(self.name + "_zetan1x.xml")
        filenames.append(self.name + "_zetan1xx.xml")
        filenames.append(self.name + "_zetadtn1.xml")
        filenames.append(self.name + "_zetaddtn1.xml") 
        filenames.append(self.name + "_zetan1_old.xml")
        filenames.append(self.name + "_zetaddtkm1.xml")
        filenames.append(self.name + "_zetaddotn.xml") 
        filenames.append(self.name + "_zetadotn.xml")
        filenames.append(self.name + "_zetan.xml")

        for filename in filenames :
            shutil.copyfile(filename, filename+'.'+str(to))


class FSLineFromMesh(Expression) :
    def __init__(self, fn, fs, draft=0.) :
        Expression.__init__(self)
        self.fs = linearize(fs)
        self.fn = fn
        self.draft = draft
    def eval(self, values, x) :
        x2d = zeros(2)
        x2d[0] = x[0]
        x2d[1] = eval_z(self.fs, x[0])-1e-10-self.draft
        self.fn.eval(values, x2d)
        
def set_cpdt(cpdt, zeta, draft=0.) :
    for i in range(0, len(cpdt.vector())) :
        cpdt.vector()[i] = -9.81*(zeta.vector()[i]-draft)

def update_free_surface_b(phi, cphi_new, cphi_m1, cphi, zeta, zeta_old,
            bmesh_to_mesh, top_to_bmesh,
            zeta_m1, zdt, zdtm1, zdtm2, zdtm3, dt,
            cpdt, cpdtm1, cpdtm2, cpdtm3,
            beam, domain_right, draft=0., phinonz_file=None):
    cphi.interpolate(cphi_new)
    zeta_old.interpolate(zeta)

    #num_top_points = len(cphi_new.function_space().mesh().coordinates())
    #if phinonz_file :
    #    phinonz_file << cphi_new
    zdt.interpolate(NormalDerivative2(phi, zeta,
        cphi_new, beam, domain_right))
    #cphi_new.interpolate(FSLineFromMesh(phi, zeta, draft))
    correct(dt, zeta, zeta_m1, zdt, zdtm1, zdtm2, zdtm3)
    set_cpdt(cpdt, zeta, draft)
    #cpdt.interpolate(project(-9.81*(zeta-draft), zeta.function_space()))
    correct(dt, cphi_new, cphi_m1, cpdt, cpdtm1, cpdtm2, cpdtm3)
    #cphi_a = zeros(num_top_points)
    #phi.vector().get_local(cphi_a, array(map(lambda i :
    #    bmesh_to_mesh[top_to_bmesh[i]],
    #    range(0, num_top_points)), dtype="I"))
    #cphi_new.vector().set_local(cphi_a)

    #zetaf = zeta_m1 + (dt/24.)*(9*zdt + 19*zdtm1 - 5*zdtm2 + zdtm3)
    #zeta.interpolate(project(zetaf, zeta.function_space()))

def update_free_surface(phi, cphi_new, cphi_m1, cphi, zeta, zeta_old,
            bmesh_to_mesh, top_to_bmesh, draft=0., do_init=False, zetan=None) :
    cphi.interpolate(cphi_new)
    zeta_old.interpolate(zeta)

    num_top_points = len(cphi_new.function_space().mesh().coordinates())
    cphi_new.interpolate(FSLineFromMesh(phi, zeta, draft))
    #cphi_a = zeros(num_top_points)
    #phi.vector().get_local(cphi_a, array(map(lambda i :
    #    bmesh_to_mesh[top_to_bmesh[i]],
    #    range(0, num_top_points)), dtype="I"))
    #cphi_new.vector().set_local(cphi_a)

    zetaf = (1/(9.81*dt)) * (cphi_m1 - cphi_new) + draft
    zeta.interpolate(project(zetaf, zeta.function_space()))

def do_skew(mesh, bmesh, depth, zeta, draft, moving_index, fixed_point, horiz_cutoff,
            vert_cutoff, boundary, nudgable=False) :
        closest_facet = bmesh.closest_cell(Point(*fixed_point))
        nearby_points = list(bmesh.topology()(1,0)(closest_facet))
        moving_top = bmesh.coordinates()[moving_index]
        nearby_points.sort(lambda i, j : \
            cmp((bmesh.coordinates()[i])[1], (bmesh.coordinates()[j])[1]))
        if abs(bmesh.coordinates()[nearby_points[0]][1]-fixed_point[1]) < \
           abs(bmesh.coordinates()[nearby_points[1]][1]-fixed_point[1]) :
            chosen_point = nearby_points[0]
        else :
            chosen_point = nearby_points[1]

        delta_mp = 0.
        skew = False
        deskew = {}
        if chosen_point == moving_index :
            if abs(moving_top[1]-fixed_point[1]) < vert_cutoff and nudgable :
                move_from = bmesh.coordinates()[chosen_point][1]
                skew = True
                for i in range(0, len(mesh.coordinates())) :
                    # Don't skew horiz neighbours in this case, as we should be
                    # close enough anyhow and we want to avoid using zeta
                    if abs(mesh.coordinates()[i][0]-boundary) < 1e-4 :
                        cur = mesh.coordinates()[i]
                        deskew[i] = cur[1]
                        (mesh.coordinates()[i])[1] += \
                            (cur[0]-horiz_cutoff)*(fixed_point[1]-move_from)*(cur[1]+depth)/((depth+move_from)*(boundary-horiz_cutoff))
                return (skew, deskew, fixed_point[1], delta_mp)
            chosen_point = nearby_points[0]

        if abs(bmesh.coordinates()[chosen_point][1]-fixed_point[1]) > EPS :
            move_from = bmesh.coordinates()[chosen_point][1]
            skew = True
            z = zeros(1)
            x = zeros(1)
            for i in range(0, len(mesh.coordinates())) :
                if abs(mesh.coordinates()[i][0]-boundary) < abs(horiz_cutoff-boundary) :
                    cur = mesh.coordinates()[i]
                    deskew[i] = cur[1]
                    x[0] = cur[0]
                    zeta.eval(z, x)
                    z[0] -= draft
                    skew_from =\
                        (z[0]+depth)*(move_from+depth)/(moving_top[1]+depth)-depth
                    skew_fixed =\
                        (z[0]+depth)*(fixed_point[1]+depth)/(moving_top[1]+depth)-depth
                    fm = cur[1]
                    if abs(fm-z[0]) <= EPS :
                        delta = 0.
                    elif fm > skew_from + EPS :
                        delta = (skew_fixed-skew_from)*(z[0]-fm)/(z[0]-skew_from)
                    elif fm < skew_from - EPS :
                        delta = (skew_fixed-skew_from)*(depth+fm)/(depth+skew_from)
                    else :
                        delta = skew_fixed-skew_from

                    if abs(cur[0] - boundary) <= EPS :
                        if abs(fm - skew_from) <= EPS :
                            (mesh.coordinates()[i])[1] = skew_fixed
                        else :
                            (mesh.coordinates()[i])[1] += delta
                    else :
                        (mesh.coordinates()[i])[1] += (cur[0]-horiz_cutoff)*delta/(boundary-horiz_cutoff)

        return (skew, deskew, fixed_point[1], delta_mp)

# Define function for projecting a 1D mesh embedded in 2D
# onto the x-axis
def flatten(mesh, line_to_bmesht) :
    mesh.order()
    num_points = len(mesh.coordinates())
    mesh.init(0,1)
    conn = mesh.topology()(0,1)
    line = Mesh()
    editor_flat = MeshEditor()
    editor_flat.open(line, "interval", 1, 1)
    editor_flat.init_vertices(num_points)
    editor_flat.init_cells(num_points-1)
    order = []
    i = 0
    for vit in vertices(mesh) :
        order.append((mesh.geometry().point(vit.index()).x(), i))
        i += 1
    print '#'*10
    order.sort(lambda c,d : cmp(c[0], d[0]))
    mp = {}
    rmp = {}
    for i in range(0, num_points) :
        mp[i] = order[i][1]
        rmp[order[i][1]] = i
    for i in range(0, num_points) :
        editor_flat.add_vertex(i, order[i][0])
    for i in range(0, num_points-1) :
        editor_flat.add_cell(i, i, i+1)
    editor_flat.close()

    line_to_bmesh = MeshFunction("uint", line, 0)
    for i in range(0, len(line.coordinates())) :
        line_to_bmesh[i] = line_to_bmesht[mp[i]]

    V_line = FunctionSpace(line, "Lagrange", 2)
    return line, line_to_bmesh, V_line

# Define function for moving the mesh top from zeta_old to zeta
def mesh_move(mesh, zeta_old, zeta, depth, draft = 0.) :
    zeta_lin = linearize(zeta)
    zeta_old_lin = linearize(zeta_old, zeta_lin.function_space())
    zold = zeros(1)
    znew = zeros(1)
    x = zeros(1)
    coords = mesh.coordinates()
    for i in range(0, len(coords)) :
        x[0] = coords[i][0]
        zeta_old_lin.eval(zold, x)
        zo = zold[0]
        zeta_lin.eval(znew, x)
        zn = znew[0]

        if abs(zo-coords[i][1]-draft) < 1e-8 :
            coords[i][1] = zn-draft
        else :
            coords[i][1] += (zn-zo)*(coords[i][1]+depth)/(zo-draft+depth)
    mesh.intersection_operator().clear()

def generate_mfs (bmesh, topline, mesh, top_to_bmesh) :
    bmesh_to_mesh_cell = bmesh.cell_map()
    top_conn = topline.topology()(1,0)
    mesh_conn = mesh.topology()(1,2)
    mesh_conn_rev = mesh.topology()(2,1)
    bmesh.init(0,1)
    bmesh_conn = bmesh.topology()(0,1)
    k = 0
    topline_cell_to_mesh_cell = MeshFunction("uint", topline, 1)
    topline_cell_to_mesh_line = MeshFunction("uint", topline, 1)
    for i in range(0, topline.num_cells()) :
        v1, v2 = top_conn(i)
        cell_on_boundary =\
            list(set(bmesh_conn(top_to_bmesh[v1])).intersection(set(bmesh_conn(top_to_bmesh[v2]))))[0]
        topline_cell_to_mesh_cell[i] = mesh_conn(bmesh_to_mesh_cell[cell_on_boundary])[0]
        topline_cell_to_mesh_line[i] =\
            bmesh_to_mesh_cell[cell_on_boundary]
    return topline_cell_to_mesh_cell, topline_cell_to_mesh_line

# Define function to update the boundary mesh coordinates to
# match a full mesh
def update_boundary_mesh(mesh, bmesh) :
    vertex_map = bmesh.vertex_map()
    for i in range(0, len(bmesh.coordinates())) :
        bmesh.coordinates()[i] = mesh.coordinates()[vertex_map[i]]
    bmesh.intersection_operator().clear()

class ic_cl(Expression) :
    def __init__(self, ic_params, left, right) :
        Expression.__init__(self)
        self.ic_symmetric = ic_params['symmetric']
        self.ic_left = ic_params['left']
        self.ic_amp = ic_params['amp']
        self.ic_offset = ic_params['offset']
        self.ic_vert_offset = ic_params['vert_offset']
        self.ic_width = ic_params['width']
        self.left = left
        self.right = right
    def eval(self, values, x) :
        if self.ic_symmetric :
            x = (self.left+self.right)*.5 - abs(x[0] - (self.left+self.right)*.5)
        else :
            x = x[0]

        if (x < self.ic_left) :
            values[0] =\
            self.ic_amp*.5*(cos(pi*(self.ic_offset)/self.ic_width)+1)+self.ic_vert_offset
        elif (x < self.ic_left+self.ic_width) :
            values[0] =\
            self.ic_amp*.5*(cos(pi*(x-self.ic_left+self.ic_offset)/self.ic_width)+1)+self.ic_vert_offset
        else :
            values[0] =\
            self.ic_amp*.5*(cos(pi*(self.ic_width+self.ic_offset)/self.ic_width)+1)+self.ic_vert_offset

# Define functions on (time-independent) 1D meshes
def make_functions(V_topline, ice=None) :
    #   Primary functions
    cphi = Function(V_topline)
    cphi_m1 = Function(V_topline)
    cphi_new = Function(V_topline)
    zeta = Function(V_topline)
    zeta_m1 = Function(V_topline)
    zeta_old = Function(V_topline)
    #   Time derivatives
    cphidt = Function(V_topline)
    zetadt = Function(V_topline)
    #   Auxiliary functions
    dirichlet = Function(V_topline)
    
    # Set initial conditions
    cphi_new.interpolate(Constant(0.))
    cphi_m1.interpolate(Constant(0.))
    zeta.interpolate(Constant(0.))
    zeta_m1.interpolate(zeta)
    zeta_old.interpolate(zeta)
    if ice is not None :
        zeta.interpolate(ice)
        zeta_m1.interpolate(ice)
        zeta_old.interpolate(ice)
    zetax = project(zeta.dx(0))
    zetaxx = project(zetax.dx(0))
        #cphi.interpolate(Constant(1.))

    return cphi, cphi_new, cphi_m1, \
            zeta, zeta_m1, zeta_old, zetax, \
           zetaxx, cphidt, zetadt, dirichlet

# Load functions for top meshes
def load_functions(V_topline, num) :

    cphi, cphi_new, cphi_m1, zeta, zeta_m1, zeta_old,\
    zetax, zetaxx, cphidt, zetadt,\
    dirichlet =\
        make_functions(V_topline, ice=None)
    
    #   Primary functions
    File("cphi.%d.xml" % num) >> cphi.vector()
    File("cphi_new.%d.xml" % num) >> cphi_new.vector()
    File("cphi_m1.%d.xml" % num) >> cphi_m1.vector()
    File("zeta.%d.xml" % num) >> zeta.vector()
    File("zeta_m1.%d.xml" % num) >> zeta_m1.vector()
    File("zeta_old.%d.xml" % num) >> zeta_old.vector()
    File("zetax.%d.xml" % num) >> zetax.vector()
    File("zetaxx.%d.xml" % num) >> zetaxx.vector()
    File("cphidt.%d.xml" % num) >> cphidt.vector()
    File("zetadt.%d.xml" % num) >> zetadt.vector()

    return cphi, cphi_new, cphi_m1, \
            zeta, zeta_m1, zeta_old, zetax, \
            zetaxx, cphidt, zetadt, dirichlet

# Load functions for top meshes
def save_functions(V_topline, num, cphi, cphi_new, cphi_m1,
           zeta, zeta_m1, zeta_old, zetax, zetaxx,
           cphidt, zetadt, dirichlet) :
    
    #   Primary functions
    File("cphi.%d.xml" % num) << cphi.vector()
    File("cphi_new.%d.xml" % num) << cphi_new.vector()
    File("cphi_m1.%d.xml" % num) << cphi_m1.vector()
    File("zeta.%d.xml" % num) << zeta.vector()
    File("zeta_m1.%d.xml" % num) << zeta_m1.vector()
    File("zeta_old.%d.xml" % num) << zeta_old.vector()
    File("zetax.%d.xml" % num) << zetax.vector()
    File("zetaxx.%d.xml" % num) << zetaxx.vector()
    File("cphidt.%d.xml" % num) << cphidt.vector()
    File("zetadt.%d.xml" % num) << zetadt.vector()

def backup_functions(num, to) :
    filenames = []
    filenames.append("cphi.%d.xml" % num)
    filenames.append("cphi_new.%d.xml" % num)
    filenames.append("cphi_m1.%d.xml" % num)
    filenames.append("zeta.%d.xml" % num)
    filenames.append("zeta_m1.%d.xml" % num)
    filenames.append("zeta_old.%d.xml" % num)
    filenames.append("zetax.%d.xml" % num)
    filenames.append("zetaxx.%d.xml" % num)
    filenames.append("cphidt.%d.xml" % num)
    filenames.append("zetadt.%d.xml" % num)

    for filename in filenames :
        shutil.copyfile(filename, filename+'.'+str(to))

def load_state() :
    t = 0.
    titeration = 0
    cl_last = 0

    f = open('state.st', 'r')
    line = f.readline()
    t = float(line.split(':')[-1])
    line = f.readline()
    titeration = int(line.split(':')[-1])
    line = f.readline()
    cl_last = int(line.split(':')[-1])

    return t, titeration, cl_last

def save_state(t, titeration, cl_last) :
    f = open('state.st', 'w')
    f.write("t:%lf\n" % t)
    f.write("titeration:%d\n" % titeration)
    f.write("cl_last:%d\n" % cl_last)

def get_args() :
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-r', '--runname')
    argparser.add_argument('-l', '--load', action='store_true')
    argparser.add_argument('-n', '--no-save', action='store_true')
    argparser.add_argument('-s', '--silent', action='store_true')
    argparser.add_argument('-f', '--frequency')
    argparser.add_argument('--stop-after')
    argparser.add_argument('--output-ci', action='store_true')
    args = argparser.parse_args()
    
    if args.runname is None :
        print "Please supply runname : -r RUN"
        sys.exit(3)
    
    stop_after = None
    if args.stop_after is not None :
        print "Stopping at iteration " + args.stop_after
        stop_after = int(args.stop_after)
    
    frame_frequency = 1
    if args.frequency is not None :
        frame_frequency = int(args.frequency)
    
    return str(args), stop_after, args.load, args.no_save, args.runname, args.output_ci, args.silent, frame_frequency

def setup_dir(runname, builddir) :
    try :
        with open("scripts/outroot.sh", 'r') as f :
            outroot = f.readlines()[0]
            outroot = outroot.split('=')[1].strip()
            outdir = outroot + '/output.' + runname
    except :
        print "Could not establish outroot; are you definitely running from build root?"
    
    try :
        if not os.path.exists(outdir) :
            os.mkdir(outdir)
        for path in ('/phi', '/adda/phidt', '/add/mesh', '/source') :
            if not os.path.exists(outdir+path) :
                os.makedirs(outdir+path)
    except error :
        print str(error)
        sys.exit(5)
    
    for source_file in os.listdir(builddir+'/python') :
        try :
            shutil.copyfile(builddir+'/python/'+source_file,
                outdir+'/source/'+source_file)
        except :
            pass
    
    os.chdir(outdir)

def get_params(runname, logfile) :
    # Settings
    try :
        pf = File("params/parameters.xml")
        p = Parameters("mk3")
        pf >> p
    except :
        print "Could not open params/parameters.xml in output directory"
        sys.exit(4)
    
    T = p.final_time
    dt = p.timestep
    depth = p.depth
    initial_draft = p.initial_draft
    domain_left = p.domain_left
    left = p.left
    right = p.right
    domain_right = p.domain_right
    refinement_level = p.mesh_refinement_level
    vertical_cells = p.mesh_vertical_cells
    horiz_cell_density = p.mesh_horizontal_cell_density
    rat = p.mesh_rat
    rhow = p.density_fluid
    ic_left = p.ic_left
    ic_width = p.ic_width
    ic_amp = p.ic_amp
    #print ic_amp
    
    if p.has_key('ic_offset') :
        ic_offset = p.ic_offset
    else :
        ic_offset = 0.
    
    if p.has_key('ic_vert_offset') :
        ic_vert_offset = p.ic_vert_offset
    else :
        ic_vert_offset = 0.
    
    if p.has_key('ic_symmetric') :
        ic_symmetric = p.ic_symmetric
    else :
        ic_symmetric = False
    
    if p.has_key('alpha') :
        alpha = p.alpha
    else :
        alpha = .001
    
    if p.has_key('beam_youngs') :
        E = p.beam_youngs
    else :
        E = 8e9
    
    if p.has_key('beam_type') :
        beam_type = p.beam_type
    else :
        beam_type = "Euler-Bernoulli"
    
    if p.has_key('ci_min') :
        ci_min = p.ci_min
    else :
        ci_min = 20
    
    if p.has_key('ci_lim') :
        ci_lim = p.ci_lim
    else :
        ci_lim = 100
    
    if p.has_key('mu') :
        mu = p.mu
    else :
        mu = 0.00

    logfile.write(p.str(True))
    logfile.write("\n")
    logfile.flush()

    return  T, dt, depth, initial_draft, domain_left, left, right, domain_right,\
            refinement_level, vertical_cells, horiz_cell_density, rat, rhow, ic_left,\
            ic_width, ic_amp, ic_offset, ic_vert_offset, ic_symmetric, alpha, E, beam_type,\
            ci_min, ci_lim, mu
    
# Refine near corners
class near_1_top_corners_cl(SubDomain) :
    def __init__(self, dist) :
        SubDomain.__init__(self)
        self.dist = dist

    def inside(self, x, on_boundary) :
        return Point(*x).distance(ulcrnr1) < self.dist or \
               Point(*x).distance(urcrnr1) < self.dist or \
               Point(*x).distance(llcrnr2) < self.dist or \
               x[1] >= ulcrnr1[1]-self.dist - EPS
class near_beam_corners_cl(SubDomain) :
    def __init__(self, dist, bar = True) :
        SubDomain.__init__(self)
        self.dist = dist
        self.bar = bar

    def inside(self, x, on_boundary) :
        return Point(*x).distance(llcrnr2) < self.dist or \
               Point(*x).distance(lrcrnr2) < self.dist or \
               (self.bar and x[1] >= -initial_draft-self.dist - EPS)
class near_3_top_corners_cl(SubDomain) :
    def __init__(self, dist) :
        SubDomain.__init__(self)
        self.dist = dist

    def inside(self, x, on_boundary) :
        return Point(*x).distance(ulcrnr3) < self.dist or \
               Point(*x).distance(urcrnr3) < self.dist or \
               Point(*x).distance(lrcrnr2) < self.dist or \
               x[1] >= ulcrnr3[1]-self.dist - EPS

def refine_mesh(subdom_cl, mesh, dist, **argv) :
    nearcornersd = subdom_cl(dist, **argv)
    nearcorner = MeshFunction("bool", mesh, mesh.topology().dim())
    nearcorner.set_all(False)
    nearcornersd.mark(nearcorner, True)
    return refine(mesh, nearcorner)

def make_mesh(i, rat) :
    all_paths_exist = os.path.exists("tmp.automesh."+str(i)) and \
            os.path.getmtime("tmp.automesh."+str(i)) > \
            os.path.getmtime("automesh."+str(i)+".geo")

    if not all_paths_exist :
        f = open("tmp.automesh."+str(i), 'w')
        p = Popen(["gmsh", "-2", "automesh."+str(i)+".geo", "-algo", "del2d"], stdout=f)
        Popen.wait(p)
        p = Popen(["dolfin-convert", "automesh."+str(i)+".msh", "automesh."+str(i)+".xml"], stdout=f)
        Popen.wait(p)
        p = Popen(["dolfin-order", "automesh."+str(i)+".xml"], stdout=f)
        Popen.wait(p)
        f.close()

    mesh = Mesh("automesh."+str(i)+".xml")
    for j in range(0, len(mesh.coordinates())) :
        mesh.coordinates()[j][0] *= rat

    return mesh

def open_files(runname) :
    file_phi1 = File("phi/model-"+runname+"-phi1_.pvd")
    file_phi2 = File("phi/model-"+runname+"-phi2_.pvd")
    file_phi3 = File("phi/model-"+runname+"-phi3_.pvd")
    file_phidt1 = File("add/phidt/model-"+runname+"-phidt1_.pvd")
    file_phidt2 = File("add/phidt/model-"+runname+"-phidt2_.pvd")
    file_phidt3 = File("add/phidt/model-"+runname+"-phidt3_.pvd")
    file_phidt_la12 = File("add/phidt/model-"+runname+"-phidtla12_.pvd")
    file_phidt_la23 = File("add/phidt/model-"+runname+"-phidtla23_.pvd")
    file_mesh1 = File("add/mesh/model-"+runname+"-mesh1_.pvd")
    file_mesh2 = File("add/mesh/model-"+runname+"-mesh2_.pvd")
    file_mesh3 = File("add/mesh/model-"+runname+"-mesh3_.pvd")
    phinonz_file = File("phinonz-"+runname+"-1_.pvd")
    phinonz2_file = File("phinonz-"+runname+"-2_.pvd")

    return file_phi1, file_phi2, file_phi3,\
    file_phidt1, file_phidt2, file_phidt3,\
    file_phidt_la12, file_phidt_la23,\
    file_mesh1, file_mesh2, file_mesh3,\
    phinonz_file, phinonz2_file

# Define 1D subdomains
# NB: The SubDomains and classes, except for inner_boundary_cl, will be
# invalidated by movement of the mesh, so stick to index lists
class top_cl(SubDomain) :
    def __init__(self, draft=0.) :
        SubDomain.__init__(self)
        self.draft = draft
    def inside(self, x, on_boundary) :
        return x[1] >= -self.draft - EPS

def get_top_indices(mesh, draft=0.) :
    top_indices = MeshFunction("uint", mesh, mesh.topology().dim()-1)
    top_indices.set_all(0)
    topsd = top_cl(draft)
    topsd.mark(top_indices, 1)
    top_indices = filter(lambda t : top_indices[t]==1, range(0, top_indices.size()))
    return top_indices

class beam_cl(SubDomain) :
    def __init__(self, initial_draft) :
        SubDomain.__init__(self)
        self.initial_draft = initial_draft
    def inside(self, x, on_boundary) :
        return x[1] >= - self.initial_draft - EPS

# Define internal boundary
class inner_boundary_cl(SubDomain):
    def __init__(self, line, gamma_top) :
        SubDomain.__init__(self)
        self.line = line
        self.gamma_top = gamma_top
        #print self.line, gamma_top
    def inside(self, x, on_boundary) :
        return abs(x[0] - self.line) <= EPS and x[1] <= self.gamma_top + EPS

# Defining the boundary meshes saves us working on the full mesh quite so often
def create_boundary(mesh, sd) :
    bmesh = BoundaryMesh(mesh)
    bmconn = bmesh.topology()(1,0)
    top = SubMesh(bmesh, sd)
    bmesh_to_mesh = bmesh.vertex_map()
    top_to_bmesh = top.data().mesh_function("parent_vertex_indices")
    num_top_points = len(top.coordinates())
    return bmesh, num_top_points, bmesh_to_mesh, top_to_bmesh, bmconn, top

def get_beam_desc(left, right, initial_draft, E, rhow) :
    beam_desc = mk2beam._PlateDescription()
    beam_desc.name = "beam"
    beam_desc.left = left
    beam_desc.right = right
    beam_desc.draft = initial_draft
    beam_desc.E = E
    beam_desc.nu = 0.3
    beam_desc.beta = 0.25
    beam_desc.rhop = 917.
    beam_desc.mup = 0.
    beam_desc.h = beam_desc.draft * rhow / beam_desc.rhop
    return beam_desc

def get_dirmost(direction, line) :
    return sorted(range(0, len(line.coordinates())),
        lambda c,d : direction*cmp(line.coordinates()[d][0],line.coordinates()[c][0]))[0]

def make_1Ds(space) :
    fns = []
    for i in range(0, 10) :
        fn = Function(space)
        fn.vector().zero()
        fns.append(fn)
    return fns

def predict(dt, zeta, zetax, zetaxx, zeta_m1, zdt_m1, zdt_m2, zdt_m3, zdt_m4) :
    zeta.interpolate(project(zeta_m1 + (dt/24.)*(55*zdt_m1 - 59*zdt_m2 + 37*zdt_m3 - 9*zdt_m4), zeta_m1.function_space()))
    zetax = project(zeta.dx(0), zetax.function_space())
    zetaxx = project(zetax.dx(0), zetaxx.function_space())
    for i in range(0, len(zeta.vector())) :
        zeta_m1v = zeta_m1.vector()[i]
        zdt_m1v = zdt_m1.vector()[i]
        zdt_m2v = zdt_m2.vector()[i]
        zdt_m3v = zdt_m3.vector()[i]
        zdt_m4v = zdt_m4.vector()[i]
        zeta.vector()[i] = zeta_m1v + (dt/24.)*(55*zdt_m1v - 59*zdt_m2v + 37*zdt_m3v - 9*zdt_m4v)
    #zeta.interpolate(project(zeta_m1 + (dt/24.)*(55*zdt_m1 - 59*zdt_m2 + 37*zdt_m3 - 9*zdt_m4), zeta_m1.function_space()))


def correct(dt, cphi_new, cphi_m1, cpdt, cpdt_m1, cpdt_m2, cpdt_m3) :
    cphi_new.interpolate(project(cphi_m1 + (dt/24.)*(9*cpdt + 19*cpdt_m1 - 5*cpdt_m2 + cpdt_m3), cphi_new.function_space()))
    for i in range(0, len(cphi_new.vector())) :
        cphi_m1v = cphi_m1.vector()[i]
        cpdtv = cpdt.vector()[i]
        cpdt_m1v = cpdt_m1.vector()[i]
        cpdt_m2v = cpdt_m2.vector()[i]
        cpdt_m3v = cpdt_m3.vector()[i]
        cphi_new.vector()[i] = cphi_m1v + (dt/24.)*(9*cpdtv + 19*cpdt_m1v - 5*cpdt_m2v + cpdt_m3v)
    #cphi_new.interpolate(project(cphi_m1 + (dt/24.)*(9*cpdt + 19*cpdt_m1 - 5*cpdt_m2 + cpdt_m3), cphi_new.function_space()))
