#include "WaveMaker.h"
#include <iostream>
#include <cassert>
#include <fstream>
#include <math.h>

WaveMaker::WaveMaker(double ampt, double fret, double lwrt, double btmt, double numt = 0, double attcoefft = 1)
 : amp(ampt), fre(fret), lwr(lwrt), btm(btmt), num(numt), attcoeff(attcoefft), symt("t")
{
 dis_ex   = amp*(sin(fre*2*symt*M_PI));
 vel_ex   = dis_ex.diff(symt);
 acc_ex   = vel_ex.diff(symt);
 if (attcoeff > 0) {
   double attack = attcoeff/fre;
   vel_ex_a = vel_ex*(symt/attack);
   acc_ex_a = vel_ex_a.diff(symt);
   if (num > 0) {
     vel_ex_d = vel_ex*((num/fre - symt)/attack);
     acc_ex_d = vel_ex_d.diff(symt);
   }
 }
}

double WaveMaker::velocity(double t, const dolfin::Array<double>& x)
{
	double values = 0;
	double f = 0;
	double attack = attcoeff/fre;
	if (num > 0 && t > num/fre) return 0;
	/*
	if (fabs(x[1]) < fabs(lwr)) f = 1;//RMV
	else if (fabs(x[1]) < fabs(btm)) f = (fabs(x[1]) + btm)/(lwr - btm);
	*/

	f = 1;
	if (fabs(x[1]) < fabs(lwr)) f = 0;
	//else if (fabs(x[1]) < fabs(btm)) f = (fabs(x[1]) - fabs(lwr))/(fabs(btm) - fabs(lwr));
	else if (fabs(x[1]) < fabs(btm)) f = 0.5*(1-cos(2*M_PI*(fabs(x[1]) - fabs(lwr))/(fabs(btm) - fabs(lwr))));
	else if (fabs(x[1]) >= fabs(btm)) f = 0;
	//else if (fabs(x[1]) < 10) f = (fabs(x[1])-5)/5;
//	f *= x[1] > -2.4? (x[1]+2.4)/2.4 : 0;
//        f *= sin(2*M_PI*x[1]/btm);
	
	if ( t < attack )
	 values = GiNaC::ex_to<GiNaC::numeric>(vel_ex_a.subs(symt==t)).to_double();
	else if ( num > 0 && t > num/fre - attack )
	 values = GiNaC::ex_to<GiNaC::numeric>(vel_ex_d.subs(symt==t)).to_double();
	else
	 values = GiNaC::ex_to<GiNaC::numeric>(vel_ex.subs(symt==t)).to_double();

	values *= f;
	return values;
}

double WaveMaker::acceleration(double t, const dolfin::Array<double>& x)
{
	double values = 0;
	double f = 0;
 	double attack = attcoeff/fre;
	if (num > 0 && t > num/fre) return 0;
	/*
	if (fabs(x[1]) < fabs(lwr)) f = 1;//RMV
	else if (fabs(x[1]) < fabs(btm)) f = (fabs(x[1]) + btm)/(lwr - btm);
	*/

	f = 1;
	if (fabs(x[1]) < fabs(lwr)) f = 0;
	else if (fabs(x[1]) < fabs(btm)) f = 0.5*(1-cos(2*M_PI*(fabs(x[1]) - fabs(lwr))/(fabs(btm) - fabs(lwr))));
	else if (fabs(x[1]) >= fabs(btm)) f = 0;
	//else if (fabs(x[1]) < fabs(btm)) f = 0.5*(1-cos(M_PI*(fabs(x[1]) - fabs(lwr))/(fabs(btm) - fabs(lwr))));
	//else if (fabs(x[1]) < 10) f = (fabs(x[1])-5)/5;

	if ( t < attack )
	 values = GiNaC::ex_to<GiNaC::numeric>(acc_ex_a.subs(symt==t)).to_double();
	else if ( num > 0 && t > num/fre - attack )
	 values = GiNaC::ex_to<GiNaC::numeric>(acc_ex_d.subs(symt==t)).to_double();
	else
	 values = GiNaC::ex_to<GiNaC::numeric>(acc_ex.subs(symt==t)).to_double();

	values *= f;
	/*
	values = motion*f;
	if (t < attack)
	  values = (t/attack)*(values-2*M_PI*fre*amp*(cos(fre*2*t*M_PI))*f);
	else if (t > num/fre - attack)
	  values = (num/fre - t)/attack*(values-2*M_PI*fre*amp*(cos(fre*2*t*M_PI))*f);
	if ( t > num/fre - attack ) values *= (num/fre - t)/attack;
	*/
	return values;
}

WaveMaker* load_wavemaker ( char* wavemakerloc )
{
  char wavemakerlocfull[300];
  sprintf(wavemakerlocfull, "params/%s", wavemakerloc);
  FILE* wavemakerfile = fopen(wavemakerlocfull, "r");
  if (!wavemakerfile) {
    std::cout << "[MK2] Can't find wavemaker file " << wavemakerloc << "!!" << std::endl;
    return NULL;
  }

  double amp = 1.0, fre = 1.0, lwr = -1.0, btm = -1.2, attcoeff = 1.0, num = 0;
  assert(fscanf(wavemakerfile, "amp = %lf\n", &amp) != EOF);
  assert(fscanf(wavemakerfile, "fre = %lf\n", &fre) != EOF);
  assert(fscanf(wavemakerfile, "profile = %lf %lf\n", &lwr, &btm) != EOF);
  assert(fscanf(wavemakerfile, "num = %lf\n", &num) != EOF);
  assert(fscanf(wavemakerfile, "attcoeff = %lf\n", &attcoeff) != EOF);
  return new WaveMaker(amp, fre, lwr, btm, num, attcoeff);
}
