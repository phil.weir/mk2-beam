#!/bin/bash

source outroot.sh

RUNNING=`ps $1 -C mk2 --no-heading -o command | sed 's/.*parameters.\([^ ]*\).*/\1/' -`

for i in $RUNNING
do
  out=`tail -n 10 $outroot/output.$i/mk2.$i.log | grep Time | tail -n 1`
  t=`ps --no-heading -o lstart,etime -p$(pgrep -f "$i ")`
  echo "$i	$out	$t"
done
