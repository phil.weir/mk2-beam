#!/bin/bash
# ./makeanim DIR FIRST LAST STEP DIR FIRST STEP
source outroot.sh
j=$6
cp $outroot/output.$1/parameters parameters-1.tmp
cp $outroot/output.$5/parameters parameters-2.tmp
for (( i=$2; i<=$3 ; i+=$4))
do
padn=`printf %06d $i`
echo "Frame $i"
for file in {left,right,plate1}
do
 padn=`printf %06d $i`
 padm=`printf %06d $j`
 cp $outroot/output.$1/$file-$padn.csv $file-1.csv
 cp $outroot/output.$5/$file-$padm.csv $file-2.csv
 #sed '1d' $file-1-1.csv > $file-1.csv
 #sed '1d' $file-2-1.csv > $file-2.csv
done;
#echo $i | octave canimtest.m > fftlog
python test.py -S $9 -D $i $j > fftlog
rsvg test.svg test.png
mv test.png $outroot/output.$1/octout-$padn.png
#mv octout.png $outroot/output.$1/octout-$padn.png
animstr="$animstr $outroot/output.$1/octout-$padn.png"
j=$(($j+$7))
done;
convert -delay $8 $animstr ctst-$1-$2-$3-$4-$5-$6-$7-O$8.gif
echo "ctst-$1-$2-$3-$4-$5-$6-$7-O$8.gif"
