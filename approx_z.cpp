#include "HEP.h"
#include <sys/stat.h>

dolfin::Array<double> v(1), xv(1);
double eval_z(Function& f, double x)
{
  xv[0] = x;
  f.eval(v, xv);
  return v[0];
}

void backup_function_vector ( char* to, std::string name, std::string vname )
{
  char filename1[500], filename2[500];
  sprintf(filename2, "%s_%s.xml", name.c_str(), vname.c_str());
  sprintf(filename2, "%s_%s.xml.%s", name.c_str(), vname.c_str(), to);

  std::ifstream ifs(filename1);
  std::ofstream ofs(filename2);

  ofs << ifs.rdbuf();
}

void save_function_vector ( const Function& f, std::string name, std::string vname )
{
  char filename[500];
  sprintf(filename, "%s_%s.xml", name.c_str(), vname.c_str());

  dolfin::File fst(filename);
  fst << *f.vector();
}

void load_function_vector ( Function& f, std::string name, std::string vname )
{
  char filename[500];
  sprintf(filename, "%s_%s.xml", name.c_str(), vname.c_str());

  dolfin::File fst(filename);
  fst >> *f.vector();
}

std::map< std::string, int > filecount;
void write_function ( Function& f, std::string name )
{
  int pres = 2;

  if (filecount.count(name) == 0) { filecount[name] = 0; }
  else { filecount[name] += 1; }

  char fname[300];
  sprintf(fname, "%s-%06d.csv", name.c_str(), filecount[name]);
  
  bool prevdone = false;
  const Mesh& mesh = *f.function_space()->mesh();
  double x, y, xint = 0, xold = 0;

  double l = mesh.geometry().x(0,0), r = l;
  for ( VertexIterator vit(mesh); !vit.end() ; ++vit)
  { l = vit->x(0)<l ? vit->x(0) : l;
    r = vit->x(0)>r ? vit->x(0) : r; }

  FILE* file = fopen(fname, "w");
  fprintf(file, "x,y\n");
  int npts = 3000;
  for ( int i = 0 ; i < npts ; i++ )
  {
  	xint = l + (r-l)*i/(double)npts;
	dolfin::Array<double> y(1), x(1); x[0] = xint;
	try {
		f.eval(y, x);
		fprintf(file, "%lf,%.15lf\n", xint, y[0]);
	} catch (std::runtime_error& str)
    	{ }
  }
  /*
  for ( VertexIterator vit(mesh) ; !vit.end() ; ++vit )
  { x = mesh.geometry().x(vit->index(),0);
    const Function& fc = f;
    y = fc.vector().getitem(vit->index());
    if (prevdone) {
     for ( int j = pres-1 ; j > 0 ; j-- ) {
      xint = x - j*(x - xold)/(double)pres;
      fprintf(file, "%lf,%lf\n", xint, approx_z(f, xint));
     }
    } else prevdone = true;
    fprintf(file, "%lf,%lf\n", x, y);
    xold = x;
  }
  */
  fclose(file);
}

double approx_z ( const Function& f, double x, bool linearize )
{
  double azEPS=1e-12;
  boost::shared_ptr<const FunctionSpace> V = f.function_space();
  VertexIterator vit(*V->mesh());
  //double x1 = vit->x(0), x2 = vit->x(0); uint i1 = vit->index(), i2 = vit->index();
  //// Ensure that i1!=i2
  //++vit; if (fabs(x1-x) > fabs(vit->x(0) - x)) { x1 = vit->x(0); i1 = vit->index(); }
  //                                        else { x2 = vit->x(0); i2 = vit->index(); }
  //// Ensure we don't look over the same node twice
  //++vit;
  //for (  ; !vit.end() ; ++vit )
  //{
  //  if ( fabs(x1 - x) > fabs(vit->x(0) - x) ) {
  //    if (fabs(x1 - x) < fabs(x2 - x)) {x2 = x1; i2 = i1;}
  //    x1 = vit->x(0); i1 = vit->index();
  //  }
  //  else if ( fabs(x2 - x) > fabs(vit->x(0) - x) ) {x2 = vit->x(0); i2 = vit->index();}
  //}
  double x1 = vit->x(0), x2 = vit->x(0), x3 = vit->x(0); uint i1 = vit->index(), i2 = vit->index(), i3 = vit->index();
  bool on_vertex = false;
  for ( ; !vit.end() ; ++vit )
  {
    //if ( x==0.095) std::cout << vit->x(0) << " ";
    if ( vit->x(0) < x && (vit->x(0) > x1 || x1 >= x) ) { x1 = vit->x(0); i1 = vit->index(); }
    if ( vit->x(0) > x && (vit->x(0) < x2 || x2 <= x) ) { x2 = vit->x(0); i2 = vit->index(); }
    if ( fabs(vit->x(0) - x) < azEPS ) { on_vertex = true; x3 = vit->x(0); i3 = vit->index(); }
  }

  //if ( fabs(x - x1) < EPS ) return f.vector().getitem(i1);
  //if ( fabs(x - x2) < EPS ) return f.vector().getitem(i2);

  if (!on_vertex && fabs(x-x1)>azEPS && (x2-x1)/(x-x1) < 1-azEPS) {
    std::cout << "[MK2] Cell not found!!" << std::endl;
    std::cout << " x: " << x << " x1: " << x1 << " x1-x=" << x1-x << " x2: " << x2
      << " i1: " << i1 << " i2: " << i2 << std::endl;
    exit(6);
  }

  V->mesh()->init(0,1);
  const MeshConnectivity& mconn = (V->mesh()->topology())(0, 1);
  const uint* conn1 = mconn(i1), *conn2 = mconn(i2);
  uint celln = 0; bool found = false;
  if (on_vertex) {
   conn1 = mconn(i3); celln = conn1[0]; found = true;
  } else {
   for ( uint r1 = 0 ; !found && r1 < mconn.size(i1) ; r1++ )
   for ( uint r2 = 0 ; !found && r2 < mconn.size(i2) ; r2++ )
     if (conn1[r1] == conn2[r2]) { celln = conn1[r1]; found = true; }
  }

  if (!found) {
    std::cout << "[MK2] Breakdown in interval connectivity!!!" << std::endl;
    std::cout << " x: " << x << " x1: " << x1 << " x2: " << x2
      << " i1: " << i1 << " i2: " << i2 << std::endl;
    exit(6);
  }

  Cell cell(*V->mesh(),celln);
  // With help from FunctionSpace.cpp!!!
  UFCCell ufcc(cell);

  double val = 0;
  if (linearize && !on_vertex) {
 //  std::cout << "(" << x << "," << x1 << "," << x2 << ") " << std::endl;
    //double z1v[1] = {-1000}, x1v[1] = {x1}, z2v[1] = {-1000}, x2v[1] = {x2};
    
//    f.eval(z1v, x1v, ufcc, cell.index());
//    f.eval(z2v, x2v, ufcc, cell.index());
//    double z1 = z1v[0], z2 = z2v[0];
    double z1, z2; // RMV!!
    dolfin::Array<double> valv(1); dolfin::Array<double> xv(1);
    xv[0] = x1; f.eval(valv, xv, cell, ufcc); z1 = valv[0];
    xv[0] = x2; f.eval(valv, xv, cell, ufcc); z2 = valv[0];

    val = ((x - x1)*z2 - (x - x2)*z1)/(x2 - x1); // RMV REINSERT if <EPS above and add this as the 'else'
  //  std::cout << " (" << x << "," << val << "," << z1 << "," << z2 << ")" << std::endl;
  } else {
    dolfin::Array<double> valv(1); dolfin::Array<double> xv(1); xv[0] = x;
    f.eval(valv, xv, cell, ufcc);
    val = valv[0];
  }

  if (isnan(val)) {
   std::cout << "[MK2] Computed nan in approx_z for x = " << x << " between vertices "
    << i1 << " (x=" << x1 << ") and " << i2 << " (x=" << x2 << ")" << std::endl; exit(7);
  }
  return val;
}
