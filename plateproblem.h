#ifndef PLATEPROBLEM_H
#define PLATEPROBLEM_H

#include <dolfin.h>
#include <list>
#include "MBM.h"
#include "RMS.h"
#include "Top.h"
#include "PlateTop.h"
#include "PlateTopDG.h"
#include "PlateTopLinear.h"
#include "dolfinadd.h"
#include "logfile.h"

// Use our own eps, above the compiler's zero!
#define PEPS 1e-18

using namespace dolfin;

struct _PlateDescription
{
    char name[300];
    char typestr[200];
    double left;
    double right;
    double draft;
    double E;
    double h;
    double nu;
    double beta;
    double rhop;
    double mup;
};
typedef struct _PlateDescription PlateDescription;

class PlateMesh : public Mesh
{
  public :
	PlateMesh(SubMesh& mesh);
};

class AlphaFunction : public Expression
{
  public:
    AlphaFunction(double alphat, Function& Wddtt, Function& Wddtkm1t, Function& Wt) :
      alpha(alphat), Wddt(Wddtt), Wddtkm1(Wddtkm1t), W(Wt)
      {}

    double alpha;
    Function &Wddt, &Wddtkm1, &W;

    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> Wddtv(1), Wddtkm1v(1);
      Wddt.eval(Wddtv, x); Wddtkm1.eval(Wddtkm1v, x);
      val[0] = alpha*Wddtv[0] + (1-alpha)*Wddtkm1v[0];
      //val[0] = alpha*approx_z(Wddt, x[0]) + (1-alpha)*approx_z(Wddtkm1, x[0]);
    }
};

class PlateDomain;

class InnerPlateSubDomain : public SubDomain
{
  public :
    InnerPlateSubDomain(double LEFTt, double RIGHTt) : LEFT(LEFTt), RIGHT(RIGHTt) {}
    double LEFT, RIGHT;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const;
};

class PlateDomainFlat : public SubDomain
{
  public:
    PlateDomainFlat(double LEFTt, double RIGHTt) : LEFT(LEFTt), RIGHT(RIGHTt) {}
    double LEFT, RIGHT;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      // First half should be negation of FreeSurface first half
      // (except for two boundary points)
      return (x[0] >= LEFT - PEPS && x[0] <= RIGHT + PEPS );
    }
};

    
class Normal : public Function
{
  public:
    Normal(FunctionSpace& tV, double lt, double rt) : Function(tV), l(lt), r(rt) {}
    double l, r;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = ( x[0] < (l+r)/2 ) ? -1 : 1;
    }
};
class Boundary : public SubDomain
{
  public:
    Boundary(double lt, double rt) : l(lt), r(rt) {}
    double l, r;
    bool inside ( const dolfin::Array<double> &x, bool on_boundary ) const
    {
      return (x[0]<l + DOLFIN_EPS) || (x[0]>r-DOLFIN_EPS);
    }
};
class PlateProblem
{
  public:
    PlateProblem(Mesh& mesht, double l, double r, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmgt = true);
    ~PlateProblem() {}
    
    //double get_D () { double values[1]; Data data; Dc.eval(values, data); return values[0]; }
    double get_D () { return Dc; }
    Mesh& mesh;
    double l, r;
    Constant Dtc, Dc, betac, rhopc, rhowc, muc, gc, hc;
    bool sbmg;
    Top::FunctionSpace tV;
    Normal n; Boundary boundary;
    File dispout, veloout, acceout, presout, forcout;
    bool log;
    Vector line_vector;

    virtual void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing ) = 0;
    virtual std::string gettype () = 0;
    virtual void nonstep () = 0;
    virtual void adjacc (double alpha) = 0;
    virtual void timestep () = 0;
    virtual void clstep () = 0;
    virtual void load_function_vectors(std::string name) {}
    virtual void save_function_vectors(std::string name) {}
    virtual void backup_function_vectors(char* to, std::string name) {}
    virtual void output_diagnostics(double t,
      Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn ) {}
    virtual void setup_diagnostics(std::string name, char* openmode) {}
    virtual void shutdown_diagnostics() {}
    virtual void init() {}
    virtual void eval_bm(Function& M, Function& W);
    virtual void init_profile(Function& W) {}
};

double rounding(double LEFT, double RIGHT, double draft, double x, double y);
class PlateRounding : public Expression
{
  public :
    PlateRounding (double LEFTt, double RIGHTt, double draftt) : LEFT(LEFTt), RIGHT(RIGHTt), draft(draftt) {}
    double LEFT, RIGHT, draft;
    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const
    {
       val[0] = rounding(LEFT, RIGHT, draft, x[0], x[1]);
    }
};

class Plater
{
  public:
  virtual PlateProblem* make (Mesh& mesht, double lt, double rt, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg) = 0;
};

typedef std::map< std::string, Plater* > PlaterMap;

const PlateMesh& make_pline( const Mesh& bdy, double left, double right, double draft, MeshFunction<uint>** p_to_bdy );

class Plate
{
  public:
    Plate(Mesh& pline, double leftt, double rightt, double draftt,
          double Dt, double Et, double ht, double nut, double beta,
  	double rhopt, double rhowt, double mup, double gt, Plater& plater, const std::string& namet, bool sbmg = true) :
      left(leftt), right(rightt), draft(draftt), name(namet), inner(left, right),
      pV(pline), pVDG(pline), tV(pline), plV(pline), innerpline(pline, inner), innerpV(innerpline),
      rounding(plV), bm(pV), Wdotn(pV), Wn(pV), Wddotn(pV), Wn1(pV), Wdtn1(pV), Wddtn1(pV), Wx(pV), Wxx(pV), M(pVDG), Wn1_linear(plV), Wz(pV), Wz_Wpress(pV), Wzdt(pV), Wzddt(pV), Wzn(pV), p(plV),
      Wddtkm1(pV), Wdtkm1(pV), Wkm1(pV), W0(pV), W0_linear(plV),
      forcing(tV), phidtn1p1(tV), gsphin1p1(tV), pressuren1p1(tV), Wn1old(pV), Wn1old_linear(plV),
      E(Et), h(ht), nu(nut), rhop(rhopt), rhow(rhowt), g(gt), refpress_on(false), refpress_now(false),
      refpress(0), refpress_x(0), refpress_zdtn1(0)
    {
      Constant zero(0.0);
      Wdotn = zero; Wn = zero; Wddotn = zero; Wx = zero; Wxx = zero; Wn1_linear = zero; M = zero;
      Wn1 = zero; Wdtn1 = zero; Wddtn1 = zero; Wz = zero; Wz_Wpress = zero; Wzdt = zero; Wzddt = zero; Wzn = zero; p = zero;
      Wddtkm1 = Wddtn1; Wdtkm1 = Wdtn1; Wkm1 = Wn1; W0 = zero; W0_linear = zero;
      forcing = zero; phidtn1p1 = zero; gsphin1p1 = zero;
      pressuren1p1 = zero;
      Wn1old = zero; Wn1old_linear = zero;
      problem = plater.make(pline, left, right, Dt, E, h, nu, beta, rhop, rhow, mup, g, sbmg);
      problem->init_profile(Wn);
      //problem->init_profile(Wn1old);
      //problem->init_profile(Wn1old_linear);
      problem->init_profile(Wn1);
      problem->init_profile(Wn1_linear);
      problem->init_profile(Wkm1);
      PlateRounding pr(left, right, draft);
      rounding = pr;
    }
    Plate(Mesh& pline, PlateDescription& d, double Dt, double rhowt, double gt, Plater& plater, const std::string& namet, bool sbmg = true) :
      left(d.left), right(d.right), draft(d.draft), name(namet), inner(left, right),
      pV(pline), pVDG(pline), tV(pline), plV(pline), innerpline(pline, inner), innerpV(innerpline),
      rounding(plV), bm(pV), Wdotn(pV), Wn(pV), Wddotn(pV), Wn1(pV), Wdtn1(pV), Wddtn1(pV), Wx(pV), Wxx(pV), M(pVDG), Wn1_linear(plV), Wz(pV), Wz_Wpress(pV), Wzdt(pV), Wzddt(pV), Wzn(pV), p(plV),
      Wddtkm1(pV), Wdtkm1(pV), Wkm1(pV), W0(pV), W0_linear(plV),
      forcing(tV), phidtn1p1(tV), gsphin1p1(tV), pressuren1p1(tV), Wn1old(pV), Wn1old_linear(plV),
      E(d.E), h(d.h), nu(d.nu), rhop(d.rhop), rhow(rhowt), g(gt), refpress_on(false), refpress_now(false),
      refpress(0), refpress_x(0), refpress_zdtn1(0)
    {
      Constant zero(0.0);
      Wdotn = zero; Wn = zero; Wddotn = zero; Wx = zero; Wxx = zero; Wn1_linear = zero; M = zero;
      Wn1 = zero; Wdtn1 = zero; Wddtn1 = zero; Wz = zero; Wz_Wpress = zero; Wzdt = zero; Wzddt = zero; Wzn = zero; p = zero;
      Wddtkm1 = Wddtn1; Wdtkm1 = Wdtn1; Wkm1 = Wn1;
      forcing = zero; phidtn1p1 = zero; gsphin1p1 = zero;
      pressuren1p1 = zero;
      Wn1old = zero; Wn1old_linear = zero;
      problem = plater.make(pline, left, right, Dt, d.E, d.h, d.nu, d.beta, d.rhop, rhow, d.mup, g, sbmg);
      problem->init_profile(Wn);
      problem->init_profile(Wn1);
      problem->init_profile(Wn1_linear);
      problem->init_profile(Wkm1);
      PlateRounding pr(left, right, draft);
      rounding = pr;
    }
    //void step(Expression& pressure);
    void step();
    void init();
    void adjacc(double alpha)
     {
       Function Wddtn1tmp(pV), Wdtn1tmp(pV), Wn1tmp(pV); Wddtn1tmp = Wddtn1; Wdtn1tmp = Wdtn1; Wn1tmp = Wn1;
       AlphaFunction Walpha(alpha, Wddtn1tmp, Wddtkm1, Wn1old);
       AlphaFunction Walphap(4*alpha, Wddtn1tmp, Wdtkm1, Wn1old);
       AlphaFunction Walphapp(16*alpha, Wddtn1tmp, Wkm1, Wn1old);
       Wddtn1 = Walpha;// Wdtn1 = Walphap; Wn1 = Walphapp;
       Wddtkm1 = Wddtn1; Wdtkm1 = Wdtn1; Wkm1 = Wn1;
       problem->adjacc(alpha);
     }
    void nonstep()
     {
       Constant zero(0.0);
//       Wn1 = Wn; Wdtn1 = zero; Wddtn1 = zero;//RMV
       Wn1 = Wn; Wdtn1 = Wdotn; Wddtn1 = Wddotn;//RMV
       Wz = Wzn;
       Wn1_linear.interpolate(Wn1);
       problem->nonstep();
     }
    void backup_function_vectors(char* to)
     {
       backup_function_vector(to, name, "Wddtkm1");
       backup_function_vector(to, name, "Wdtkm1");
       backup_function_vector(to, name, "Wkm1");
       backup_function_vector(to, name, "Wn");
       backup_function_vector(to, name, "Wdotn");
       backup_function_vector(to, name, "Wddotn");
       backup_function_vector(to, name, "Wn1");
       backup_function_vector(to, name, "Wdtn1");
       backup_function_vector(to, name, "Wddtn1");
       problem->backup_function_vectors(to, name);
     }
    void save_function_vectors()
     {
       char refpress_name[300];
       sprintf(refpress_name, "%s_refpress", name.c_str());
       FILE* refpress_file = fopen(refpress_name, "w");
       fprintf(refpress_file, "%d\n%lf\n%lf\n%lf", refpress_now, refpress, refpress_x, refpress_zdtn1);
       fclose(refpress_file);

       save_function_vector(Wddtkm1, name, "Wddtkm1");
       save_function_vector(Wdtkm1, name, "Wdtkm1");
       save_function_vector(Wkm1, name, "Wkm1");
       save_function_vector(Wn, name, "Wn");
       save_function_vector(Wdotn, name, "Wdotn");
       save_function_vector(Wddotn, name, "Wddotn");
       save_function_vector(Wn1, name, "Wn1");
       save_function_vector(Wdtn1, name, "Wdtn1");
       save_function_vector(Wddtn1, name, "Wddtn1");
       problem->save_function_vectors(name);
     }
    void load_function_vectors()
     {
       char refpress_name[300];
       sprintf(refpress_name, "%s_refpress", name.c_str());
       FILE* refpress_file = fopen(refpress_name, "r");
       fscanf(refpress_file, "%lf\n%lf\n%lf", &refpress, &refpress_x, &refpress_zdtn1);
       std::cout << refpress_on << refpress_now << refpress << refpress_x << std::endl;
       fclose(refpress_file);

       load_function_vector(Wddtkm1, name, "Wddtkm1");
       load_function_vector(Wdtkm1, name, "Wdtkm1");
       load_function_vector(Wkm1, name, "Wkm1");
       load_function_vector(Wn, name, "Wn");
       load_function_vector(Wdotn, name, "Wdotn");
       load_function_vector(Wddotn, name, "Wddotn");
       load_function_vector(Wn1, name, "Wn1");
       load_function_vector(Wdtn1, name, "Wdtn1");
       load_function_vector(Wddtn1, name, "Wddtn1");
       problem->load_function_vectors(name);
     }
    void timestep()
     {
       Wddtkm1 = Wddtn1;
       Wdtkm1  = Wdtn1;
       Wkm1    = Wn1;
       Wn     = Wn1;
       Wdotn  = Wdtn1;
       Wddotn = Wddtn1;
       Wzn    = Wz;
       problem->timestep();
     }
    void clstep()
     {
       Wn1old = Wn1; //Wn1old_linear.interpolate(Wn1old);
       Wn1old_linear = Wn1_linear;
       Wddtkm1 = Wddtn1;
       Wdtkm1  = Wdtn1;
       Wkm1    = Wn1;
       problem->clstep();
     }
    double evalerr()
     {
       return eval_err(Wn1old, Wn1);
     }
    void output_diagnostics(double t);
    void setup_diagnostics(char* openmode);
    void shutdown_diagnostics();
    FILE* mbmf, *rmsf, *midbmf;
    void eval_bm()
      {
        problem->eval_bm(M, Wn1);
      }
    double eval_bm_();
    double eval_rms_()
     {
       RMS::Functional RMSM(innerpline);
       Constant Ec(E), Ic(1.0*h*h*h/12), nuc(nu); // I=b*h^3/12 (Wiki)
       RMSM.W = Wn1; RMSM.E = Ec; RMSM.I = Ic; RMSM.nu = nuc;
       return sqrt(assemble(RMSM));
     }
    double eval_mean_bending_moment();
    std::string gettype()
     {
       return problem->gettype();
     }
   
    double left, right, draft;
    static Plate* this_plate(std::list<Plate*>& platelist, double x, bool edges, double length);
    static Plate* find_plate(std::list<Plate*>& platelist, bool onright, double x, bool edges, double length);
    static PlaterMap& get_platermap();
    static PlaterMap platermap;
    static Plater& get_plater(const std::string& name) { return *platermap[name]; }

    std::string name;
    InnerPlateSubDomain inner;
    PlateTop::FunctionSpace pV;
    PlateTopDG::FunctionSpace pVDG;
    PlateTopLinear::FunctionSpace plV;
    SubMesh innerpline;
    PlateTop::FunctionSpace innerpV;
    Top::FunctionSpace tV;
    Function rounding, bm, Wdotn, Wn, Wddotn, Wn1, Wdtn1, Wddtn1, Wx, Wxx, M, Wn1_linear, Wz, Wz_Wpress, Wzdt, Wzddt, Wzn, p;
    Function Wddtkm1, Wdtkm1, Wkm1;
    Function W0, W0_linear;
    Function forcing, phidtn1p1, gsphin1p1;
    Function pressuren1p1, Wn1old, Wn1old_linear;
    PlateProblem* problem;
    double E, h, nu, rhop, rhow, g;
    bool refpress_on, refpress_now;
    double refpress, refpress_x, refpress_zdtn1;
};

typedef std::list<Plate*> PlateList;

#endif
