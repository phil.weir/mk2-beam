source outroot.sh
cp $outroot/output.$1/mbm-0.csv mbma.csv
cp $outroot/output.$2/mbm-0.csv mbmb.csv
cp $outroot/output.$3/mbm-0.csv mbmc.csv
cp $outroot/output.$4/mbm-0.csv mbmd.csv
octave mbmctst4.m > fftlog
cp test.png mbm-$1-$2-$3-$4-0.png
display test.png
