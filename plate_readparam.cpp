#include "logfile.h"
#include "readparam.h"
#include "outroot.h"

double get_int_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line)
{ return GiNaC::ex_to<GiNaC::numeric>(get_ginac_from_line(parser, paramfile, label, rest_of_line)).to_int(); }

double get_double_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line)
{ return GiNaC::ex_to<GiNaC::numeric>(get_ginac_from_line(parser, paramfile, label, rest_of_line)).to_double(); }

double get_int_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line)
{ return GiNaC::ex_to<GiNaC::numeric>(get_ginac_from_line(parser, line, label, rest_of_line)).to_int(); }

double get_double_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line)
{ return GiNaC::ex_to<GiNaC::numeric>(get_ginac_from_line(parser, line, label, rest_of_line)).to_double(); }

GiNaC::ex& get_ginac_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line)
{
  char line[300], key[300], buffer[300];
  char* ret = fgets(line, 300, paramfile);
  GiNaC::ex& ans = get_ginac_from_line(parser, line, label, rest_of_line);
  if (rest_of_line) assert(fgets(buffer, 500, paramfile) != NULL);
  return ans;
}

GiNaC::ex& get_ginac_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line)
{
  char key[300], buffer[300], input[300];
  sprintf(key, "%s = %%[^\\r\\t#%s]%s", label, rest_of_line?"":"\\n", rest_of_line?"":"\\n");
  sscanf(line, key, input);
  GiNaC::ex& input_ex = *(new GiNaC::ex()); input_ex = parser(input);
  return input_ex;
}

PlateDescription* load_plate_description ( char* plateloc, GiNaC::parser& parser )
{
  char platelocfull[200];
  sprintf(platelocfull, "params/%s", plateloc);
  FILE* platefile = fopen(platelocfull, "r");
  if (!platefile) {
    std::cout << "[BEAMLIB] Can't find plate file " << platelocfull << "!!" << std::endl;
    return NULL;
  }

  PlateDescription* d = new PlateDescription;
  assert(fscanf(platefile, "type = %[^\r\t\n#]\n", d->typestr) != EOF);
  d->left = get_double_from_line(parser, platefile, "left", false);
  //assert(fscanf(platefile, "left = %lf\n", &d->left) != EOF);
  assert(fscanf(platefile, "right = %lf\n", &d->right) != EOF);
  assert(fscanf(platefile, "draft = %lf\n", &d->draft) != EOF);
  //assert(fscanf(platefile, "E = %lf\n", &d->E) != EOF);
  d->E = get_double_from_line(parser, platefile, "E", false);
  assert(fscanf(platefile, "h = %lf\n", &d->h) != EOF);
  assert(fscanf(platefile, "nu = %lf\n", &d->nu) != EOF);
  assert(fscanf(platefile, "beta = %lf\n", &d->beta) != EOF);
  assert(fscanf(platefile, "rhop = %lf\n", &d->rhop) != EOF);
  assert(fscanf(platefile, "mup = %lf\n", &d->mup) != EOF);
  return d;
}
