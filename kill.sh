source outroot.sh
if [ -e $outroot/output.$1/mk2.pid ]
then
  kill `cat $outroot/output.$1/mk2.pid`
  rm $outroot/output.$1/mk2.pid
else
  echo "No run for $1"
fi
