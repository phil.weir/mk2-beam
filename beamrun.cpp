#include <dolfin.h>
#include "beam_dx.h"
#include <sys/stat.h>
#include <errno.h>
#include <ginac/ginac.h>
#include "readparam.h"
#include "outroot.h"
#include <getopt.h>
#include "BeamRunSpace.h"
#include "dolfinadd.h"
#include "plateproblem.h"
#include "plus.h"
#include "diff.h"
#include "ddiff.h"

using namespace dolfin;

void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );

void eval_beam_dx(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc)
{
  beam_dx::LinearForm Ldx(fsV); beam_dx::BilinearForm adx(fsV,fsV); Ldx.zeta = zetan1;
  Function zetaxtmp(fsV);
  Ldx.l = lc;
  solve ( adx == Ldx, zetaxtmp );
  zetax = zetaxtmp;
}

class Exponential : public Expression
{
  public:
    Exponential (double tt, double at=1, double ct=0, double At=1) : t(tt), a(at), c(ct), A(At) {}
    double a, A, c, t;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*cos(2*M_PI*a*(x[0]-c))*sin(2*M_PI*t); }
};

class GiNaCFunction : public Expression
{
  public:
     GiNaCFunction(GiNaC::symbol xsymt, GiNaC::ex et) : xsym(xsymt), e(et) {}
     GiNaC::symbol xsym;
     GiNaC::ex e;
     void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
     {
       values[0] = GiNaC::ex_to<GiNaC::numeric>(e.subs(xsym==x[0])).to_double();
       //if (x[0] >= 15.) values[0] = 0.;
     }
};

int parse_arguments(int argc, char** argv, char* runname)
{
  char option;
  while ( (option = getopt (argc, argv, "hr:")) != EOF )
  {
  	switch (option)
	{
		case 'h' :
			std::cout << "DRY BEAM SIMULATOR\n"
			             "==================\n"
			             "\n"
			             "To use, write a list of beams, one per line, to beams.conf\n"
			             "in $OUTDIR/beamrun.output.$RUNNAME/params\n"
			             "\n"
			             "Syntax : beamrun [-r RUNNAME]\n"
				     "\n"
				     "RUNNAME defaults to 'default'\n";
			return 0;
			break;
		case 'r' :
			sprintf(runname, "%s", optarg);
			break;
		default:
			break;
	}
  }

  return 0;
}

bool makedir(char* dir)
{
  int mret = mkdir(dir, 0755);
  return !(mret == 0 || errno == EEXIST);
}

int output_initialize(char* runname)
{
  char outdir[1000], wdir[1003], fdir[1003];
  sprintf(outdir, "%s/beamrun.output.%s", OUTROOT, runname);
  sprintf(wdir,   "%s/beamrun.output.%s/W", OUTROOT, runname);
  sprintf(fdir,   "%s/beamrun.output.%s/q", OUTROOT, runname);

  if (makedir(outdir) || makedir(wdir) || makedir(fdir)) {
  	std::cerr << "!! Could not mkdir the outdir : " << outdir << std::endl;
  	return 1;
  }

  if (chdir(outdir) == -1) {
  	std::cerr << "!! Could not chdir to the outdir : " << outdir << std::endl;
  	return 2;
  }

  return 0;
}

int parse_params(std::list<PlateDescription*>& beam_descriptions, double& Dt, double& T,
	GiNaC::parser& parser, GiNaC::ex& forcing_ex, bool& springbed, int& npts)
{
  FILE *list_file, *beam_file;
  char buffer[400];

  // Open the standard location beam list
  list_file = fopen("params/beamrun.conf", "r");

  if (list_file == NULL)
  {
    std::cerr << "!! Could not open beam list, beamrun.conf" << std::endl;
    return 1;
  }

  // Iterate through the beam list, line by line, and load associated description
  // file for each beam
  int section = 0;
  PlateDescription* d;
  char ginac_string[200];
  while ( true )
  {
    char* ret = fgets(buffer, 400, list_file);
    if (feof(list_file)) break;

    // Ugly trim
    buffer[strlen(buffer)-1] = '\0';

    // Check for new section
    if (buffer[0] == '=')
    {
    	section++;
	continue;
    }

    switch (section) { 
    	// Parameters
    	case 1:
	  // Point density
	  npts = get_int_from_line(parser, buffer, "npts", false);

	  // Dt
  	  Dt = get_double_from_line(parser, list_file, "Dt", false);

	  // Final time
  	  T = get_double_from_line(parser, list_file, "T", false);

	  // Forcing function
	  ret = fgets(buffer, 400, list_file);
	  sscanf(buffer, "forcing = %s\n", ginac_string);
  	  forcing_ex = parser(ginac_string);

	  // Springbed
	  ret = fgets(buffer, 400, list_file);
	  char resp;
	  sscanf(buffer, "springbed = %c\n", &resp);
	  springbed = (resp=='Y');
	  break;

	// Beams
        case 2:
          d = load_plate_description(buffer, parser);

          if (d == NULL)
          {
            std::cerr << "!! Could not load beam description for " << buffer << std::endl;
            return 2;
          }

          sprintf(d->name, "%s", buffer);
          beam_descriptions.push_back(d);
          
          std::cout << "[BEAMRUN]   Found a " << d->typestr << " called " << d->name << std::endl;
	  break;
		
	default:
	  break;
    }
  }

  fclose(list_file);
  return 0;
}

int main(int argc, char** argv)
{
  // Tagline
  char tagline[300] =
               "BeamRun: Dry beam simulator for MK2\n"
               "===================================";

  // Switch off logging (better yet, redirect it)
  dolfin::set_log_active(false);

  // Number of cells (or points?)
  int npts = 101;

  // Declare springbed bool (spring-like Bernoulli pressure restorative force)
  bool springbed = false;

  // Declare forcing expression
  GiNaC::symbol symx("x"), symt("t");
  GiNaC::symbol sympi("pi");
  GiNaC::symtab tab;
  tab["x"] = symx; tab["t"] = symt; tab["pi"] = sympi;
  GiNaC::parser parser(tab);
  GiNaC::ex forcing_ex;

  // Basic constants
  double w = 1., Dt = 0.1, T = 1.0, rhow = 1025., g = 9.81;
  double farleft = 0., farright = 1.;

  // Declarations for beam descriptions
  char runname[300] = "default";
  std::list<PlateDescription*> beam_descriptions;

  std::cout << std::endl;
  std::cout << tagline << std::endl << std::endl;
  std::cout << "[BEAMRUN] Launching..." << std::endl;

  // Parse arguments
  parse_arguments(argc, argv, runname);
  if (output_initialize(runname) != 0) return 2;
  std::cout << "[BEAMRUN] Initialized dirs" << std::endl;
  if (parse_params(beam_descriptions, Dt, T, parser, forcing_ex, springbed, npts) != 0) return 3;
  forcing_ex = forcing_ex.subs(sympi==M_PI);
  std::cout << "[BEAMRUN] Loaded plate descriptions" << std::endl << std::endl;

  // Basic info
  std::cout << "[BEAMRUN] Runname is [" << runname << "]" << std::endl;
  int num_beams = beam_descriptions.size();
  std::cout << "[BEAMRUN] Using " << num_beams << " beam" << (num_beams==1?"":"s") << std::endl;

  // Write log
  char logfilename[400]; sprintf(logfilename, "beamrun.%s.log", runname);
  logfile.open(logfilename);
  logfile << tagline << std::endl << std::endl;
  logfile << "PARAMETERS\n==========" << std::endl;
  logfile << "Dt : " << Dt << " T : " << T << " g : " << g << std::endl;
  logfile << "(springbed=" << springbed << ",rhow=" << rhow << ")" << std::endl;
  logfile << std::endl;

  Constant zero(0);

  // Make beams out of the descriptions
  PlateList beam_list;
  PlaterMap& platermap = Plate::get_platermap();
  farleft = (*beam_descriptions.begin())->left;
  farright = (*beam_descriptions.begin())->right;
  int ptct = 1; char platename[200];
  for ( std::list<PlateDescription*>::iterator pdit = beam_descriptions.begin() ; pdit != beam_descriptions.end() ; pdit++ )
  {
    PlateDescription* d = *pdit;

    // Select plater (beam factory of specified type)
    Plater* plater = platermap[d->typestr];

    if (!plater)
    {
    	std::cerr << "!! Unavailable plate type - " << (*pdit)->typestr << std::endl;
	return 4;
    }

    // Work out leftmost and rightmost points
    if (d->left < farleft)   farleft = d->left;
    if (d->right > farright) farright = d->right;

    // Make a mesh for this plate
    IntervalMesh &mesh = *(new IntervalMesh(npts, d->left, d->right));
    logfile << "[BEAMRUN] Initializing " << d->name << std::endl;
    d->draft = d->h*(d->rhop/rhow);
    Plate* plate = new Plate(mesh, *d, Dt, rhow, g, *plater, d->name, springbed);
    plate->init();
    logfile << std::endl;
    // I think this mesh can now be deleted : delete mesh

    plate->problem->log = false;

    beam_list.push_back(plate);
  }

  // Provide a function space to write the forcing
  IntervalMesh mesh(200, farleft, farright);
  BeamRunSpace::FunctionSpace V(mesh);
  Function force(V);

  // Time loop
  int i = 0;
  double t = 0;
  char outname[300];

  // Launch diagnostics
  char outmode[2] = "w";
  for ( PlateList::iterator plit = beam_list.begin() ; plit != beam_list.end() ; plit++ )
  	(*plit)->setup_diagnostics(outmode);

  // Dummy constant
  Constant lc(1.0);

  // Update time and check we haven't finished
  while ( (t=Dt*i) <= T + Dt*1e-5 )
  {
  	// Inform user
	printf("%.3lf", Dt*i);
	fflush(stdout);

	// Define force
	double r = 1e3;
   	GiNaCFunction q(symx, forcing_ex.subs(symt==t));
	force = q;
	Fscale(force, 1/r);
  	write_function(force, "q/q");

	// Execute step for each beam
        for ( PlateList::iterator plit = beam_list.begin() ; plit != beam_list.end() ; plit++ )
	{
	  Plate* p = *plit;
	  p->p = q;
	  p->step();
	  sprintf(outname, "W/W_%s", p->name.c_str());
  	  write_function(p->Wn1, outname);

          eval_beam_dx(p->Wx, p->pV, p->Wn1, lc);
          eval_beam_dx(p->Wxx, p->pV, p->Wx, lc);
	  sprintf(outname, "W/W_%s_Wxx", p->name.c_str());
  	  write_function(p->Wxx, outname);
	  p->eval_bm();
	  sprintf(outname, "W/W_%s_M", p->name.c_str());
  	  write_function(p->M, outname);

	  // Output bending moment, etc.
    	  p->output_diagnostics(t);

	  // Update beam variables for subsequent timestep
	  p->timestep();
	}

	// Clear output
	std::cout << "\r\r\r\r\r\r\r\r\r\r";
	i++;
  }

  // Finish diagnostics
  for ( PlateList::iterator plit = beam_list.begin() ; plit != beam_list.end() ; plit++ )
  	(*plit)->shutdown_diagnostics();

  // Tidy up output
  std::cout << std::endl;
  std::cout << "[BEAMRUN] Complete" << std::endl;
  logfile.close();

  return 0;
}
