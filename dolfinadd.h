#ifndef DOLFINADD_H
#define DOLFINADD_H

#include <dolfin.h>

using namespace dolfin;

double eval_z(Function& f, double x);
double approx_z ( const Function& f, double x, bool linearize = false );
double eval_err (Function& Wn1old, Function& Wn1);
void save_function_vector ( const Function& f, std::string name, std::string vname );
void load_function_vector ( Function& f, std::string name, std::string vname );
void backup_function_vector ( char* to, std::string name, std::string vname );
void write_function ( Function& f, std::string name );

#endif
