#!/bin/bash
source outroot.sh

sed -e '1,10d' -e '40,$d' $outroot/output.$1/mk2.$1.log > head1
sed -e '1,10d' -e '40,$d' $outroot/output.$2/mk2.$2.log > head2
cat $outroot/output.$1/automesh.geo >> head1
cat $outroot/output.$2/automesh.geo >> head2
#./compare.sh $1 $2
diff --suppress-common-lines -y head1 head2
