#include "GeoMaker.h"
#include "outroot.h"

// Use our own eps, above the compiler's zero!
#define EPS 0.0000000001

Mesh& GeoMaker::simplePlateDescriptionList (double left, double right, double depth, std::list<PlateDescription*>& platelist, double mcl[], double rat, GiNaC::ex& ic, GiNaC::symbol& xsym, bool extrude = false)
{
  //double pl = 1000, C = 0;
  char filename[50] = "automesh.geo";
  double ml = mcl[1], ms = mcl[0], mm = mcl[2], mL = mcl[3]; std::cout << mL <<std::endl;
  double /*msb = ms,*/ mmb = mm, mlb = ml; ///*RMV*/ ml = 100; mL = 100; mm = 100;
  std::map<double, PlateDescription*> orderedplates;
  double maxdraft = 0.2;
  for ( std::list<PlateDescription*>::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  {
    orderedplates[(*plit)->left] = *plit;
    maxdraft = (*plit)->draft > maxdraft ? (*plit)->draft : maxdraft;
  }
  std::ofstream geofile(filename);
  int pnum = -1;
//  geofile << "Point(" << ++pnum << ") = { " << left/rat << ", " << -mm << ", 0, " << mm << " };" << std::endl;
  //int plates = orderedplates.size();
  //double lfs = 0, rfs = 0;
//  if (plates > 0) {
   geofile << "Point(" << ++pnum << ") = { " << left/rat << ", 0, 0, " << ms << " };" << std::endl;
//   geofile << "Point(" << ++pnum << ") = { " << left/rat+2*ms << ", 0, 0, " << mm << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << left/rat+mm << ", 0, 0, " << mm << " };" << std::endl;
  //geofile << "Point(" << ++pnum << ") = { " << right/rat-mm << ", " << depth << ", 0, " << mm << " };" << std::endl;
//  geofile << "Point(0) = { " << left/rat+mm << ", 0, 0, " << mm << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << left/rat+ml << ", 0, 0, " << mm << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << (left)/rat +mL << ", 0, 0, " << mm << " };" << std::endl;//RMV
   for ( std::map<double, PlateDescription*>::iterator opit = orderedplates.begin() ; opit != orderedplates.end() ; opit++ )
   {
    PlateDescription& plate = *(opit->second);
//    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat-mm*10 << ", 0, 0, " << mm << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat-10*ms << ", 0, 0, " << ms/3 << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat << ", 0, 0, " << ms/3 << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat << ", " << -plate.draft << ", 0, " << ms/3 << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat+2*ms << ", " << -plate.draft << ", 0, " << ms/3 << " };" << std::endl;
//    geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat-ms*10 << ", " << -plate.draft << ", 0, " << ms << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat << ", " << -plate.draft << ", 0, " << ms/3 << " };" << std::endl;
    geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat << ", 0, 0, " << ms/3 << " };" << std::endl;
//    geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat+2*ms << ", 0, 0, " << mm << " };" << std::endl;

    geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat+5*ml << ", 0, 0, " << ml << " };" << std::endl;
   }
//  geofile << "Point(" << ++pnum << ") = { " << (right)/rat-3*ml << ", 0, 0, " << mm << " };" << std::endl;
   geofile << "Point(" << ++pnum << ") = { " << (right)/rat << ", 0, 0, " << ml << " };" << std::endl;
 // }
 /* else {
   for ( int l = 0 ; l <= right/rat/ms ; l++ )
    geofile << "Point(" << ++pnum << ") = { " << l*ms << ", " << GiNaC::ex_to<GiNaC::numeric>(ic.subs(xsym==l*ms)).to_double()
     << ", 0, " << ms*2 << " };" << std::endl;
    lfs = GiNaC::ex_to<GiNaC::numeric>(ic.subs(xsym==left)).to_double();
    rfs = GiNaC::ex_to<GiNaC::numeric>(ic.subs(xsym==right)).to_double();
//  }*/
//  geofile << "Point(" << ++pnum << ") = { " << right/rat << ", " << depth+mm << ", 0, " << ml << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << (right)/rat << ", " << rfs-5*ms << ", 0, " << mm << " };" << std::endl;
  //geofile << "Point(" << ++pnum << ") = { " << (right)/rat << ", " << -ms << ", 0, " << ms << " };" << std::endl;
  geofile << "Point(" << ++pnum << ") = { " << (right)/rat << ", " << depth << ", 0, " << ml << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << right/rat-4*ml << ", " << depth << ", 0, " << ml << " };" << std::endl;
  //geofile << "Point(" << ++pnum << ") = { " << right/rat-mm << ", " << depth << ", 0, " << mm << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << right/rat-5e-1 << ", " << depth << ", 0, " << mm << " };" << std::endl;

  std::map<double, PlateDescription*>::iterator opit = orderedplates.end();
  if (opit != orderedplates.begin())
   do
   {
     opit--;
     PlateDescription& plate = *(opit->second);
     geofile << "Point(" << ++pnum << ") = { " << (plate.right)/rat << ", " << depth << ", 0, " << ml << " };" << std::endl;
     geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat << ", " << depth << ", 0, " << ml << " };" << std::endl;
   } while ( opit != orderedplates.begin() );
  geofile << "Point(" << ++pnum << ") = { " << left/rat+10*ms << ", " << depth << ", 0, " << ms << " };" << std::endl;
  geofile << "Point(" << ++pnum << ") = { " << left/rat << ", " << depth << ", 0, " << ms << " };" << std::endl;
   geofile << "Point(" << ++pnum << ") = { " << left/rat << ", " << -ms << ", 0, " << ms << " };" << std::endl;
//  geofile << "Point(" << ++pnum << ") = { " << left/rat << ", " << depth+mm << ", 0, " << ml << " };" << std::endl;
  //geofile << "Point(" << ++pnum << ") = { " << left/rat << ", " << lfs-2*ms << ", 0, " << mm << " };" << std::endl;
  for ( int i = 0 ; i < pnum ; i++ )
     geofile << "Line(" << i << ") = {" << i << "," << i+1 << "};" << std::endl;
  geofile << "Line(" << pnum << ") = {" << pnum << ",0};" << std::endl;
  geofile << "Line Loop(" << pnum << ") = {0";
  for ( int i = 1 ; i <= pnum ; i++ )
     geofile << "," << i;
  geofile << "};" << std::endl;
  int ps1 = pnum;
  geofile << "Plane Surface(0) = {" << ps1 << "};" << std::endl;
  if (extrude) {
   geofile << std::endl;
   geofile << "Extrude {0,0,1} { Surface{0}; };" << std::endl;
  }
  geofile << std::endl;
  geofile << "Field[1] = MathEval;" << std::endl;
  geofile << "Field[1].F = \"" << mmb << "+" << mlb << "*sin(0.5*3.14*y/" << -depth /* RMV */<< ")" <<
    "*sin(0.5*3.14*y/" << -depth /* RMV */<< ")\";" << std::endl;
  geofile << "Field[2] = MathEval;" << std::endl;
  geofile << "Field[2].F = \"(x-" << (right-200)/rat << ")*" << mlb << "/" << 200/rat << "\";" << std::endl;
  geofile << "Field[3] = Max;" << std::endl;
  geofile << "Field[3].FieldsList = {1,2};" << std::endl;
  geofile << "Background Field = 3;" << std::endl;

/*
  for ( std::map<double, PlateDescription*>::iterator opit = orderedplates.begin() ; opit != orderedplates.end() ; opit++ )
  {
    PlateDescription& plate = *(opit->second);
    geofile << "Point(" << ++pnum << ") = { " << (plate.left)/rat-mm*10 << ", " << -2*mm << ", 0, " << mm << " };" << std::endl;
  }
  */

/*
  geofile << "Point(" << ++pnum << ") = { " << left/rat+mm << ", " << -maxdraft*3 << ", 0, " << ml << " };" << std::endl;
  geofile << "Point(" << ++pnum << ") = { " << right/rat-mm << ", " << -maxdraft*3 << ", 0, " << ml << " };" << std::endl;
  geofile << "Point(" << ++pnum << ") = { " << right/rat-mm << ", " << -maxdraft*3-mm << ", 0, " << ml << " };" << std::endl;
  geofile << "Point(" << ++pnum << ") = { " << left/rat+mm << ", " << -maxdraft*3-mm << ", 0, " << ml << " };" << std::endl;
  geofile << "Line(" << ++lnum << ") = {" << pnum-3 << ", " << pnum-2 << "};" << std::endl;
  geofile << "Line(" << ++lnum << ") = {" << pnum-2 << ", " << pnum-1 << "};" << std::endl;
  geofile << "Line(" << ++lnum << ") = {" << pnum-1 << ", " << pnum << "};" << std::endl;
  geofile << "Line(" << ++lnum << ") = {" << pnum << ", " << pnum-3 << "};" << std::endl;
  geofile << "Line Loop(" << lnum << ") = {" << lnum-3 << "," <<lnum-2<<","<<lnum-1<<","<<lnum << "," <<lnum-3<<"};" << std::endl;
  geofile << "Plane Surface(1) = {" << lnum << "};" << std::endl;
  geofile << "Physical Surface(2) = {0,1};";
  */
  geofile.close();

  Mesh& mesh = generateGMSHMeshFromFile(filename, rat, extrude);
  /*
  for (VertexIterator vit(mesh) ; !vit.end() ; ++vit)
  {
    double x = mesh.geometry().x(vit->index(), 0);
    if (x*rat < pl)
     mesh.geometry().x(vit->index(),0) *= rat;
    else if (x*rat > pl+C)
     mesh.geometry().x(vit->index(),0) = x*rat+C;
    else mesh.geometry().x(vit->index(), 0) = x*rat + (x*rat-pl);
  }*/
  return mesh;
}

Mesh& GeoMaker::generateGMSHMeshFromFile(char* filename, double rat = 1.0, bool mode3d = false)
{
  if (system ( "rm -f tmp.automesh" )) std::cout << "[MK2] GeoMaker: could not remove temporary mesh generation file" << std::endl;
  std::ostringstream gmshcommand; gmshcommand << GMSH_PATH << " -" << (mode3d?'3':'2') << " " << filename
   <<" -algo del" << (mode3d?'3':'2') << "d >> tmp.automesh";
   std::cout << gmshcommand.str().c_str() << std::endl;
  if (system ( gmshcommand.str().c_str() ))
      std::cout << "[MK2] ERROR IN MESH GENERATION!! ANY FUTURE OUTPUT COULD BE INCORRECT!!" << std::endl;
  if (system ( "dolfin-convert automesh.msh automesh.xml >> tmp.automesh"))
      std::cout << "[MK2] ERROR IN MESH GENERATION!! ANY FUTURE OUTPUT COULD BE INCORRECT!!" << std::endl;
  if (system ( "dolfin-order automesh.xml >> tmp.automesh") )
      std::cout << "[MK2] ERROR IN MESH GENERATION!! ANY FUTURE OUTPUT COULD BE INCORRECT!!" << std::endl;
  Mesh& mesh = *(new Mesh("automesh.xml"));
  for (VertexIterator vit(mesh) ; !vit.end() ; ++vit)
  {
    mesh.geometry().x(vit->index(),0) *= rat;
  }
  return mesh;
}
