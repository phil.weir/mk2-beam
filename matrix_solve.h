#include <dolfin.h>
#include <dolfin/la/PETScMatrix.h>
#include <dolfin/la/PETScVector.h>

void matrix_solve (
        dolfin::PETScMatrix& A1,
        dolfin::PETScMatrix& B12m,
        dolfin::PETScMatrix& C12,
        dolfin::PETScMatrix& B12p,
        dolfin::PETScMatrix& A2,
        dolfin::PETScMatrix& B23m,
        dolfin::PETScMatrix& C23,
        dolfin::PETScMatrix& B23p,
        dolfin::PETScMatrix& A3,
        dolfin::PETScVector& U1,
        dolfin::PETScVector& LA12,
        dolfin::PETScVector& U2,
        dolfin::PETScVector& LA23,
        dolfin::PETScVector& U3,
        dolfin::PETScVector& L1,
        dolfin::PETScVector& G12m,
        dolfin::PETScVector& G12p,
        dolfin::PETScVector& L2,
        dolfin::PETScVector& G23m,
        dolfin::PETScVector& G23p,
        dolfin::PETScVector& L3,
	double tol=PETSC_DEFAULT
        );
