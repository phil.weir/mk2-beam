#!/bin/bash

top=$1
file=$2
here=$PWD

source $top/orders.sh

echo " checking $file"
cp $file $top/tmp
cd $top/tmp
cp $file tmp.ufl
for exchange in $EXCHANGES
do
 sed -e "s/$exchange/g" tmp.ufl > tmp1.ufl
 mv tmp1.ufl tmp.ufl
done;
diff $file.last tmp.ufl > /tmp/changed
if [ ! $? -eq 0 -o "$3" == "M" ]
then
  mv tmp.ufl $file
  ffc -l dolfin $file > ../ffc.log
  name=`echo $file | sed -e 's/\(.*\)\..*/\1/' -e 's/\.//' -`
  newh=$name.h
  cp $newh $here
  cp $file $file.last
fi;
cd $here
if [ "$3" == "M" -a -e $top/ffc.log ]
then
  cat $top/ffc.log
fi
