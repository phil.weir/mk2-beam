#!/bin/bash

source orders.sh

builddir=$1
mkdir -p $builddir/include
mkdir -p $builddir/include/tmp
tmpdir=$builddir/include/tmp
nowdir=`pwd`
cd $tmpdir

exchg=""
for exchange in $EXCHANGES
do
  exchg="$exchg \-e 's/$exchange/g'"
done

for dir in . plates navier
do
 for fullfile in `ls $nowdir/$dir/*.ufl`
 do
   file=`basename $fullfile`
   name=`echo $file | sed -e 's/\(.*\)\..*/\1/' -e 's/\.//' -`
   if [ -e ../$name.h -a ../$name.h -nt $fullfile -a "$2" != "M" -a ../$name.h -nt $nowdir/orders.sh ]
   then
     continue
   fi

   cp $fullfile .
   cp $file tmp.ufl
 
   echo $exchg tmp.ufl | xargs sed > tmp1.ufl
   mv tmp1.ufl tmp.ufl
 
   rebuild=0
   if [ "$2" == "M" ]
   then
     echo "  $file"
     rebuild=1
   else
     if [ -e $file.last ]
     then
       diff $file.last tmp.ufl > /tmp/changed
       diffret=$?
     else
       echo "  $file [NEW]"
       diffret=0
       rebuild=1
     fi

     if [ -e $file.last -a ! -e ../$name.h ]
     then
       echo "  $file [MISSING .h]"
       rebuild=1
     fi
 
     if [ "$diffret" -ne "0" ]
     then
       echo "  $file"
       rebuild=1
     fi
   fi

   if [ $rebuild -eq 1 ]
   then
     mv tmp.ufl $file
     ffc -l dolfin $file > ../ffc.log
     if [ $? -ne 0 ]
     then
       echo "  FFC ERROR"
       rm -f $name.h
       exit 1
     fi
     mv $name.h ..
     cp $file $file.last
   fi
   touch ../$name.h
   #cp $newh $here
   #cp $file $file.last
 done
done
cd $nowdir
