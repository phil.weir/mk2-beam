// THIS IS ALMOST VERBATIM FROM THE FENICS PROJECT!!!!!
//
// Copyright (C) 2005-2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2005-12-02
// Last changed: 2006-08-07

#ifndef __DOUBLE_FLUID_MESH_H
#define __DOUBLE_FLUID_MESH_H

#include <dolfin.h>

namespace dolfin
{

  class DoubleFluidMesh : public Mesh
  {
  public:
    enum Type {right, left, crisscross};
    
    DoubleFluidMesh(double width, double height, uint nx, uint ny, double draft, double left1, double right1, double left2, double right2, Type type = right);

  };
  
}

#endif
