#include <dolfin.h>
#include <ginac/ginac.h>

class WaveMaker
{
  public:
   WaveMaker(double ampt, double fret, double lwrt, double btmt, double numt, double attcoefft);
   double velocity(double t, const dolfin::Array<double>& x);
   double acceleration(double t, const dolfin::Array<double>& x);
   double amp, fre, lwr, btm, num, attcoeff;
   GiNaC::symbol symt;
   GiNaC::ex dis_ex, vel_ex, vel_ex_a, vel_ex_d, acc_ex, acc_ex_a, acc_ex_d;
};

WaveMaker* load_wavemaker ( char* wavemakerloc );
