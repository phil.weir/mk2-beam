#!/bin/bash
source outroot.sh

rm -rf $outroot/output.$1
touch $outroot/output.$1/mk2.$1.log
tail -f $outroot/output.$1/mk2.$1.log&
echo -n "Same as: "
read -e AS
echo -n "But: "
read -e BUT
echo "$AS
$BUT" | ./mk2 -A -D $1
