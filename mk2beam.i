%module mk2beam

%{
#include <dolfin.h>
#include <numpy/arrayobject.h>
%}
%import "std_string.i"
%import "std_map.i"
%import "dolfin/swig/shared_ptr_classes.i"
%import "dolfin/swig/typemaps/std_pair.i"
%import "dolfin/swig/typemaps/numpy.i"
%import "dolfin/swig/typemaps/array.i"
%import "dolfin/swig/typemaps/std_set.i"
%import <exception.i>
%include "dolfin/swig/exceptions.i"
%import <std_string.i>

%{
#include "plateproblem.h"
#include "PlateTop.h"
#include "PlateTopLinear.h"
%}

struct _PlateDescription
{
    char name[300];
    char typestr[200];
    double left;
    double right;
    double draft;
    double E;
    double h;
    double nu;
    double beta;
    double rhop;
    double mup;
};
typedef struct _PlateDescription PlateDescription;


class PlateMesh;

class AlphaFunction : public Expression
{
  public:
    AlphaFunction(double alphat, Function& Wddtt, Function& Wddtkm1t, Function& Wt) :
      alpha(alphat), Wddt(Wddtt), Wddtkm1(Wddtkm1t), W(Wt);
    double alpha;
    Function &Wddt, &Wddtkm1, &W;
    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const;
};

class PlateDomain;

class PlateDomainFlat : public SubDomain
{
  public:
    PlateDomainFlat(double LEFTt, double RIGHTt) : LEFT(LEFTt), RIGHT(RIGHTt) ;
    double LEFT, RIGHT;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const;
};

class Normal : public Function
{
  public:
    Normal(FunctionSpace& tV, double lt, double rt) : Function(tV), l(lt), r(rt);
    double l, r;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};
class Boundary : public SubDomain
{
  public:
    Boundary(double lt, double rt) : l(lt), r(rt) ;
    double l, r;
    bool inside ( const dolfin::Array<double> &x, bool on_boundary ) const;
};
    
class PlateProblem
{
  public:
    PlateProblem(dolfin::Mesh& mesht, double l, double r, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmgt = true);
    ~PlateProblem() ;
    
    double get_D ();
    dolfin::Mesh& mesh;
    double l, r;
    dolfin::Constant &Dtc, &Dc, &betac, &rhopc, &rhowc, &muc, &gc, &hc;
    bool sbmg;
    Top::FunctionSpace &tV;
    Normal n; Boundary boundary;
    bool log;
    dolfin::Vector &line_vector;

    virtual void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing ) = 0;
    virtual std::string gettype () = 0;
    virtual void nonstep () = 0;
    virtual void adjacc (double alpha) = 0;
    virtual void timestep () = 0;
    virtual void clstep () = 0;
    virtual void load_function_vectors(std::string name) {}
    virtual void save_function_vectors(std::string name) {}
    virtual void output_diagnostics(double t,
      dolfin::Function& Wn1, dolfin::Function& Wdtn1, dolfin::Function& Wddtn1,
      dolfin::Function& Wn, dolfin::Function& Wdotn, dolfin::Function& Wddotn ) {}
    virtual void setup_diagnostics(std::string name, char* openmode) {}
    virtual void shutdown_diagnostics() {}
};

class Plater
{
  public:
  virtual PlateProblem* make (dolfin::Mesh& mesht, double lt, double rt, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg) = 0;
};

typedef std::map< std::string, Plater* > PlaterMap;

const PlateMesh& make_pline( const Mesh& bdy, double left, double right, double draft, MeshFunction<uint>** p_to_bdy );

class Plate
{
  public:
    Plate(dolfin::Mesh& pline, double leftt, double rightt, double draftt,
          double Dt, double Et, double ht, double nut, double beta,
  	double rhopt, double rhowt, double mup, double gt, Plater& plater, const std::string& namet, bool sbmg = true) ;
    Plate(dolfin::Mesh& pline, PlateDescription& d, double Dt, double rhowt, double gt, Plater& plater, const std::string& namet, bool sbmg = true) ;
    //void step(Expression& pressure);
    void step();
    void adjacc(double alpha);
    void nonstep();
    void save_function_vectors();
    void load_function_vectors();
    void backup_function_vectors(char* to);
    void timestep();
    void clstep();
    double evalerr();
    void output_diagnostics(double t);
    void setup_diagnostics(char* openmode);
    void shutdown_diagnostics();
    FILE* mbmf, *rmsf, *midbmf;
    double eval_rms_();
    double eval_mean_bending_moment();
    std::string gettype();
    double left, right, draft;
    static Plate* this_plate(std::list<Plate*>& platelist, double x, bool edges, double length);
    static Plate* find_plate(std::list<Plate*>& platelist, bool onright, double x, bool edges, double length);
    static PlaterMap& get_platermap();
    static PlaterMap platermap;
    static Plater& get_plater(const std::string& name);

    std::string name;
    PlateTop::FunctionSpace &pV;
    PlateTopLinear::FunctionSpace &plV;
    Top::FunctionSpace &tV;
    dolfin::Function &bm, &Wdotn, &Wn, &Wddotn, &Wn1, &Wdtn1, &Wddtn1, &Wx, &Wxx, &Wn1_linear, &Wz, &Wz_Wpress, &Wzdt, &Wzddt, &Wzn, &p;
    dolfin::Function &Wddtkm1, &Wdtkm1, &Wkm1;
    dolfin::Function &forcing, &phidtn1p1, &gsphin1p1;
    dolfin::Function &pressuren1p1, &Wn1old, &Wn1old_linear;
    PlateProblem* problem;
    double E, h, nu, rhop, rhow, g;
    bool refpress_on, refpress_now;
    double refpress, refpress_x, refpress_zdtn1;
};

typedef std::list<Plate*> PlateList;

void write_function ( dolfin::Function& f, std::string name );
