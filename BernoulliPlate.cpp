#include "BernoulliPlate.h"
#include "logfile.h"

std::string BernoulliPlate::gettype()
{
  return "Euler-Bernoulli Beam";
}

void BernoulliPlate::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  /* This is really bad and needs fixed. We know that a linear map will not change the response
   * so we map the ends to 0 elevation as the solver doesn't introduce any vibrations provided
   * this is the case */
  PlateFloat::FunctionSpace V(mesh);   PlateFloat::BilinearForm a(V,V);   PlateFloat::LinearForm L(V);
  diff::FunctionSpace  Vd(mesh);  diff::BilinearForm  ad(Vd,Vd);  diff::LinearForm  Ld(Vd);
  ddiff::FunctionSpace Vdd(mesh); ddiff::BilinearForm add(Vdd,Vdd); ddiff::LinearForm Ldd(Vdd);

  a.Dt = Dtc; a.D = Dc; a.h = hc; a.beta = betac; a.rhop = rhopc; a.rhow = rhowc; a.mu = muc; a.g = gc;
  Constant sbmgc(sbmg?1.0:0.0);
  a.sbmg = sbmgc;
  L.Dt = Dtc; L.beta = betac;
  Ldd.Dt = Dtc; Ldd.beta = betac; L.mu = muc;
  Ld.Dt = Dtc;

  Function Wn1f(V), Wddtn1f(Vdd), Wdtn1f(Vd);
  Constant zerof(0.0, 0.0);
  L.p = pp; L.Wdotn = Wdotn; L.Wn = Wn; L.Wddotn = Wddotn; L.f = forcing;
  VariationalProblem elprob (a, L); // RMV INSERT BC IF NESS
  elprob.solve(Wn1f); Wn1 = Wn1f[0];

  if (log) presout << pp;
  if (log) forcout << forcing;
  Ldd.Wn = Wn; Ldd.Wn1 = Wn1; Ldd.Wddotn = Wddotn; Ldd.Wdotn = Wdotn;
  VariationalProblem ddiffprob (add, Ldd);
  ddiffprob.solve(Wddtn1);
//  Wddtn1 = Wddtn1f;

  Ld.Wddotn = Wddotn; Ld.Wddotn1 = Wddtn1; Ld.Wdotn = Wdotn;
  VariationalProblem diffprob(ad, Ld);
  diffprob.solve(Wdtn1);
//  Wdtn1 = Wdtn1f;
  if (log) dispout << Wn1;
  if (log) veloout << Wdtn1;
  if (log) acceout << Wddtn1;
}
