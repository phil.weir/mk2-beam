source outroot.sh
padn=`printf %06d $2`
for file in {left,right,plate1,left1}
do
 rm $file.csv
 cp $outroot/output.$1/$file$3-$padn.csv $file.csv
 #sed '1d' $file-1.csv > $file.csv
done;
cp $outroot/output.$1/parameters parameters.tmp
python test.py -S $2
rsvg -w 1200 test.svg test.png
rsvg -w 4000 test.svg printable.png
#octave test.m > fftlog
#gnuplot test.gp > fftlog
cp test.png $outroot/output.$1/test$padn.png
for file in {left,right,plate1}
do
 echo 'x,y' > $file-1.csv
 sed -e '1,5d' -e 's/\([0-9]\) /\1,/' $file.fft.mat >> $file-1.csv
 cp $file-1.csv $outroot/output.$1/$file-$padn-fft.csv
done;
display test.png
