#!/usr/bin/env python
from ufl import *
set_level(DEBUG)
E = FiniteElement("Lagrange", "interval", PLATE_A)
E_M = MixedElement(E,E,E)

U = TestFunction(E_M)
v, t, lda = split(U)
DZ = TrialFunction (E_M)
Z = Coefficient (E_M)
w,u,mu = split (Z)

w_n = Coefficient (E)
w_n_dot = Coefficient (E)
w_ddot_n = Coefficient (E)
u_n = Coefficient (E)
u_dot_n = Coefficient (E)
u_ddot_n = Coefficient (E)
beta = Constant (interval)
Dt = Constant (interval)

w_ddot = 1/(beta*Dt**2)*w-w_n-1/(beta*Dt)*w_dot_n-1/(2*beta-1)*w_ddot_n
u_ddot = 1/(beta*Dt**2)*u-u_n-1/(beta*Dt)*u_dot_n-1/(2*beta-1)*u_ddot_n

c_1 = Constant (interval)
c_2 = Constant (interval)
c_3 = Constant (interval)
c_4 = Constant (interval)
c_5 = Constant (interval)
c_6 = Constant (interval)
c_7 = Constant (interval)
q = Coefficient (E)
n = E.cell().n

L = c_1*v*w_ddot*dx+c_2*diff(v, x)*diff(w_ddot, x)*dx+c_3*diff(v, x)*diff(mu, x)*dx+c_5*v*w*dx+c_4*diff(v, x)*diff(w, x)*diff(u, x)+1/2*diff(w, x)**2*dx
L = L-c_2*v*diff(w_ddot, x)*n*ds-c_3*v*diff(mu, x)*n*ds
L = L+t*u_ddot*dx+c_6*diff(t, x)*diff(u, x)+1/2*diff(w, x)**2*dx-t*diff(u, x)+1/2*diff(w, x)**2*n*ds
L = L+lda*mu*dx-c_7*diff(lda, x)*diff(w, x)*dx+c_7*lda*diff(w, x)*n*ds
L = L-v*q*dx
a = -derivative (L,Z,DZ)
