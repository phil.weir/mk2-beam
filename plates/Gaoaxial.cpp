#include "Gaoaxial.h"
#include "expr.h"
#include "GaoaxialForm.h"
#include "EulerBernoulliBM.h"
#include "thspace.h"
//#include "GaoaxialForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class GaoaxialScaledFunction : public Expression
{
  public:
    GaoaxialScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

void GaoaxialScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  dolfin::Array<double> x2(1); x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class NormalFunction;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

GaoaxialBeam::GaoaxialBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     Vth(mesht), inited(false), thdotn(Vth), thn(Vth), thddotn(Vth), thn1(Vth), thdtn1(Vth), thddtn1(Vth),
     thddtkm1(Vth), thdtkm1(Vth), thkm1(Vth), thn1old(Vth)
{
  Constant zero(0);
  thdotn = zero;
  thn = zero;
  thddotn = zero;
  thn1 = zero;
  thdtn1 = zero;
  thddtn1 = zero;
  thddtkm1 = zero;
  thdtkm1 = zero;
  thkm1 = zero;
  thn1old = zero;
}

std::string GaoaxialBeam::gettype()
{
  return "Gao Beam without axial extension BC (i.e. lacking axial force instead)";
}

void GaoaxialBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;
  //Mesh& mesh(*Wn1.function_space()->mesh());

  const FunctionSpace& V(*Wn1.function_space());
  EulerBernoulliBM::LinearForm LBM(V);
  EulerBernoulliBM::BilinearForm aBM(V,V);

  double w = 1;
  double I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;
  Constant Ic(I), Ec(E);
  LBM.E = Ec;
  LBM.I = Ic;
  LBM.w = Wn1;
  Function M(V);
  solve(aBM == LBM, M);
  BM.interpolate(M);
}

void GaoaxialBeam::init ()
{
  L = (r-l);
  //L = 1;//RMV

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  thspace::FunctionSpace& Vth = *(new thspace::FunctionSpace(*calcmesh));
  Function *_thdotn = new Function(Vth);
  Function *_thn = new Function(Vth);
  Function *_thddotn = new Function(Vth);
  Function *_thn1 = new Function(Vth);
  Function *_thdtn1 = new Function(Vth);
  Function *_thddtn1 = new Function(Vth);
  Function *_thddtkm1 = new Function(Vth);
  Function *_thdtkm1 = new Function(Vth);
  Function *_thkm1 = new Function(Vth);
  Function *_thn1old = new Function(Vth);
  thdotn = *_thdotn;
  thn = *_thn;
  thddotn = *_thddotn;
  thn1 = *_thn1;
  thdtn1 = *_thdtn1;
  thddtn1 = *_thddtn1;
  thddtkm1 = *_thddtkm1;
  thdtkm1 = *_thdtkm1;
  thkm1 = *_thkm1;
  thn1old = *_thn1old;
}

void GaoaxialBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void GaoaxialBeam::shutdown_diagnostics()
{
  fclose(stef);
}

double eval_euler_bernoulli_strain_energy(Function& W, Function& th, double EI, double kAG, double L, double l);

void GaoaxialBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

  if (inited) fprintf(stef, "%lf,%lf\n", t, eval_euler_bernoulli_strain_energy(Wn1, thn1, E*I, kappa*A*G, L, l));
  fflush(stef);
}

void GaoaxialBeam::nonstep () {
   thn1 = thn; thdtn1 = thdotn; thddtn1 = thddotn;//RMV
}
void GaoaxialBeam::timestep () {
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
   thn      = thn1;
   thdotn   = thdtn1;
   thddotn  = thddtn1;
 }
void GaoaxialBeam::clstep()
 {
   thn1old  = thn1;
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
 }
void GaoaxialBeam::adjacc(double alpha) {
   Function thddtn1tmp(Vth), thdtn1tmp(Vth), thn1tmp(Vth); thddtn1tmp = thddtn1; thdtn1tmp = thdtn1; thn1tmp = thn1;
   AlphaFunction thalpha(alpha, thddtn1tmp, thddtkm1, thn1old);
   AlphaFunction thalphap(4*alpha, thddtn1tmp, thdtkm1, thn1old);
   AlphaFunction thalphapp(16*alpha, thddtn1tmp, thkm1, thn1old);
   thddtn1 = thalpha;// thdtn1 = thalphap; thn1 = thalphapp;
   thddtkm1 = thddtn1; thdtkm1 = thdtn1; thkm1 = thn1;
}

void GaoaxialBeam::load_function_vectors(std::string name)
{
   load_function_vector(thddtkm1, name, "thddtkm1");
   load_function_vector(thdtkm1, name, "thdtkm1");
   load_function_vector(thkm1, name, "thkm1");
   load_function_vector(thn, name, "thn");
   load_function_vector(thdotn, name, "thdotn");
   load_function_vector(thddotn, name, "thddotn");
   load_function_vector(thn1, name, "thn1");
   load_function_vector(thdtn1, name, "thdtn1");
   load_function_vector(thddtn1, name, "thddtn1");
}

void GaoaxialBeam::save_function_vectors(std::string name)
{
   save_function_vector(thddtkm1, name, "thddtkm1");
   save_function_vector(thdtkm1, name, "thdtkm1");
   save_function_vector(thkm1, name, "thkm1");
   save_function_vector(thn, name, "thn");
   save_function_vector(thdotn, name, "thdotn");
   save_function_vector(thddotn, name, "thddotn");
   save_function_vector(thn1, name, "thn1");
   save_function_vector(thdtn1, name, "thdtn1");
   save_function_vector(thddtn1, name, "thddtn1");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

void GaoaxialBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  GaoaxialForm::FunctionSpace V(*calcmesh);
  GaoaxialForm::CoefficientSpace_Wn VW(*calcmesh);
  //GaoaxialForm::CoefficientSpace_n Vn(*calcmesh);
  GaoaxialForm::LinearForm Ft(V); GaoaxialForm::JacobianForm Jt(V, V);

  GaoaxialScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  std::vector< const BoundaryCondition* > bcU;
  Ends ends(0, 1);
  SubSpace Vthx(V, 1); Function thx_Jt_ends(VW); thx_Jt_ends = zero;
  //DirichletBC* endbc = new DirichletBC(Vthx, thx_Jt_ends, ends); //bcU.push_back(endbc);
  
  //VariJtionalProblem tprob(Jt, Ft, bcU);
  Constant zero2(0,0);
  Function F(V), Z(V); Z.vector()->zero();

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1, k = 2*pow(h/2,3)*E/(3*(1-nu*nu)*rhop*A), a = h*E/((1-nu)*rhop*A);
  Constant c1(L*L*L/(T*T)), c2(k/L), c3(.5*(1+nu)), c5(a*L), c4(sbmg ? rhow*g*L*L*L/(rhop*A) : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L*L/(rhop*A));
  //Normal n(0.5);//Vn, 0, 1);
  Fscale(q, L*L/(rhop*A));
  //Z[0] = zero; Z[1] = zero;

  Jt.c1 = c1; Jt.c2 = c2; Jt.c3 = c3; Jt.c4 = c4; Jt.c5 = c5; Jt.beta = betac; Jt.Dt = Dtc;// Jt.n = n;
  Ft.c1 = c1; Ft.c2 = c2; Ft.c3 = c3; Ft.c4 = c4; Ft.c5 = c5; Ft.beta = betac; Ft.Dt = Dtc;// Ft.n = n;
  Ft.c1 = c1; Ft.beta = betac; Ft.Dt = Dtc;
  Ft.q = q;
  Ft.Wn = WnS; Ft.Wdotn = WdotnS; Ft.Wddotn = WddotnS;
  //Ft.thn = thn; Ft.thdotn = thdotn; Ft.thddotn = thddotn;

  Parameters params("nonlinear_variational_solver");
  Parameters npars("newton_solver");
  npars.add("maximum_iterations", 50);
  npars.add("relative_tolerance", 1e-6);
  params.add(npars);

  int i = 0;
  //do {
  	Ft.Z = Z; Jt.Z = Z;
	//tprob.solve(F);
        solve(Ft == 0, Z, bcU, Jt, params);
	//Fadd(Z, F);
  //} while ( i++ < 13 );

  GaoaxialScaledFunction Wnews(1/L, -l/L, Z[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);

  //GaoaxialForm::CoefficientSpace_Wn VW2(mesh);
  //GaoaxialScaledFunction Monews(1/L, -l/L, Z[1]);
  //Function Mof(VW2); Mof = Monews;
  ////Fscale(Mof, rhop*A/L);
  //write_function(Mof, "W/W_434_Gaoaxial_Mo"); // RMV
  //GaoaxialScaledFunction thnews(1/L, -l/L, Z[2]);
  //Function thf(VW2); thf = thnews;
  //write_function(thf, "W/W_434_Gaoaxial_th"); // RMV

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
