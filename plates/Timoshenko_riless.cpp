#include "Timoshenko_riless.h"
#include "expr.h"
#include "Timoshenko_rilessForm.h"
#include "thspace.h"
//#include "Timoshenko_rilessForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class Timoshenko_rilessScaledFunction : public Expression
{
  public:
    Timoshenko_rilessScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

void Timoshenko_rilessScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  dolfin::Array<double> x2(1); x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

Timoshenko_rilessBeam::Timoshenko_rilessBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     Vth(mesht), inited(false), thdotn(Vth), thn(Vth), thddotn(Vth), thn1(Vth), thdtn1(Vth), thddtn1(Vth),
     thddtkm1(Vth), thdtkm1(Vth), thkm1(Vth), thn1old(Vth)
{
  Constant zero(0);
  thdotn = zero;
  thn = zero;
  thddotn = zero;
  thn1 = zero;
  thdtn1 = zero;
  thddtn1 = zero;
  thddtkm1 = zero;
  thdtkm1 = zero;
  thkm1 = zero;
  thn1old = zero;
}

std::string Timoshenko_rilessBeam::gettype()
{
  return "Timoshenko Beam (without rotational inertia)";
}

void Timoshenko_rilessBeam::init ()
{
  L = (r-l);

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;
  logfile << "After scaling: " << Dc/(L*L) << " Wxxxx + " << rhopc*hc*1 << " Wdd = " << rhowc*gc << " W +..." << std::endl;

  thspace::FunctionSpace& Vth = *(new thspace::FunctionSpace(*calcmesh));
  Function *_thdotn = new Function(Vth);
  Function *_thn = new Function(Vth);
  Function *_thddotn = new Function(Vth);
  Function *_thn1 = new Function(Vth);
  Function *_thdtn1 = new Function(Vth);
  Function *_thddtn1 = new Function(Vth);
  Function *_thddtkm1 = new Function(Vth);
  Function *_thdtkm1 = new Function(Vth);
  Function *_thkm1 = new Function(Vth);
  Function *_thn1old = new Function(Vth);
  thdotn = *_thdotn;
  thn = *_thn;
  thddotn = *_thddotn;
  thn1 = *_thn1;
  thdtn1 = *_thdtn1;
  thddtn1 = *_thddtn1;
  thddtkm1 = *_thddtkm1;
  thdtkm1 = *_thdtkm1;
  thkm1 = *_thkm1;
  thn1old = *_thn1old;
}

void Timoshenko_rilessBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void Timoshenko_rilessBeam::shutdown_diagnostics()
{
  fclose(stef);
}

double eval_strain_energy(Function& W, Function& th, double EI, double kAG, double L, double l);

void Timoshenko_rilessBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

  if (inited) fprintf(stef, "%lf,%lf\n", t, eval_strain_energy(Wn1, thn1, E*I, kappa*A*G, L, l));
  fflush(stef);
}

void Timoshenko_rilessBeam::nonstep () {
   thn1 = thn; thdtn1 = thdotn; thddtn1 = thddotn;//RMV
}
void Timoshenko_rilessBeam::timestep () {
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
   thn      = thn1;
   thdotn   = thdtn1;
   thddotn  = thddtn1;
 }
void Timoshenko_rilessBeam::clstep()
 {
   thn1old  = thn1;
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
 }
void Timoshenko_rilessBeam::adjacc(double alpha) {
   Function thddtn1tmp(Vth), thdtn1tmp(Vth), thn1tmp(Vth); thddtn1tmp = thddtn1; thdtn1tmp = thdtn1; thn1tmp = thn1;
   AlphaFunction thalpha(alpha, thddtn1tmp, thddtkm1, thn1old);
   AlphaFunction thalphap(4*alpha, thddtn1tmp, thdtkm1, thn1old);
   AlphaFunction thalphapp(16*alpha, thddtn1tmp, thkm1, thn1old);
   thddtn1 = thalpha;// thdtn1 = thalphap; thn1 = thalphapp;
   thddtkm1 = thddtn1; thdtkm1 = thdtn1; thkm1 = thn1;
}

void Timoshenko_rilessBeam::load_function_vectors(std::string name)
{
   load_function_vector(thddtkm1, name, "thddtkm1");
   load_function_vector(thdtkm1, name, "thdtkm1");
   load_function_vector(thkm1, name, "thkm1");
   load_function_vector(thn, name, "thn");
   load_function_vector(thdotn, name, "thdotn");
   load_function_vector(thddotn, name, "thddotn");
   load_function_vector(thn1, name, "thn1");
   load_function_vector(thdtn1, name, "thdtn1");
   load_function_vector(thddtn1, name, "thddtn1");
}

void Timoshenko_rilessBeam::save_function_vectors(std::string name)
{
   save_function_vector(thddtkm1, name, "thddtkm1");
   save_function_vector(thdtkm1, name, "thdtkm1");
   save_function_vector(thkm1, name, "thkm1");
   save_function_vector(thn, name, "thn");
   save_function_vector(thdotn, name, "thdotn");
   save_function_vector(thddotn, name, "thddotn");
   save_function_vector(thn1, name, "thn1");
   save_function_vector(thdtn1, name, "thdtn1");
   save_function_vector(thddtn1, name, "thddtn1");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

void Timoshenko_rilessBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  Timoshenko_rilessForm::FunctionSpace V(*calcmesh);
  Timoshenko_rilessForm::CoefficientSpace_Wn VW(*calcmesh);
  //Timoshenko_rilessForm::CoefficientSpace_thn Vth(*calcmesh);
  //Timoshenko_rilessForm::CoefficientSpace_n Vn(*calcmesh);
  Timoshenko_rilessForm::LinearForm Lt(V); Timoshenko_rilessForm::BilinearForm at(V, V);

  Timoshenko_rilessScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  std::vector< const BoundaryCondition* > bcU;
  VariationalProblem tprob(at, Lt, bcU);
  Function F(V);

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant c1(rhop*L*L/(kappa*G*T*T)), /*c2(rhop*I/(kappa*A*G*T*T)),*/ c3(E*I/(kappa*A*G*L*L)), c4(sbmg ? rhow*g*L*L/(kappa*A*G):0.),
    betac(beta), Dtc(Dt);
  Fscale(q, L/(kappa*A*G));

  //std::cout << E*I/(kappa*L*L*A*G) << std::endl;

  at.c1 = c1; /*at.c2 = c2; */at.c3 = c3; at.c4 = c4; at.beta = betac; at.Dt = Dtc;// at.n = n;
  Lt.c1 = c1; /*Lt.c2 = c2; */Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;
  //Lt.thn = thn; Lt.thdotn = thdotn; Lt.thddotn = thddotn;

  for ( int i = 0 ; i < 1 ; i++ ) {
  	tprob.solve(F);
  }
  Timoshenko_rilessScaledFunction Wnews(1/L, -l/L, F[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);
  thn1.interpolate(F[1]);
  //write_function("W/W_434_Timo_thn1", thn1);
  write_function(thn1, "W/W_434_Timo_riless_thn1"); // RMV
  Function Mo(VW);
  Mo.interpolate(F[2]); Fscale(Mo, 1/L);
  write_function(F[2], "W/W_434_Timo_riless_Mo"); // RMV

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  //ddiff(thddtn1, Dt, beta, thn, thn1, thddotn, thdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
  //diff(thdtn1, Dt, thddtn1, thddotn, thdotn);
}
