#ifndef HOLLOW_H
#define HOLLOW_H

#include "../plateproblem.h"
using namespace dolfin;

class HollowBeam : public PlateProblem
{
  public:
    HollowBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg):
     PlateProblem(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg), L(1.0), inited(false) {}
    
    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing );
    std::string gettype();
    void nonstep () {}
    void timestep () {}
    void clstep () { }
    void init();
    void adjacc(double alpha) {}
    Mesh* calcmesh;
    double L;
    bool inited;
};

class HollowBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new HollowBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

#endif
