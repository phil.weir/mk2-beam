#ifndef FORBES_H
#define FORBES_H

#include "../plateproblem.h"
using namespace dolfin;

class ForbesBeam : public PlateProblem
{
  public:
    ForbesBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg):
     PlateProblem(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg), L(1.0), inited(false) {}
    
    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing );
    std::string gettype();
    void nonstep () {}
    void eval_bm(Function& M, Function& Wn1);
    void timestep () {}
    void clstep () { }
    void init();
    void adjacc(double alpha) {}
    Mesh* calcmesh;
    double L;
    bool inited;
};

class ForbesBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new ForbesBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

#endif
