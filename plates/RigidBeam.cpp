#include "RigidBeam.h"
#include "oldddiff.h"
#include "../logfile.h"

class XExpression : public Expression
{
  public:
    XExpression ( double leftt, double rightt ) : left(leftt), right(rightt) {}
    double left, right;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = x[0] - (left+right)/2;
    }
};

class CombinedForcing : public Expression
{
  public:
    CombinedForcing(Function& pt, Function& ft, Function& Wt, double rhowt, double gt) :
     p(pt), f(ft), W(Wt), rhow(rhowt), g(gt) {}
    Function &p, &f, &W; double rhow, g;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> pv(2), fv(2), Wv(2);
      try {pv[0] = approx_z(p, x[0]);} catch (std::runtime_error& str) { std::cout << "Failed p at " << x[0] << "," << x[1] << std::endl; exit(213); }
      try {fv[0] = approx_z(f, x[0]);} catch (std::runtime_error& str) { std::cout << "Failed f at " << x[0] << "," << x[1] << std::endl; exit(214); }
      try {Wv[0] = approx_z(W, x[0]);} catch (std::runtime_error& str) { std::cout << "Failed W at " << x[0] << "," << x[1] << std::endl; exit(215); }
      values[0] = pv[0] + fv[0];// - rhow*g*Wv[0];
    }
};

class RNormalExpression : public Expression
{
    public:
    RNormalExpression(double lt, double rt) : l(lt), r(rt) {}
    double l, r;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = (x[0] > 1e-1 ? l:r);
    }
};

std::string RigidBeam::gettype()
{
  return "Rigid Beam";
}

void RigidBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  /* This is really bad and needs fixed. We know that a linear map will not change the response
   * so we map the ends to 0 elevation as the solver doesn't introduce any vibrations provided
   * this is the case */
  /*Len::Functional LenM(mesh);*/ Int::Functional IntM(mesh); AngMom::Functional AngMomM(mesh);
  CombinedForcing F(pp, forcing, Wn1, rhowc, gc*sbmg);
  diff::FunctionSpace  Vd(mesh);  diff::BilinearForm  ad(Vd,Vd);  diff::LinearForm  Ld(Vd);
  makeW::FunctionSpace VmakeW(mesh); makeW::BilinearForm amakeW(VmakeW,VmakeW); makeW::LinearForm LmakeW(VmakeW);
  oldddiff::FunctionSpace Vdd(mesh); oldddiff::BilinearForm add(Vdd,Vdd); oldddiff::LinearForm Ldd(Vdd);

  double left = 250, right = 400;//RMV!!
  XExpression xe(left, right);

  double rddp, thdd, l = right - left, m = rhopc*hc*1, off = left + l/2; Constant offc(off);
  IntM.f = F; AngMomM.f = F; AngMomM.off = offc; AngMomM.xe = xe;
  thdd = (2/(m*pow(l,3)))*assemble(AngMomM);
  rddp = assemble(IntM)/(m*l);
  Constant thddc(thdd), rddpc(rddp);

  amakeW.Dt = Dtc; amakeW.beta = betac;
  LmakeW.Dt = Dtc; LmakeW.beta = betac; LmakeW.rddp = rddpc; LmakeW.thdd = thddc; LmakeW.x = xe;
  amakeW.g = gc; amakeW.h = hc;
  Ldd.Dt = Dtc; Ldd.beta = betac;
  Ld.Dt = Dtc;

  Function Wn1f(VmakeW), Wddtn1f(Vdd), Wdtn1f(Vd);
  VariationalProblem elprob (amakeW, LmakeW); // RMV INSERT BC IF NESS
  LmakeW.Wn = Wn; LmakeW.Wddotn = Wddotn; LmakeW.Wdotn = Wdotn;
  elprob.solve(Wn1f); Wn1.interpolate(Wn1f);

  if (log) presout << pp;
  if (log) forcout << forcing;
  Ldd.Wn = Wn; Ldd.Wn1 = Wn1; Ldd.Wddotn = Wddotn; Ldd.Wdotn = Wdotn;
  VariationalProblem oldddiffprob (add, Ldd);
  oldddiffprob.solve(Wddtn1f);
  Wddtn1.interpolate(Wddtn1f);
//  Wddtn1 = Wddtn1f;

  Ld.Wddotn = Wddotn; Ld.Wddotn1 = Wddtn1; Ld.Wdotn = Wdotn;
  VariationalProblem diffprob(ad, Ld);
  diffprob.solve(Wdtn1f);
  Wdtn1.interpolate(Wdtn1f);
//  Wdtn1 = Wdtn1f;
  if (log) dispout << Wn1;
  if (log) veloout << Wdtn1;
  if (log) acceout << Wddtn1;
  //Constant zero(0.0);
  //Wdtn1 = zero; Wddtn1 = zero;

//  LD.line = line; LD.oldf = Wn1a;
//  VariationalProblem dealignprob (aD, LD);
//  dealignprob.solve(Wn1);
}
