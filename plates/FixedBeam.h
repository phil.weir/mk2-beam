#ifndef FIXEDBEAM_H
#define FIXEDBEAM_H

#include "../plateproblem.h"
using namespace dolfin;

class FixedBeam : public PlateProblem
{
  public:
    FixedBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg):
     PlateProblem(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg)
     {
     }
    
    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
     Function& Wn, Function& Wdotn, Function& Wddotn,
     Function& forcing );
    std::string gettype();
    void nonstep () { }
    void timestep () { }
    void clstep () { }
    void adjacc(double alpha) {}
};

class FixedBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new FixedBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

#endif
