#include "Forbes.h"
#include "ForbesForm.h"
#include "ForbesBM.h"
#include "diff.h"
#include "ddiff.h"
#include "PlateNorm.h"
#include "plus.h"
#include "drop.h"
#include "../logfile.h"

std::string ForbesBeam::gettype()
{
  return "Forbes Beam";
}

void ForbesBeam::init ()
{
  calcmesh = new Mesh(mesh);
  L = (r-l);
//  L = 1;
  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
    calcmesh->geometry().x(vit->index(), 0) /= L;
  calcmesh->intersection_operator().clear();
  inited = true;
  logfile << "After scaling: " << Dc/(L*L) << " Wxxxx + " << rhopc*hc*1 << " Wdd = " << rhowc*gc << " W +..." << std::endl;
}

class ScaledFunction : public Expression
{
  public:
    ScaledFunction (double scalet, Function& fnt) : scale(scalet), fn(fnt) {}
    double scale;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = approx_z(fn, x[0]*scale); }

};

void ForbesBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;
  //Mesh& mesh(*Wn1.function_space()->mesh());

  const FunctionSpace& V(*Wn1.function_space());
  ForbesBM::LinearForm LBM(V);
  ForbesBM::BilinearForm aBM(V,V);

  double w = 1;
  LBM.D = Dc;
  LBM.h = hc;
  LBM.w = Wn1;
  solve(aBM == LBM, BM);
}

void ForbesBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  
 // 
  ForbesForm::FunctionSpace V(*calcmesh);   ForbesForm::BilinearForm aF(V,V);   ForbesForm::LinearForm LF(V);
  //ForbesForm::FunctionSpace V(mesh);   ForbesForm::BilinearForm aF(V,V);   ForbesForm::LinearForm LF(V);//RMV
  diff::FunctionSpace  Vd(mesh);  diff::BilinearForm  ad(Vd,Vd);  diff::LinearForm  Ld(Vd);
  ddiff::FunctionSpace Vdd(mesh); ddiff::BilinearForm add(Vdd,Vdd); ddiff::LinearForm Ldd(Vdd);
  plus::BilinearForm ap(V,V); plus::LinearForm Lp(V);
  drop::BilinearForm ar(V,V); drop::LinearForm Lr(V);
  PlateNorm::Functional NormM(*calcmesh);
 // PlateNorm::Functional NormM(mesh);
  Constant Lc(L);
  double rhop = (double)rhopc, g = (double)gc, rhow = (double)rhowc, h = (double)hc;
  Constant sbmgc(sbmg?1.0:0.0);
  Constant c1(rhop*h*1), c2(rhow*g*sbmg);

  std::cout << "Don't you want Forbes Mixed?" << std::endl;
  //aF.rhow = rhowc;
  //aF.g = gc;
  //aF.mu = muc;
  aF.Dt = Dtc;
  aF.D = Dc; aF.h = hc;
  aF.beta = betac; //aF.rhop = rhopc;
  aF.c1 = c1; aF.c2 = c2;
 
  aF.l = Lc; LF.l = Lc;
  LF.c1 = c1; LF.c2 = c2;
  //aF.sbmg = sbmgc;
  aF.Dt = Dtc;
  LF.beta = betac; LF.h = hc;// LF.rhop = rhopc; LF.sbmg = sbmgc; LF.rhow = rhowc; LF.g = gc;
  LF.D = Dc; 
  //LF.mu = muc;
  LF.Dt = Dtc;
  Ldd.Dt = Dtc; Ldd.beta = betac;
  //aF.mu = muc;
  Ld.Dt = Dtc;

  Function Wn1f(V), Wddtn1f(Vdd), Wdtn1f(Vd);
  ScaledFunction WnS(L, Wn), WdotnS(L, Wdotn), WddotnS(L, Wddotn), ppS(L, pp), fS(L, forcing),
   Wn1S(L, Wn1);
  LF.f = fS; LF.p = ppS;
  //LF.f = forcing; LF.p = pp;
  LF.Wdotn = WdotnS; LF.Wn = WnS; LF.Wddotn = WddotnS;
  //LF.Wdotn = Wdotn; LF.Wn = Wn; LF.Wddotn = Wddotn;
  int i = 0, err; double tol = 1e-16;
  Function Wnew(V), Wold(V); Wold = Wn1S;
  //Function Wnew(V), Wold(V); Wold = Wn1;
  do
  {
    i++;
    LF.W = Wold; aF.W = Wold;
    //VariationalProblem elprob (aF, LF); // RMV INSERT BC IF NESS
    Function dW(V);
    solve(aF==LF, dW);
    //NormM.f = dW;
    //err = assemble(NormM);
    //std::cout << std::endl << err << std::endl;
    Lp.f = dW; Lp.g = Wold;
    solve(ap==Lp, Wnew);
    Wold = Wnew;
  } while (i < 8);
  ScaledFunction Wnews(1/L, Wnew);
  Wn1.interpolate(Wnews);
  //Wn1.interpolate(Wnew);

  if (log) presout << pp;
  if (log) forcout << forcing;
  Ldd.Wn = Wn; Ldd.Wn1 = Wn1; Ldd.Wddotn = Wddotn; Ldd.Wdotn = Wdotn;
  solve(add==Ldd, Wddtn1f);
  Wddtn1.interpolate(Wddtn1f);
//  Wddtn1 = Wddtn1f;

  Ld.Wddotn = Wddotn; Ld.Wddotn1 = Wddtn1; Ld.Wdotn = Wdotn;
  solve(ad==Ld, Wdtn1f);
  Wdtn1.interpolate(Wdtn1f);
//  Wdtn1 = Wdtn1f;
  if (log) dispout << Wn1;
  if (log) veloout << Wdtn1;
  if (log) acceout << Wddtn1;
}

