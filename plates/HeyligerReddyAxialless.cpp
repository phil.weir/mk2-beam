#include "HeyligerReddyAxialless.h"
#include "expr.h"
#include "HeyligerReddyAxiallessForm.h"
#include "thspace.h"
//#include "HeyligerReddyAxiallessForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"
#include "HeyligerReddyAxiallessBM.h"

class HeyligerReddyAxiallessScaledFunction : public Expression
{
  public:
    HeyligerReddyAxiallessScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

void HeyligerReddyAxiallessScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  dolfin::Array<double> x2(1); x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class NormalFunction;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

HeyligerReddyAxiallessBeam::HeyligerReddyAxiallessBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     Vu(mesht), Vpsi(mesht), inited(false),
     psidotn(Vpsi), psin(Vpsi), psiddotn(Vpsi), psin1(Vpsi), psidtn1(Vpsi), psiddtn1(Vpsi),
     psiddtkm1(Vpsi), psidtkm1(Vpsi), psikm1(Vpsi), psin1old(Vpsi),
     M(Vu), udotn(Vu), un(Vu), uddotn(Vu), un1(Vu), udtn1(Vu), uddtn1(Vu),
     uddtkm1(Vu), udtkm1(Vu), ukm1(Vu), un1old(Vu)
{
  Constant zero(0);
  psidotn = zero; psin = zero; psiddotn = zero; psin1 = zero; psidtn1 = zero;
  psiddtn1 = zero; psiddtkm1 = zero; psidtkm1 = zero; psikm1 = zero; psin1old = zero;
  udotn = zero; un = zero; uddotn = zero; un1 = zero; udtn1 = zero;
  uddtn1 = zero; uddtkm1 = zero; udtkm1 = zero; ukm1 = zero; un1old = zero;
}

std::string HeyligerReddyAxiallessBeam::gettype()
{
  return "Heyliger-Reddy without axial extension Beam";
}

void HeyligerReddyAxiallessBeam::init ()
{
  L = (r-l);
  //L = 1;//RMV

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  //thspace::FunctionSpace& Vth = *(new thspace::FunctionSpace(*calcmesh));
  //Function *_thdotn = new Function(Vth);
  //Function *_thn = new Function(Vth);
  //Function *_thddotn = new Function(Vth);
  //Function *_thn1 = new Function(Vth);
  //Function *_thdtn1 = new Function(Vth);
  //Function *_thddtn1 = new Function(Vth);
  //Function *_thddtkm1 = new Function(Vth);
  //Function *_thdtkm1 = new Function(Vth);
  //Function *_thkm1 = new Function(Vth);
  //Function *_thn1old = new Function(Vth);
  //thdotn = *_thdotn;
  //thn = *_thn;
  //thddotn = *_thddotn;
  //thn1 = *_thn1;
  //thdtn1 = *_thdtn1;
  //thddtn1 = *_thddtn1;
  //thddtkm1 = *_thddtkm1;
  //thdtkm1 = *_thdtkm1;
  //thkm1 = *_thkm1;
  //thn1old = *_thn1old;

  HeyligerReddyAxiallessForm::CoefficientSpace_Wn& Vu     = *(new HeyligerReddyAxiallessForm::CoefficientSpace_Wn(*calcmesh));
  HeyligerReddyAxiallessForm::CoefficientSpace_psin& Vpsi = *(new HeyligerReddyAxiallessForm::CoefficientSpace_psin(*calcmesh));

  Function *_udotn = new Function(Vu);		Function *_un = new Function(Vu);	Function *_uddotn = new Function(Vu);
  Function *_un1 = new Function(Vu);		Function *_udtn1 = new Function(Vu);	Function *_uddtn1 = new Function(Vu);
  Function *_uddtkm1 = new Function(Vu);	Function *_udtkm1 = new Function(Vu);	Function *_ukm1 = new Function(Vu);
  Function *_un1old = new Function(Vu);

  Function *_psidotn = new Function(Vpsi);	Function *_psin = new Function(Vpsi);		Function *_psiddotn = new Function(Vpsi);
  Function *_psin1 = new Function(Vpsi);	Function *_psidtn1 = new Function(Vpsi);	Function *_psiddtn1 = new Function(Vpsi);
  Function *_psiddtkm1 = new Function(Vpsi);	Function *_psidtkm1 = new Function(Vpsi);	Function *_psikm1 = new Function(Vpsi);
  Function *_psin1old = new Function(Vpsi);

  udotn = *_udotn; un = *_un; uddotn = *_uddotn; un1 = *_un1; udtn1 = *_udtn1; uddtn1 = *_uddtn1;
  uddtkm1 = *_uddtkm1; udtkm1 = *_udtkm1; ukm1 = *_ukm1; un1old = *_un1old;

  psidotn = *_psidotn; psin = *_psin; psiddotn = *_psiddotn; psin1 = *_psin1; psidtn1 = *_psidtn1; psiddtn1 = *_psiddtn1;
  psiddtkm1 = *_psiddtkm1; psidtkm1 = *_psidtkm1; psikm1 = *_psikm1; psin1old = *_psin1old;
}

void HeyligerReddyAxiallessBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void HeyligerReddyAxiallessBeam::shutdown_diagnostics()
{
  fclose(stef);
}

void HeyligerReddyAxiallessBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;
  //Mesh& mesh(*Wn1.function_space()->mesh());

  const FunctionSpace& V(*Wn1.function_space());
  HeyligerReddyAxiallessBM::LinearForm LBM(V);
  HeyligerReddyAxiallessBM::BilinearForm aBM(V,V);

  double w = 1;
  double I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;
  Constant Ic(I), Ec(E);
  LBM.E = Ec;
  LBM.I = Ic;
  HeyligerReddyAxiallessScaledFunction psin1S(1/L, -l/L, psin1);
  LBM.psi = psin1S;
  LBM.w = Wn1;
  Function M(V);
  solve(aBM == LBM, M);
  BM.interpolate(M);
  M.vector() = BM.vector();
}

void HeyligerReddyAxiallessBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

}

void HeyligerReddyAxiallessBeam::nonstep () {
   un1   = un;   udtn1   = udotn;   uddtn1   = uddotn;//RMV
   psin1 = psin; psidtn1 = psidotn; psiddtn1 = psiddotn;//RMV
}
void HeyligerReddyAxiallessBeam::timestep () {
   uddtkm1   = uddtn1;   udtkm1   = udtn1;   ukm1   = un1;   un   = un1;   udotn   = udtn1;   uddotn   = uddtn1;  
   psiddtkm1 = psiddtn1; psidtkm1 = psidtn1; psikm1 = psin1; psin = psin1; psidotn = psidtn1; psiddotn = psiddtn1;
 }
void HeyligerReddyAxiallessBeam::clstep()
 {
   un1old   = un1;   uddtkm1   = uddtn1;   udtkm1   = udtn1;   ukm1   = un1;  
   psin1old = psin1; psiddtkm1 = psiddtn1; psidtkm1 = psidtn1; psikm1 = psin1;
 }
void HeyligerReddyAxiallessBeam::adjacc(double alpha) {
   Function uddtn1tmp(Vu), udtn1tmp(Vu), un1tmp(Vu);
   uddtn1tmp = uddtn1; udtn1tmp = udtn1; un1tmp = un1;
   AlphaFunction ualpha(alpha, uddtn1tmp, uddtkm1, un1old);
   uddtn1 = ualpha; uddtkm1 = uddtn1; udtkm1 = udtn1; ukm1 = un1;

   Function psiddtn1tmp(Vpsi), psidtn1tmp(Vpsi), psin1tmp(Vpsi);
   psiddtn1tmp = psiddtn1; psidtn1tmp = psidtn1; psin1tmp = psin1;
   AlphaFunction psialpha(alpha, psiddtn1tmp, psiddtkm1, psin1old);
   psiddtn1 = psialpha; psiddtkm1 = psiddtn1; psidtkm1 = psidtn1; psikm1 = psin1;
}

void HeyligerReddyAxiallessBeam::load_function_vectors(std::string name)
{
   load_function_vector(uddtkm1, name, "uddtkm1");
   load_function_vector(udtkm1, name, "udtkm1");
   load_function_vector(ukm1, name, "ukm1");
   load_function_vector(un, name, "un");
   load_function_vector(udotn, name, "udotn");
   load_function_vector(uddotn, name, "uddotn");
   load_function_vector(un1, name, "un1");
   load_function_vector(udtn1, name, "udtn1");
   load_function_vector(uddtn1, name, "uddtn1");

   load_function_vector(psiddtkm1, name, "psiddtkm1");
   load_function_vector(psidtkm1, name, "psidtkm1");
   load_function_vector(psikm1, name, "psikm1");
   load_function_vector(psin, name, "psin");
   load_function_vector(psidotn, name, "psidotn");
   load_function_vector(psiddotn, name, "psiddotn");
   load_function_vector(psin1, name, "psin1");
   load_function_vector(psidtn1, name, "psidtn1");
   load_function_vector(psiddtn1, name, "psiddtn1");
}

void HeyligerReddyAxiallessBeam::save_function_vectors(std::string name)
{
   save_function_vector(uddtkm1, name, "uddtkm1");
   save_function_vector(udtkm1, name, "udtkm1");
   save_function_vector(ukm1, name, "ukm1");
   save_function_vector(un, name, "un");
   save_function_vector(udotn, name, "udotn");
   save_function_vector(uddotn, name, "uddotn");
   save_function_vector(un1, name, "un1");
   save_function_vector(udtn1, name, "udtn1");
   save_function_vector(uddtn1, name, "uddtn1");

   save_function_vector(psiddtkm1, name, "psiddtkm1");
   save_function_vector(psidtkm1, name, "psidtkm1");
   save_function_vector(psikm1, name, "psikm1");
   save_function_vector(psin, name, "psin");
   save_function_vector(psidotn, name, "psidotn");
   save_function_vector(psiddotn, name, "psiddotn");
   save_function_vector(psin1, name, "psin1");
   save_function_vector(psidtn1, name, "psidtn1");
   save_function_vector(psiddtn1, name, "psiddtn1");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

void HeyligerReddyAxiallessBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  //if (!inited) init();
  Constant zero(0);

  HeyligerReddyAxiallessForm::FunctionSpace V(*calcmesh);
  HeyligerReddyAxiallessForm::CoefficientSpace_Wn VW(*calcmesh);
  //HeyligerReddyAxiallessForm::CoefficientSpace_n Vn(*calcmesh);
  HeyligerReddyAxiallessForm::LinearForm Ft(V); HeyligerReddyAxiallessForm::JacobianForm Jt(V, V);
  std::vector< const BoundaryCondition* > bcU;

  HeyligerReddyAxiallessScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  Ends ends(0, 1);
  
  Function F(V), Z(V); Z.vector()->zero();

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1, k = 2*pow(h/2,3)*E/(3*(1-nu*nu)*rhop*A), a = 3*(h/2)*E/(rhop*A);
  Constant
  	c1(rhop*A*L/(T*T)),
  	c3(16*rhop*I/(105*L*T*T)), c2(rhop*I/(21*L*T*T)),
	c4(sbmg ? rhow*g*L : 0.),
  	c5(A*E/L),
  	c6((8/15.)*G*A/L),
  	c7(E*I/(21*L*L*L)), c8(16*E*I/(105*L*L*L)),
  	c9(A*rhop/(T*T)),
  	c10(68*E*I/(105*L*L)), c11(16*E*I/(105*L*L)),
  	c12(8*G*A/15),
  	c13(68*rhop*I/(105*T*T)), c14(16*rhop*I/(105*T*T)),
    betac(beta), Dtc(Dt), Lc(L), cweight(rhop*h*g);

  Jt.c1 = c1; Jt.c2 = c2; Jt.c3 = c3; Jt.c4 = c4; Jt.c5 = c5;
  Jt.c6 = c6; Jt.c7 = c7; Jt.c8 = c8; Jt.c10 = c10;
  Jt.c11 = c11; Jt.c12 = c12; Jt.c13 = c13; Jt.c14 = c14;
  Ft.c1 = c1; Ft.c2 = c2; Ft.c3 = c3; Ft.c4 = c4; Ft.c5 = c5;
  Ft.c6 = c6; Ft.c7 = c7; Ft.c8 = c8; Ft.c10 = c10;
  Ft.c11 = c11; Ft.c12 = c12; Ft.c13 = c13; Ft.c14 = c14;
  Jt.beta = betac; Jt.Dt = Dtc;// Jt.n = n;
  Ft.beta = betac; Ft.Dt = Dtc;// Ft.n = n;
  Ft.q = q; //Ft.Lc = Lc; Jt.Lc = Lc;
  Ft.Wn = WnS; Ft.Wdotn = WdotnS; Ft.Wddotn = WddotnS;
  //Ft.un = un; Ft.udotn = udotn; Ft.uddotn = uddotn;
  Ft.psin = psin; Ft.psidotn = psidotn; Ft.psiddotn = psiddotn;

  Parameters params("nonlinear_variational_solver");
  Parameters npars("newton_solver");
  npars.add("maximum_iterations", 50);
  npars.add("relative_tolerance", 1e-6);
  params.add(npars);

  int i = 0;
  	Ft.Z = Z; Jt.Z = Z;
	solve(Ft == 0, Z, bcU, Jt, params);

  HeyligerReddyAxiallessScaledFunction Wnews(1/L, -l/L, Z[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);
  un1.interpolate(Z[1]);
  psin1.interpolate(Z[2]);

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
  ddiff(uddtn1, Dt, beta, un, un1, uddotn, udotn);
  diff(udtn1, Dt, uddtn1, uddotn, udotn);
  ddiff(psiddtn1, Dt, beta, psin, psin1, psiddotn, psidotn);
  diff(psidtn1, Dt, psiddtn1, psiddotn, psidotn);
}
