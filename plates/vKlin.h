#ifndef VKLIN_H
#define VKLIN_H

#include "../plateproblem.h"
#include "uspace.h"
#include <stdlib.h>
using namespace dolfin;

class vKlinBeam : public PlateProblem
{
  public:
    vKlinBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg);

    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing );
    std::string gettype();
    void nonstep ();
    void timestep ();
    void clstep();
    void adjacc(double alpha);
    void load_function_vectors(std::string name);
    void save_function_vectors(std::string name);
    void output_diagnostics(double t,
      Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn );
    void setup_diagnostics(std::string name, char* openmode);
    void shutdown_diagnostics();
    void init();
    Mesh* calcmesh;
    FILE* stef;
    double L;
    double Dt;
    double beta;
    double G;
    double h;
    double A;
    double rhop;
    double rhow;
    double g;
    double nu;
    double E;
    double I;
    double D;
    double c1;
    double c2;
    double c3;
    double c4;
    double c5;
    double c6;
    double c7;

    uspace::FunctionSpace Vu;
    void eval_bm(Function& M, Function& Wn1);
    bool inited;
    Function udotn, un, uddotn, un1, udtn1, uddtn1;
    Function uddtkm1, udtkm1, ukm1, un1old;
    
};

class vKlinBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new vKlinBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

#endif
