#ifndef EULERBERNOULLICRACKED_H
#define EULERBERNOULLICRACKED_H

#include "../plateproblem.h"
#include "thspace.h"
#include <stdlib.h>
using namespace dolfin;

class EulerBernoulliCrackedBeam : public PlateProblem
{
  public:
    EulerBernoulliCrackedBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg);

    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing );
    std::string gettype();
    void nonstep ();
    void timestep ();
    void clstep();
    void adjacc(double alpha);
    void load_function_vectors(std::string name);
    void save_function_vectors(std::string name);
    void output_diagnostics(double t,
      Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn );
    void setup_diagnostics(std::string name, char* openmode);
    void shutdown_diagnostics();
    void init();
    Mesh* calcmesh;
    FILE* stef;
    double L;
    double Dt;
    double beta;
    double G;
    double h;
    double A;
    double rhop;
    double rhow;
    double g;
    double nu;
    double E;
    double I;
    double D;
    double kAG;
    double d2;
    double c1;
    double c2;
    double c3;
    double c4;
    Function* CKDBM;
    bool crack_bm;
    void eval_bm(Function& BM, Function& Wn1);

    bool inited;
    
};

class EulerBernoulliCrackedBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new EulerBernoulliCrackedBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

#endif
