#include "Drozdov.h"
#include "DrozdovBM.h"
#include "expr.h"
#include "DrozdovForm.h"
#include "thspace.h"
//#include "DrozdovForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class DrozdovScaledFunction : public Expression
{
  public:
    DrozdovScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

void DrozdovScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  dolfin::Array<double> x2(1); x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class NormalFunction;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

DrozdovBeam::DrozdovBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     Vth(mesht), inited(false), thdotn(Vth), thn(Vth), thddotn(Vth), thn1(Vth), thdtn1(Vth), thddtn1(Vth),
     thddtkm1(Vth), thdtkm1(Vth), thkm1(Vth), thn1old(Vth)
{
  Constant zero(0);
  thdotn = zero;
  thn = zero;
  thddotn = zero;
  thn1 = zero;
  thdtn1 = zero;
  thddtn1 = zero;
  thddtkm1 = zero;
  thdtkm1 = zero;
  thkm1 = zero;
  thn1old = zero;
}

std::string DrozdovBeam::gettype()
{
  return "Drozdov Beam";
}

void DrozdovBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;
  //Mesh& mesh(*Wn1.function_space()->mesh());

  const FunctionSpace& V(*Wn1.function_space());
  DrozdovBM::LinearForm LBM(V);
  DrozdovBM::BilinearForm aBM(V,V);

  double w = 1;
  double I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;
  Constant Ic(I), Ec(E);
  LBM.E = Ec;
  LBM.I = Ic;
  LBM.w = Wn1;
  solve(aBM == LBM, BM);
}

void DrozdovBeam::init ()
{
  L = (r-l);
  //L = 1;//RMV

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  thspace::FunctionSpace& Vth = *(new thspace::FunctionSpace(*calcmesh));
  Function *_thdotn = new Function(Vth);
  Function *_thn = new Function(Vth);
  Function *_thddotn = new Function(Vth);
  Function *_thn1 = new Function(Vth);
  Function *_thdtn1 = new Function(Vth);
  Function *_thddtn1 = new Function(Vth);
  Function *_thddtkm1 = new Function(Vth);
  Function *_thdtkm1 = new Function(Vth);
  Function *_thkm1 = new Function(Vth);
  Function *_thn1old = new Function(Vth);
  thdotn = *_thdotn;
  thn = *_thn;
  thddotn = *_thddotn;
  thn1 = *_thn1;
  thdtn1 = *_thdtn1;
  thddtn1 = *_thddtn1;
  thddtkm1 = *_thddtkm1;
  thdtkm1 = *_thdtkm1;
  thkm1 = *_thkm1;
  thn1old = *_thn1old;
}

void DrozdovBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void DrozdovBeam::shutdown_diagnostics()
{
  fclose(stef);
}

double eval_euler_bernoulli_strain_energy(Function& W, Function& th, double EI, double kAG, double L, double l);

void DrozdovBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

  if (inited) fprintf(stef, "%lf,%lf\n", t, eval_euler_bernoulli_strain_energy(Wn1, thn1, E*I, kappa*A*G, L, l));
  fflush(stef);
}

void DrozdovBeam::nonstep () {
   thn1 = thn; thdtn1 = thdotn; thddtn1 = thddotn;//RMV
}
void DrozdovBeam::timestep () {
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
   thn      = thn1;
   thdotn   = thdtn1;
   thddotn  = thddtn1;
 }
void DrozdovBeam::clstep()
 {
   thn1old  = thn1;
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
 }
void DrozdovBeam::adjacc(double alpha) {
   Function thddtn1tmp(Vth), thdtn1tmp(Vth), thn1tmp(Vth); thddtn1tmp = thddtn1; thdtn1tmp = thdtn1; thn1tmp = thn1;
   AlphaFunction thalpha(alpha, thddtn1tmp, thddtkm1, thn1old);
   AlphaFunction thalphap(4*alpha, thddtn1tmp, thdtkm1, thn1old);
   AlphaFunction thalphapp(16*alpha, thddtn1tmp, thkm1, thn1old);
   thddtn1 = thalpha;// thdtn1 = thalphap; thn1 = thalphapp;
   thddtkm1 = thddtn1; thdtkm1 = thdtn1; thkm1 = thn1;
}

void DrozdovBeam::load_function_vectors(std::string name)
{
   load_function_vector(thddtkm1, name, "thddtkm1");
   load_function_vector(thdtkm1, name, "thdtkm1");
   load_function_vector(thkm1, name, "thkm1");
   load_function_vector(thn, name, "thn");
   load_function_vector(thdotn, name, "thdotn");
   load_function_vector(thddotn, name, "thddotn");
   load_function_vector(thn1, name, "thn1");
   load_function_vector(thdtn1, name, "thdtn1");
   load_function_vector(thddtn1, name, "thddtn1");
}

void DrozdovBeam::save_function_vectors(std::string name)
{
   save_function_vector(thddtkm1, name, "thddtkm1");
   save_function_vector(thdtkm1, name, "thdtkm1");
   save_function_vector(thkm1, name, "thkm1");
   save_function_vector(thn, name, "thn");
   save_function_vector(thdotn, name, "thdotn");
   save_function_vector(thddotn, name, "thddotn");
   save_function_vector(thn1, name, "thn1");
   save_function_vector(thdtn1, name, "thdtn1");
   save_function_vector(thddtn1, name, "thddtn1");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

void DrozdovBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  DrozdovForm::FunctionSpace V(*calcmesh);
  DrozdovForm::CoefficientSpace_Wn VW(*calcmesh);
  //DrozdovForm::CoefficientSpace_n Vn(*calcmesh);
  DrozdovForm::LinearForm Lt(V); DrozdovForm::BilinearForm at(V, V);

  DrozdovScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);
std::cout << "Don't you want Drozdov Newton?" << std::endl;

  std::vector< const BoundaryCondition* > bcU;
  Ends ends(0, 1);
  SubSpace Vthx(V, 1); Function thx_at_ends(VW); thx_at_ends = zero;
  //DirichletBC* endbc = new DirichletBC(Vthx, thx_at_ends, ends); //bcU.push_back(endbc);
  
  //VariationalProblem tprob(at, Lt, bcU);
  Function F(V), Z(V); Z.vector()->zero();

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1, k = 2*pow(h/2,3)*E/(3*(1-nu*nu)*rhop*A), a = 3*(h/2)*E/(rhop*A);
  Constant c1(rhop*A*L*L/(T*T)), c2(E*h*h*h/(12.*(1-nu*nu)*L*L)), c3(E*h), c4(sbmg ? rhow*g*L*L : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L);
  //Normal n(0.5);//Vn, 0, 1);
  Fscale(q, L);
  //Z[0] = zero; Z[1] = zero;

  at.c1 = c1; at.c2 = c2; at.c3 = c3; at.c4 = c4; at.beta = betac; at.Dt = Dtc;// at.n = n;
  Lt.c1 = c1; Lt.c2 = c2; Lt.c3 = c3; Lt.c4 = c4; Lt.beta = betac; Lt.Dt = Dtc;// Lt.n = n;
  Lt.c1 = c1; Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;
  //Lt.thn = thn; Lt.thdotn = thdotn; Lt.thddotn = thddotn;

  int i = 0;
  do {
  	Lt.Z = Z; at.Z = Z;
	//tprob.solve(F);
        solve(at == Lt, F);
	Fadd(Z, F);
  } while ( i++ < 13 );

  DrozdovScaledFunction Wnews(1/L, -l/L, Z[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);

  //DrozdovForm::CoefficientSpace_Wn VW2(mesh);
  //DrozdovScaledFunction Monews(1/L, -l/L, Z[1]);
  //Function Mof(VW2); Mof = Monews;
  ////Fscale(Mof, rhop*A/L);
  //write_function(Mof, "W/W_434_Droz_Mo"); // RMV
  //DrozdovScaledFunction thnews(1/L, -l/L, Z[2]);
  //Function thf(VW2); thf = thnews;
  //write_function(thf, "W/W_434_Droz_th"); // RMV
  //DrozdovScaledFunction Qnews(1/L, -l/L, Z[3]);
  //Function Qf(VW2); Qf = Qnews;
  //write_function(Qf, "W/W_434_Droz_Q"); // RMV

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
