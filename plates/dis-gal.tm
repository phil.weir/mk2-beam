<TeXmacs|1.0.7.3>

<style|generic>

<\body>
  <section|Discontinuous Galerkin Product Rule>

  These following definitions hold.

  <\with|par-left|1cm>
    <\with|font-base-size|8>
      <math|[x<rsub|0>,\<cdots\>,x<rsub|n>]> is a partition of <math|[a,b]>

      <math|f(x<rsub|0><rsup|\<upl\>>)=f(a)>, sim. <math|x<rsub|n><rsup|->>
      and/or <math|g>.

      <math|jump(f,x<rsub|i>)=[[f]]<rsub|i>=f(x<rsub|i><rsup|\<upl\>>)-f(x<rsub|i><rsup|\<um\>>)>

      <math|avg(f,x<rsub|i>)=\<langle\>f\<rangle\><rsub|i>=<frac|1|2>*[f(x<rsub|i><rsup|+>)-f(x<rsub|i><rsup|\<um\>>)]>
    </with>
  </with>

  Now consider,

  <\eqnarray*>
    <tformat|<cwith|1|5|2|2|cell-halign|l>|<table|<row|<cell|<big|int><rsub|a><rsup|b>f*g<rsub|x>*d
    x>|<cell|=<rsub|CG>>|<cell|<left|[>f*g<right|]><rsup|b><rsub|a>-<big|int><rsub|a><rsup|b>f<rsub|x>*g*d
    x>>|<row|<cell|>|<cell|=<rsub|DG>>|<cell|<big|sum><rsub|i=0><rsup|n-1><big|int><rsub|x<rsub|i>><rsup|x<rsub|i+1>>f*g<rsub|x>*d
    x>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|i=0><rsup|n-1><left|{><left|[>f*g<right|]><rsup|x<rsup|\<um\>><rsub|i+1>><rsub|x<rsup|\<upl\>><rsub|i>>-<big|int><rsub|x<rsub|i>><rsup|x<rsub|i+1>>f<rsub|x>*g*d
    x<right|}>>>|<row|<cell|>|<cell|=>|<cell|[f*g]<rsup|b><rsub|a>-<big|sum><rsub|i=1><rsup|n-1><left|{>(f*g)
    (x<rsub|i><rsup|\<um\>>)-(f*g) (x<rsub|i><rsup|\<upl\>>)<right|}>-<big|sum><rsub|i=0><rsup|n-1><big|int><rsub|x<rsub|i>><rsup|x<rsub|i+1>>f<rsub|x>*g*d
    x>>|<row|<cell|>|<cell|=>|<cell|[f*g]<rsup|b><rsub|a>-<big|int><rsub|a><rsup|b>f<rsub|x>*g*d
    x+<big|sum><rsub|i=1><rsup|n-1>\<langle\>f*g\<rangle\><rsub|i><eq-number><label|ref-init-simp>>>>>
  </eqnarray*>

  However, letting <math|[f<rsub|i<rsup|\<pm\>>>]\<assign\>f(x<rsub|i><rsup|\<pm\>>)>

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<langle\>f*g\<rangle\><rsub|i>>|<cell|=>|<cell|[f<rsub|i<rsup|+>>]*[g<rsub|i<rsup|\<upl\>>>]-[f<rsub|i<rsup|\<um\>>>]*[g<rsub|i<rsup|\<um\>>>]>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<left|{>2*[f<rsub|i<rsup|+>>]*[g<rsub|i<rsup|\<upl\>>>]-2*[f<rsub|i<rsup|\<um\>>>]*[g<rsub|i<rsup|\<um\>>>]+[f<rsub|i<rsup|\<um\>>>]*[g<rsub|i<rsup|\<upl\>>>]-[f<rsub|i<rsup|->>]*[g<rsub|i<rsup|+>>]+[f<rsub|i<rsup|+>>]*[g<rsub|i<rsup|->>]-[f<rsub|i<rsup|+>>]*[g<rsub|i<rsup|->>]<right|}>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<left|{><left|(>[f<rsub|i<rsup|+>>]+[f<rsub|i<rsup|\<um\>>>]<right|)>*[g<rsub|i<rsup|\<upl\>>>]-([f<rsub|i<rsup|\<upl\>>>]+[f<rsub|i<rsup|\<um\>>>])*[g<rsub|i<rsup|\<um\>>>]+([g<rsub|i<rsup|\<upl\>>>]+[g<rsub|i<rsup|\<um\>>>])*[f<rsub|i<rsup|\<upl\>>>]-([g<rsub|i<rsup|\<upl\>>>]+[g<rsub|i<rsup|\<um\>>>])[f<rsub|i<rsup|\<um\>>>]<right|}>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*([f<rsub|i<rsup|+>>]+[f<rsub|i<rsup|\<um\>>>])*([g<rsub|i<rsup|\<upl\>>>]-[g<rsub|i<rsup|\<um\>>>])+<frac|1|2>*([f<rsub|i<rsup|+>>]-[f<rsub|i<rsup|\<um\>>>])*([g<rsub|i<rsup|\<upl\>>>]+[g<rsub|i<rsup|\<um\>>>])>>|<row|<cell|>|<cell|=>|<cell|[[f<rsub|i>]]*\<langle\>g<rsub|i>\<rangle\>+\<langle\>f<rsub|i>\<rangle\>*[[g<rsub|i>]]>>>>
  </eqnarray*>

  Substituting into (<reference|ref-init-simpl>),

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><rsub|a><rsup|b>f*g<rsub|x>*d
    x>|<cell|=>|<cell|[f*g]<rsup|b><rsub|a>-<big|int><rsub|a><rsup|b>f<rsub|x>*g*d
    x+<big|sum><rsub|i=1><rsup|n-1>([[f<rsub|i>]]*\<langle\>g<rsub|i>\<rangle\>+\<langle\>f<rsub|i>\<rangle\>*[[g<rsub|i>]]),>>>>
  </eqnarray*>

  or, in UFL,

  <\code>
    f*Dx(g,0)*dx = f*g*n*ds + Dx(f,0)*g*dx - jump(f)*avg(g)*dS -
    avg(f)*jump(g)*dS<math|,>
  </code>

  where <math|<with|mode|text|<verbatim|n>>(x)=<choice|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<um\>1>|<cell|if>|<cell|x=a>>|<row|<cell|1>|<cell|if>|<cell|x=b>>>>>>.
</body>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|ref-init-simp|<tuple|1|?>>
    <associate|ref-init-simpl|<tuple|1|?>>
  </collection>
</references>