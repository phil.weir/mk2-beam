#include "EulerBernoulliCracking.h"
#include "EulerBernoulliCrackingBM.h"
#include "expr.h"
#include "EulerBernoulliCrackingForm.h"
#include "thspace.h"
//#include "EulerBernoulliCrackingForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

double cracking_bm_tol = 5e3;
bool has_cracked = false;

void add_cracks(MeshFunction<uint>& mf, Function& M)
{
  const Mesh& mesh = mf.mesh();
  for (VertexIterator vit(mesh) ; !vit.end() ; ++vit)
  {
    double m = eval_z(M, vit->x(0));
    if ( (fabs(m) > cracking_bm_tol && !has_cracked)) //RMV
    {
    	mf[vit->index()] = 1;
        std::cout << vit->index() << " " << vit->x(0) << " " << m << std::endl;
        has_cracked = true;
    }
  }
}

class Cracks : public SubDomain
{
  public:
    Cracks(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt), found(false) {}
    double l, r, tol;
    mutable bool found;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      bool ret = fabs(x[0] - .5) <= tol;
      if (ret) found = true;
      return ret;
    }
};
class EulerBernoulliCrackingScaledFunction : public Expression
{
  public:
    EulerBernoulliCrackingScaledFunction (double scalet, double transt, Function& fnt, double mult=1.) : scale(scalet), trans(transt), fn(fnt), mul(mult) { }
    double scale, trans;
    Function& fn;
    double mul;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

extern dolfin::Array<double> x2a;
void EulerBernoulliCrackingScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  x2a[0] = x[0]*scale + trans;
  fn.eval(values, x2a);
  values[0] *= mul;
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class Normal;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

EulerBernoulliCrackingBeam::EulerBernoulliCrackingBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     inited(false)
{
  Constant zero(0);
}

void EulerBernoulliCrackingBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;

  //EulerBernoulliCrackingBM::FunctionSpace V(*calcmesh);
  //EulerBernoulliCrackingBM::LinearForm LBM(V);
  //EulerBernoulliCrackingBM::BilinearForm aBM(V,V);

  ////Cracks cracks(l, r);
  ////MeshFunction<unsigned int> cracksf(*calcmesh, 0);
  ////cracksf.set_all(0);
  ////cracks.mark(cracksf, 1);
  ////if (!cracks.found) std::cout << "No crack found! Check node exists halfway along beam" << std::endl;

  //double w = 1;
  //Constant Ec(E), Ic(I);
  //aBM.dS = *cracks;
  //LBM.dS = *cracks;
  //LBM.E = Ec;
  //LBM.I = Ic;
  //EulerBernoulliCrackingScaledFunction WS(L, l, Wn1);
  //LBM.w = WS;
  //Function DGBM(V);
  //solve(aBM == LBM, DGBM);
  EulerBernoulliCrackingScaledFunction M(1/L, -l/L, *CKDBM);
  BM.interpolate(M);
}

std::string EulerBernoulliCrackingBeam::gettype()
{
  return "Euler-Bernoulli Beam with dynamic cracking";
}

void EulerBernoulliCrackingBeam::init ()
{
  L = (r-l);

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  EulerBernoulliCrackingBM::FunctionSpace* V = new EulerBernoulliCrackingBM::FunctionSpace(*calcmesh);
  CKDBM = new Function(*V);
  cracks = new MeshFunction<uint>(*calcmesh, 0);
  cracks->set_all(0);
}

void EulerBernoulliCrackingBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void EulerBernoulliCrackingBeam::shutdown_diagnostics()
{
  fclose(stef);
}

void EulerBernoulliCrackingBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
}

void EulerBernoulliCrackingBeam::nonstep () {
}
void EulerBernoulliCrackingBeam::timestep () {

  add_cracks(*cracks, *CKDBM);
  //std::cout << "BM at cracking point : " << current << std::endl;
  //if (fabs(current) > crack_bm_tol)
  //{
  //	crack_bm = true;
  //}

 }
void EulerBernoulliCrackingBeam::clstep()
 {
 }
void EulerBernoulliCrackingBeam::adjacc(double alpha) {
}

void EulerBernoulliCrackingBeam::load_function_vectors(std::string name)
{
}

void EulerBernoulliCrackingBeam::save_function_vectors(std::string name)
{
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

class c2Expression : public Expression
{
  public:
    c2Expression(double c2t, double indentt) : c2(c2t), indent(indentt) {}
    double c2, indent;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = (x[0] < indent) ? c2*1e8 : c2; }
};

void EulerBernoulliCrackingBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  EulerBernoulliCrackingForm::FunctionSpace V(*calcmesh);
  EulerBernoulliCrackingForm::CoefficientSpace_Wn VW(*calcmesh);
  //EulerBernoulliCrackingForm::CoefficientSpace_n Vn(*calcmesh);
  EulerBernoulliCrackingForm::LinearForm Lt(V); EulerBernoulliCrackingForm::BilinearForm at(V, V);

  EulerBernoulliCrackingScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  //Cracks cracks(l, r);
  //MeshFunction<unsigned int> cracksf(*calcmesh, 0);
  //cracksf.set_all(0);
  //if (crack_bm) {
  //	cracks.mark(cracksf, 1);
  //        if (!cracks.found) std::cout << "No crack found! Check node exists halfway along beam" << std::endl;
  //}

  //if (has_cracked) write_function(*CKDBM, "cracked_plate_M");
  Function F(V);

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant c1(rhop*A*L*L*L/(T*T)), c2(E*I/L), c4(sbmg ? rhow*g*L*L*L : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L*L);
  Fscale(q, L*L);

  c2Expression c2e(E*I/L, 0.1);

  at.c1 = c1; at.c2 = c2; at.c4 = c4; at.beta = betac; at.Dt = Dtc;
  Lt.c1 = c1; Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;

  at.dS = *cracks;
  Lt.dS = *cracks;
  solve(at == Lt, F);
  EulerBernoulliCrackingScaledFunction Wnews(1/L, -l/L, F[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);
  //EulerBernoulliCrackingScaledFunction Mnews(1/L, -l/L, F[1]);
  CKDBM->interpolate(F[1]);

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
