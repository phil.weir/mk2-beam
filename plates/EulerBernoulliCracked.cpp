#include "EulerBernoulliCracked.h"
#include "EulerBernoulliCrackedBM.h"
#include "expr.h"
#include "EulerBernoulliCrackedForm.h"
#include "thspace.h"
//#include "EulerBernoulliCrackedForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

//double crack_loc = 15.068;
double crack_loc = 20.;
double crack_eps = .002;

class Cracks : public SubDomain
{
  public:
    Cracks(double lt, double rt, double Lt) : l(lt), r(rt), L(Lt), found(false) {}
    double l, r, L;
    mutable bool found;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      bool ret = fabs(x[0] - (crack_loc-l)/L) <= crack_eps;
      if (ret) found = true;
      return ret;
    }
};
class EulerBernoulliCrackedScaledFunction : public Expression
{
  public:
    EulerBernoulliCrackedScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

dolfin::Array<double> x2a(1);
void EulerBernoulliCrackedScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  x2a[0] = x[0]*scale + trans;
  fn.eval(values, x2a);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class Normal;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

EulerBernoulliCrackedBeam::EulerBernoulliCrackedBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     crack_bm(false), inited(false)
{
  Constant zero(0);
}

void EulerBernoulliCrackedBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;

  EulerBernoulliCrackedBM::FunctionSpace V(*calcmesh);
  EulerBernoulliCrackedBM::LinearForm LBM(V);
  EulerBernoulliCrackedBM::BilinearForm aBM(V,V);

  Cracks cracks(l, r, L);
  MeshFunction<unsigned int> cracksf(*calcmesh, 0);
  cracksf.set_all(0);
  cracks.mark(cracksf, 1);
  if (!cracks.found) std::cout << "No crack found! Check node exists halfway along beam" << std::endl;

  double w = 1;
  Constant Ec(E), Ic(I);
  aBM.dS = cracksf;
  LBM.dS = cracksf;
  LBM.E = Ec;
  LBM.I = Ic;
  EulerBernoulliCrackedScaledFunction WS(L, l, Wn1);
  LBM.w = WS;
  Function DGBM(V);
  solve(aBM == LBM, DGBM);
  EulerBernoulliCrackedScaledFunction M(1/L, -l/L, DGBM);
  BM.interpolate(M);
  Fscale(BM, 1/(L*L));
  *CKDBM->vector() = *BM.vector();
}

std::string EulerBernoulliCrackedBeam::gettype()
{
  return "Euler-Bernoulli Beam with crack";
}

double cracked;
void EulerBernoulliCrackedBeam::init ()
{
  L = (r-l);

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  EulerBernoulliCrackedBM::FunctionSpace* V = new EulerBernoulliCrackedBM::FunctionSpace(mesh);
  CKDBM = new Function(*V);
}

void EulerBernoulliCrackedBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void EulerBernoulliCrackedBeam::shutdown_diagnostics()
{
  fclose(stef);
}

void EulerBernoulliCrackedBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
}

void EulerBernoulliCrackedBeam::nonstep () {
}
void EulerBernoulliCrackedBeam::timestep () {

  double current = eval_z(*CKDBM, 20);
  //std::cout << "BM at cracking point : " << current << std::endl;

 }
void EulerBernoulliCrackedBeam::clstep()
 {
 }
void EulerBernoulliCrackedBeam::adjacc(double alpha) {
}

void EulerBernoulliCrackedBeam::load_function_vectors(std::string name)
{
}

void EulerBernoulliCrackedBeam::save_function_vectors(std::string name)
{
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

class c2Expression : public Expression
{
  public:
    c2Expression(double c2t, double indentt) : c2(c2t), indent(indentt) {}
    double c2, indent;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = (x[0] < indent) ? c2*1e8 : c2; }
};

void EulerBernoulliCrackedBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  EulerBernoulliCrackedForm::FunctionSpace V(*calcmesh);
  EulerBernoulliCrackedForm::CoefficientSpace_Wn VW(*calcmesh);
  //EulerBernoulliCrackedForm::CoefficientSpace_n Vn(*calcmesh);
  EulerBernoulliCrackedForm::LinearForm Lt(V); EulerBernoulliCrackedForm::BilinearForm at(V, V);

  EulerBernoulliCrackedScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  Cracks cracks(l, r, L);
  MeshFunction<unsigned int> cracksf(*calcmesh, 0);
  cracksf.set_all(0);
  	cracks.mark(cracksf, 1);
	  if (!cracks.found) std::cout << "No crack found! Check node exists halfway along beam" << std::endl;

  Function F(V);

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant c1(rhop*A*L*L*L/(T*T)), c2(E*I/L), c4(sbmg ? rhow*g*L*L*L : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L*L);
  Fscale(q, L*L);

  c2Expression c2e(E*I/L, 0.1);

  at.c1 = c1; at.c2 = c2; at.c4 = c4; at.beta = betac; at.Dt = Dtc;
  Lt.c1 = c1; Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;

  at.dS = cracksf;
  Lt.dS = cracksf;
  solve(at == Lt, F);
  EulerBernoulliCrackedScaledFunction Wnews(1/L, -l/L, F[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
