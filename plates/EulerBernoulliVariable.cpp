#include "EulerBernoulliVariable.h"
#include "EulerBernoulliVariableBM.h"
#include "expr.h"
#include "EulerBernoulliVariableForm.h"
#include "thspace.h"
//#include "EulerBernoulliVariableForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class DExpression : public Expression
{
  public :
    DExpression(double E0t, double ht, double nut, double nt, double alt) : Expression(), E0(E0t), h(ht), nu(nut), n(nt), al(alt) {}
    double E0, h, nu, n, al;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = E0*pow(h,3)/(12*(1-nu*nu))*((4*(n+2)*(n+2)*(n+al)*(n+3*al)-3*(n+1)*(n+3)*(n+2*al)*(n+2*al))/((n+2)*(n+2)*(n+3)*(n+al)));
    }

};

class EulerBernoulliVariableScaledFunction : public Expression
{
  public:
    EulerBernoulliVariableScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

dolfin::Array<double> vx2a(1);
void EulerBernoulliVariableScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  vx2a[0] = x[0]*scale + trans;
  fn.eval(values, vx2a);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class Normal;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

EulerBernoulliVariableBeam::EulerBernoulliVariableBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     inited(false)
{
  Constant zero(0);
}

void EulerBernoulliVariableBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;

  const FunctionSpace& V(*Wn1.function_space());
  EulerBernoulliVariableBM::LinearForm LBM(V);
  EulerBernoulliVariableBM::BilinearForm aBM(V,V);

  double w = 1;
  Constant Ec(E), Ic(I);
  LBM.E = Ec;
  LBM.I = Ic;
  LBM.w = Wn1;
  solve(aBM == LBM, BM);
}

std::string EulerBernoulliVariableBeam::gettype()
{
  return "Euler-Bernoulli Beam with crack";
}

void EulerBernoulliVariableBeam::init ()
{
  L = (r-l);

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;
}

void EulerBernoulliVariableBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void EulerBernoulliVariableBeam::shutdown_diagnostics()
{
  fclose(stef);
}

void EulerBernoulliVariableBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
}

void EulerBernoulliVariableBeam::nonstep () {
}
void EulerBernoulliVariableBeam::timestep () {
 }
void EulerBernoulliVariableBeam::clstep()
 {
 }
void EulerBernoulliVariableBeam::adjacc(double alpha) {
}

void EulerBernoulliVariableBeam::load_function_vectors(std::string name)
{
}

void EulerBernoulliVariableBeam::save_function_vectors(std::string name)
{
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Cracks : public SubDomain
{
  public:
    Cracks(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt), found(false) {}
    double l, r, tol;
    mutable bool found;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      bool ret = fabs(x[0] - .5) <= tol;
      if (ret) found = true;
      return ret;
    }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

class c2Expression : public Expression
{
  public:
    c2Expression(double c2t, double indentt) : c2(c2t), indent(indentt) {}
    double c2, indent;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = (x[0] < indent) ? c2*1e8 : c2; }
};

void EulerBernoulliVariableBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  EulerBernoulliVariableForm::FunctionSpace V(*calcmesh);
  EulerBernoulliVariableForm::CoefficientSpace_Wn VW(*calcmesh);
  //EulerBernoulliVariableForm::CoefficientSpace_n Vn(*calcmesh);
  EulerBernoulliVariableForm::LinearForm Lt(V); EulerBernoulliVariableForm::BilinearForm at(V, V);

  EulerBernoulliVariableScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  Cracks cracks(l, r);
  MeshFunction<unsigned int> cracksf(*calcmesh, 0);
  cracksf.set_all(0);
  //cracks.mark(cracksf, 1);
  //if (!cracks.found) std::cout << "No crack found! Check node exists halfway along beam" << std::endl;

  Function F(V);

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant c1(rhop*A*L*L*L/(T*T)), c2(1/L), c4(sbmg ? rhow*g*L*L*L : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L*L);
  Fscale(q, L*L);

  double E0 = E, n = 0.5, al = 0.2;
  DExpression D(E0, h, nu, n, al);

  at.c1 = c1; at.c2 = c2; at.D = D; at.c4 = c4; at.beta = betac; at.Dt = Dtc;
  Lt.c1 = c1; Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;

  at.dS = cracksf;
  Lt.dS = cracksf;
  solve(at == Lt, F);
  EulerBernoulliVariableScaledFunction Wnews(1/L, -l/L, F[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
