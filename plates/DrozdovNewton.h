#ifndef DROZDOVNEWTON_H
#define DROZDOVNEWTON_H

#include "../plateproblem.h"
#include "thspace.h"
#include <stdlib.h>
using namespace dolfin;

class DrozdovNewtonBeam : public PlateProblem
{
  public:
    DrozdovNewtonBeam(Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg);

    void step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing );
    std::string gettype();
    void nonstep ();
    void timestep ();
    void clstep();
    void adjacc(double alpha);
    void load_function_vectors(std::string name);
    void save_function_vectors(std::string name);
    void output_diagnostics(double t,
      Function& Wn1, Function& Wdtn1, Function& Wddtn1,
      Function& Wn, Function& Wdotn, Function& Wddotn );
    void setup_diagnostics(std::string name, char* openmode);
    void shutdown_diagnostics();
    void init();
    Mesh* calcmesh;
    FILE* stef;
    double L;
    double Dt;
    double beta;
    double G;
    double h;
    double A;
    double rhop;
    double rhow;
    double g;
    double nu;
    double E;
    double I;
    double D;
    double kAG;
    double d2;
    double c1;
    double c2;
    double c3;
    double c4;

    thspace::FunctionSpace Vth;
    void eval_bm(Function& M, Function& Wn1);
    bool inited;
    Function thdotn, thn, thddotn, thn1, thdtn1, thddtn1;
    Function thddtkm1, thdtkm1, thkm1, thn1old;
    
};

class DrozdovNewtonBeamer : public Plater
{
  public:
  PlateProblem* make (Mesh& mesht, double left, double right, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmg)
  { return new DrozdovNewtonBeam(mesht, left, right, Dt, E, h, nu, beta, rhop, rhow, mu, g, sbmg); }
};

FunctionSpace& setup_timoshenko_form(Mesh* calcmesh, Constant& muc, Constant& Dtc,
	Constant& Dc, Constant& betac, Constant& kAGc, Constant& d2c,
	Constant& c1c, Constant& c2c, Constant& c3c,
	Function& F, Function& Wdotn, Function& Wn, Function& Wddotn,
	Function& thdotn, Function& thn, Function& thddotn);
void solve_timoshenko_form(Function& Zold, Function& dZ);

#endif
