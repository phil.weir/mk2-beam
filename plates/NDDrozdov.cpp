#include "NDDrozdov.h"
#include "NDDrozdovForm.h"
#include "diff.h"
#include "ddiff.h"
#include "PlateNorm.h"
#include "plus.h"
#include "drop.h"
#include "../logfile.h"

std::string NDDrozdovBeam::gettype()
{
  return "Non-Dimensionalized Drozdov Beam";
}

void NDDrozdovBeam::init ()
{
  calcmesh = new Mesh(mesh);
  L = (r-l);
  //L = 1;//RMV
  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
    calcmesh->geometry().x(vit->index(), 0) /= L;
  calcmesh->intersection_operator().clear();
  inited = true;
  logfile << "After scaling: " << Dc/(L*L) << " Wxxxx + " << rhopc*hc*1 << " Wdd = " << rhowc*gc << " W +..." << std::endl;
}

class NormalFunction : public Expression
{
  public:
    NormalFunction(double mpt) : mp(mpt) {}
    double mp;
    void eval (dolfin::Array<double>& values, const dolfin::Array<double> &x ) const
    { values[0] = x[0] < mp ? -1 : 1; }

};

class ScaledFunction : public Expression
{
  public:
    ScaledFunction (double scalet, Function& fnt) : scale(scalet), fn(fnt) {}
    double scale;
    Function& fn;
    void eval ( dolfin::Array<double>& values, const dolfin::Array<double> &x ) const
    { values[0] = approx_z(fn, x[0]*scale); }

};

void NDDrozdovBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  
 // 
  NDDrozdovForm::FunctionSpace V(*calcmesh);   NDDrozdovForm::BilinearForm aF(V,V);   NDDrozdovForm::LinearForm LF(V);
  //NDDrozdovForm::FunctionSpace V(mesh);   NDDrozdovForm::BilinearForm aF(V,V);   NDDrozdovForm::LinearForm LF(V);//RMV
  diff::FunctionSpace  Vd(mesh);  diff::BilinearForm  ad(Vd,Vd);  diff::LinearForm  Ld(Vd);
  ddiff::FunctionSpace Vdd(mesh); ddiff::BilinearForm add(Vdd,Vdd); ddiff::LinearForm Ldd(Vdd);
  plus::BilinearForm ap(V,V); plus::LinearForm Lp(V);
  drop::BilinearForm ar(V,V); drop::LinearForm Lr(V);
  PlateNorm::Functional NormM(*calcmesh);
 // PlateNorm::Functional NormM(mesh);
  Constant Lc(L);

  aF.rhow = rhowc; aF.g = gc; aF.mu = muc; aF.Dt = Dtc;
  aF.D = Dc; aF.h = hc;
  aF.beta = betac; aF.rhop = rhopc;
 
  aF.l = Lc; LF.l = Lc;
  Constant sbmgc(sbmg?1.0:0.0);
  aF.sbmg = sbmgc;
  aF.Dt = Dtc;
  NormalFunction n( (l+r)/2 );
  aF.n = n; LF.n = n;
  LF.beta = betac; LF.h = hc; LF.rhop = rhopc; LF.D = Dc; LF.sbmg = sbmgc; LF.rhow = rhowc; LF.g = gc;
  LF.mu = muc; LF.Dt = Dtc;
  Ldd.Dt = Dtc; Ldd.beta = betac;
  aF.mu = muc;
  Ld.Dt = Dtc;

  Function Wn1f(V), Wddtn1f(Vdd), Wdtn1f(Vd);
  ScaledFunction WnS(L, Wn), WdotnS(L, Wdotn), WddotnS(L, Wddotn), ppS(L, pp), fS(L, forcing),
   Wn1S(L, Wn1);
  LF.f = fS; LF.p = ppS;
  //LF.f = forcing; LF.p = pp;
  LF.Wdotn = WdotnS; LF.Wn = WnS; LF.Wddotn = WddotnS;
  //LF.Wdotn = Wdotn; LF.Wn = Wn; LF.Wddotn = Wddotn;
  int i = 0, err; double tol = 1e-16;
  Function Wnew(V), Wold(V); Wold = Wn1S;
  //Function Wnew(V), Wold(V); Wold = Wn1;
  do
  {
    i++;
    LF.W = Wold; aF.W = Wold;
    VariationalProblem elprob (aF, LF); // RMV INSERT BC IF NESS
    Function dW(V);
    elprob.solve(dW);
    NormM.f = dW;
    err = assemble(NormM);
    Lp.f = dW; Lp.g = Wold;
    VariationalProblem plprob (ap, Lp);
    plprob.solve(Wnew);
    Wold = Wnew;
  } while (err > tol && i < 25);
  ScaledFunction Wnews(1/L, Wnew);
  Wn1.interpolate(Wnews);
  //Wn1.interpolate(Wnew);

  if (log) presout << pp;
  if (log) forcout << forcing;
  Ldd.Wn = Wn; Ldd.Wn1 = Wn1; Ldd.Wddotn = Wddotn; Ldd.Wdotn = Wdotn;
  VariationalProblem ddiffprob (add, Ldd);
  ddiffprob.solve(Wddtn1f); Wddtn1.interpolate(Wddtn1f);
//  Wddtn1 = Wddtn1f;

  Ld.Wddotn = Wddotn; Ld.Wddotn1 = Wddtn1; Ld.Wdotn = Wdotn;
  VariationalProblem diffprob(ad, Ld);
  diffprob.solve(Wdtn1f); Wdtn1.interpolate(Wdtn1f);
//  Wdtn1 = Wdtn1f;
  if (log) dispout << Wn1;
  if (log) veloout << Wdtn1;
  if (log) acceout << Wddtn1;
//  Constant zero(0.0);
//  Wddtn1 = zero; Wdtn1 = zero; Wn1 = Wn;
}
