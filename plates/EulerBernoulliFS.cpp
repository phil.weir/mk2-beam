#include "EulerBernoulliFS.h"
#include "expr.h"
#include "EulerBernoulliFSForm.h"
#include "thspace.h"
//#include "EulerBernoulliFSForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class EulerBernoulliFSScaledFunction : public Expression
{
  public:
    EulerBernoulliFSScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

dolfin::Array<double> x2(1);
void EulerBernoulliFSScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class Normal;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

EulerBernoulliFSBeam::EulerBernoulliFSBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I), kAG(5.0/6 * h * G),
     Vth(mesht), inited(false), thdotn(Vth), thn(Vth), thddotn(Vth), thn1(Vth), thdtn1(Vth), thddtn1(Vth),
     thddtkm1(Vth), thdtkm1(Vth), thkm1(Vth), thn1old(Vth)
{
  Constant zero(0);
  thdotn = zero;
  thn = zero;
  thddotn = zero;
  thn1 = zero;
  thdtn1 = zero;
  thddtn1 = zero;
  thddtkm1 = zero;
  thdtkm1 = zero;
  thkm1 = zero;
  thn1old = zero;
}

std::string EulerBernoulliFSBeam::gettype()
{
  return "Euler-Bernoulli Beam";
}

void EulerBernoulliFSBeam::init ()
{
  L = (r-l);

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  thspace::FunctionSpace& Vth = *(new thspace::FunctionSpace(*calcmesh));
  Function *_thdotn = new Function(Vth);
  Function *_thn = new Function(Vth);
  Function *_thddotn = new Function(Vth);
  Function *_thn1 = new Function(Vth);
  Function *_thdtn1 = new Function(Vth);
  Function *_thddtn1 = new Function(Vth);
  Function *_thddtkm1 = new Function(Vth);
  Function *_thdtkm1 = new Function(Vth);
  Function *_thkm1 = new Function(Vth);
  Function *_thn1old = new Function(Vth);
  thdotn = *_thdotn;
  thn = *_thn;
  thddotn = *_thddotn;
  thn1 = *_thn1;
  thdtn1 = *_thdtn1;
  thddtn1 = *_thddtn1;
  thddtkm1 = *_thddtkm1;
  thdtkm1 = *_thdtkm1;
  thkm1 = *_thkm1;
  thn1old = *_thn1old;
}

void EulerBernoulliFSBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void EulerBernoulliFSBeam::shutdown_diagnostics()
{
  fclose(stef);
}

double eval_euler_bernoulli_strain_energy(Function& W, Function& th, double EI, double kAG, double L, double l)
{
}

void EulerBernoulliFSBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

  if (inited) fprintf(stef, "%lf,%lf\n", t, eval_euler_bernoulli_strain_energy(Wn1, thn1, E*I, kappa*A*G, L, l));
  fflush(stef);
}

void EulerBernoulliFSBeam::nonstep () {
   thn1 = thn; thdtn1 = thdotn; thddtn1 = thddotn;//RMV
}
void EulerBernoulliFSBeam::timestep () {
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
   thn      = thn1;
   thdotn   = thdtn1;
   thddotn  = thddtn1;
 }
void EulerBernoulliFSBeam::clstep()
 {
   thn1old  = thn1;
   thddtkm1 = thddtn1;
   thdtkm1  = thdtn1;
   thkm1    = thn1;
 }
void EulerBernoulliFSBeam::adjacc(double alpha) {
   Function thddtn1tmp(Vth), thdtn1tmp(Vth), thn1tmp(Vth); thddtn1tmp = thddtn1; thdtn1tmp = thdtn1; thn1tmp = thn1;
   AlphaFunction thalpha(alpha, thddtn1tmp, thddtkm1, thn1old);
   AlphaFunction thalphap(4*alpha, thddtn1tmp, thdtkm1, thn1old);
   AlphaFunction thalphapp(16*alpha, thddtn1tmp, thkm1, thn1old);
   thddtn1 = thalpha;// thdtn1 = thalphap; thn1 = thalphapp;
   thddtkm1 = thddtn1; thdtkm1 = thdtn1; thkm1 = thn1;
}

void EulerBernoulliFSBeam::load_function_vectors(std::string name)
{
   load_function_vector(thddtkm1, name, "thddtkm1");
   load_function_vector(thdtkm1, name, "thdtkm1");
   load_function_vector(thkm1, name, "thkm1");
   load_function_vector(thn, name, "thn");
   load_function_vector(thdotn, name, "thdotn");
   load_function_vector(thddotn, name, "thddotn");
}

void EulerBernoulliFSBeam::save_function_vectors(std::string name)
{
   save_function_vector(thddtkm1, name, "thddtkm1");
   save_function_vector(thdtkm1, name, "thdtkm1");
   save_function_vector(thkm1, name, "thkm1");
   save_function_vector(thn, name, "thn");
   save_function_vector(thdotn, name, "thdotn");
   save_function_vector(thddotn, name, "thddotn");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

class c2Expression : public Expression
{
  public:
    c2Expression(double c2t, double indentt) : c2(c2t), indent(indentt) {}
    double c2, indent;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = (x[0] < indent) ? c2*1e8 : c2; }
};

void EulerBernoulliFSBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  EulerBernoulliFSForm::FunctionSpace V(*calcmesh);
  EulerBernoulliFSForm::CoefficientSpace_Wn VW(*calcmesh);
  //EulerBernoulliFSForm::CoefficientSpace_n Vn(*calcmesh);
  EulerBernoulliFSForm::LinearForm Lt(V); EulerBernoulliFSForm::BilinearForm at(V, V);

  EulerBernoulliFSScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);

  std::vector< const BoundaryCondition* > bcU;
  Ends ends(0, 1);
  SubSpace Vthx(V, 1); Function thx_at_ends(VW); thx_at_ends = zero;
  DirichletBC* endbc = new DirichletBC(Vthx, thx_at_ends, ends); bcU.push_back(endbc);
  
  //VariationalProblem tprob(at, Lt, bcU);
  Function F(V);

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant c1(rhop*A*L*L*L/(T*T)), c2(E*I/L), c4(sbmg ? rhow*g*L*L*L : 0.),
    betac(beta), Dtc(Dt), cweight(rhop*h*g*L*L);
  //Normal n(Vn, l/L, r/L);
  Fscale(q, L*L);

  c2Expression c2e(E*I/L, 0.1);

  at.c1 = c1; at.c2 = c2; at.c4 = c4; at.beta = betac; at.Dt = Dtc;// at.n = n;
  Lt.c1 = c1; Lt.beta = betac; Lt.Dt = Dtc;
  Lt.q = q;
  Lt.Wn = WnS; Lt.Wdotn = WdotnS; Lt.Wddotn = WddotnS;
  //Lt.thn = thn; Lt.thdotn = thdotn; Lt.thddotn = thddotn;

  //tprob.solve(F);
  solve(at == Lt, F, bcU);
  EulerBernoulliFSScaledFunction Wnews(1/L, -l/L, F[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);
  //write_function(Wn1, "Wn1"); // RMV
  //write_function(Wddtn1, "Wddtn1"); // RMV
  //write_function(Wdtn1, "Wdtn1"); // RMV
  //write_function(pp, "press"); // RMV

  thn1.interpolate(F[1]);
  //EulerBernoulliFSForm::CoefficientSpace_Wn VW2(mesh);
  //EulerBernoulliFSScaledFunction Monews(1/L, -l/L, F[1]);
  //Function Mof(VW2); Mof = Monews;
  //Fscale(Mof, 1/c2);
  //write_function(Mof, "W/W_434_EulB_Mo"); // RMV
  //EulerBernoulliFSScaledFunction thnews(1/L, -l/L, F[2]);
  //Function thf(VW2); thf = thnews;
  //write_function(thf, "W/W_434_EulB_th"); // RMV

  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
}
