#include "Hollow.h"
#include "HollowForm.h"
#include "diff.h"
#include "ddiff.h"
#include "PlateNorm.h"
#include "plus.h"
#include "drop.h"
#include "../logfile.h"

std::string HollowBeam::gettype()
{
  return "Non-Dimensionalized Drozdov Beam";
}

void HollowBeam::init ()
{
  calcmesh = new Mesh(mesh);
  L = (r-l);
  //L = 1;//RMV
  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
    calcmesh->geometry().x(vit->index(), 0) /= L;
  calcmesh->intersection_operator().clear();
  inited = true;
  logfile << "After scaling: " << Dc/(L*L) << " Wxxxx + " << rhopc*hc*1 << " Wdd = " << rhowc*gc << " W +..." << std::endl;
}

class ScaledFunction : public Expression
{
  public:
    ScaledFunction (double scalet, Function& fnt) : scale(scalet), fn(fnt) {}
    double scale;
    Function& fn;
    void eval ( dolfin::Array<double>& values, const dolfin::Array<double> &x ) const
    { values[0] = approx_z(fn, x[0]*scale); }

};

void HollowBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  
 // 
  HollowForm::FunctionSpace V(*calcmesh);   HollowForm::BilinearForm aF(V,V);   HollowForm::LinearForm LF(V);
  //HollowForm::FunctionSpace V(mesh);   HollowForm::BilinearForm aF(V,V);   HollowForm::LinearForm LF(V);//RMV
  diff::FunctionSpace  Vd(mesh);  diff::BilinearForm  ad(Vd,Vd);  diff::LinearForm  Ld(Vd);
  ddiff::FunctionSpace Vdd(mesh); ddiff::BilinearForm add(Vdd,Vdd); ddiff::LinearForm Ldd(Vdd);
  plus::BilinearForm ap(V,V); plus::LinearForm Lp(V);
  drop::BilinearForm ar(V,V); drop::LinearForm Lr(V);
  PlateNorm::Functional NormM(*calcmesh);
 // PlateNorm::Functional NormM(mesh);
  Constant Lc(L);

  aF.rhow = rhowc; aF.g = gc; aF.mu = muc; aF.Dt = Dtc;
  //aF.D = Dc;
  aF.h = hc;
  aF.beta = betac; aF.rhop = rhopc;
  //aF.hc = Hc2;
  //aF.l = Lc; LF.l = Lc;
  Constant sbmgc(sbmg?1.0:0.0);
  aF.sbmg = sbmgc;
  aF.Dt = Dtc;
  LF.beta = betac; LF.h = hc; LF.rhop = rhopc; //LF.D = Dc;
  LF.sbmg = sbmgc; LF.rhow = rhowc; LF.g = gc; //LF.hc = Hc2;
  LF.mu = muc; LF.Dt = Dtc;
  Ldd.Dt = Dtc; Ldd.beta = betac;
  aF.mu = muc;
  Ld.Dt = Dtc;

  Function Wn1f(V), Wddtn1f(Vdd), Wdtn1f(Vd);
  ScaledFunction WnS(L, Wn), WdotnS(L, Wdotn), WddotnS(L, Wddotn), ppS(L, pp), fS(L, forcing),
   Wn1S(L, Wn1);
  LF.f = fS; LF.p = ppS;
  //LF.f = forcing; LF.p = pp;
  LF.Wdotn = WdotnS; LF.Wn = WnS; LF.Wddotn = WddotnS;
  //LF.Wdotn = Wdotn; LF.Wn = Wn; LF.Wddotn = Wddotn;
  int i = 0, err; double tol = 1e-16;
  Function Wnew(V), Wold(V); Wold = Wn1S;
  //Function Wnew(V), Wold(V); Wold = Wn1;
  do
  {
    i++;
    LF.W = Wold;
  //aF.W = Wold;
    VariationalProblem elprob (aF, LF); // RMV INSERT BC IF NESS
    Function dW(V);
    elprob.solve(dW);
    NormM.f = dW;
    err = assemble(NormM);
    Lp.f = dW; Lp.g = Wold;
    VariationalProblem plprob (ap, Lp);
    plprob.solve(Wnew);
    Wold = Wnew;
  } while (err > tol && i < 16);
  ScaledFunction Wnews(1/L, Wnew);
  Wn1.interpolate(Wnews);
  //Wn1.interpolate(Wnew);

  if (log) presout << pp;
  if (log) forcout << forcing;
  Ldd.Wn = Wn; Ldd.Wn1 = Wn1; Ldd.Wddotn = Wddotn; Ldd.Wdotn = Wdotn;
  VariationalProblem ddiffprob (add, Ldd);
  ddiffprob.solve(Wddtn1f); Wddtn1.interpolate(Wddtn1f);
//  Wddtn1 = Wddtn1f;

  Ld.Wddotn = Wddotn; Ld.Wddotn1 = Wddtn1; Ld.Wdotn = Wdotn;
  VariationalProblem diffprob(ad, Ld);
  diffprob.solve(Wdtn1f); Wdtn1.interpolate(Wdtn1f);
//  Wdtn1 = Wdtn1f;
  if (log) dispout << Wn1;
  if (log) veloout << Wdtn1;
  if (log) acceout << Wddtn1;
}
