#include "vK.h"
#include "EulerBernoulliBM.h"
#include "expr.h"
#include "vKForm.h"
#include "uspace.h"
//#include "vKForm.h"
#include "diffdg.h"
#include "dgplus.h"
#include "../logfile.h"

class vKScaledFunction : public Expression
{
  public:
    vKScaledFunction (double scalet, double transt, Function& fnt) : scale(scalet), trans(transt), fn(fnt) { }
    double scale, trans;
    Function& fn;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const;
};

void vKScaledFunction::eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
{
  dolfin::Array<double> x2(1); x2[0] = x[0]*scale + trans;
  fn.eval(values, x2);
  //values[0] = approx_z(fn, x[0]*scale + trans);
}

class NormalFunction;
void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void diff(Function& dtn1, double Dt, Function& ddtn1, Function& ddotn, Function& dotn);
void ddiff(Function& ddtn1, double Dt, double beta, Function& n, Function& n1, Function& ddotn, Function& dotn);

vKBeam::vKBeam(Mesh& mesht, double left, double right, double Dtt, double Et, double ht, double nut, double betat,
	double rhopt, double rhowt, double mu, double gt, bool sbmg) :
     PlateProblem(mesht, left, right, Dtt, Et, ht, nut, betat, rhopt, rhowt, mu, gt, sbmg), L(1.0),
     Dt(Dtt), beta(betat), G(Et/(3*(1-2*nut))), h(ht), A(ht*1),
     rhop(rhopt), rhow(rhowt), g(gt), nu(nut), E(Et), I(h*h*h*1/(12*(1-nu*nu))), D(E*I),
     Vu(mesht), inited(false), udotn(Vu), un(Vu), uddotn(Vu), un1(Vu), udtn1(Vu), uddtn1(Vu),
     uddtkm1(Vu), udtkm1(Vu), ukm1(Vu), un1old(Vu)
{
  Constant zero(0);
  udotn = zero;
  un = zero;
  uddotn = zero;
  un1 = zero;
  udtn1 = zero;
  uddtn1 = zero;
  uddtkm1 = zero;
  udtkm1 = zero;
  ukm1 = zero;
  un1old = zero;
}

void vKBeam::eval_bm(Function& BM, Function& Wn1)
{
  if (!inited) return;
  //Mesh& mesh(*Wn1.function_space()->mesh());

  const FunctionSpace& V(*Wn1.function_space());
  EulerBernoulliBM::LinearForm LBM(V);
  EulerBernoulliBM::BilinearForm aBM(V,V);

  double w = 1;
  double I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;
  Constant Ic(I), Ec(E);
  LBM.E = Ec;
  LBM.I = Ic;
  LBM.w = Wn1;
Function M(V);
  solve(aBM == LBM, M);
BM.interpolate(M);
}

std::string vKBeam::gettype()
{
  return "vK Beam";
}

void vKBeam::init ()
{
  L = (r-l);
  //L = 1000;//RMV

  calcmesh = new Mesh(mesh);

  for (VertexIterator vit(*calcmesh) ; !vit.end() ; ++vit)
  {
    calcmesh->geometry().x(vit->index(), 0) -= l;
    calcmesh->geometry().x(vit->index(), 0) /= L;
  }
  calcmesh->intersection_operator().clear();
  inited = true;

  uspace::FunctionSpace& Vu = *(new uspace::FunctionSpace(*calcmesh));
  Function *_udotn = new Function(Vu);
  Function *_un = new Function(Vu);
  Function *_uddotn = new Function(Vu);
  Function *_un1 = new Function(Vu);
  Function *_udtn1 = new Function(Vu);
  Function *_uddtn1 = new Function(Vu);
  Function *_uddtkm1 = new Function(Vu);
  Function *_udtkm1 = new Function(Vu);
  Function *_ukm1 = new Function(Vu);
  Function *_un1old = new Function(Vu);
  udotn = *_udotn;
  un = *_un;
  uddotn = *_uddotn;
  un1 = *_un1;
  udtn1 = *_udtn1;
  uddtn1 = *_uddtn1;
  uddtkm1 = *_uddtkm1;
  udtkm1 = *_udtkm1;
  ukm1 = *_ukm1;
  un1old = *_un1old;
}

void vKBeam::setup_diagnostics(std::string name, char* openmode)
{
  char stefile[200];
  sprintf(stefile, "ste-%s.csv", name.c_str());
  stef = fopen(stefile, openmode);
}

void vKBeam::shutdown_diagnostics()
{
  fclose(stef);
}

double eval_euler_bernoulli_strain_energy(Function& W, Function& u, double EI, double kAG, double L, double l);

void vKBeam::output_diagnostics(double t,
  Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn )
{
  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(3*(1-2*nu))), kappa = 5.0/6;

  if (inited) fprintf(stef, "%lf,%lf\n", t, eval_euler_bernoulli_strain_energy(Wn1, un1, E*I, kappa*A*G, L, l));
  fflush(stef);
}

void vKBeam::nonstep () {
   un1 = un; udtn1 = udotn; uddtn1 = uddotn;//RMV
}
void vKBeam::timestep () {
   uddtkm1 = uddtn1;
   udtkm1  = udtn1;
   ukm1    = un1;
   un      = un1;
   udotn   = udtn1;
   uddotn  = uddtn1;
 }
void vKBeam::clstep()
 {
   un1old  = un1;
   uddtkm1 = uddtn1;
   udtkm1  = udtn1;
   ukm1    = un1;
 }
void vKBeam::adjacc(double alpha) {
   Function uddtn1tmp(Vu), udtn1tmp(Vu), un1tmp(Vu);
   uddtn1tmp = uddtn1; udtn1tmp = udtn1; un1tmp = un1;
   AlphaFunction ualpha(alpha, uddtn1tmp, uddtkm1, un1old);
   uddtn1 = ualpha; uddtkm1 = uddtn1; udtkm1 = udtn1; ukm1 = un1;
   //Function uddtn1tmp(Vu), udtn1tmp(Vu), un1tmp(Vu); uddtn1tmp = uddtn1; udtn1tmp = udtn1; un1tmp = un1;
   //AlphaFunction ualpha(alpha, uddtn1tmp, uddtkm1, un1old);
   //AlphaFunction ualphap(4*alpha, uddtn1tmp, udtkm1, un1old);
   //AlphaFunction ualphapp(16*alpha, uddtn1tmp, ukm1, un1old);
   //uddtn1 = ualpha;// udtn1 = ualphap; un1 = ualphapp;
   //uddtkm1 = uddtn1; udtkm1 = udtn1; ukm1 = un1;
}

void vKBeam::load_function_vectors(std::string name)
{
   load_function_vector(uddtkm1, name, "uddtkm1");
   load_function_vector(udtkm1, name, "udtkm1");
   load_function_vector(ukm1, name, "ukm1");
   load_function_vector(un, name, "un");
   load_function_vector(udotn, name, "udotn");
   load_function_vector(uddotn, name, "uddotn");
   load_function_vector(un1, name, "un1");
   load_function_vector(udtn1, name, "udtn1");
   load_function_vector(uddtn1, name, "uddtn1");
}

void vKBeam::save_function_vectors(std::string name)
{
   save_function_vector(uddtkm1, name, "uddtkm1");
   save_function_vector(udtkm1, name, "udtkm1");
   save_function_vector(ukm1, name, "ukm1");
   save_function_vector(un, name, "un");
   save_function_vector(udotn, name, "udotn");
   save_function_vector(uddotn, name, "uddotn");
   save_function_vector(un1, name, "un1");
   save_function_vector(udtn1, name, "udtn1");
   save_function_vector(uddtn1, name, "uddtn1");
}

class Exponential : public Expression
{
  public:
    Exponential (double at, double ct=0, double At=1) : a(at), c(ct), A(At) {}
    double a, A, c;
    void eval(dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    { values[0] = A*exp(a*(x[0]-c)); }
};

class Ends : public SubDomain
{
  public:
    Ends(double lt, double rt, double tolt=1e-5) : l(lt), r(rt), tol(tolt) {}
    double l, r, tol;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return x[0] <= l+tol || x[0] >= r-tol;
    }
};

void vKBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn, Function& forcing )
{
  if (!inited) init();
  Constant zero(0);

  vKForm::FunctionSpace V(*calcmesh);
  vKForm::CoefficientSpace_Wn VW(*calcmesh);
  //vKForm::CoefficientSpace_n Vn(*calcmesh);
  vKForm::LinearForm Ft(V); vKForm::JacobianForm Jt(V, V);

  vKScaledFunction WnSF(L, l, Wn), WdotnSF(L, l, Wdotn), WddotnSF(L, l, Wddotn),
    Wn1SF(L, l, Wn1), pSF(L, l, pp), fSF(L, l, forcing);
  Function WnS(VW), WdotnS(VW), WddotnS(VW), Wn1S(VW), q(VW), fS(VW);
  WnS = WnSF;
  WdotnS = WdotnSF; WddotnS = WddotnSF; Wn1S = Wn1SF; q = pSF; fS = fSF;
  Fscale(WnS, 1/L); Fscale(WdotnS, 1/L); Fscale(WddotnS, 1/L); Fscale(Wn1S, 1/L);
  Fadd(q, fS);  Fscale(q, pow(L,2)/(rhop*A));

  std::vector< const BoundaryCondition* > bcU;
  Ends ends(0, 1);
  
  Function F(V), Z(V); Z.vector()->zero();

  double w = 1;
  double A = w*h, I = w*h*h*h/(12*(1-nu*nu)), G(E/(2*(1+nu))), kappa = 5.0/6;
  double T = 1;
  Constant
  	c1(pow(L,3)/pow(T,2)),
	c2(L*I/(A*pow(T,2))),
	c3(1/(rhop*A)),
	c4(E*L/rhop),
	c5(rhow*g*pow(L,3)/(rhop*A)),
	c6(E*pow(T,2)/(rhop*L)),
	c7(E*I/L),



    betac(beta), Dtc(Dt), Lc(L);
  Parameters params("nonlinear_variational_solver");
  Parameters npars("newton_solver");
  npars.add("maximum_iterations", 23);
  npars.add("relative_tolerance", 1e-6);
  params.add(npars);

  Jt.c1 = c1; Jt.c2 = c2; Jt.c3 = c3; Jt.c4 = c4; Jt.c5 = c5;
  Jt.c6 = c6; Jt.c7 = c7;
  //Jt.c2 = zero; //Jt.c_4 = zero; //RMV!!
  Ft.c1 = c1; Ft.c2 = c2; Ft.c3 = c3; Ft.c4 = c4; Ft.c5 = c5;
  Ft.c6 = c6; Ft.c7 = c7;
  //Ft.c2 = zero; //Ft.c_4 = zero; //RMV!!
  Jt.beta = betac; Jt.Dt = Dtc;
  Ft.beta = betac; Ft.Dt = Dtc;
  Ft.q = q;
  Ft.Wn = WnS; Ft.Wdotn = WdotnS; Ft.Wddotn = WddotnS;
  Ft.un = un; Ft.udotn = udotn; Ft.uddotn = uddotn;


  int i = 0;
  //do {
  	Ft.Z = Z; Jt.Z = Z;
        solve(Ft==0, Z, bcU, Jt, params);
	//Fadd(Z, F);
  //} while ( i++ < 25 );

  vKScaledFunction Wnews(1/L, -l/L, Z[0]);
  Wn1.interpolate(Wnews); Fscale(Wn1, L);
  un1.interpolate(Z[1]);


  ddiff(Wddtn1, Dt, beta, Wn, Wn1, Wddotn, Wdotn);
  diff(Wdtn1, Dt, Wddtn1, Wddotn, Wdotn);
  ddiff(uddtn1, Dt, beta, un, un1, uddotn, udotn);
  diff(udtn1, Dt, uddtn1, uddotn, udotn);


}
