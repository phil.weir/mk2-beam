#include "FixedBeam.h"
#include "../logfile.h"

std::string FixedBeam::gettype()
{
  return "Fixed";
}

void FixedBeam::step ( Function& pp, Function& Wn1, Function& Wdtn1, Function& Wddtn1,
  Function& Wn, Function& Wdotn, Function& Wddotn,
  Function& forcing )
{
  Constant zero(0.0);
  if (log) presout << pp;
  if (log) forcout << forcing;
  Wdtn1 = zero; Wddtn1 = zero; Wn1 = Wn;
}
