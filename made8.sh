source outroot.sh
files=`ls $outroot | grep "$1"`
for i in $files
do
	file $outroot/$i/mk2*log >/dev/null 2>&1
	if [ $? == 0 ]; then
		runname=`echo $i|sed 's/.*\.//g'`
		j=`tail -n 10 $outroot/output.$runname/mk2.$runname.log 2>&1 | grep Time | tail -n 1`
		k=`cat $outroot/output.$runname/mk2.$runname.log 2>&1 | grep "Time: 8"`
		if [ "$k" != "" ]; then
			echo $i : $j
		fi
	fi
done

