#include "logfile.h"
#include "readparam.h"
#include "outroot.h"
#include "HEP.h"

void HEP::parse_args(int argc, char* argv[], char* paramloc, char* dirname, char* mkoutdir, char* outdir, char* logstr, std::ostream& logfile, bool& resume)
{
  char outputname[15] = "output.def"; strcpy(paramloc, "parameters"); strcpy(dirname, outputname);
  bool uion = true;

  logfile << "PID: " << pid << std::endl;
  logfile << "Command: " << argv[0];
  char startmode = 'N';
  for ( argl = 1 ; argl < argc ; argl++ )
  {
    logfile << " " << argv[argl];
    if (argv[argl][0] == '-')
    {
      if ( argv[argl][1] == 'L' ) {
        sprintf(mkoutdir, "./procsetup.sh %d", pid);
        sprintf(outdir, "%s/output.%d", OUTROOT, pid);
        sprintf(logstr, "mk2.%d.log", pid);
        sprintf(texstr, "mk2.%d.tex", pid);
	sprintf(runname, "%d", pid);
	strcpy(dirname, runname);
	pid_args = true;
        default_args = false;
      }
      if ( argv[argl][1] == 'S' ) {
        argl++; logfile << " " << argv[argl];
	sscanf(argv[argl],"%c",&startmode); // Maybe should copy properly
        argl++; logfile << " " << argv[argl];
	if (startmode == 'T')
	 sscanf(argv[argl],"%lf",&ostarttime); // Maybe should copy properly
	else if (startmode == 'F')
	 sscanf(argv[argl],"%d",&ostartframe); // Maybe should copy properly
      }
      if ( argv[argl][1] == 'C' ) {
        ciout = true;
      }
      if ( argv[argl][1] == '3' ) {
        mode3d = true;
      }
      if ( argv[argl][1] == 'R' ) {
        resume = true;
      }
      if ( argv[argl][1] == 'F' ) {
        fout = true;
      }
      if ( argv[argl][1] == 'T' ) {
        platetest = true;
      }
      if ( argv[argl][1] == 'A' ) {
        aout = true;
      }
      if ( argv[argl][1] == 'N' ) {
        argl++; logfile << " " << argv[argl];
	sscanf(argv[argl],"%d",&outfreq); // Maybe should copy properly
      }
      if ( argv[argl][1] == 'D' ) {
        argl++; logfile << " " << argv[argl];
        sprintf(mkoutdir, "./procsetup.sh %s", argv[argl]);
        sprintf(outdir, "%s/output.%s", OUTROOT, argv[argl]);
        sprintf(logstr, "mk2.%s.log", argv[argl]);
        sprintf(texstr, "mk2.%s.tex", argv[argl]);
	sprintf(runname, "%s", argv[argl]);
	strcpy(dirname,argv[argl]);
	dir_args = true;
        default_args = false;
      }
      if ( argv[argl][1] == 'H' ) {
        std::cout << "MK2 Program\n===========\n\n-D dir\tOutput to dir\n-L\tUse PID for dir\n\t-C\tOutput intermediate steps\n";
	std::cout << "-P\tOutput from plate subsystem\n-H\tOutput this help list\n-A\tOutput additional plots" << std::endl;
	std::cout << "-F\tOutput forcing over time (if mode 3)\n-N num\tOutput every num time-steps\n-S [F|T] num\tOutput from time/frame num\n" << std::endl;
	std::cout << "-G\tSkip any user input steps (non-interactive mode)\n-T\tPlate test (forcing in parameter file)\n" << std::endl;
      }
      if ( argv[argl][1] == 'G' ) {
        uion = false;
      }
      if ( argv[argl][1] == 'P' ) {
        plate_output = true;
      }
    }
    else { strcpy(paramloc,argv[argl]);
           default_args = false; }
  }
  logfile << std::endl;

  if (default_args) { sprintf(runname, "[default]"); }
  if (uion) {
    char asstr[100], butstr[200];
    printf("[MK2] This run is the same as ... but ...\n");
    printf("[MK2] As: "); if (!fgets(asstr, 100, stdin)) std::cout << "[MK2] Entry error!" << std::endl;
    printf("[MK2] But: "); if (!fgets(butstr, 100, stdin)) std::cout << "[MK2] Entry error!" << std::endl;
    logfile << "This run is the same as " << asstr << std::endl << " but " << butstr << std::endl;
    trackerfile.open("tracker.log", std::ios::out | std::ios::app );
    trackerfile << runname << " is same as " << asstr << std::endl << " but " << butstr << std::endl;
    trackerfile.close();
  } else {
    logfile << "No user information on run provided" << std::endl;
  }
  if (startmode=='F') logfile << "Output to begin at frame " << ostartframe;
  if (startmode=='T') logfile << "Output to begin at time " << ostarttime;
}

double HEP::set_parameters ( char* paramloc )
{
  /* Initialize the GiNaC input */
  tab["x"] = x; tab["pi"] = pi; tab["t"] = symt;
  GiNaC::parser parser(tab);

  //char paramloc2[200]; sprintf(paramloc2, "%s", paramloc);
  paramfile = fopen("parameters", "r");
  std::cout << paramloc << std::endl;
  if (!paramfile) { logfile << "[ERR] COULD NOT OPEN PARAMETER FILE!! " << paramloc <<std::endl;
                    std::cout << "[ERR] COULD NOT OPEN PARAMETER FILE!! " << paramloc <<std::endl; exit(3); }
  std::ofstream pidfile("mk2.pid"); pidfile << pid; pidfile.close();

  texfile.open(texstr);
  do {
   assert(fgets(buffer, 500, paramfile) != NULL);
  }
  while ( buffer[0] != '=' );
  assert(fscanf(paramfile, "N = %d", &N) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "M = %d", &M) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  Dt = get_double_from_line(parser, paramfile, "Dt", false);
  rhow = get_double_from_line(parser, paramfile, "rhow", false);
  std::cout << Dt << std::endl;
  std::cout << rhow << std::endl;
 
  char plateloc[400]; int platect;
  assert(fscanf(paramfile, "platect = %d", &platect) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  std::cout << platect << std::endl;
  for ( int i = 0 ; i < platect ; i++ ) {
   assert(fscanf(paramfile, "plate = %[^\r\t#]", plateloc) != EOF);
   assert(fgets(buffer, 500, paramfile) != NULL);
   std::cout << plateloc << std::endl;
   platedescriptions.push_back(load_plate_description(plateloc, parser));
  }
  char wavemakerloc[400];
  assert(fscanf(paramfile, "wavemaker = %[^\r\t#]", wavemakerloc) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  wavemaker = load_wavemaker(wavemakerloc);

  assert(fscanf(paramfile, "tol(exp) = %lf", &tolexp) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
 
  assert(fscanf(paramfile, "horiz = %d", &horizfm) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  T = get_double_from_line(parser, paramfile, "T", false);
  lscale = get_double_from_line(parser, paramfile, "l", false);
  musurf = get_double_from_line(parser, paramfile, "mu_s", false);
  g = get_double_from_line(parser, paramfile, "g", false);
 
  assert(fscanf(paramfile, "alpha = %lf", &alpha) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
 
  char automeshc;
  assert(fscanf(paramfile, "mesh = %c", &automeshc) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL); automesh = automeshc=='A';
  char depthstr[500];
  assert(fscanf(paramfile, "depth = %[^\r\t#]", depthstr) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  char falsebtmstr[500];
  assert(fscanf(paramfile, "falsebtm = %[^\r\t#]", falsebtmstr) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "length = %lf", &length) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "clmax = %d", &clmax) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
 
  assert(fscanf(paramfile, "maxfs = %d", &maxfs) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "k_guess = %lf", &k_guess) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  char tiltedmode;
  assert(fscanf(paramfile, "nlfluid = %c%c", &icmode, &tiltedmode) != EOF); nlfluid = icmode=='Y';
  tilted_fluid = tiltedmode=='Y';
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "IC mode = %c", &icmode) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "IC = %[^\r\t#]", expstr) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "mcl = %lf %lf %lf %lf", &mcl[0], &mcl[1], &mcl[2], &mcl[3]) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "mrat = %lf", &mrat) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "FL = %lf", &fl) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "FR = %lf", &fr) != EOF);
  assert(fgets(buffer, 500, paramfile) != NULL);
  assert(fscanf(paramfile, "FORCING = %d", &forcingmode) != EOF);
  assert(fgets(forcingstr, 500, paramfile) != NULL);
 
  fclose(paramfile);
  if (fabs(fl - fr) <= EPS) { fl = 0.0; fr = length; }

  sprintf(buffer, "N = %d M = %d Dt = %lf rhow = %lf tol(exp) = %lf\n", N, M, Dt, rhow, tolexp); logfile << buffer;
  sprintf(buffer, "T = %lf g = %lf alpha = %lf\n", T, g, alpha); logfile << buffer;
  sprintf(buffer, "mu_s = %lf length = %lf\nclmax = %d k_guess = %lf icmode = %c\n", musurf, length, clmax, k_guess, icmode); logfile << buffer;
  sprintf(buffer, "Mesh near %lf far %lf mid %lf vfar %lf (h/v ratio %lf)\n", mcl[0], mcl[1], mcl[2], mcl[3], mrat); logfile << buffer;
  sprintf(buffer, "FL = %lf FR = %lf Nonlinear fluid? = %c Tilted? = %c\n", fl, fr, nlfluid?'y':'n', tilted_fluid?'y':'n'); logfile << buffer;
  logfile << std::endl;
  int pc =0;
  for (std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  {
    PlateDescription& pd = **pdit;
    sprintf(buffer, "[Plate %d] %s\n", ++pc, pd.typestr); logfile << buffer;
    sprintf(buffer, "left = %lf right = %lf ", pd.left, pd.right); logfile << buffer;
    if (draft < 0)
     sprintf(buffer, "draft is Arch.\n");
    else
     sprintf(buffer, "draft = %lf\n", pd.draft);
    logfile << buffer;
    sprintf(buffer, "E = %lf h = %lf nu = %lf\n", pd.E, pd.h, pd.nu); logfile << buffer;
    sprintf(buffer, "beta = %lf rhop = %lf mup = %lf\n", pd.beta, pd.rhop, pd.mup); logfile << buffer;
    logfile << std::endl;
  }
  sprintf(buffer, "[Wavemaker]\n"); logfile << buffer;
  sprintf(buffer, "amp = %lf fre = %lf lwr = %lf btm = %lf\nnum = %lf attcoeff = %lf\n",
    wavemaker->amp, wavemaker->fre, wavemaker->lwr, wavemaker->btm, wavemaker->num, wavemaker->attcoeff); logfile << buffer;

  sprintf(buffer, "\\documentclass[11pt,a4paper]{article}\n\\begin{document}\n\\begin{tabular}{ | l | l | }\n\\hline\n"); texfile << buffer;
  sprintf(buffer, "Run Name & %s \\\\\n", runname); texfile << buffer;
  sprintf(buffer, "Mesh & %d x %d \\\\\n", N, M); texfile << buffer;
  sprintf(buffer, "\\(\\Delta t\\) & %lf \\\\\n \\(\\rho\\sb{w}\\) & %lf \\\\\n \\(\\mu\\sb{p}\\) & %lf \\\\\n \\(\\rho\\sb{p}\\) & %lf \\\\\n",
    Dt, rhow, mup, rhop); texfile << buffer;
  sprintf(buffer, "CI tol & \\(10\\sp{%lf}\\) \\\\\n \\(T\\) & %lf \\\\\n \\(g\\) & %lf \\\\\n \\(E\\) & %lf GPa \\\\\n",
    tolexp, T, g, E*pow(10,-9)); texfile << buffer;
  sprintf(buffer, "\\(h\\) & %lf \\\\\n \\(\\nu\\sb{p}\\) & %lf \\\\\n \\(\\beta\\) & %lf \\\\\n \\(\\alpha\\) & %lf \\\\\n",
    h, nu, beta, alpha); texfile << buffer;
  sprintf(buffer, "Max CI & %d \\\\\n \\(k\\sb{guess}\\) & %lf \\\\\n",
    clmax, k_guess); texfile << buffer;
  sprintf(buffer, "Mesh near & %lf \\\\\n Mesh far & %lf \\\\\n Mesh mid & %lf \\\\\n", mcl[0], mcl[1], mcl[2]); texfile << buffer;
  sprintf(buffer, "Mesh vfar & %lf \\\\\n Mesh h/v ratio & %lf\\\\\\n", mcl[3], mrat); texfile << buffer;
  sprintf(buffer, "IC left & %lf \\\\\n IC right & %lf \\\\\n",
    fl, fr); texfile << buffer;

  icpi = parser(expstr);
  depth = parser(depthstr).subs(pi==M_PI);
  if (falsebtmstr[0]=='!') falsebtm = depth;
  else falsebtm = parser(falsebtmstr).subs(pi==M_PI);
  ic = icpi.subs(pi==M_PI);
  logfile << "Initial Conditions (zeta): " << icpi << std::endl;
  logfile << "Depth Conditions: " << depth << std::endl;
  if (falsebtmstr[0]=='!')
    logfile << "False Bottom Conditions: " << "NONE" << std::endl;
  else
    logfile << "False Bottom Conditions: " << falsebtm << std::endl;
  sprintf(buffer, "IC for \\(\\zeta\\) & \\("); texfile << buffer;
  texfile << GiNaC::latex;
  texfile << icpi;
  sprintf(buffer, "\\) \\\\\n"); texfile << buffer;
  sprintf(buffer, "Depth & \\("); texfile << buffer;
  texfile << depth;
  sprintf(buffer, "False btm & \\("); texfile << buffer;
  texfile << falsebtm;
  sprintf(buffer, "\\) \\\\\n"); texfile << buffer;

  /* Process forcing options */
  switch ( forcingmode ) {
    case 1:
      sscanf(forcingstr, " (%lf,%lf,%lf)", &f_wd_x, &f_wd_m, &f_wd_h);
      logfile << "Forcing Conditions [1]: WD " << f_wd_x << " fm L; " << f_wd_m << "N; " << f_wd_h << " high" << std::endl;
      sprintf(buffer, "Weight drop & %lfN from %lfm at %lfm from left end\\\\\n", f_wd_m, f_wd_h, f_wd_x); texfile << buffer;
      break;
    case 2:
      fce = strchr(forcingstr,'\t'); *fce = '\0';
      f_ex_ex = parser(forcingstr).subs(pi==M_PI);
      logfile << "Forcing Conditions [2]: " << f_ex_ex << std::endl;
      sprintf(buffer, "Plate forcing & \\("); texfile << buffer;
      texfile << f_ex_ex;
      sprintf(buffer, "\\) \\\\\n"); texfile << buffer;
      break;
    case 3:
      sscanf(forcingstr, " (%lf,%lf,", &f_wd_x, &f_wde_t);
      fce = strchr(forcingstr,'\t')-1; *fce = '\0';
      //fce = strchr(buffer,'#'); *fce = '\0';
      fce = strchr(forcingstr,',')+1;
      f_ex_ex = parser(strchr(fce,',')+1);
      logfile << "Forcing Conditions [3]: " << f_ex_ex << " at " << f_wd_x << " until " << f_wde_t << std::endl;
      sprintf(buffer, "Plate forcing & \\("); texfile << buffer;
      texfile << f_ex_ex;
      sprintf(buffer, "\\) at %lf until %lf\\\\\n",f_wd_x, f_wde_t); texfile << buffer;
      break;
    default:
      logfile << "Forcing Conditions [0]: (No forcing)" << std::endl;
      sprintf(buffer, "Forcing & (none)\\\\\n"); texfile << buffer;
      forcingmode = 0;
  }

  /* End TeX output */
  sprintf(buffer, "\\hline\n\\end{tabular}\n\\end{document}\n"); texfile << buffer;
  texfile.close();
  return 0;
}
