#include "Navier.h"
#include "Hybrid.h"
#include "HybridPressure.h"
//#include "TentativeVelocity.h"
//#include "VelocityUpdate.h"
//#include "PressureUpdate.h"
#include "EulJoin.h"
#include "lTop.h"
#include "GeoMaker.h"
#include "vhTop.h"
#include "Top1.h"
#include "Top2.h"
#include <sys/stat.h>
#include "Predict.h"
#include "Standard.h"
#include "EulFSCond.h"
#include "Domain.h"
#include "DomainMixed.h"
#include "Domain2.h"
#include "hDomain.h"
#include "hTop.h"
#include "dDomain.h"
#include "dTop.h"
#include "hDoubleTop.h"
#include "beam_dx.h"

#define BIGEPS 1e-12
#define DZ 1e-5

void chebyshev_smoothing(Function& zeta);

extern double rhow;
void eval_navfscond (File& file, Function& pdtf, Function& zdtf, FunctionSpace& dpV, FunctionSpace& dzV, Constant& gc, Function& zeta, Expression& w,
  Expression& u, Function& beach_nu, Function& cphi, Constant& muc, Constant& lc)
{
  EulFSCond::BilinearForm a(dzV, dzV); EulFSCond::LinearForm L(dzV);
  L.zeta = zeta;
  L.w = w;
  L.u = u;
  solve(a == L, zdtf);
}

void eval_navcorrect (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, Constant& Dtc,
  Function& zetan, Function& zdtn1, Function& zdtn, Function& zdtnm1, Function& zdtnm2,
  Function& phitopn, Function& pdtn1, Function& pdtn, Function& pdtnm1, Function& pdtnm2 )
{
  double Dt = Dtc;
  for ( int i = 0 ; i < zetan1.vector()->size() ; i ++ )
  {
    double un = (*zetan.vector())[i];
    double udtkm1 = (*zdtn1.vector())[i];
    double udtn = (*zdtn.vector())[i];
    double udtnm1 = (*zdtnm1.vector())[i];
    double udtnm2 = (*zdtnm2.vector())[i];
    zetan1.vector()->setitem(i, (un + (Dt/24)*(9*udtkm1 + 19*udtn - 5*udtnm1 + udtnm2) ));
  }
  for ( int i = 0 ; i < phitopn1.vector()->size() ; i ++ )
  {
    double un = (*phitopn.vector())[i];
    double udtkm1 = (*pdtn1.vector())[i];
    double udtn = (*pdtn.vector())[i];
    double udtnm1 = (*pdtnm1.vector())[i];
    double udtnm2 = (*pdtnm2.vector())[i];
    phitopn1.vector()->setitem(i, (un + (Dt/24)*(9*udtkm1 + 19*udtn - 5*udtnm1 + udtnm2) ));
  }
}

void eval_navpredict (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, FunctionSpace& dpV, Constant& Dtc,
  Function& zetan, Function& zdtn, Function& zdtnm1, Function& zdtnm2, Function& zdtnm3,
  Function& phitopn, Function& pdtn, Function& pdtnm1, Function& pdtnm2, Function& pdtnm3, Constant& lc, Constant& muc)
{
  double Dt = Dtc;
  for ( int i = 0 ; i < zetan1.vector()->size() ; i ++ )
  {
    double un = (*zetan.vector())[i];
    double udtn = (*zdtn.vector())[i];
    double udtnm1 = (*zdtnm1.vector())[i];
    double udtnm2 = (*zdtnm2.vector())[i];
    double udtnm3 = (*zdtnm3.vector())[i];
    zetan1.vector()->setitem(i, (un + (Dt/24)*(55*udtn - 59*udtnm1 + 37*udtnm2 - 9*udtnm3)));
  }
  for ( int i = 0 ; i < phitopn1.vector()->size() ; i ++ )
  {
    double un = (*phitopn.vector())[i];
    double udtn = (*pdtn.vector())[i];
    double udtnm1 = (*pdtnm1.vector())[i];
    double udtnm2 = (*pdtnm2.vector())[i];
    double udtnm3 = (*pdtnm3.vector())[i];
    phitopn1.vector()->setitem(i, (un + (Dt/24)*(55*udtn - 59*udtnm1 + 37*udtnm2 - 9*udtnm3)));
  }
}

void eval_tent (Function& ucurrent, Function& pcurrent, double Dt, FunctionSpace& VH, Function& uprev,
  Expression& Wdt, Expression& Wddt, MeshFunction<uint>& p1dommf, std::map< std::string, File* > &basicfiles)
{

  Hybrid::LinearForm L(VH); Hybrid::BilinearForm a(VH, VH);
  Function U0(VH);
  Function Uh(VH); Uh.vector()->zero();
  SubSpace VU(VH, 2);
  SubSpace VUu(VU, 0);
  SubSpace VUv(VU, 1);
  SubSpace VP(VH, 3);
  Constant gravity(0, 0);
  L.f = gravity;
  a.U0 = U0;

  Constant zero(0);
  Constant zero_vector(0, 0);
  DirichletBC wmbc(VUu, zero, p1dommf , 1);
  DirichletBC fsbc(VP, zero, p1dommf, 2);
  DirichletBC beambc(VUv, zero, p1dommf , 3);
  DirichletBC basebc(VUv, zero, p1dommf , 4);
  DirichletBC wallsbc(VUv, zero, p1dommf , 5);//Really? Does this even work?

  std::vector<const BoundaryCondition*> bcs;
  //bcs.push_back(&wmbc);
  //bcs.push_back(&beambc);
  //bcs.push_back(&basebc);
  //bcs.push_back(&wallsbc);
  bcs.push_back(&fsbc);

  for (int i = 0 ; i < 10 ; i++)
  {
     U0 = Uh;
     solve(a == L, Uh, bcs);
     *basicfiles["u"] << Uh[0][0];
     *basicfiles["v"] << Uh[0][1];
     *basicfiles["p"] << Uh[1];
     std::cout << "iter " << i << std::endl;
  }
  *basicfiles["u"] << Uh[0][0];
  *basicfiles["v"] << Uh[0][1];
  *basicfiles["p"] << Uh[1];

  //if (mode3d) { eval_velpot_3d(phi, V, phitop, bc, bcw, Wdt, wave, p1dommf, lc); return; }

  //DirichletBC wmbc(VT, zero_vector, p1dommf , 1);
  //DirichletBC fsbc(VP, zero, p1dommf, 2);
  //DirichletBC beambc(VT, zero_vector, p1dommf , 3);
  //DirichletBC basebc(VT, zero_vector, p1dommf , 4);
  //DirichletBC wallsbc(VT, zero_vector, p1dommf , 5);//Really? Does this even work?

  //std::vector<const BoundaryCondition*> bcu;
  ////bcu.push_back(&wmbc);
  ////bcu.push_back(&beambc);
  ////bcu.push_back(&basebc);
  ////bcu.push_back(&wallsbc);
  //std::vector<const BoundaryCondition*> bcp;
  //bcp.push_back(&fsbc);

  //Constant Dtc(Dt);
  //Constant f(0, -9.81);
  //Function utent(VT);
  //TentativeVelocity::BilinearForm aT(VT, VT);
  //TentativeVelocity::LinearForm LT(VT);
  //LT.u0 = uprev;
  //aT.k = Dtc;
  //LT.k = Dtc;
  //LT.f = f;
  //aT.ds = p1dommf;
  //LT.ds = p1dommf;
  ////LinearVariationalProblem probT(aT, LT, utent, bcu);
  ////LinearVariationalSolver solvT(probT);
  ////solvT.parameters["linear_solver"] = "gmres";
  ////solvT.solve();
  //solve(aT==LT, utent, bcu);
  //*basicfiles["u"] << utent[0];
  //*basicfiles["v"] << utent[1];

  //Constant rhowc(rhow);
  //PressureUpdate::BilinearForm aP(VP, VP);
  //PressureUpdate::LinearForm LP(VP);
  //LP.u1 = utent;
  //LP.k = Dtc;
  //LP.rho = rhowc;
  ////LinearVariationalProblem probP(aP, LP, pcurrent, bcp);
  ////LinearVariationalSolver solvP(probP);
  ////solvP.parameters["linear_solver"] = "gmres";
  ////solvP.solve();
  //solve(aP==LP, pcurrent, bcp);

  //VelocityUpdate::BilinearForm aV(VT, VT);
  //VelocityUpdate::LinearForm LV(VT);
  //LV.p1 = pcurrent;
  //LV.k = Dtc;
  //LV.rho = rhowc;
  //LV.u1 = utent;
  //aV.ds = p1dommf;
  //LV.ds = p1dommf;
  //solve(aV==LV, ucurrent, bcu);

  // //NavierEquations::JacobianForm J(eV, eV);
  // //NavierEquations::LinearForm F(eV);
  // //Constant zero(0.,0.);
  // ////F.Wdot = Wdt;
  // ////F.Wddot = Wddt;
  // ////F.zdt = zdt;
  // //std::vector< const BoundaryCondition* > bcv(0);// bcv[0] = &bc;// bcv[1] = &bcw;
  // //J.ds = p1dommf;
  // ////J.u = ucurrent[0];
  // ////J.v = ucurrent[1];
  // //F.u = ucurrent[0];
  // //F.v = ucurrent[1];
  // //F.ds = p1dommf;
  // //Constant rhowc(rhow);
  // //F.rhow = rhowc;
  // //J.rhow = rhowc;
  // //     F.U = naviern1;// J.U = naviern1;
  // //     solve(F == 0, naviern1, bcv, J, params);
}

extern double inner; // ALSO SET IN FLUIDMESH

extern bool enable_refpress;
extern double refpress_trigger;
extern dolfin::Array<double> vtmp, vtmp2, xtmp, xtmp2;
class LinearFunction;

void eval_beam_dx(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc);

void linearize(Function& linear, Function& poly);

int mk2_exit (int code = 1);

int output_plane_csv (double length, double depth, int titeration, Function& zeta_linear, Function& zeta, Function& F);

// FOR DISCUSSION OF HOW SINGULARITY AFFECTS CONVERGENCE, SEE FEM BK PP369-370
class Navier::Beach_nuFunction : public Expression
{
  public:
    Beach_nuFunction (double lmdat) : lmda(lmdat) {}
    double lmda;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      double x0 = length-2*lmda, alpha = 0.2; // Bonnefoy et al., 2006b
      if ( x[0] < x0 ) { values[0] = 0.0; return; }
      double Lb = length - x0, u = (x[0] - x0)/Lb;
      values[0] = alpha*u*u*(3-2*u);
      return;
    }
};

class TestExpression : public Expression
{
  public:
    TestExpression () : Expression() {}
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = sin(x[0]);
    }
};

class Navier::DzFunction : public Expression
{
  public:
  DzFunction ( Function& phit, Function& zeta_lineart, Function& zetat, Function& cphit, PlateList& plt, double tt=0 ) :
  	phi(phit), zeta_linear(zeta_lineart), zeta(zetat), cphi(cphit), pl(plt), t(tt) {}

  Function& phi, &zeta_linear, &zeta, &cphi; PlateList &pl; double t;
  void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    dolfin::Array<double> p1(2), p2(2); p1[0] = x[0]; p2[0] = x[0]; double dz = DZ, dx = dz*0.2;
    p1[1] = eval_z(zeta_linear, x[0]) - dz*1e-2; p2[1] = eval_z(zeta_linear, x[0]) - dz*1e-2 - dz;

    double o = 0;
    if (x[0] < 0.5*dx || Plate::this_plate(pl, x[0]-0.5*dx, true, length))        o = 0.5*dx;
    if (x[0] > length-0.5*dx || Plate::this_plate(pl, x[0]+0.5*dx, true, length)) o = -0.5*dx;

    double zetax = ( eval_z(zeta, x[0]+0.5*dx+o) - eval_z(zeta, x[0]-0.5*dx+o) ) / dx;
    double cphix = ( eval_z(cphi, x[0]+0.5*dx+o) - eval_z(cphi, x[0]-0.5*dx+o) ) / dx;
    dolfin::Array<double> v1(1), v2(1);
    try { phi.eval(v1, p1); } catch (std::runtime_error& str) { 
	mk2_exit(20);
    }

    dolfin::Array<double> n(2); n[0] = -zetax / sqrt(1+zetax*zetax); n[1] = 1/sqrt(1+zetax*zetax);
    p2[0] = p1[0] - dz*n[0]; p2[1] = p1[1] - dz*n[1];
    bool underplate = false;
    for ( PlateList::iterator plit = pl.begin() ; plit != pl.end() && !underplate ; plit++ )
      underplate = underplate || ( p2[0] >= (*plit)->left - EPS && p2[0] <= (*plit)->right + EPS );

    double phis = cphix/sqrt(1+zetax*zetax);
    double phin = 0;
    if (p2[0] < 0 || underplate || p2[0] > length)
    { p2[0] = p1[0]; p2[1] = p1[1] - dz/sqrt(1+zetax*zetax);
      try { phi.eval(v2, p2); } catch (std::runtime_error& str) { 
          mk2_exit(20);
      }
      double phiz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      underplate = false;
      phin = ( (1+zetax*zetax)*phiz - cphix*zetax ) / sqrt(1+zetax*zetax);
      //phin = phiz; phis = 0; //RMV!!
    } else {
      try { phi.eval(v2, p2); } catch (std::runtime_error& str) { 
	mk2_exit(20);
      }
      phin = (v1[0] - v2[0])/dz;
    }
    values[0] = phin*phin + phis*phis;
  }
};

class Navier::NavierDotFromInterval : public Expression
{
  public:
    NavierDotFromInterval ( PlateList& platelistt, double Dtt, Function& n1t, Function& nt, bool freesurfacet = true ) :
    	Dt(Dtt), n1(n1t), n(nt), freesurface(freesurfacet), platelist(platelistt) {}
    double Dt;

    Function &n1, &n;
    bool freesurface;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);

      double xt = x[0];
      if (plate) {
	double l = plate->left, r = plate->right;
        //values[0] = ((xt-r)*eval_z(intf, l) + (l-xt)*eval_z(intf,r)) / (l-r); // ENSURE CONTINUITY ON FS/P INTERFACE!
	values[0] = 0;
      } else
	values[0] = (eval_z(n1, xt)-eval_z(n, xt))/Dt;
        //values[0] = approx_z(intf, xt); // RMV true
    }
};

// You're responsible for making sure this only gets evaluated where it's supposed to!
// NOW UPDATED TO GIVE FS DOM (NOTE MOD NEEDED TO USE PLATE AS MUTUAL BDY PTS TREATED AS FS)
class Navier::MeshFromInterval : public Expression
{
  public:
    MeshFromInterval ( PlateList& platelistt, Function& intft, bool freesurfacet = true ) : intf(intft), freesurface(freesurfacet), platelist(platelistt) {}
    Function &intf;
    bool freesurface;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);

      double xt = x[0];
      if (plate) {
	double l = plate->left, r = plate->right;
        values[0] = ((xt-r)*eval_z(intf, l) + (l-xt)*eval_z(intf,r)) / (l-r); // ENSURE CONTINUITY ON FS/P INTERFACE!
      } else
	intf.eval(values, x);
        //values[0] = approx_z(intf, xt); // RMV true
    }
};
extern double sf_n;
extern dolfin::Array<double> p1, p2;
extern dolfin::Array<double> n;
extern dolfin::Array<double> xvA, vA, v2A, vV2A;  
class Navier::SquareFunction : public Expression
{
  public:
  SquareFunction ( Function& phit, Function& zeta_lineart, Function& zetat, Function& cphit, PlateList& plt, Function& gradphit, Function& zetaxt, MeshFunction<uint>& cell_mapt, MeshFunction<uint>& cell_to_facett, MeshConnectivity& mconnt, double tt=0 ) :
  	phi(phit), zeta_linear(zeta_lineart), zeta(zetat), cphi(cphit), pl(plt), gradphi(gradphit), zetax(zetaxt), cell_map(cell_mapt), cell_to_facet(cell_to_facett), mconn(mconnt), t(tt) {}
  
  Function& phi, &zeta_linear, &zeta, &cphi; PlateList &pl; Function& gradphi, &zetax; MeshFunction<uint> &cell_map, &cell_to_facet; MeshConnectivity& mconn; double t;
  //void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x) const
  void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x, const ufc::cell &cell) const
  {
   int index = 0;
   index = cell_map[cell.index];
   dolfin::Cell cell2d(*gradphi.function_space()->mesh(), mconn(index)[0]);
   dolfin::Point normal = cell2d.normal(cell_to_facet[cell.index]);
   dolfin::Array<double> &gp = vV2A, &xv = xvA, &z = vA;
   xv[0] = x[0];
    try { zeta_linear.eval(z, x); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << x[0]<<std::endl;
        mk2_exit(20);
    }
   xv[1] = z[0]-1e-6;
    try { gradphi.eval(gp, xv); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << xv[0] << "," << xv[1] << std::endl;
        mk2_exit(20);
    }
    try { zetax.eval(z, x); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << x[0]<<std::endl;
        mk2_exit(20);
    }

   values[0] = (normal[0]*gp[0]+normal[1]*gp[1])*sqrt(1+z[0]*z[0]);
   double first_value = values[0];
   //values[0] = gp[1];
   //values[0] = normal[0];
   //values[0] = normal[0];
   //return;
   //gradphi.function_space()->mesh()->init(1,0);
   //gradphi.function_space()->mesh()->init(2,1);
   //const MeshConnectivity& mc = gradphi.function_space()->mesh()->topology()(1,0);
   //const MeshConnectivity& mc2 = gradphi.function_space()->mesh()->topology()(2,1);
   ////Facet c(*gradphi.function_space()->mesh(), mc2(cell2d.index())[cell_to_facet[cell.index]]);
   //Cell c(*zeta.function_space()->mesh(), cell.index);
   //values[0] = c.midpoint().x();
   //std::cout << x[0] << "," << values[0] << "," << c.str(true) << std::endl;
   ////values[0] = gradphi.function_space()->mesh()->geometry().point(mc(mc2(cell2d.index())[cell_to_facet[cell.index]])[0]).y();
   ////std::cout << 
   ////      gradphi.function_space()->mesh()->coordinates()[(mc(cell2d.index())[0])]<< std::endl;
   //////values[0] = gradphi.function_space()->mesh()->coordinates()[mc(cell2d.index())[0]][1];
   //return;

                                        p1[0] = x[0]; p2[0] = x[0]; double dz = DZ, dx = dz;
    p1[1] = eval_z(zeta_linear, x[0]) - dz*1e-2; p2[1] = eval_z(zeta_linear, x[0]) - dz*1e-2 - dz;

    double o = 0;
    if (x[0] < 0.5*dx || Plate::this_plate(pl, x[0]-0.5*dx, true, length))        o = 0.5*dx;
    if (x[0] > length-0.5*dx || Plate::this_plate(pl, x[0]+0.5*dx, true, length)) o = -0.5*dx;

    double zetax = ( eval_z(zeta, x[0]+0.5*dx+o) - eval_z(zeta, x[0]-0.5*dx+o) ) / dx;
    double cphix = ( eval_z(cphi, x[0]+0.5*dx+o) - eval_z(cphi, x[0]-0.5*dx+o) ) / dx;
    dolfin::Array<double> &v1 = vA, &v2 = v2A;
    try { phi.eval(v1, p1); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << p1[0] << "," << p1[1] << std::endl;
	mk2_exit(20);
    }

                                n[0] = -zetax / sqrt(1+zetax*zetax); n[1] = 1/sqrt(1+zetax*zetax);
                                //n[0] = normal[0]; n[1] = normal[1];
    p2[0] = p1[0] - dz*n[0]; p2[1] = p1[1] - dz*n[1];
    bool underplate = false;
    for ( PlateList::iterator plit = pl.begin() ; plit != pl.end() && !underplate ; plit++ )
      underplate = underplate || ( p2[0] >= (*plit)->left - EPS && p2[0] <= (*plit)->right + EPS );

    double phiz = 0;
    if (p2[0] < 0 || underplate || p2[0] > length)
    { p2[0] = p1[0]; p2[1] = p1[1] - dz/sqrt(1+zetax*zetax);
      try { phi.eval(v2, p2); } catch (std::runtime_error& str) { 
          std::cout << "SquareFunction failed at edge " << p2[0] << "," << p2[1] << std::endl;
          mk2_exit(20);
      }
      underplate = false;
      phiz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      values[0] = (1+zetax*zetax)*phiz - cphix*zetax;

    } else {
      try { phi.eval(v2, p2); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at interior " << p2[0] << "," << p2[1] << std::endl;
	mk2_exit(20);
      }
      phiz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      values[0] = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      //values[0] = v2[0];
    }
   // double old_val = values[0];
   //values[0] = (normal[0]*gp[0]+normal[1]*phiz)*sqrt(1+z[0]*z[0]);
    //if (sf_n > -.5) 
    //    std::cout << sf_n << ", " << x[0] << ", " << values[0] << ", " << first_value << ", " << 
    //      normal[0] << ", " << normal[1] << ", " << gp[0] << ", " << gp[1] << ", " << z[0] << ", " <<
    //      n[0] << ", " << n[1] << ", " << phiz << ", " << cphix << ", " << zetax << ", " << std::endl;
  }
};
class Navier::PlateMask : public Expression
{
  public:
   PlateMask ( Function& ft, PlateList& platelistt ) : f(ft), platelist(platelistt) {}
   Function &f; PlateList& platelist;
   void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x) const
   {
     if (!nlfluid) { values[0] = 0; return; }
     //std::cout << x[0] << " " << x[1] << std::endl;
     f.eval(values, x);
     return;
     //values[0] = 0; return; //RMV
     // RMV INCLUDE CORNERS OR NOT (SEE (FAR) BELOW)?
     Plate* plate = Plate::this_plate(platelist, x[0], false, length);
     if (plate && fabs(x[1] - eval_z(plate->Wn1_linear,x[0]) + plate->draft) <=1e-2/*+mcl[0]*3*/ )
     {
       dolfin::Array<double> xb(2); xb[0] = x[0]; xb[1] = x[1] - EPS;//RMV
       dolfin::Array<double> test(1);
       if (plate && fabs(x[1] - eval_z(plate->Wn1_linear,x[0]) + plate->draft) <= 1e-8 ) {
        //std::cout << x[0] << ": " << x[1]+0.5 << " " << approx_z(plate->Wn1,x[0],true) << " " << approx_z(plate->Wn1,x[0],false)<< std::endl;
        xb[1] = eval_z(plate->Wn1_linear,x[0]) - plate->draft - EPS - 1e-11;//RMV
//	fflush(stdout);
       }
      try { f.eval(test, xb); } catch (std::runtime_error& str) { 
        std::cout << "PlateMask failed at " << x[0] << "," << x[1]+0.5 << " (,"<<xb[1]+0.5<<")"<<std::endl;
	mk2_exit(20);
      }
//       f.eval(test, xb);
       values[0] = test[0];
     }
     else values[0] = 0.0;
   }
};

    extern dolfin::Array<double> fsx;
class Navier::FSLineFromMesh : public Expression
{
  public:
   FSLineFromMesh (Function& ft, Function& zeta_lineart, bool incdraftt = false) : f(ft), zeta_linear(zeta_lineart), incdraft(incdraftt) {}
  Function &f, &zeta_linear;
  bool incdraft;
  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    fsx[0] = x[0]; fsx[1] = eval_z(zeta_linear, x[0])-BIGEPS; // NOTE OUR OWN EPS!!!!!!!!@!!!! RMV
    try { f.eval(values, fsx); } catch (std::runtime_error& str) {
     std::cout << "Failed FSLineFromMesh at " << fsx[0] << "," << fsx[1] << std::endl;
     std::cout << "zeta is " << eval_z(zeta_linear, x[0]) << " " << "x is " << fsx[0] << " mesh is " <<
      f.function_space()->mesh()->geometry().x(0,0) << ","<<
      f.function_space()->mesh()->geometry().x(0,1) << std::endl;
     mk2_exit(18);
    }
    if (incdraft && fabs(values[0]) > 0.0) std::cout << values[0] << std::endl;
  }
};

void breakme();

double dist(double* x1, double* x2);
extern dolfin::Array<double> x;
double evaluate_lagrange(double* x, double* zs, double* ps[], int n);

void Navier::Single_mesh_move (Mesh& mesh, PlateList& platelist, Function& zeta_linear, Function& zetaold_linear, Function& varbottom, Function& varbottomold)
{
  for ( VertexIterator vit(mesh) ; !vit.end() ; ++vit )
  {
    int ind; ind = vit->index();
    double x = vit->x(0), zold = vit->x(1), z = mesh.geometry().x(vit.pos(),1);

    double uold = eval_z(varbottomold, x), u = eval_z(varbottom, x);
    Plate* platel = Plate::find_plate(platelist, false, x, false, length), *plater = Plate::find_plate(platelist, true, x, false, length);
    bool underplate = (platel == plater) && platel!=NULL;

    double val = 0, valold = 0;
    if ( fabs(zold-uold) <= EPS ) {
      z = u;
    }
    else if ( underplate ) {
    	Plate* plate = platel;
	double fl[2], fr[2], up[2], down[2];
	up[0] = x; up[1] = eval_z(plate->Wn1old_linear, x)-plate->draft;
	down[0] = x; down[1] = uold;
	fl[0] = plate->left; fl[1] = eval_z(zetaold_linear, plate->left);
	fr[0] = plate->right; fr[1] = eval_z(zetaold_linear, plate->right);
	double zs[4] = { eval_z(zeta_linear, fl[0])-fl[1], eval_z(zeta_linear, fr[0])-fr[1],
			 eval_z(plate->Wn1_linear, x)-plate->draft-up[1], u-down[1] };

    	if (fabs(zold-up[1]) <= EPS)
		z += zs[2];
	else {
	    	double* ps[4] = { fl, fr, up, down };
		double xv[2]; xv[0] = x; xv[1] = zold;
		z = zold + evaluate_lagrange(xv, zs, ps, 4);
	}
    }
    else if (plater && fabs (x - plater->left) <= EPS && fabs(zold - eval_z(plater->Wn1old_linear, plater->left) + plater->draft) <= BIGEPS)
      z = eval_z(plater->Wn1_linear, x) - plater->draft;
    else if (platel && fabs (x - platel->right) <= EPS && fabs(zold - eval_z(platel->Wn1old_linear, platel->right) + platel->draft) <= BIGEPS)
      z = eval_z(platel->Wn1_linear, x) - platel->draft;
    else {
	double up[2], down[2];

	double zs[4];
	int points = 2;
	up[0] = x; up[1] = eval_z(zetaold_linear, x);
	down[0] = x; down[1] = uold;
    	double* ps[4]; ps[0] = up; ps[1] = down;
	zs[0] = eval_z(zeta_linear, x)-up[1]; zs[1] = u-uold;

	if (fabs(zold-up[1]) <= EPS)
		z += zs[0];
	else {
	      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	      {
	      	Plate* plate=*plit;
		ps[points] = new double[2];
		double *pl = ps[points];
		ps[points][0] = plate->right;
		ps[points][1] = eval_z(plate->Wn1old_linear, pl[0])-plate->draft;
		zs[points] = eval_z(plate->Wn1_linear, pl[0])-plate->draft-ps[points][1];
		points++;

		ps[points] = new double[2];
		double *pr = ps[points];
		ps[points][0] = plate->left;
		ps[points][1] = eval_z(plate->Wn1old_linear, pr[0])-plate->draft;
		zs[points] = eval_z(plate->Wn1_linear, pr[0])-plate->draft-ps[points][1];
		points++;
	      }
	      double xv[2]; xv[0] = x; xv[1] = zold;
	      z = zold + evaluate_lagrange(xv, zs, ps, points);
	      for ( int i = 2; i < points ; i++ )
	        free(ps[i]);
	}
    }
    if (isnan(z)) {
      std::cout << "[MK2] Computed nan for vertical component vertex " << ind << " at x = " << x << " and zold = " << zold << std::endl;
      mk2_exit(7);
    }
	mesh.geometry().x(vit.pos(), 1) = z;
    //if (fabs(x - 201.534) < 4 && fabs(zold+6) < 2) std::cout << x << "," << zold+6 << "->" << z+6 << std::endl;
  }
  mesh.intersection_operator().clear();
  //mesh.intersection_operator().reset_kernel();
}

class Navier::WavedNeumann : public Expression
{
  public:
    WavedNeumann (PlateList& platelistt, Function& zetat) : Expression(2), platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS ) {
	values[0] = -wavemaker->velocity(t, x);
      }
      return;
    }
};

class Navier::WaveddNeumann : public Expression
{
  public:
    WaveddNeumann (PlateList& platelistt, Function& zetat) : Expression(2), platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS ) {
	values[0] = -wavemaker->acceleration(t, x);
      }
      return;
    }
};

class Navier::WdotNeumann : public Expression
{
  public:
    WdotNeumann (PlateList& platelistt, Function& zetat, bool updatedt) : Expression(2), platelist(platelistt), zeta(zetat), updated(updatedt) {}
    PlateList& platelist; Function& zeta; bool updated;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;

        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if ( fabs(x[1] - z) <= 0.1 ) {
		if (updated && plate->refpress_on) {
			values[1] = eval_z(plate->Wzdt, x[0]);
		} else
			values[1] = eval_z(updated?plate->Wdtn1:plate->Wdotn, x[0]);
	}
	//if ( tilted_fluid ) {
	//	values[1] = values[1]/(1+pow(eval_z(plate->Wx, x[0]), 2));
	//}
//	values[1]=0;//RMV
      }
    }
};

class Navier::WddotNeumann : public Expression
{
  public:
    WddotNeumann (PlateList& platelistt, Function& zetat, bool updatedt) : Expression(2), platelist(platelistt), zeta(zetat), updated(updatedt) {}
    PlateList& platelist; Function& zeta;
    bool updated;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;

        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if ( fabs(x[1] - z) < 0.1 ) {
		if (updated && plate->refpress_on) {
			values[1] = eval_z(plate->Wzddt, x[0]);
		} else
			values[1] = eval_z(updated?plate->Wddtn1:plate->Wddotn, x[0]);
	}
	//if ( tilted_fluid ) {
	//	values[1] = values[1]/(1+pow(eval_z(plate->Wx, x[0]), 2));
	//}
	//values[0] = 1e-10*x[0]*x[0];
	//if ( fabs(x[0] - 150) <= EPS && fabs(x[1] - z) <= .1) std::cout << x[1]-z << approx_z(plate->Wddtn1, x[0]) << std::endl;
//	values[1]=0;//RMV
      }
    }
};

class Navier::VelNeumann : public Expression
{
  public:
    VelNeumann (PlateList& platelistt, Function& zetat, Function& zdtt, Function& cphixt, Function& zxt, double phixlt) :
     Expression(2), platelist(platelistt), zeta(zetat), zdt(zdtt), cphix(cphixt), zx(zxt), phixl(phixlt) {}
    PlateList& platelist; Function& zeta; Function& zdt, &cphix, &zx;
    double phixl;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
//     if (fabs(x[1]+3) < EPS) {values[1] = cos(2*M_PI*x[0]/3/3);return;}
 //     if (fabs(x[1]) < EPS) values[1] = -cos(M_PI*x[0]/3);
//      values[1] *= t*1e2;
//      return;//RMV
//      values[0] = 1;//sin(M_PI*x[1]/3);
  //    return;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS )
      {
	values[0] = -wavemaker->velocity(t, x);
	return;
      }
   }
	/*

      {
	//double z = approx_z(zeta, x[0], true);
        *if ( fabs(x[1] - z) <= 1.0 )
	{
          // double zxi = approx_z(zx, x[0]), zdti = approx_z(zdt, x[0]);
	   //double phix = approx_z(cphix, x[0]) - zxi*zdti;
	   values[0] = (-(x[1]-z)*wavemaker->velocity(t, x) + (x[1] + 1.0 - z)*phixl)/1.0; //0;//-phix*0.5;
	   return;
           //values[0] = -zxi*phix;
      	   //values[1] = zdti;
   	   //values[0] /= sqrt(1+zxi*zxi); values[1] /= sqrt(1+zxi*zxi);
	} else* values[0] = wavemaker->velocity(t, x);
    *  } else* //if ( fabs(x[0]) <= EPS && x[1] < approx_z(zeta, 0.0) - EPS) {//RMV
//    *  } else* if ( fabs(x[1]+3) <= EPS ){//&& x[1] < approx_z(zeta, 0.0) - EPS) {//RMV
 //   values[1] = sin(t*M_PI*2*1e2) * sin (M_PI*x[0]/30);
	return;
      }*/
/*      } else if ( (fabs(x[0]) <= EPS *|| fabs(x[0] - 10) <= EPS*) && x[1]  approx_z(zeta, 0.0) - EPS) {
        return;
      }
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;
        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if (fabs(x[1]-z) <= 0.1)// && x[0] > plate->left)
         values[1] = approx_z(plate->Wdtn1, x[0]);
	//else values[1] = 0;
      }
      return;
	if (x[1] < -2.0) {* std::cout << x[0] << " " << x[1] << " " << z << std::endl; *return; }
        double zxi = approx_z(zx, x[0]), zdti = approx_z(zdt, x[0]);
	//double phix = approx_z(cphix, x[0]) - zxi*zdti;
	//if (fabs(x[0]) <=EPS) phix = wavemaker->velocity(t,x);
      //RMV/  values[0] = -zxi*phix;
	values[1] = zdti;
	values[0] /= sqrt(1+zxi*zxi); values[1] /= sqrt(1+zxi*zxi);
	
    //values[0] = 0;

    //values[1] = sin(t*M_PI*2*1e2) * sin (M_PI*x[0]/30);//RMV
	//values[0] = 0;
//	values[1] = 1000;
	//values[0] = 0; values[1] = zdti;
	*if (fabs(x[1]-z) <= 1) { values[1] = approx_z(zdt, x[0]);}
        else
	  values[1] = 0; // RMV change to be continuous at ends*/
      //}
};

class Navier::Single_FreeSurfaceFlat : public SubDomain
{
  public:
    Single_FreeSurfaceFlat(PlateList& platelistt) : platelist(platelistt) {}
    PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return !Plate::this_plate(platelist, x[0], false, length);
    }
};

class Navier::Single_TopSubDomain : public SubDomain
{
  public:
    Single_TopSubDomain(PlateList& platelistt, Expression* zetat=NULL) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist;
    Expression* zeta;
    // dolfin man
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      // Allow this to run through as constant
      // before we get a function space
      Plate* plate = Plate::this_plate(platelist, x[0], true, length);
      if (!plate && zeta)
      { dolfin::Array<double> val(1), xv(1); xv[0] = x[0];
        zeta->eval(val, xv); return fabs(x[1]-val[0]) <= EPS; }
      if (!plate) return fabs(x[1]) <= EPS;
      if ((fabs(x[0] - plate->left) <= EPS || fabs(x[0] - plate->right) <= EPS)
       && (fabs(x[1]+plate->draft) <= EPS || fabs(x[1])<= EPS)) return true;
      return fabs(x[1] + plate->draft) <= EPS;
    }
};

class Navier::Single_FreeSurfaceIns : public SubDomain // : public Navier::Single_TopSubDomain
{
  public:
    Single_FreeSurfaceIns(PlateList& platelistt, Function* zetat = NULL) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist;
    Function* zeta;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);
      if (plate) return false;
      double z = zeta? eval_z(*zeta, x[0]) : 0;
      return fabs(x[1] - z) <= EPS && x[0] > EPS;
    }
};

//class Navier::Single_FreeSurface : public SubDomain // : public Navier::Single_c);
class Navier::Single_Plates : public SubDomain // : public Navier::Single_TopSubDomain
{
  public:
    Single_Plates(PlateList& platelistt) : platelist(platelistt) {}
    PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], true, length);
      return plate && fabs(x[1]-eval_z(plate->Wn1_linear,x[0])+plate->draft) <= 1e-3; //RMV
    }
};

class Navier::Single_FreeSurface : public SubDomain // : public Navier::Single_TopSubDomain
{
  public:
    Single_FreeSurface(PlateList& platelistt, Function* zeta_lineart = NULL, bool endst = true) : platelist(platelistt), zeta_linear(zeta_lineart), zetae(NULL), Dx(0), ends(endst) {}
    Single_FreeSurface(PlateList& platelistt, Expression* zetat, double Dxt) : platelist(platelistt), zeta_linear(NULL), zetae(zetat), Dx(Dxt) {}
    PlateList& platelist;
    Function* zeta_linear;
    Expression* zetae;
    double Dx;
    bool ends;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], !ends, length);
      double z = 0;
      if (plate) return false;
      if (zeta_linear) z = eval_z(*zeta_linear, x[0]);
      else if (zetae) {
       double a, b, s, t;
       xvA[0] = Dx*(int)(x[0]/Dx); s = xvA[0];
       zetae->eval(vA, xvA);
       a = vA[0];
       xvA[0] += Dx; t = xvA[0];
       zetae->eval(vA, xvA);
       b = vA[0];
       z = a+(b-a)*(x[0]-s)/Dx;
      }
      return fabs(x[1] - z) <= BIGEPS;
    }
};

class Navier::WaveVelDBC : public Expression
{
  public:
    WaveVelDBC(double topt, double tt, double adt, double phi0t) : top(topt), t(tt), ad(adt), phi0(phi0t)
    {
     L = sin (2*M_PI*t*5);
     a = L / (2*(top-ad)); b = L*ad/(ad-top); c = phi0 - a*top*top - b*top;
    }
    double top, t, ad, phi0, L, a, b, c;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    values[0] = a*x[1]*x[1] + b*x[1] + c;
  }
};

class Navier::WaveAccDBC : public Expression
{
  public:
    WaveAccDBC(double topt, double tt, double adt, double phi0t) : top(topt), t(tt), ad(adt), phi0(phi0t)
    {
     L = 2*M_PI*5*cos (2*M_PI*t*5);
     a = L / (2*(top-ad)); b = L*ad/(ad-top); c = phi0 - a*top*top - b*top;
    }
    double top, t, ad, phi0, L, a, b, c;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    values[0] = a*x[1]*x[1] + b*x[1] + c;
  }
};

class Navier::ApproxzFunction : public Expression
{
  public:
    ApproxzFunction(Function& gradphit, Function& zetaxt, Function& zeta_lineart, Function& phin1t) :
    	gradphi(gradphit), zetax(zetaxt), zeta_linear(zeta_lineart), phin1(phin1t)
    {
    }
  Function& gradphi, &zetax, &zeta_linear, &phin1;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
  	xvA[0] = x[0]; xvA[1] = eval_z(zeta_linear, x[0]) - BIGEPS;
        try { gradphi.eval(vV2A, xvA); }
	catch (std::runtime_error& str) { std::cout << "GP: Failed gradphi at " << xvA[0] << "," << xvA[1] << std::endl; mk2_exit(34); }
  	values[0] = -vV2A[0]*eval_z(zetax, x[0]) + vV2A[1];
  }
};

class PhiFunction : public Expression
{
  public:
    PhiFunction(double Dtt) : Dt(Dtt) {}
    double Dt;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = -9.81*Dt*0.25*(cos(M_PI*x[0]/4)+1);
    }
};

class PhixFS : public Expression
{
  public:
    PhixFS(Function& cphixt, Function& phizt, Function& zetaxt)
     : cphix(cphixt), phiz(phizt), zetax(zetaxt) {}
    Function &cphix, &phiz, &zetax;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> xb(2);
	xb[0] = x[0]; xb[1] = x[1]; double cphixf, zetaxf; dolfin::Array<double> phizv(1);
      cphixf = eval_z(cphix, x[0]); phiz.eval(phizv, xb); zetaxf = eval_z(zetax, x[0]);
      values[0] = cphixf + phizv[0]*zetaxf;
    }
};

class Navier::BernoulliPressure : public Expression
{
  public:
    BernoulliPressure(double rhowt, double gt, Function& phidtt, Function& gradsqt) :
      rhow(rhowt), g(gt), phidt(phidtt), gradsq(gradsqt) {}
    double rhow, g;
    Function &phidt, &gradsq;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> xb(2); xb[0] = x[0]; xb[1] = x[1];
      dolfin::Array<double> phidtv(1), gradsqv(1);
      try { phidt.eval(phidtv, xb); } catch (std::runtime_error& str) { std::cout << "BP: Failed phidt at " << xb[0] << "," << xb[1] << std::endl; mk2_exit(34); }
      try { gradsq.eval(gradsqv, xb); } catch (std::runtime_error& str) { std::cout << "BP: Failed gradsq at " << xb[0] << "," << xb[1] << std::endl; mk2_exit(34); }
      values[0] = - rhow * ( phidtv[0] + 0.5*gradsqv[0]);//RMV + g*x[1] );
    }
};

/*
void do_PressureFunction (Function& pressure, double rhow, Function& phidtp, Function& gsphip, Function& W, double draft)
{
  pressure.vector() = phidtp.vector();
  for (uint i = 0 ; i < pressure.vector().size() ; i++)
  {
      double phidt = phidtp.vector().getitem(i), gsphi = gsphip.vector().getitem(i);
      pressure.vector().setitem(i, - rhow * (phidt + 0.5 * gsphi));
  }
}
*/
extern dolfin::Array<double> phidtA, gsphiA;
class Navier::EulPressureFunction : public Expression
{
  public:
    EulPressureFunction(Plate* platet, double rhowt, Function& pressuret, Function& Wt, Function& W_lineart, double draftt, double gt) :
      plate(platet), rhow(rhowt), pressure(pressuret), W(Wt), W_linear(W_lineart), draft(draftt), g(gt) {}

    Plate* plate;
    double rhow;
    Function &pressure, &W, &W_linear;
    double draft, g;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      double z = eval_z(W_linear, x[0]) - draft;
      xvA[0] = x[0]; xvA[1] = z - BIGEPS;//RMV
        pressure.eval(phidtA, xvA);
        W.eval(vA, xvA);
        values[0] = phidtA[0] + rhow*g*(-draft);
      }
};

class Navier::LukeBoundary : public Expression
{
  public:
    LukeBoundary(double cnrpresst, double rhowt, Function& phidtpt, Function& gsphipt, Function& W_lineart, double draftt, double gt) :
      cnrpress(cnrpresst), rhow(rhowt), phidtp(phidtpt), gsphip(gsphipt), W_linear(W_lineart), draft(draftt), g(gt) {}

    double cnrpress, rhow;
    Function &phidtp, &gsphip, &W_linear;
    double draft, g;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> phidt(1), gsphi(1); double z = eval_z(W_linear, x[0]) - draft;
      dolfin::Array<double> xv(2); xv[0] = x[0]; xv[1] = z - 1e-2;//RMV
      //if (xv[1] > -6) xv[1] = -6 + 1e-10;
      //phidtp.eval(phidt, xv);
      try { phidtp.eval(phidt, xv); } catch (std::runtime_error& str)
      { std::cout << "WzFunction failed at " << xv[0] << "," << xv[1]<<std::endl; mk2_exit(13);}
      gsphip.eval(gsphi, xv);
      values[0] = - (cnrpress/rhow + phidt[0] + 0.5 * gsphi[0])/g + draft;
    }
};

class Navier::Pdtn1Function : public Expression
{
  public:
    Pdtn1Function ( PlateList& platelistt, Function& gspt, double gt, Function& zt ) : gsp(gspt), g(gt), z(zt), platelist(platelistt) {}
    Function &gsp;
    double g;
    Function &z;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);

      double xt = x[0];
      if (plate) {
	double l = plate->left, r = plate->right;
	double lv = -0.5*eval_z(gsp, l)-g*eval_z(z,l);
	double rv = -0.5*eval_z(gsp, r)-g*eval_z(z,r);
        values[0] = ((xt-r)*lv + (l-xt)*rv) / (l-r); // ENSURE CONTINUITY ON FS/P INTERFACE!
      } else
        values[0] = (nlfluid?-0.5*eval_z(gsp, x[0]):0) - g*eval_z(z, x[0]); // RMV true
    }
};

class NavTopLeftCorner: public SubDomain
{
  //bool inside (const dolfin::Array<double> &x, bool on_boundary) const
 // { return fabs(x[0]) <= 5e-1 && fabs(x[1]) <= EPS; }
    public:
    NavTopLeftCorner(PlateList &platelistt, Function& zetat) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	if ((fabs(x[0] - (*plit)->left) <= EPS || fabs(x[0] - (*plit)->right) <= EPS)) {
		if (fabs(x[1]+(*plit)->draft-eval_z((*plit)->Wn1, x[0])) <= EPS)
		{
		std::cout << "WHAT?" << std::endl;
		   return true;
		} else {
		   return false;
		}
	}
      return false;
    }
};

class RightEnd : public SubDomain
{
  public:
    RightEnd(double lengtht) : length(lengtht) {}
    double length;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return fabs(x[0]-length) <= EPS && fabs(x[1]+3) <= 0.02;
    }
};

class NavAllBdy : public SubDomain
{
  public:
    NavAllBdy(double lengtht, double bottomt) : length(lengtht), bottom(bottomt) {}
    double length, bottom;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      //Plate* plate = Plate::this_plate(platelist, x[0], false, length);
      return fabs(x[0]-length) <= EPS || fabs(x[0]) <= EPS || fabs(x[1]-bottom) <= 1e-5;
    }
};

class NavNearTop : public SubDomain
{
  public:
    NavNearTop(PlateList& platelistt, double lengtht) : platelist(platelistt), length(lengtht) {}
    PlateList& platelist;
    double length;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      if ( fabs(x[0]) == 0 || fabs(x[0]-length) == 0) return true;
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	if ((fabs(x[0] - (*plit)->left) <= EPS || fabs(x[0] - (*plit)->right) <= EPS)) {
		if (x[1]+EPS>=-(*plit)->draft+eval_z((*plit)->Wn1, x[0]))
		{
		   return true;
		} else {
		   return false;
		}
	}
      return false;
    }
};


int Navier::Single_run (char* dirname, bool resume)
{
  bool platetest = false;
  bool leave = false;
  parameters["linear_algebra_backend"] = "uBLAS";

  dolfin::Array<double> bounds(4);
  bounds[0] = 0.0; bounds[1] = 0.0; bounds[2] = length; //bounds[3] = 1.0;

  /* Initialize output files */
  char fnbuffer[500];
  int files1db = 0, files1da = 0, files2db = 0, files2da = 0;
  char test1d[500], test2d[500];
  std::map< std::string, File* > basicfiles;
  std::map< std::string, File* > addfiles;

  sprintf(fnbuffer, "phi/model-%s-phi.pvd", dirname);
  basicfiles["phi"] = new File(fnbuffer); files2db++;
  sprintf(fnbuffer, "disp/model-%s-W.pvd", dirname);
  basicfiles["W"] = new File(fnbuffer); files1db++;
  sprintf(fnbuffer, "disp/model-%s-zeta.pvd", dirname);
  basicfiles["zeta"] = new File(fnbuffer); files1db++;
  sprintf(fnbuffer, "pressure/model-%s-pressure.pvd", dirname);
  basicfiles["pressure"] = new File(fnbuffer); files2db++;
  sprintf(fnbuffer, "mesh/model-%s-mesh.pvd", dirname);
  sprintf(test2d, "mesh/model-%s-mesh000000.vtu", dirname);
  sprintf(test1d, "mesh/model-%s-mesh000001.vtu", dirname);
  File* meshfileptr = new File(fnbuffer);
  basicfiles["mesh"] = meshfileptr;
  sprintf(fnbuffer, "add/misc/model-%s-misc.pvd", dirname);
  basicfiles["misc"] = new File(fnbuffer);
  sprintf(fnbuffer, "phinonz-%s.pvd", dirname);
  basicfiles["phinonz"] = new File(fnbuffer);
  sprintf(fnbuffer, "phinonz-%s.pvd", dirname);
  basicfiles["phinonz"] = new File(fnbuffer);
  sprintf(fnbuffer, "phi/model-%s-p.pvd", dirname);
  basicfiles["p"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "phi/model-%s-u.pvd", dirname);
  basicfiles["u"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "phi/model-%s-v.pvd", dirname);
  basicfiles["v"] = new File(fnbuffer); files2da++;

  addfiles["mesh"] = meshfileptr; files2da++;
  sprintf(fnbuffer, "add/acc/model-%s-Wdd.pvd", dirname);
  addfiles["Wdd"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/vel/model-%s-Wd.pvd", dirname);
  addfiles["Wd"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/gsp/model-%s-gsp.pvd", dirname);
  addfiles["gsp"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/gsp/model-%s-gspfs.pvd", dirname);
  addfiles["gspfs"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/phidt/model-%s-phidt.pvd", dirname);
  addfiles["phidt"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/phinonz/model-%s-phinonz.pvd", dirname);
  addfiles["phinonz"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/btm/model-%s-btm.pvd", dirname);
  addfiles["btm"] = new File(fnbuffer); files1da++;

  /* Make mesh */
  Mesh mesh;
  Mesh line;

  /* Calulate average depth */
  double avdepth = -1, avfalsebtm = -1;
  AltUnitInterval tmpline ( bounds[2], N, mcl[1] ); // Add 'inner' consideration
  Top::FunctionSpace tmpV(tmpline);
  GiNaCFunction tmpvarbottomE ( x, depth.subs(symt==0) );
  GiNaCFunction tmpfalsebtmE ( x, falsebtm.subs(symt==0) );
  Function tmpvarbottom(tmpV); tmpvarbottom = tmpvarbottomE;
  Function tmpfalsebtm(tmpV); tmpfalsebtm = tmpfalsebtmE;
  avdepth = -get_interval_norm(tmpvarbottom)/(bounds[2] - bounds[0]);
  avfalsebtm = -get_interval_norm(tmpfalsebtm)/(bounds[2] - bounds[0]);
  //std::cout << depth << avdepth << std::endl;
  logfile << "(Average depth: " << avdepth << ")"<< std::endl;
  bounds[3] = avdepth;

  /* Generate mesh */
  GeoMaker geomaker;
  char filename[50] = "automesh.geo";
  for ( std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  { PlateDescription* plated = *pdit;
    if (plated->draft<0) 
     {
       //plated->draft = (plated->h)*(plated->rhop/rhow);
       //logfile << "(Just calc'd archimedian draft for plate ["<<plated->left<<","<<plated->right<<"] at " <<plated->draft<< ")"<<std::endl;
     }
  }
  if (automesh)
   mesh = geomaker.simplePlateDescriptionList(bounds[0], bounds[2], avdepth, platedescriptions, mcl, mrat, ic, x, mode3d);
  else
   mesh = geomaker.generateGMSHMeshFromFile(filename, mrat, mode3d);
  //mesh = generate_mesh_by_gmsh(length,avdepth,0.3,3,5,mcl,mclmd,automesh);
  *addfiles["mesh"] << mesh;

  /* Generate free surface mesh function */
//  MeshFunction<uint> tlcdommf(mesh, 1);
//  MeshFunction<uint> pdommf(mesh, 1);

  /* Derive the top boundary submesh */
  BoundaryMesh bdy(mesh);
  *addfiles["mesh"] << bdy; // Used for 2d size test
  PlateList platelist;
  PlaterMap& platermap = Plate::get_platermap();
  k_guess = wavemaker->fre;
  int ptct = 1; char platename[200];
  MeshFunction<uint>* p_to_bdy;

  for ( std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  {
    //(*pdit)->draft = (*pdit)->h*((*pdit)->rhop-rhow)/(*pdit)->rhop;
    (*pdit)->h = (*pdit)->draft*(rhow/(*pdit)->rhop);
    logfile << "Calculated " << (*pdit)->name << " thickness to be : " << (*pdit)->draft << std::endl;
    //std::cout << (*pdit)->draft << std::endl;
    Plater* plater = platermap[(*pdit)->typestr];
    if (!plater) { std::cout << "[MK2] ERROR: UNAVAILABLE PLATE TYPE - " << (*pdit)->typestr << std::endl; mk2_exit(11); }

    sprintf(platename, "%s_plate_%d", runname, ptct);
    const Mesh& plinec = make_pline(bdy, (*pdit)->left, (*pdit)->right, (*pdit)->draft, &p_to_bdy);
    Mesh& pline = *(new Mesh(plinec));
    Plate* plate = new Plate(pline, **pdit, Dt, rhow, g, *plater, platename, !platetest);
    plate->problem->log = plate_output;
    double D = plate->problem->get_D(), Cs = sqrt((D*pow(k_guess,4)+rhow*g)/(rhop*h*0.8+rhow*k_guess*(1/tanh(k_guess*avdepth))));
    logfile << "Estimated value of Cs: " << Cs << std::endl;
    platelist.push_back(plate);
    //std::cout << plate->Wn.function_space()->mesh().geometry().x(0,0) << " "
    //          << plate->Wn.function_space()->mesh().geometry().x(0,1) << std::endl;
	dolfin::Array<double> v(1), xv(1); xv[0] = 18;
	  const double* _x = xv.data();
  const Point point(plate->Wn.function_space()->mesh()->geometry().dim(), _x);
//  int id = plate->Wn.function_space()->mesh().any_intersected_entity(point);
//std::cout << id << " " <<  plate->Wn.function_space()->mesh().geometry().dim() << " " << point << std::endl;
*addfiles["mesh"] << plate->Wn;

        try { plate->Wn.eval(v, xv); }
	catch (std::runtime_error& str)
	  { std::cout << "Failed Wn at " << xv[0] << "," << std::endl; }
        
     //std::cout << eval_z((*plit)->Wn, x[0]) << std::endl;
  }
  //  (*plit)->initdomain.mark(pdommf, 1);
  //RightEnd tlcdom(length);
//  tlcdom.mark(tlcdommf, 1);

  /* Instantiate subdomains */
  Single_ZetaInitialConditions zetaicpre(x, ic, platelist, icmode, false);

  //Single_FreeSurface freesurface(platelist, &zetaic, 0.01); // RMV!
  Single_FreeSurface freesurface(platelist); // RMV!
  //double asdf[2]; asdf[0] = 0; asdf[1] = 0;
//  Single_TopSubDomain topsdom(platelist, &zetaic);
  Single_TopSubDomain topsdom(platelist);
//  Boundary FSEnds;
//  MeshFunction<uint> fsdommf(mesh, 1);
//  freesurface.mark(fsdommf, 1);

  /* Size test */
  struct stat res1d, res2d;
  std::cout << test1d << " " << test2d << std::endl;
  stat(test1d, &res1d); stat(test2d, &res2d);
  /* We ignore the handful of basic mesh files and the forcing file */
  int maxnesspts = res1d.st_size * (files1db + aout*files1da) +
                   res2d.st_size * (files2db + aout*files2da);
  int cfact = ciout? 5 : 1;
  int predsize = cfact*(1+(T-ostarttime)/Dt/outfreq-ostartframe)*maxnesspts/(1000*1000);
  int predsizepts = cfact*maxnesspts/1000;
  logfile << "Max anticipated disk space required: ";
  if (predsize > 500)
    logfile << predsize/(1000) << "." << (predsize%1000)/100 << "GB (";
  else
    logfile << predsize << "MB (";
  if (predsizepts > 500)
    logfile << predsizepts/1000 << "." << (predsizepts%1000)/100 << "MB)\n";
  else
    logfile << predsizepts << "KB)\n";
  logfile
   << " (threshold is " << maxfs << "MB; each 2D file is " << res2d.st_size/1000 << "KB and 1D is" << res1d.st_size/1000 << "KB)" << std::endl;
  if (ciout)
    logfile << 
    "WARNING: THIS ASSUMES AV. 5 CORRECTION ITERATIONS"
    << std::endl;
  if (predsize > maxfs) {
    std::cout << "[MK2] Prediction exceeds disk space threshold - alter parameter file settings" << std::endl;
    //mk2_exit (10);
  }
  /*
  MeshFunction<uint> nsdommf(mesh, 2);
  nearfreesurface.mark(nsdommf, 1);
  SubMesh nsmesh(mesh, nsdommf, 1);
  *addfiles["mesh"] << nsmesh;
  MeshFunction<uint> fsnsdommf(nsmesh, 1);
  freesurface.mark(fsnsdommf, 1);*/

  /* Define our delta functional */
  double Err, Err1, Err2, Err3, tol = pow ( 10, tolexp );
  int lkjh = 0;

  /* Create progress bar */
  Progress progress("Progress");

  /* Generate 1D meshes */
  Single_FreeSurfaceFlat fsflat(platelist);
  SubMesh fsbdy(bdy, freesurface);
  *addfiles["mesh"] << fsbdy;
  MeshFunction<uint> *mf_tmp;
  MeshFunction<uint>& fsbdy_to_bdy_v = *fsbdy.data().mesh_function("parent_vertex_indices");
  line = AltUnitInterval(fsbdy, platelist, &mf_tmp, fsbdy_to_bdy_v, bdy, *p_to_bdy);
  Mesh halfline(refine(line));

  *addfiles["mesh"] << line;
  *addfiles["mesh"] << halfline;
  SubMesh fsline(line, fsflat);
  SubMesh halffsline(halfline, fsflat);

  MeshFunction<uint> mf(fsline, 1);
  MeshFunction<uint>& fsline_to_line_v = *fsline.data().mesh_function("parent_vertex_indices");
  MeshFunction<uint>& bdy_to_mesh = bdy.cell_map();

  line.init(0,1);
  MeshConnectivity& fsline_conn = fsline.topology()(1,0);
  MeshConnectivity& line_conn = line.topology()(0,1);
  MeshConnectivity& mesh_conn = mesh.topology()(1,2);
  MeshConnectivity& mesh_rev_conn = mesh.topology()(2,1);
  MeshFunction<uint> cell_to_facet(fsline, 1);
  int k;
  for ( MeshEntityIterator mit(fsline, 1); !mit.end() ; ++mit) {
        k = (*fsline.data().mesh_function("parent_vertex_indices"))[fsline_conn(mit->index())[0]];
        if (fsline_conn.size(mit->index()) == 1)
                k = line_conn(fsline_to_line_v[fsline_conn(mit->index())[0]])[0];
        else {
                int n = fsline_conn(mit->index())[0], m = fsline_conn(mit->index())[1];
                n = fsline_to_line_v[n];
                m = fsline_to_line_v[m];
                int a = line_conn.size(n), b = line_conn.size(m);
                for ( int i = 0 ; i < a ; i++ )
                   for ( int j = 0 ; j < b ; j++ )
                       if (line_conn(n)[i] == line_conn(m)[j])
                           k = line_conn(n)[i];
        }
        mf[mit->index()] = bdy_to_mesh[(*mf_tmp)[k]];

        int mc = mesh_conn(mf[mit->index()])[0];
        for ( int ii = 0 ; ii < mesh_rev_conn.size(mc) ; ii++ )
                if ( mesh_rev_conn(mc)[ii] == mf[mit->index()] )
                        cell_to_facet[mit->index()] = ii;
        //std::cout << fsline.geometry().x(fsline_conn(mit->index())[0], 0) << " " << mesh.geometry().x(mesh_conn(mf[mit->index()])[0],0) << std::endl;
  }
  //std::cout << mf[0] << std::endl;
  //std::cout << mesh.geometry().x(mesh_conn(mf[0])[0],0) << std::endl;

  *addfiles["mesh"] << fsline;
  line.order();
  Top::FunctionSpace topV(line);
  Top::FunctionSpace toplV(line);
  //SquareFunction asdf; Function asdff(topV); asdff = asdf;
  //std::cout << approx_z(asdff, 40, true) << " " << approx_z(asdff, 40, false) << std::endl;
  //mk2_exit(1);
  Top::FunctionSpace fsV(fsline);
  Function zetaic(fsV);
  eval_flatten(zetaic, fsV, zetaicpre);
  hTop::FunctionSpace fsVh(fsline);
  lTop::FunctionSpace fsVH(halffsline);


  lTop::FunctionSpace fsVl(fsline);
  dTop::FunctionSpace dgfsV(fsline);
  DoubleTop::FunctionSpace dfsV(fsline);
  hDoubleTop::FunctionSpace dfsVh(fsline);
  Predict::FunctionSpace dfsVh2(fsline);
  Domain::FunctionSpace V0(mesh);
  DomainMixed::FunctionSpace Vu(mesh);
  
  
  TestExpression te;
  Function testh(fsVh), testH(fsVH), testl(fsVl);
  testh = te;
  *addfiles["mesh"] << testh;
  testH.interpolate(testh);
  *addfiles["mesh"] << testH;
  testl.interpolate(testh);
  *addfiles["mesh"] << testl;
  testh.interpolate(testl);
  *addfiles["mesh"] << testh;
  testH.interpolate(testh);
  *addfiles["mesh"] << testH;
  //Top::FunctionSpace pV1(plate.pline);
  //hTop::FunctionSpace pV1h(plate.pline);

  /* Treat FS ends */
//  MeshFunction<uint> bfsdommf(fsline, 0);
//  FSEnds.mark(bfsdommf, 1);

  /* Set constants */
  Constant zero(0.0), zero2(0.0,0.0);
  Constant gc(g), Dtc(Dt), rhowc(rhow), muc(musurf), lc(lscale);

  /* Declare our intertemporal variables */
   /* ...on free surface */
  Function cux(fsV); cux = zero;
  Function cvx(fsV); cvx = zero;
//  Function cphi(fsV); cphi = zero;
  Function zetan1(fsVh), zetan(fsVh), zeta0(fsVh),
    zdtn(fsV), zdtnm1(fsV), zdtnm2(fsV), zdtnm3(fsV), zetan1H(fsVH);
  zdtn = zero; zdtnm1 = zero; zdtnm2 = zero; zdtnm3 = zero;
  zetan = zero; zeta0 = zero; zetan1 = zero;
  Function gsphin1fs(fsV); gsphin1fs = zero; Function gsphinfs(fsV); gsphinfs = zero; 
  Function zetan1old(fsVh); zetan1old = zero;
  Function zetan1old_linear(fsVl); zetan1old_linear = zero;
  Function zetan1_linear(fsVl); zetan1_linear = zero;
  Function zetax_linear(fsVl); zetax_linear = zero;
  Function zetan1old2(fsVh); zetan1old2 = zero;
  Function utopn1old(Vu); utopn1old = zero2;
  Mesh mesh0(mesh);
  Hybrid::CoefficientSpace_f VEu1(mesh0);
  Hybrid::CoefficientSpace_f VEu(mesh0);
  Function udtn(VEu), udtnm1(VEu), udtnm2(VEu), udtnm3(VEu);
  udtn = zero2; udtnm1 = zero2; udtnm2 = zero2; udtnm3 = zero2;
  Function udtn1(VEu), zdtn1(fsV), zddtn1(fsV); udtn1 = zero2; zdtn1 = zero; zddtn1 = zero;
  Function utopn(VEu), utopn1(VEu); utopn = zero2; utopn1 = zero2;

  Beach_nuFunction beach_nu(10); Function beach_nuf(fsV); beach_nuf = beach_nu;
//  beach_nuf = zero;//RMV

   /* ...in fluid */

  if (forcingmode == 1)
  {
    double vel = -sqrt(2*g*f_wd_h);
    PointFunction WDdf (vel, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, bounds[2]);
    if (plate) plate->Wdotn = WDdf; // NOTE THAT WE NEGLECT THE MASS OF THE PLATE HERE!
  } else if (forcingmode == 2)
  {
    GiNaCFunction f_ex_f(x, f_ex_ex.subs(symt==0));
    (*platelist.begin())->forcing = f_ex_f;
  } else if (forcingmode == 3)
  {
    double val = GiNaC::ex_to<GiNaC::numeric>(f_ex_ex.subs(symt==0)).to_double();
    PointFunction WDdf (val, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, bounds[2]);
    if (plate) plate->forcing = WDdf;
    //if (fout) {fprintf(forcingfile, "%lf,%lf\n", 0.0, val);fflush(forcingfile);}
  }

  /* Initialize our zeta vector and W_(n+1) */
  zeta0 = zetaic;
  Function fszero(fsV); fszero = zero;
//  Function zeta0tmp(fsV); zeta0tmp.interpolate(zeta0); zeta0=fszero;//RMV
  zetan = zeta0;
  Function zeta0old(fsV); zeta0old = zero;
  // Assume no motion to begin with!!
  //Wn = Wn1;
  Single_ZetaInitialConditions plic(x, ic, platelist, icmode, true);
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  {  (*plit)->Wn = plic;
     (*plit)->nonstep(); }

  /* Instantiate wavemaker */
  /* (amp, fre, lwr, btm, waves [0 for inf], attcoeff) */
  //wavemaker = new WaveMaker(0.1, 0.4, -0.18, -0.2, 2, 0.1);
  /* Set up by readparams */

  /* Set up the bottom */
  Constant constdepth(avdepth);
  Constant constfalsebtm(avfalsebtm);
  Function varbottomold(toplV); varbottomold = constdepth;
  Function varbottom(toplV); varbottom = constdepth;
  Function movebottomold(toplV); movebottomold = constfalsebtm;
  Function movebottom(toplV); movebottom = constfalsebtm;
  std::cout << eval_z(varbottomold, 1e-15) << std::endl;
  std::cout << eval_z(movebottom, 1e-15) << std::endl;
      linearize(zetan1_linear, zeta0);
      linearize(zetan1old_linear, zetan1old);
      Single_mesh_move(mesh, platelist, zetan1_linear, zetan1old_linear, movebottom, movebottomold); // BTM AT RIGHT TS
      *addfiles["mesh"] << mesh;

  /* Initial values of pdtn and zdtn */
  Function phinonzfs(fsV); phinonzfs = zero;
      zetan1_linear.vector()->zero();
      linearize(zetan1_linear, zeta0);
      linearize(zetan1old_linear, zetan1old);
      zetan1old = zeta0; zetan = zeta0;
      linearize(zetan1old_linear, zetan1old);

  /* Output initial values */
  time_t then; time(&then);
  struct tm * timeinfo = localtime(&then);
  logfile << "Start time: " << asctime(timeinfo) << std::endl;

  /* TIME LOOP */
  int titeration = 0;
  int ct = 0;
  int cl_last = 0;
  double alfac = 1;
  Constant zal(0.0);
  zetan1old2 = zetan; utopn1old = utopn;

  struct stat fsinfo; int fsret = 0; fsret = stat("state.st", &fsinfo);

  char openmode[4] = "w";
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
  	(*plit)->init();
  }

  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  	(*plit)->setup_diagnostics(openmode);
  if (fout) forcingfile = fopen("forcing.csv", openmode);

  while ( t <= T+1e-9 )
  {
    sf_n = t;
    /* Set bottom for this timestep */
    varbottom = GiNaCFunction(x, -depth.subs(symt==t)); // NEGATED.
    movebottom = GiNaCFunction(x, -falsebtm.subs(symt==t)); // NEGATED.
//    if (aout&&(outfreq==0||titeration%outfreq==0)) *addfiles["btm"] << varbottom;
  
    /* Execute predictor step */
       //RMV CHK THESE WORK
       utopn1old = utopn1;
       zetan1old2 = zetan1;
    eval_navpredict(zetan1, utopn1, dfsVh2, fsVh, Dtc,
      zetan, zdtn, zdtnm1, zdtnm2, zdtnm3,
      utopn, udtn, udtnm1, udtnm2, udtnm3, lc, muc);
    //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
    // Plate* plate = *plit;
    // if (plate->refpress_on)
    //   eval_predict(plate->Wz, plate->Wz_p, dpVh2, pVh, Dtc,
    //   	plate->Wzn, plate->Wzdtn, plate->Wzdtnm1, plate->Wzdtnm2, plate->Wzdtnm3,
    //    plate->Wz_pn, plate->Wz_pdtn, plate->Wz_pdtnm1, plate->Wz_pdtnm2, plate->Wz_pdt, lc, mu);
    //}
    linearize(zetan1_linear, zetan1);
    //eval_alpha(zetan, fsV, zal, zetan1old);
    //eval_alpha(phitopn, fsV, zal, phitopn1old);

     //dolfin::Array<double> xarr(2); xarr[0] = 0; xarr[1] = eval_z(zetan1, 0);
     //DzFunction slze(-wavemaker->acceleration(t, xarr)/g - approx_z(zetaxn1, 0), length/10);
     //Function slz(fsV); slz.interpolate(slze);
    //double zxl = approx_z(zetaxn1, 0);RMV
    //double wcphixl = -wavemaker->velocity(t, xarr);// + zxl*approx_z(phizfs,0); // zdtn1 not evaluated!
    //double wcphixl = approx_z(cphix, 0) - zxl*approx_z(phizfs,0); // zdtn1 not evaluated!
//    wcphixl /= sqrt(1+zxl*zxl);
    //DzFunction slpe(wcphixl - approx_z(cphix, 0), length);//cphix should be phix
    //Function slp(fsV); slp.interpolate(slpe);

    //eval_predict(zetan1, phitopn1, dfsVh2, fsVh, Dtc,
    //   zetan, zdtn, zdtnm1, zdtnm2, zdtnm3,
    //   phitopn, pdtn, pdtnm1, pdtnm2, pdtn1, lc, muc);
    //eval_alpha(zetan1, fsV, zal, zetan1old2);
    //eval_alpha(phitopn1, fsV, zal, phitopn1old);
    //linearize(zetan1_linear, zetan1);

     //eval_dx(zetaxn1, fsV, zetan1, lc);
    //linearize(zetax_linear, zetaxn1);

    /* Assume pressure from previous step (!) and use to predict Wn1, Wdtn1, Wddtn1 */
//    pressuren1p1 = zero;//RMV
//    plateproblem1.step(pressuren1p1, Wn1, Wdtn1, Wddtn1, Wn, Wdotn, Wddotn, forcing);
//    if (aout&&(outfreq==0||titeration%outfreq==0)) { *addfiles["Wdd"] << Wddtn1; *addfiles["Wd"] << Wdtn1; }
  
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
    	(*plit)->output_diagnostics(t);
        //(*plit)->nonstep();//RMV??????
    }

    /* CORRECTION LOOP */
    int cl = 0; Err = 1; Err1 = 1; Err2 = 1; Err3 = 1; leave = false;
    bool startplate = true; int startresponse = 2;

    do
    {
      bool lognow = ciout||((outfreq==0||titeration%outfreq==0)&&(t>=ostarttime&&titeration/outfreq>=ostartframe));

      /* Move the mesh */
      Single_mesh_move(mesh, platelist, zetan1_linear, zetan1old_linear, movebottom, movebottomold);

      zetan1old = zetan1;
      linearize(zetan1old_linear, zetan1old);
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      {
        (*plit)->clstep();
      }
      varbottomold = varbottom; // slow and repettitve
      movebottomold = movebottom; // slow and repettitve

      /* Declare a new function space for the adjusted mesh */
      Domain::FunctionSpace V(mesh);
      Hybrid::CoefficientSpace_f Vum(mesh);
      HybridPressure::FunctionSpace Vup(mesh);
      //DomainMixed::FunctionSpace Vum(mesh);
  	Function gsphin1(V); gsphin1 = zero;// Function phin1(Vh); phin1 = zero; Function phidtn1(V); phidtn1 = zero;
      //NavierEquations::CoefficientSpace_U eV(mesh);
      Domain2::FunctionSpace V2(mesh);
      Domain::FunctionSpace nV(mesh);//RMV
      hDomain::FunctionSpace Vh(mesh);
      dDomain::FunctionSpace dV(mesh);

      //Function naviern1(eV);
      Function pcurrent(Vup);
      Function ucurrent(Vum);
      (*ucurrent.vector()) = (*utopn1.vector());
      Function uprev(Vum);
      (*uprev.vector()) = (*utopn.vector());

      /* Solve the potential problems */
       /* 1. Declare the relevant utility functions */
//	Function phix(V); eval_phix(phix, V, phin1);//RMV
      //if (tilted_fluid)
        for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
        {
      	  Plate* plate = (*plit);
	  eval_beam_dx(plate->Wx, plate->pV, plate->Wn1, lc);
          eval_beam_dx(plate->Wxx, plate->pV, plate->Wx, lc);
	  plate->eval_bm();
        }
      WdotNeumann Wdote(platelist, zetan1, true); // cphix is out of date!!
      WddotNeumann Wddote(platelist, zetan1, true);
      WavedNeumann wavede(platelist, zetan1);
      WaveddNeumann wavedde(platelist, zetan1);
      //Standard::CoefficientSpace_Wdot cVWdot(mesh);
      //Function veln(cVWdot); veln = Wdote; Function accn(cVWdot); accn = Wddote;
       /* 2. Evaluate boundary conditions */
      MeshFromInterval utopn1f(platelist, utopn1);
      //MeshFromInterval vtopn1f(platelist, vtopn1);
      Constant one(1.00);
      RightEnd resubdom(bounds[2]);
      Single_FreeSurface freesurfacen1(platelist, &zetan1_linear, true);
      Single_Plates platesn1(platelist);
      MeshFunction<uint> p1dommf(mesh, 1);
      //AllBdy everywhere(length, platelist); everywhere.mark(p1dommf, 1); platesn1.mark(p1dommf, 3); freesurfacen1.mark(p1dommf, 2);
      p1dommf.set_all(0);
      NavAllBdy everywhere(bounds[2], bounds[3]); NavNearTop waveside(platelist, bounds[2]); everywhere.mark(p1dommf, 4); waveside.mark(p1dommf, 1);
      //NavTopLeftCorner plateedges(platelist, zetan1); plateedges.mark(p1dommf, 5);
      platesn1.mark(p1dommf, 3); freesurfacen1.mark(p1dommf, 2);
      //DirichletBC bc(V, utopn1f, freesurfacen1);
      //WaveVelDBC wavdirbc(eval_z(zetan1, 0), t, avdepth, vtmp[0]);
      //DirichletBC bcw(V, wavdirbc, waveside);
      //DirichletBC bc(Vh, fszero, resubdom);
      //DirichletBC bc(Vh, utopn1f, fsdommf, 1);
      //DirichletBC bc(Vh, fszero, tlcdommf, 1);
       /* 3. Solve the velocity potential problem */
      //MeshFromInterval zetan1e(platelist, zetan1);
      //MeshFromInterval zdtn1e(platelist, zdtn1);
      //NavierDotFromInterval zddtn1e(platelist, Dt, zdtn1, zdtn);
      //MeshFromInterval vtopn1A(platelist, vtopn1);
      //eval_velpot ( phin1, Vh, bc, veln );//RMV
      //eval_navpot ( naviern1, eV, ucurrent, bc, bcw, Wdote, Wddote, zetan1e, zdtn1e, zddtn1e, wavede, p1dommf, lc );//RMV
      Hybrid::FunctionSpace VH(mesh);
      eval_tent(ucurrent, pcurrent, Dt, VH, uprev, Wdote, Wddote, p1dommf, basicfiles);

       /* 4. Evaluate phi_{n}/n_{z} */
      //MeshFromInterval zetaxn1A(platelist, zetaxn1);
      //cphi = FSLineFromMesh(phin1, zetan1);
      //eval_dx(cphix, fsV, phitopn1, lc);
      //MeshFromInterval cphixA(platelist, cphix); // DOES THIS INTRODUCE DISCONTINUITIES ON THE PLATE/FS INTERFACES?
      //Function phinonz(nV); phinonz = zero;
      //Function phiz(nV); phiz.interpolate(zero);
//      DzFunction phinonze(phin1, zetan1); phinonzfs = phinonze;
//      DirichletBC bcpz(nV, phinonzfs, FSEnds);
/*
      for ( VertexIterator vit(fsline); !vit.end() ; ++vit)
      {
        dolfin::Array<double> B(1), Ax(2); Ax[0] = vit->x(0);
	if (!Plate::this_plate(platelist, Ax[0], false, length)){
	 Ax[1] = approx_z(zetan1, vit->x(0), false);
	 double Axp[2]; Axp[0] = Ax[0]; Axp[1] = Ax[1];
	 Point point(2, Axp);
         if (mesh.any_intersected_entity(point) == -1) { std::cout << "Failed phinonz at " << Ax[0] << "," << Ax[1] << std::endl; }
         //try { phin1.eval(B, Ax); } catch (std::runtime_error& str) { std::cout << "Failed phinonz at " << Ax[0] << "," << Ax[1] << std::endl; }
	}
      }*/
      //eval_phinonz(phinonz, nV, zetaxn1A, phin1, cphixA);
      //eval_phiz(phiz, nV, phin1);
      //phinonzfs = FSLineFromMesh(phinonz, zetan1);
      //Function gradphi(V2);
      //eval_phinonz(gradphi, V2, zetaxn1A, phin1, cphixA);
      //eval_dx(zetaxn1, fsV, zetan1, lc);
      //SquareFunction phinonzfsest(phin1, zetan1_linear, zetan1, phitopn1, platelist, gradphi, zetaxn1, mf, cell_to_facet, mesh_conn, t);
      ////for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      ////{
      ////  Plate *plate = *plit;
      ////  SquareFunction phinonzfse(phin1, plate->Wz
      ////}
      //  
      //phinonzfs.interpolate(phinonzfsest);
      //if (sf_n > -.5)
      // write_function(phinonzfs, "phinonzfs"); // RMV
      //sf_n = -1.;
      //phizfs = FSLineFromMesh(phiz, zetan1);
       /* 5. Find |grad(phi)|^2 - NOTE THAT WE DO THIS REGARDLESS OF nlfluid TO MAKE SURE OUTPUT AVAILABLE!! */
      //Function gsphin1(V);
      //gsphin1 = zero;
      //gsphin1fs = zero;
      //if (nlfluid)
      //{
      //	eval_gradsq(gsphin1, V, phin1, lc); // BC DEIMPLEMENTED
      //	//gsphin1fs = DzFunction(phin1, zetan1_linear, zetan1, phitopn1, platelist, t);//RMV
      //	gsphin1fs = FSLineFromMesh(gsphin1, zetan1_linear);
      //}

      //ApproxzFunction phinonzfs2(gradphi, zetax_linear, zetan1_linear, phin1);
      //phinonzfs = phinonzfs2;

       /* 6. Solve the acceleration potential problem */
      //Function phidtn1(dV);
      //std::cout << "[" << std::endl;
      //PlateMask gsphin1mask(gsfszero, platelist);//RMV
      //PlateMask gsphin1mask(gsphin1, platelist);//RMV
      //Function gsphin1maskf(V);
      //gsphin1maskf = gsphin1mask;
      //std::cout << "]" << std::endl;
//      Pdtn1Function pdtn1f(platelist, gsphin1fs, g, zetan1);
//      //*basicfiles["zeta"] << zetan1;
//
//      DirichletBC bcdt(dV, pdtn1f, freesurfacen1);
//      //Function Wddotef(dV); Wddotef = Wddote;
//      //*addfiles["gsp"] << Wddotef;
//      //SquareFunction constant(1e-9);
//      WaveAccDBC wavadirbc(eval_z(zetan1, 0), t, avdepth, vtmp[0]);
//
//      DirichletBC bcwdt(V, wavadirbc, waveside);
//
//      //*basicfiles["phinonz"] << (*platelist.begin())->Wddtn1;
//	eval_accpot ( phidtn1, dV, pdtn1f, bcdt, bcwdt, Wddote, wavedde, gsphin1, p1dommf, pdtn1f, lc ); // THIS STILL ISN'T QUITE RIGHT AS PLATEMASK HAS ONLY ONE (ZERO) VAL AT PLATE CORNERS
      //*basicfiles["phinonz"] << phidtn1;
      //pdtn1 = FSLineFromMesh(phidtn1, zetan1);

      if (startplate && false)
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      {
        Plate *plate = *plit;
	
        /* Evaluate pressure */
        Navier::EulPressureFunction pf (plate, rhow, pcurrent, plate->Wn1, plate->Wn1_linear, plate->draft, g);

	plate->p = pf;
	plate->problem->log = lognow && plate_output && (ciout);

	plate->step();
      }

      /* Get several of these to make sure this isn't just a pair!
       * Even better, get the error rather than looking for a converged
       * delta */
      Err3 = Err2;
      Err2 = Err1;
      Err1 = Err;
      Err = 0;
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
        //Err += (*plit)->evalerr();
        Err += eval_err((*plit)->Wddtkm1, (*plit)->Wddtn1);
      //Err += eval_err(zetan1, zetan1old);
      leave = /*(cl>1)&&*/Err<=Err1 && (Err<tol&&Err1<tol);//&&Err2<tol&&Err3<tol);

      //RMV!!! Put this back in at top of loop!
      if (startplate)
       for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	(*plit)->adjacc(alpha);

      /* Calculate zdtn1 and pdtn1 */
      //RMV CHK THIS WORKS
      utopn1old = udtn1;
      //Constant zero (0.0); Function zerof(fsV); zerof = zero;
      ////if (nlfluid)
      // FSLineFromMesh ucurrentfs(ucurrent[0], zetan1_linear, true);
      // FSLineFromMesh wcurrentfs(ucurrent[1], zetan1_linear, true);
      // //FSLineFromMesh wfs(naviern1[1], zetan1);
      // eval_navfscond ( *addfiles["phinonz"], udtn1, zdtn1, fsVh, fsV, gc, zetan1, wcurrentfs, ucurrentfs, beach_nuf, utopn1, muc, lc );//RMV
      ////else {
      //// //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
      //// // Plate* plate = *plit;
      //// // if (plate->refpress_on)
      //// //   eval_fscond ( *addfiles["phinonz"], plate->Wz_pdt, plate->Wzdt, pVh, pV, gc, plate->Wz, zerof, plate->Wz_pnonz, beach_nuf, plate->Wz_p, muc, lc );
      //// //}
      //// eval_fscond ( *addfiles["phinonz"], udtn1, zdtn1, fsVh, fsV, gc, zetan1, zerof, phinonzfs, beach_nuf, phitopn1, muc, lc );//RMV
      ////}
      ////SquareFunction zdtn1fsest(phin1, zetan1_linear, zetan1, phitopn1, platelist, t);
      ////zdtn1 = zdtn1fsest;
      //zetan1old2 = zetan1;
      //utopn1old = utopn1;
       FSLineFromMesh ucurrentnewfs(ucurrent[0], zetan1_linear);
       FSLineFromMesh wcurrentnewfs(ucurrent[1], zetan1_linear);
      //Function udtcurrent(Vum);
      //udtcurrent = naviern1[0];
      //Function vdtcurrent(Vum);
      //vdtcurrent = naviern1[1];
      //Function u1dtn1(VEu1), u2dtn1(VEu1);
      //(*u1dtn1.vector()) = (*udtcurrent.vector());
      //(*u2dtn1.vector()) = (*vdtcurrent.vector());

      //EulJoin::BilinearForm aej(VEu, VEu);
      //EulJoin::LinearForm Lej(VEu);
      //Lej.coeff0 = u1dtn1;
      //Lej.coeff1 = u2dtn1;
      //solve(aej == Lej, udtn1);
      //Function u1dtn1a(udtn1, 0), u2dtn1a(udtn1, 1);
      //u1dtn1a.interpolate(u1dtn1);
      //u2dtn1a.interpolate(u2dtn1);
      eval_navcorrect(zetan1, utopn1, dfsVh2, Dtc,
        zetan, zdtn1, zdtn, zdtnm1, zdtnm2,
	utopn, udtn1, udtn, udtnm1, udtnm2);
      zetan1H.interpolate(zetan1);
      chebyshev_smoothing(zetan1H);
      zetan1.interpolate(zetan1H);
       eval_navfscond ( *addfiles["phinonz"], udtn1, zdtn1, fsVh, fsV, gc, zetan1, wcurrentnewfs, ucurrentnewfs, beach_nuf, utopn1, muc, lc );//RMV
      //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
      // Plate* plate = *plit;
      // if (plate->refpress_on)
      //   eval_correct(plate->Wz, plate->Wz_p, dpVh2, Dtc,
      //     plate->Wzn, plate->Wzdt, plate->Wzdtn, plate->Wzdtnm1, plate->Wzdtnm2,
      //     plate->Wz_pn, plate->Wz_p, plate->Wz_pdtn, plate->Wz_pdtnm1, plate->Wz_pdtnm2);
      //}
      linearize(zetan1_linear, zetan1);

      leave = leave && startplate && (startresponse==2);
      leave = cl >= clmax;
      //if (startresponse) startresponse = 2;
      /*if (leave) std::cout << "X ";
      std::cout << Err << ":(" << r++ << ")-" << startresponse << (startplate?".":" "); fflush(stdout);
      if (Err < tol) { if (!startplate) std::cout << "| "; else { if (startresponse==0) {startresponse = 1; std::cout << "; "; }} startplate = true; }*/
      //else if (startresponse==2) { startplate = false; std::cout << "< "; }
      //leave = (cl>2);

      //leave = cl>=50;
      /* Augment correction iteration counter */
      cl++;
      ct++;

      if (leave)
    	/* Calculate Wz */
    	for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
    	{
    	  Plate *plate = *plit;
    	  //*addfiles["mesh"] << plate->Wz;
    	  //*addfiles["mesh"] << plate->p;

	  double fs = eval_z(zetan1    , plate->left);
	  double ps = eval_z(plate->Wn1, plate->left);
	  double zs = eval_z(plate->Wz , plate->left);
	  double prs = eval_z(plate->p , plate->left);

	  if (plate->refpress_on && zs > ps + 1e-3) {
  		write_function(plate->Wz, "W_Wz"); // RMV
  		write_function(plate->Wn1, "W_Wn1"); // RMV
  		write_function(plate->Wn1_linear, "W_Wn1_linear"); // RMV
		//std::cout << "$" << lkjh++ << " " << t << std::endl;
	  	
	  	//plate->refpress_on = false;
		std::cout << "refpress not off" << std::endl;
	  } else if (enable_refpress && (!plate->refpress_on) && fs-ps+plate->draft < refpress_trigger) {
	  	plate->refpress = prs;
		plate->refpress_now = true;
		Function zerof(zdtn1.function_space()); zerof = zero;
  		write_function(zerof, "W_zdtn1"); // RMV
		//*basicfiles["phi"] << zdtn1;
		std::cout << "refpress on" << std::endl;
	  }
	  //std::cout << t << " " << asdf++ << " " << fs << " " << ps << " " << zs << " " << fs-ps+plate->draft << " " << zs-(fs-ps+plate->draft) << " " << prs << std::endl;
    	}

      //leave = (leave||(cl>=5));
	//char name[100];
	//sprintf(name, "test_%d_%d", titeration, cl);
  	//write_function(zetan1, name); // RMV

      /* Do any additional outputting */
      //*basicfiles["phinonz"] << (*platelist.begin())->Wn1_linear;
      if (lognow&&(leave||ciout)) {
        *basicfiles["zeta"] << zetan1;
    	for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	{
		(*plit)->problem->dispout << (*plit)->Wn1;
  		write_function((*plit)->Wn1_linear, "plate/W_Wn1"); // RMV
  		write_function((*plit)->Wxx, "plate/W_Wxx"); // RMV
  		write_function((*plit)->M, "plate/W_M"); // RMV
  		write_function((*plit)->p, "plate/W_p"); // RMV
  		write_function((*plit)->Wddtn1, "plate/W_Wddtn1"); // RMV
	}
	*basicfiles["u"] << ucurrent[0];
	*basicfiles["v"] << ucurrent[1];
	*basicfiles["p"] << pcurrent;
	*addfiles["gsp"] << gsphin1;
        //*basicfiles["phi"] << phin1;
        //*addfiles["phidt"] << phidtn1;
	//*addfiles["gsp"] << gsphin1;
	//*addfiles["gspfs"] << gsphin1fs;
//	Function phix(V); eval_phix(phix, V, phin1);
//        *basicfiles["misc"] << phix;//veln; //zetaxn1A;//RMV

        if (aout) {
          *addfiles["mesh"] << mesh;
	  *addfiles["btm"] << varbottom;
	  //eval_phix(phinonzxfs, dV, phidtn1);
	  //phinonzxfs = Wddote;
	  Function phix(dV);
	  //eval_phix(phix, dV, phidtn1, lc);
          //*addfiles["phinonz"] << phix;

          Top::FunctionSpace fsV1(fsline);
          Top::FunctionSpace fsV2(fsline);
/*	  Function phidtn1x(fsV1);
	  eval_dx(phidtn1x, fsV1, zdtn1);
	  Function phidtn1xx(fsV2);
	  eval_dx(phidtn1xx, fsV2, phidtn1x);*/
          //*addfiles["phidt"] << phidtn1;
	  //Function phiz(V);
	  //eval_phiz(phiz, V, phin1, p1dommf);
	  //Function phizxx(V);
	  //eval_phix(phizxx, V, phizx);
	  //*addfiles["gsp"] << phiz;
	  Function phidtn1xb(fsV);
	  //eval_dx(phidtn1xb, fsV, phitopn1old);
	  Function phidtn1xxb(fsV);
	  //eval_dx(phidtn1xxb, fsV, phidtn1xb);
	  //*addfiles["gspfs"] << (*platelist.begin())->Wddtn1;
	}
      }
    } while ( !leave );
    //std::cout << "===" << t << std::endl;

    /* Return alpha to its original state */
    alpha /= alfac; alfac = 1;

    /* Update the log */
    char outchar = (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) ? '*' : ' ';
    logfile << outchar << "Time: " << t << " (" << titeration << ") #CI:" << cl << " Ddisp: " << Err << " Disp(k-1): " << Err1;
    if (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) logfile << " [Frame " << titeration/outfreq << "]";
    logfile << std::endl;
    if (titeration%10 == 0) {
      logfile << "[T: " << T << " (" << T/Dt << ")] " << (int)(10000*t/T)/100.0 << "%";
      logfile << " PID: " << pid; // We need this to kill it.
      if ( dir_args ) logfile << " Run-name: " << runname;
      if ( default_args ) logfile << " [default]";
      time_t now, taken; time(&now); taken = now - then;
      struct tm * timeinfo = localtime(&now);
      logfile << std::endl << "  Real time: + " << taken/3600 << "h " << (taken/60)%60 << "m " << taken%60 << "s ";
      //proc_t p;
      //look_up_our_self(&p);
      //logfile << std::endl << " Memory: " << p.rss << "KB" << std::endl;
      char sec[3], min[3], mon[3], day[3], year[5]; sprintf(sec, "%2d", timeinfo->tm_sec); sprintf(min, "%2d", timeinfo->tm_min);
      logfile << "(" << timeinfo->tm_hour << ":" << min << ":" << sec << ")";
      if (t > 0) {
       //time_t end; end = then + T*taken/t;
       time_t end; end = now + (taken/ct)*cl_last*(T-t)/Dt;
       struct tm * endinfo = localtime(&end);
       sprintf(sec, "%2d", endinfo->tm_sec); sprintf(min, "%2d", endinfo->tm_min);
       sprintf(day, "%2d", endinfo->tm_mday); sprintf(mon, "%2d", endinfo->tm_mon);
       sprintf(year, "%4d", endinfo->tm_year);
       // Earliest Estimated Finish
       // Current Corrections Finish
       logfile << " CCF: " << endinfo->tm_hour << ":" << min << ":" << sec << " " <<
       	day << "/" << mon << "/" << year;
      }
      logfile << std::endl;
    }

    /* Swap functions for next time-iteration */
    udtnm3 = udtnm2;
    udtnm2 = udtnm1;
    udtnm1 = udtn;
    udtn   = udtn1;
    utopn= utopn1;
    zdtnm3 = zdtnm2;
    zdtnm2 = zdtnm1;
    zdtnm1 = zdtn;
    zdtn   = zdtn1;
    zetan  = zetan1;
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      (*plit)->timestep();
    gsphinfs = gsphin1fs;
    varbottomold = varbottom;
    movebottomold = movebottom;
    // SORT OUT PHIN FOR OVING BOTTOM AND PHIDOTN AS WELL AS NOT NESS BOTTOM!

    /* Augment time variable */
    t += Dt;
    titeration++;
    progress = t/T;
    cl_last = cl;

    if (
    titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) {
        // Save state
         std::string name("fluid");
         /* Swap functions for next time-iteration */
         //save_function_vector(pdtnm3, name, "pdtnm3");
         //save_function_vector(pdtnm2, name, "pdtnm2");
         //save_function_vector(pdtnm1, name, "pdtnm1");
         //save_function_vector(pdtn, name, "pdtn");
         //save_function_vector(phitopn, name, "phitopn");
         //save_function_vector(zdtnm3, name, "zdtnm3");
         //save_function_vector(zdtnm2, name, "zdtnm2");
         //save_function_vector(zdtnm1, name, "zdtnm1");
         //save_function_vector(zdtn, name, "zdtn");
         //save_function_vector(zetan, name, "zetan");

         for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
           (*plit)->save_function_vectors();

         save_function_vector(gsphinfs, name, "gsphinfs");
         save_function_vector(varbottomold, name, "varbottomold");
         save_function_vector(movebottomold, name, "movebottomold");
         // SORT OUT PHIN FOR OVING BOTTOM AND PHIDOTN AS WELL AS NOT NESS BOTTOM!

         FILE* fstate; fstate = fopen("state.st", "w");
         fprintf(fstate, "t:%lf\n", t);
         fprintf(fstate, "titeration:%d\n", titeration);
         fprintf(fstate, "cl_last:%d\n", cl_last);
         progress = t/T;
	 fclose(fstate);
    }
  }
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  	(*plit)->shutdown_diagnostics();

  /* Tidy up */
  if (fout) fclose(forcingfile);
  logfile << "Successfully completed." << std::endl;
  return 0;
}
