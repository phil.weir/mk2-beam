#include "plateproblem.h"
#include "DefaultBM.h"
#include "MBM.h"
#include "Err.h"
#include "logfile.h"
#include "EulerBernoulli.h"
#include "Timoshenko_riless.h"
#include "BernoulliPlate.h"
#include "FixedBeam.h"
#include "RigidBeam.h"
#include "Hollow.h"
#include "LinearizedDrozdov.h"
#include "Forbes.h"
#include "EulerBernoulliCracked.h"
#include "EulerBernoulliCracking.h"
#include "EulerBernoulliVariable.h"
#include "EulerBernoulliThickness.h"
#include "Timoshenko.h"
#include "HeyligerReddy.h"
#include "HeyligerReddyAxialless.h"
#include "ForbesMixed.h"
#include "Gao.h"
#include "Gaoaxial.h"
#include "Drozdov.h"
#include "DrozdovNewton.h"
#include "DrozdovAxialless.h"
#include "BM.h"
#include "vK.h"
#include "vKlin.h"
#include "vKaxial.h"
#include "MaxBM.h"

double rounding_rh = .0;
double rounding(double LEFT, double RIGHT, double draft, double x, double y)
{
      double shape = 0, rh = rounding_rh, rv = rh/4.;
      //shape = t;

      if (x < LEFT+rh) {
        if (x-LEFT < 1e-8) shape = rv;
        else
              	shape = rv*(1-sqrt(1-pow(x-LEFT-rh,2)/(rh*rh)));
      }
      if (RIGHT-x < rh) {
        if (RIGHT-x < 1e-4) shape = rv;
        else
              	shape = rv*(1-sqrt(1-pow(RIGHT-x-rh,2)/(rh*rh)));
      }
      return shape;
}

class PlateDomain : public SubDomain // : public HEP::Single_TopSubDomain
{
  public:
    PlateDomain (double LEFTt, double RIGHTt, double draftt) : LEFT(LEFTt), RIGHT(RIGHTt), draft(draftt) {}
    double LEFT, RIGHT, draft;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      if (x[0] < LEFT - PEPS || x[0] > RIGHT + PEPS )
      	return false;

      //double s = (x[0]-LEFT)/(RIGHT-LEFT), t = 0., a = 0.05;
      //if (s < 0.08) t -= a*draft*(0.08-s)/0.08;
      //else if (s > 0.92) t -= a*draft*(s-0.92)/0.08;
      //else t -= 0;
      
      double shape = 0, rh = rounding_rh, rv = rh/4.;
      //shape = t;

      if (x[1] + draft > rv+1e-5) return false;
      if (x[0] < LEFT+rh) {
        if (x[0]-LEFT < 1e-8) shape = rv;
        else
              	shape = rv*(1-sqrt(1-pow(x[0]-LEFT-rh,2)/(rh*rh)));
      }
      if (RIGHT-x[0] < rh) {
        if (RIGHT-x[0] < 1e-4) shape = rv;
        else
              	shape = rv*(1-sqrt(1-pow(RIGHT-x[0]-rh,2)/(rh*rh)));
      }
      bool ret = fabs(x[1]+draft-shape) <= 4e-3; // Note that flat cond required (actually TopSD isn't!)
      //if (!ret && fabs(x[1]+draft-shape) <= 1e-2) {
      //	std::cout << ":" << x[0] << " " << shape << " " << draft+x[1] << std::endl;
      //  std::cout << ret << std::endl;
      //}
      if (shape > 0 && !ret)  std::cout << x[0] << " " << x[1]+draft << " " << shape << std::endl;
      return ret;
    }
};

PlaterMap Plate::platermap;
void Fadd ( Function& F, Function& G );

class GradualExpression : public Expression
{
  public:
    GradualExpression(double leftt, double split_pointt, double left_velt, double right_velt, Function& Wbt) :
    	left(leftt), split_point(split_pointt), left_vel(left_velt), right_vel(right_velt), Wdtn1(Wbt)
      {}

    double left, split_point, left_vel, right_vel;
    Function &Wdtn1;

    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const
    {
    	if (x[0] <= split_point+PEPS) {
	  val[0] = (x[0] - left)*right_vel/(split_point - left) + (split_point - x[0])*left_vel/(split_point - left);
	} else { Wdtn1.eval(val, x); }
    }
};

class GradualFExpression : public Expression
{
  public:
    GradualFExpression(double leftt, double split_pointt, Function& Wat, Function& Wbt) :
    	left(leftt), split_point(split_pointt), Wa(Wat), Wb(Wbt)
      {}

    double left, split_point;
    Function &Wa, &Wb;

    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const
    {
    	if (x[0] <= split_point+PEPS) {
	  val[0] = (x[0] - left)*eval_z(Wb, x[0])/(split_point - left) + (split_point - x[0])*eval_z(Wa, x[0])/(split_point - left);
	} else { Wb.eval(val, x); }
    }
};

class SplitExpression : public Expression
{
  public:
    SplitExpression(double split_pointt, Function& Wat, Function& Wbt) :
    	split_point(split_pointt), Wa(Wat), Wb(Wbt)
      {}

    double split_point;
    Function &Wa, &Wb;

    void eval ( dolfin::Array<double>& val, const dolfin::Array<double> &x ) const
    {
    	if (x[0] <= split_point+PEPS) { Wa.eval(val, x); }
	else { Wb.eval(val, x); }
    }
};

void Plate::init()
{
  problem->init();
}

void Plate::step()
{
  problem->step(p, Wn1, Wdtn1, Wddtn1, Wn, Wdotn, Wddotn, forcing);
  eval_bm_();
  Wn1_linear.interpolate(Wn1);
}

//void Plate::step(Expression& pressure)
//{ pressuren1p1 = pressure;
//  problem->step(pressuren1p1, Wn1, Wdtn1, Wddtn1, Wn, Wdotn, Wddotn, forcing);
//  eval_bm_();
//  if (refpress_on) {
//    double cty_vel = eval_z(Wdtn1, refpress_x), old_vel = eval_z(Wzdt, left);
//    double cty_acc = eval_z(Wddtn1, refpress_x), old_acc = eval_z(Wzddt, left);
//    GradualExpression ge(left, refpress_x, refpress_zdtn1, cty_vel, Wdtn1);
//    Wzdt = ge;
//    GradualExpression gea(left, refpress_x, 0, cty_acc, Wddtn1); //RMV
//    //Wzddt = gea;
//    Wzddt = Wddtn1;
//
//    //GradualExpression ge2(left, eval_z(Wz, left) + dt*(refpress_zdtn1+old_vel)*0.5, refpress_x, eval_z(Wn1, refpress_x), Wn1);
//    Wz = Wzn;
//    Function jumpconst(Wz.function_space());
//    double dt = 0.0125;
//    Constant jumpc(dt*(refpress_zdtn1+old_vel)*0.5);
//    jumpconst = jumpc;
//    Fadd(Wz, jumpconst);
//
//    GradualFExpression ge2(left, refpress_x, Wz, Wn1);
//    Wn1_linear = ge2;
//  } else Wn1_linear.interpolate(Wn1);
//}

PlateProblem::PlateProblem
    (Mesh& mesht, double lt, double rt, double Dt, double E, double h, double nu, double beta, double rhop, double rhow, double mu, double g, bool sbmgt) :
      mesh(mesht),
      l(lt), r(rt),
      Dtc(Dt), Dc(0), betac(beta), rhopc(rhop), rhowc(rhow), muc(mu), gc(g),
      hc(h), sbmg(sbmgt), tV(mesh),
      n(tV, l, r), boundary(l, r),
      dispout("plate/plate_disp.pvd"), veloout("plate/plate_velo.pvd"), acceout("plate/plate_acce.pvd"), 
      presout("plate/plate_pres.pvd"), forcout("plate/plate_forc.pvd"), log(false)
{
  // Calc archimedean draft if draft negative
  logfile << "Plate from " << l << " to " << r << std::endl;
  std::cout << h << " " << Dt << std::endl;
  logfile << "(" << rhop << " + " << mu*Dt/(2*h) << ") A + " << E*Dt*Dt*h*h/(12*(1-nu*nu)) << " W_{xxxx} + " << Dt*Dt*rhow*g/h << " W" << std::endl;
  logfile << "D = " << E*pow(h,3)/(12*(1-nu*nu)) << std::endl;
  double d = E*pow(h,3)/(12*(1-nu*nu)); Dc = Constant(d);
}

Plate* Plate::this_plate(PlateList& platelist, double x, bool edges, double length)
{
  Plate* platel = find_plate(platelist, false, x, edges, length);
  Plate* plater = find_plate(platelist, true, x, edges, length);
  return (platel==plater && platel) ? platel : NULL;
}

Plate* Plate::find_plate(PlateList& platelist, bool onright, double x, bool edges, double length)
{
  Plate* plate = NULL;
  int sign = onright? 1:-1, es = edges?-1:1;
  double pt = onright? length:0;
  for ( PlateList::iterator plit = platelist.begin(); plit != platelist.end() ; plit++ )
  {
    if (sign*((*plit)->left-x) > es*PEPS && sign*((*plit)->left-pt) < 0 ) { plate = *plit; pt = plate->left; }
    if (sign*((*plit)->right-x) > es*PEPS && sign*((*plit)->right-pt) < 0 ) { plate = *plit; pt = plate->right; }
  }
  return plate;
}

PlaterMap& Plate::get_platermap()
{
  /* Generate plate list */
  platermap["Hollow"] = new HollowBeamer();
  platermap["Fixed"] = new FixedBeamer();
  platermap["Rigid"] = new RigidBeamer();
  platermap["Linearized Drozdov"] = new LinearizedDrozdovBeamer();
  platermap["Forbes"] = new ForbesMixedBeamer();
  platermap["Timoshenko"] = new TimoshenkoBeamer();
  platermap["Timoshenko RI-less"] = new Timoshenko_rilessBeamer();
  platermap["Heyliger-Reddy"] = new HeyligerReddyBeamer();
  platermap["Heyliger-Reddy without axial extension"] = new HeyligerReddyAxiallessBeamer();
  platermap["Euler-Bernoulli"] = new EulerBernoulliBeamer();
  platermap["Euler-Bernoulli Cracked"] = new EulerBernoulliCrackedBeamer();
  platermap["Euler-Bernoulli beam with dynamic cracking"] = new EulerBernoulliCrackingBeamer();
  platermap["Euler-Bernoulli Variable"] = new EulerBernoulliVariableBeamer();
  platermap["Euler-Bernoulli Thickness"] = new EulerBernoulliThicknessBeamer();
  platermap["Forbes Mixed"] = new ForbesMixedBeamer();
  platermap["Forbes non-Mixed"] = new ForbesBeamer();
  platermap["Gao"] = new GaoBeamer();
  platermap["Gao Axial-less"] = new GaoaxialBeamer();
  platermap["Drozdov"] = new DrozdovNewtonBeamer();
  platermap["Drozdov without axial extension"] = new DrozdovAxiallessBeamer();
  platermap["Drozdov Newton"] = new DrozdovNewtonBeamer();
  platermap["Drozdov without axial extension"] = new DrozdovAxiallessBeamer();
  platermap["Drozdov non-Newton"] = new DrozdovBeamer();
  platermap["von Karman"] = new vKBeamer();
  platermap["von Karman HA-less"] = new vKlinBeamer();
  platermap["von Karman Axial-less"] = new vKaxialBeamer();

  return platermap;
}

void Plate::setup_diagnostics(char* openmode)
{
  /* Mean bending moment record */
  char mbmfile[200];
  sprintf(mbmfile, "mbm-%s.csv", name.c_str());
  mbmf = fopen(mbmfile, openmode);
  char rmsfile[200];
  sprintf(rmsfile, "rms-%s.csv", name.c_str());
  rmsf = fopen(rmsfile, openmode);
  char midbmfile[200];
  sprintf(midbmfile, "midbm-%s.csv", name.c_str());
  midbmf = fopen(midbmfile, openmode);
  problem->setup_diagnostics(name, openmode);
}

void Plate::shutdown_diagnostics()
{
  fclose(mbmf);
  fclose(rmsf); 
  fclose(midbmf); 
  problem->shutdown_diagnostics();
}

void Plate::output_diagnostics(double t)
{
  fprintf(mbmf, "%lf,%lf\n", t, eval_mean_bending_moment()); fflush(mbmf);
  fprintf(rmsf, "%lf,%lf\n", t, eval_rms_()); fflush(rmsf);
  double w = 1, I = w*h*h*h/(12*(1-nu*nu));
  //fprintf(midbmf, "%lf,%lf\n", t, -E*I*eval_z(Wxx, 0.5*(right+left))/(1-nu*nu)); fflush(midbmf);
  //fprintf(midbmf, "%lf,%lf,%lf,%lf,%lf,%lf\n", E, I, eval_z(Wxx, 0.5*(right+left)), 0.5*(right+left), nu, h); fflush(midbmf);

  MBM::Functional MBMM(M.function_space()->mesh());
  MBMM.BM = M;
  double r = assemble(MBMM);
  if (r != 0) r = sqrt(r);
  fprintf(midbmf, "%lf,%lf,%lf\n", t, r, M.vector()->norm("linf")); fflush(midbmf);

  problem->output_diagnostics(t, Wn1, Wdtn1, Wddtn1, Wn, Wdotn, Wddotn);
}

PlateMesh::PlateMesh ( SubMesh& mesh ) 
{
  MeshEditor me;
  me.open(*this, 1, 1);
  me.init_vertices(mesh.num_vertices());
  me.init_cells(mesh.num_edges());
  for ( VertexIterator vit(mesh); !vit.end() ; ++vit)
  	me.add_vertex(vit->index(), vit->x(0));
  const MeshConnectivity& mconn = (mesh.topology())(1, 0);
  for ( MeshEntityIterator cit(mesh, 1); !cit.end() ; ++cit)
  	me.add_cell(cit->index(), mconn(cit->index())[0], mconn(cit->index())[1]);
  me.close();
}

const PlateMesh& make_pline( const Mesh& bdy, double left, double right, double draft, MeshFunction<uint>** p_to_bdy )
{
  PlateDomain initdomain(left, right, draft);
  MeshFunction<uint> mf(bdy, 0);
  mf.set_all(0);
  initdomain.mark(mf, 1);
  MeshFunction<uint> fmf(bdy, 1);
  const MeshConnectivity& mconn = (bdy.topology())(1, 0);
  fmf.set_all(0);
  for ( MeshEntityIterator cit(bdy, 1); !cit.end() ; ++cit)
  	if (mf[mconn(cit->index())[0]]==1 && mf[mconn(cit->index())[1]]==1)
	  fmf[cit->index()] = 1;
  SubMesh& pbdy = *(new SubMesh(bdy, fmf, 1));
  pbdy.order();
  
  *p_to_bdy = &(*(pbdy.data().mesh_function("parent_vertex_indices")));
  const PlateMesh& pm = *(new PlateMesh(pbdy));

  return pm;
}

/* IGNORE FREESURFACE RMV */
double eval_err (Function& Wn1old, Function& Wn1)
{
  double Err = 0;
  Err::Functional ErrM(Wn1old.function_space()->mesh());
//  ErrM.Wn = zetan1old; ErrM.Wn1 = zetan1; Err += assemble(ErrM);
  ErrM.Wn = Wn1old; ErrM.Wn1 = Wn1; Err += assemble(ErrM);
  return ( Err > PEPS ) ? sqrt(Err) : Err;
}

void PlateProblem::eval_bm(Function& M, Function& W)
{
  DefaultBM::FunctionSpace pV(*M.function_space()->mesh());
  DefaultBM::BilinearForm a(pV, pV);
  DefaultBM::LinearForm L(pV);
  Function bm(pV);
  L.w = W; L.D = Dc;
  solve(a == L, bm);
  M.interpolate(bm);
}

double Plate::eval_bm_()
{
  BM::BilinearForm a(pV, pV);
  BM::LinearForm L(pV);
  Constant Ec(E), Ic(1.0*h*h*h/12), nuc(nu); // I=b*h^3/12 (Wiki)
  L.W = Wn1; L.E = Ec; L.I = Ic; L.nu = nuc;
  solve(a == L, bm);
}

bool InnerPlateSubDomain::inside (const dolfin::Array<double> &x, bool on_boundary) const
{
  double inner_left = LEFT+(RIGHT-LEFT)/3;
  inner_left = LEFT;
  double inner_right = RIGHT-(RIGHT-LEFT)/3;
  inner_right = RIGHT;
  return x[0] > inner_left-DOLFIN_EPS && x[0] < inner_right+DOLFIN_EPS;
}

double Plate::eval_mean_bending_moment()
{
  MaxBM::BilinearForm a(pV, pV);
  MaxBM::LinearForm L(pV);

  //MBM::Functional MBMM(Wn1.function_space()->mesh());
  Constant Ec(E), Ic(1.0*h*h*h/12), nuc(nu); // I=b*h^3/12 (Wiki)
  L.W = Wn1; L.E = Ec; L.I = Ic; L.nu = nuc;
  Function BM(pV);
  solve(a == L, BM);
  return BM.vector()->norm("linf");
  //return assemble(MBMM);
}
