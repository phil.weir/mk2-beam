#!/usr/bin/python
import sys
import os

outroot_file = open('outroot.sh')
outroot = outroot_file.readline().split("=")[1].strip()

runname = sys.argv[1]

copy_tree = [runname]

infile = outroot + "/output." + runname + "/copied_from"
while (os.path.exists(infile)) :
	copied_from_file = open(infile)
	runname = copied_from_file.readline().strip()
	copy_tree.append(runname)
	infile = outroot + "/output." + runname + "/copied_from"
copy_tree.reverse()
print "\n".join(copy_tree)
