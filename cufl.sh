source outroot.sh
if [ "$2" = "C" ];
then
  for file in `ls *.ufl`
  do
   echo $file
   diff $outroot/output.$1/source/$file $file
  done
  for file in `ls plates/ | grep ufl`
  do
   echo $file
   diff $outroot/output.$1/source/$file plates/$file
  done
  for file in `ls 3D/ | grep ufl`
  do
   echo $file
   diff $outroot/output.$1/source/$file 3D/$file
  done
else
  for file in `ls $outroot/output.$1/source/ | grep ufl`
  do
   echo $file
   diff $outroot/output.$1/source/$file $outroot/output.$2/source/$file
  done
fi
