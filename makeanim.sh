#!/bin/bash
# ./makeanim DIR FIRST LAST STEP
source outroot.sh
for (( i=$2; i<=$3 ; i+=$4))
do
padn=`printf %06d $i`
echo "Frame $i"
for bit in {left,left1,plate1,right}
do
 file="$outroot/output.$1/$bit-$padn.csv"
 ofile="$bit.csv"
 cp $file $ofile
 #cp $file $file.tmp
 #sed '1d' $file.tmp > $ofile
done;
#echo $i | octave anim.m > fftlog
cp $outroot/output.$1/parameters parameters.tmp
python test.py -S $i > fftlog
rsvg test.svg test.png
#mv octout.png $outroot/output.$1/octout-$padn.png
mv test.png $outroot/output.$1/octout-$padn.png
animstr="$animstr $outroot/output.$1/octout-$padn.png"
done;
convert -delay $5 $animstr $outroot/output.$1/anim-$2-$3-$4-O$5.gif
