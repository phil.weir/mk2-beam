#!/bin/bash

# Note that if this matches the UFL and is changed to match the header file,
# no rebuild will occur as the header file is seen to be the same!
EXCHANGES="PLATE_A/4 FLUID_A/2 FLUID_B/2 TOP/2"

#top=$PWD
#
#rm -rf ffc.log
#for dir in . plates
#do
#cd $top/$dir
#for file in *.ufl
#do
# cp $file $top/tmp
# cd $top/tmp
# sed -e "s/PLATE_A/$PLATE_A/" $file > tmp.ufl
# diff $file tmp.ufl > /tmp/changed
# if [ ! $? -eq 0 ]
# then
#   mv tmp.ufl $file
#   ffc -l dolfin $file > $top/ffc.log
#   name=`echo $file | sed -e 's/\(.*\)\..*/\1/' -e 's/\.//' -`
#   newh=$name.h
#   oldh=$top/$dir/$name.h
#   diff $newh $oldh > /tmp/changed
#   if [ ! $? -eq 0 ]
#   then
#     echo "$file changed..."
#     cp $newh $oldh
#   fi;
# fi;
#done;
#cd $top
#done;
#
#touch orders.lastbuild
