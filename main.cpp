// IS DRAFT DEPTH BELOW MEAN-FREE SURFACE??
#include <errno.h>
#include "HEP.h"
#include <sys/stat.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include "NormV.h"
#include "Norm.h"
#include "outroot.h"
#include "Euler.h"
#include "Navier.h"

using namespace dolfin;

WaveMaker* HEP::wavemaker;
double HEP::LEFT1, HEP::RIGHT1, HEP::draft, HEP::fl, HEP::fr, HEP::length;
double HEP::LEFT2, HEP::RIGHT2;
double HEP::mcl[4];
bool HEP::twoplates = true;

double HEP::get_volume_norm ( Function& f )
{
  NormV::Functional NormVM(f.function_space()->mesh());
  NormVM.f = f;
  return assemble(NormVM);
}

double HEP::get_interval_norm ( Function& f )
{
  Norm::Functional NormM(f.function_space()->mesh());
  NormM.f = f;
  return assemble(NormM);
}

// Linear approximation
// EXPECTS A LINEAR FUNCTION SPACE!!
// Very bad, doesn't handle nodes properly.
void HEP::predict (Vector& r, double Dt, Vector& av, Vector& bv, Vector& cv, Vector& dv, Vector& ev)
{
  uint i;
  for ( i = 0 ; i < r.size() ; i++ )
   r.setitem(i, av.getitem(i) + (Dt/24)*(55*bv.getitem(i) - 59*cv.getitem(i) + 37*dv.getitem(i) - 9*ev.getitem(i)));
};

void HEP::correct (Vector& r, double Dt, Vector& av, Vector& bv, Vector& cv, Vector& dv, Vector& ev)
{
  uint i;
  for ( i = 0 ; i < r.size() ; i++ )
   r.setitem(i, av.getitem(i) + (Dt/24)*(9*bv.getitem(i) + 19*cv.getitem(i) - 5*dv.getitem(i) + ev.getitem(i)));
};

//DUMMY
void HEP::mesh_vector_inject (FunctionSpace& V, Mesh& mesha, MeshFunction<uint>& mapa, Mesh& meshb, MeshFunction<uint>& mapb, Vector& v1, Vector& v2)
{
/*
  for ( VertexIterator vit(mesha) ; !vit.end() ; ++vit )
  {
    Vertex ve(meshb, mapa.get(*vit));
    v2.setitem(mapb.get(ve), v1.getitem(vit->index()));
  }
  */
}

void HEP::mesh_vector_inject (FunctionSpace& V, Mesh& mesh1, MeshFunction<uint>& map, Vector& v1, Vector& v2)
{
/*
  for ( VertexIterator vit(mesh1) ; !vit.end() ; ++vit )
    v2.setitem(map.get(*vit), v1.getitem(vit->index()));
*/
}

void HEP::dz ( Mesh& mesh, FunctionSpace& V, Vector& v, Function& f )
{
/*
  double d = 0.01;
  for ( VertexIterator vit(mesh) ; !vit.end() ; ++vit )
  {
   const double* x = vit->x();
   double xdz[2] = {x[0], x[1] - d}, val[1], valdz[1];
   if (x[1]>d) {
     f.eval(val, x);
     f.eval(valdz, xdz);
   } else { // BAD APPROXIMATION but we don't need anything this far
            // from the surface
     xdz[1] = x[1] + d;
     f.eval(val, xdz);
     f.eval(valdz, x);
   }
   v.setitem(vit->index(), (val[0] - valdz[0])/d);
  }
  */
}

int main (int argc, char* argv[])
{
  std::ofstream dolfinlog("dolfin.log");
  dolfin::LogManager::logger.set_output_stream(dolfinlog);
  dolfin::set_log_active(false);

  HEP* problem = NULL;
  bool euler;
  for ( int argl = 1 ; argl < argc ; argl++ )
  {
    if (argv[argl][0] == '-')
    {
      if ( argv[argl][1] == 'E' ) {
        std::cout << "Euler Equations" << std::endl;
        problem = new Euler();
      }
      if ( argv[argl][1] == 'n' ) {
        std::cout << "Navier-Stokes Equations" << std::endl;
        problem = new Navier();
      }
    }
  }
  if (problem == NULL) problem = new HEP();

  return problem->run(argc, argv);
}

HEP::HEP () :
  forcingmode(0), f_wd_h(0), f_wd_x(0), f_wd_m(0), f_wde_t(0), f_ex_ex(0),
  default_args(true), pid_args(false), dir_args(false), plate_output(false), ciout(false), fout(false), aout(false), euler(false),
  ostartframe(0), ostarttime(0.0),
  outfreq(1), platetest(false), forcingfile(NULL), pid(getpid()), x("x"), pi("pi", "\\pi"), symt("t"), mrat(1.0), mode3d(false)
{
  strcpy(runname, "[NONE]");
  //struct rlimit chg, old;
  //struct rlimit *chgp;
  ////chg.rlim_cur = old.rlim_cur;
  //chg.rlim_cur = 1;
  //chg.rlim_max = 1;
  //chgp = &chg;
}

int HEP::run (int argc, char* argv[])
{
  char paramloc[200] = "", dirname[200] = "";
  char logstr[100], mkoutdir[50] = "./procsetup.sh ";
  char outpre[500] = "/maybehome/pweir/Mathematics/mk2/";
  //char outpre[500] = "/media/Phil\\ Weir/Photos-old/Mathematics/";
  char outdir[500] = "output.def", mkout[500];

  sprintf(logstr, "mk2.log"); 
  std::ostringstream logfilebuf;
  bool resume = false;
  parse_args(argc, argv, paramloc, dirname, mkoutdir, outdir, logstr, logfilebuf, resume);

  std::ostringstream bakparam; bakparam << "cp -R source.tgz "
    << outdir << "/source";
  sprintf(mkout, "%s %s/", mkoutdir, OUTROOT);
  if (system(mkout))
  { std::cout << "[MK2] ERROR COPYING SOURCE" << std::endl; }
  //sprintf(mkout, "ln -fs %s/%s %s", outroot, outdir, outdir);
  //std::cout << mkout << std::endl;
  if (system(bakparam.str().c_str()) )
  { std::cout << "[MK2] ERROR LINKING DIR" << std::endl; }
  std::ostringstream copyparam; copyparam << "cp " << outdir << "/params/" << paramloc << " " << outdir << "/parameters";
  if (system(copyparam.str().c_str()) || chdir(outdir) )
  { std::cout << "[MK2] ERROR WITH DIRECTORY OPERATIONS" << std::endl; exit(7); }

  std::cout << "[MK2] Logging to: " << logstr << std::endl;
  std::cout << "[MK2] '-H' gives command line options" << std::endl;

  struct stat fsinfo; int fsret = 0; fsret = stat(logstr, &fsinfo);
  if (resume && fsret==0) {
    char logbak[110]; int r = 0;
    while ( fsret == 0 ) {
      r++;
      sprintf(logbak, "%s.%d", logstr, r);
      fsret = stat(logbak, &fsinfo);
    }
    sprintf(mkout, "mv %s %s", logstr, logbak);
    system(mkout);
  }

  logfile.open(logstr);
  logfile << "Hydroelastic Plate Model [based on Kyoung et al, 2006]" << std::endl;
  logfile << "======================================================" << std::endl;
  logfile << "Euler-Bernoulli plate (with optional viscous term) over\npotential flow in a finite domain using time-" << std::endl;
  logfile << "stepping and a predictor-corrector method." << std::endl << std::endl;

  logfile << "OUTPUT DIR: ./" << outdir << std::endl;
  logfile << "PARAMETER SET: ./" << paramloc << (default_args?" [default]":"") << std::endl;
  logfile << "=============" << std::endl;
  if (twoplates)
    logfile << "(SINGLE FLOE SOLVER)" << std::endl;
  else
    logfile << "(MULTIPLE FLOE SOLVER)" << std::endl;

  logfile << logfilebuf.str() << std::endl;

  set_parameters(paramloc);
  int retval = 0;
  //if (platetest) retval = Platetest_run(dirname);
  retval = Single_run(dirname, resume);
  std::cout << "[MK2] Finished. Exiting code " << retval << std::endl;
  return retval;
}
