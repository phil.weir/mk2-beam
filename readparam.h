#include <ginac/ginac.h>
#include "plateproblem.h"

double get_int_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line=true);
double get_double_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line=true);
double get_int_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line=true);
double get_double_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line=true);
GiNaC::ex& get_ginac_from_line (GiNaC::parser& parser, FILE* paramfile, const char* label, bool rest_of_line=true);
GiNaC::ex& get_ginac_from_line (GiNaC::parser& parser, char* line, const char* label, bool rest_of_line=true);
PlateDescription* load_plate_description ( char* plateloc, GiNaC::parser& parser );
