// THIS IS ALMOST VERBATIM FROM THE FENICS PROJECT!!!!!
//
// Copyright (C) 2005-2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2005-12-02
// Last changed: 2006-08-07

#ifndef __FLUID_MESH_H
#define __FLUID_MESH_H

#include <dolfin.h>

namespace dolfin
{

  class FluidMesh : public Mesh
  {
  public:
    enum Type {right, left, crisscross};
    
    FluidMesh(double width, double height, uint nx, uint ny, double draft, double left, double right, Type type = right);

  };
  
}

dolfin::Mesh& generate_mesh_by_gmsh(double width, double height, double draft, double left, double right, double cl[3], double clmd, bool automesh=true);
#endif
