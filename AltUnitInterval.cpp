// Copyright (C) 2008 Kristian B. Oelgaard.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2007-11-23
// Last changed: 2008-11-13

#include <dolfin.h>
#include "AltUnitInterval.h"
#include "Top.h"

using namespace dolfin;

AltUnitInterval::AltUnitInterval(Mesh& fs, PlateList& platelist, MeshFunction<uint>** mfp, MeshFunction<uint>& fs_to_bdy, Mesh& bdy,
        MeshFunction<uint>& p_to_bdy)
{
  MeshEditor editor;
  editor.open(*this, CellType::interval, 1, 1);
  int ndect = 0;
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
    ndect += (*plit)->pV.mesh()->geometry().size();
  editor.init_vertices(fs.geometry().size()+ndect-2*platelist.size());
  editor.init_cells(fs.geometry().size()+ndect-1-2*platelist.size());
  
  std::map<double,uint> nodepos;
  std::map<double,uint> altnodepos;
  for ( VertexIterator vit(fs) ; !vit.end() ; ++vit )
    nodepos[vit->x(0)] = fs_to_bdy[vit->index()];
  PlateList::iterator plit = platelist.begin();
  for ( ; plit != platelist.end() ; plit++ )
   for ( VertexIterator vit(*(*plit)->pV.mesh()) ; !vit.end() ; ++vit ) {
    if (nodepos.find(vit->x(0)) != nodepos.end())
        altnodepos[vit->x(0)] = nodepos[vit->x(0)];
    nodepos[vit->x(0)] = p_to_bdy[vit->index()];
   }
  // Autosorts

  int r = 0;
  std::map<double,uint>::iterator nit = nodepos.begin(), nito = nit; nit++;
  editor.add_vertex(r, nito->first);
  r++;
  bdy.init(0,1);
  bdy.init(1,0);
  MeshConnectivity& conn = bdy.topology()(0,1);
  MeshConnectivity& rconn = bdy.topology()(1,0);
  for ( ; nit != nodepos.end() ; ++nit, ++nito )
  {
    if (fabs(nit->first - nito->first) <= DOLFIN_EPS) continue;
    editor.add_vertex(r, nit->first);

    editor.add_cell(r-1, r-1, r);
    r++;
  }
  editor.close();

  MeshFunction<uint>& mf = *(new MeshFunction<uint>(*this, 1));
  mf.set_all(0);
  *mfp = &mf;
  r = 1;
  nito = nodepos.begin();
  for ( nit = nodepos.begin(), nit++ ; nit != nodepos.end() ; ++nit, ++nito )
  {
    std::map<double,uint>::reverse_iterator last = nodepos.rbegin();
    if (!Plate::this_plate(platelist, (nit->first+nito->first)/2, false, last->first))
    {
        int n = nito->second, m = nit->second;
        int a = conn.size(n), b = conn.size(m);
        int k = -1;
        for ( int ii = 0 ; ii < 2 ; ii++ ) {
         for ( int i = 0 ; i < a ; i++ )
            for ( int j = 0 ; j < b ; j++ )
            {
                if (conn(n)[i] == conn(m)[j])
                    k = conn(n)[i];
            }
         if ( k != -1 ) break;
         if ( altnodepos.find(nito->first) != altnodepos.end() )
            n = altnodepos[nito->first];
         if ( altnodepos.find(nit->first) != altnodepos.end() )
            m = altnodepos[nit->first];
        }

        //mf[nit->second] = r;
        //std::cout << k << std::endl;
        //std::cout << nito->first << std::endl;
        //std::cout << k << " " << n << " " << fs.geometry().x(n,0) << std::endl;
        //std::cout << fs.geometry().x(rconn(k)[0],0) << " " << fs.geometry().x(rconn(k)[1], 0) << std::endl << std::endl;
        if (k != -1)
                mf[r-1] = k;
    }
    r++;
  }
  
  //for ( r = 0 ; r < this->geometry().size() ; r++ )
  //      std::cout << fs.geometry().x(rconn(mf[r])[0], 0) << std::endl;

  //this->order();
}

//-----------------------------------------------------------------------------
AltUnitInterval::AltUnitInterval(double length, uint nx, double cl) : Mesh()
{
  // Open mesh for editing
  MeshEditor editor;
  editor.open(*this, CellType::interval, 1, 1);

  // Create vertices and cells:
  editor.init_vertices((nx+1));
  editor.init_cells(nx);

  if (nx <= 0) nx = (uint)length/cl;
  cl = length/nx;

  // Create main vertices:
  for (uint ix = 0; ix <= nx; ix++)
  {
    const double x = ix*cl;
    editor.add_vertex(ix, x);
  }

  // Create intervals
  for (uint ix = 0; ix < nx; ix++) {
        const uint v0 = ix;
        const uint v1 = v0 + 1;
        editor.add_cell(ix, v0, v1);
  }

  // Close mesh editor
  editor.close();
  Top::FunctionSpace topVa(*this);
}
//-----------------------------------------------------------------------------
