source outroot.sh
padn=`printf %06d $2`
cp $outroot/output.$1/phi-$padn.csv phi-1.csv
sed -e '1d' -e 's/,/ /g' phi-1.csv > phi.csv
gnuplot phi.gp > fftlog
mv test.png $outroot/output.$1/test$padn.png
