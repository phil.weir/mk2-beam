#include "HEP.h"
#include "vhTop.h"
#include <sys/stat.h>
#include "Predict.h"
#include "Standard.h"
#include "FSCond.h"
#include "Domain.h"
#include "hDomain.h"
#include "hTop.h"
#include "dDomain.h"
#include "dTop.h"
#include "hDoubleTop.h"
#include "BernoulliPlate.h"
#include "plates/FixedBeam.h"
#include "plates/RigidBeam.h"

#define BIGEPS 1e-15

using namespace dolfin;

int HEP::Platetest_run (char* dirname)
{
  /* Initialize output files */
  char fnbuffer[500];
  int files2da = 0;
  char test1d[500], test2d[500];
  std::map< std::string, File* > basicfiles;
  std::map< std::string, File* > addfiles;
  sprintf(fnbuffer, "mesh/model-%s-mesh.pvd", dirname);
  sprintf(test2d, "mesh/model-%s-mesh000000.vtu", dirname);
  sprintf(test1d, "mesh/model-%s-mesh000001.vtu", dirname);
  File* meshfileptr = new File(fnbuffer);
  basicfiles["mesh"] = meshfileptr;
  addfiles["mesh"] = meshfileptr; files2da++;

  if (fout) forcingfile = fopen("forcing.csv", "w");

  /* Make mesh */
  Mesh mesh;
  Mesh line;

  /* Calulate average depth */
  double avdepth = -1, avfalsebtm = -1;
  AltUnitInterval tmpline ( length, N, mcl[1] ); // Add 'inner' consideration
  Top::FunctionSpace tmpV(tmpline);
  GiNaCFunction tmpvarbottomE ( x, depth.subs(symt==0) );
  GiNaCFunction tmpfalsebtmE ( x, falsebtm.subs(symt==0) );
  Function tmpvarbottom(tmpV); tmpvarbottom = tmpvarbottomE;
  Function tmpfalsebtm(tmpV); tmpfalsebtm = tmpfalsebtmE;
  avdepth = get_interval_norm(tmpvarbottom)/length;
  avfalsebtm = get_interval_norm(tmpfalsebtm)/length;
  avdepth = -1;
  avfalsebtm = -1;
  logfile << "(Average depth: " << avdepth << ")"<< std::endl;

  /* Generate mesh */
  mesh = generate_mesh_by_gmsh(length,avdepth,0.3,3,5,mcl,mcl[2],automesh);
  *addfiles["mesh"] << mesh;

  /* Derive the top boundary submesh */
  BoundaryMesh bdy(mesh);
  *addfiles["mesh"] << bdy; // Used for 2d size test

  /* Generate plate list */
  PlateList platelist;
  PlaterMap& platermap = Plate::get_platermap();
  for ( std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  {
    Plate* plate = new Plate(bdy, **pdit, Dt, rhow, g, *platermap[(*pdit)->typestr], "platetest", false);
    plate->problem->log = true;// ONLY WAY TO GET INFO.
    double D = plate->problem->get_D(), Cs = sqrt((D*pow(k_guess,4)+rhow*g)/(rhop*h*0.8+rhow*k_guess*(1/tanh(k_guess*avdepth))));
    logfile << "Estimated value of Cs: " << Cs << std::endl;
    platelist.push_back(plate);
  }

  /* Set constants */
  Constant zero(0.0);
  Constant gc(g), Dtc(Dt), rhowc(rhow);

  if (forcingmode == 1)
  {
    double vel = -sqrt(2*g*f_wd_h);
    PointFunction WDdf (vel, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, length);
    if (plate) plate->Wdotn = WDdf; // NOTE THAT WE NEGLECT THE MASS OF THE PLATE HERE!
  } else if (forcingmode == 3)
  {
    double val = GiNaC::ex_to<GiNaC::numeric>(f_ex_ex.subs(symt==0)).to_double();
    PointFunction WDdf (val, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, length);
    if (plate) plate->forcing = WDdf;
    if (fout) {fprintf(forcingfile, "%lf,%lf\n", 0.0, val);fflush(forcingfile);}
  }

  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  { if (forcingmode == 2)
    {
      GiNaCFunction f_ex_f(x, f_ex_ex.subs(symt==0));
      (*plit)->forcing = f_ex_f;
    }
    (*plit)->Wn = Single_ZetaInitialConditions(x, ic, platelist, icmode, true);
    (*plit)->nonstep(); }

  /* Output initial values */
  time_t then; time(&then);
  struct tm * timeinfo = localtime(&then);
  logfile << "Start time: " << asctime(timeinfo) << std::endl;

  /* TIME LOOP */
  double t = 0.0; int titeration = 0;
  while ( t < T )
  {
    /* Assume pressure from previous step (!) and use to predict Wn1, Wdtn1, Wddtn1 */
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      (*plit)->nonstep();
  
    /* Evaluate any transient forcing */
    if (forcingmode == 3 && t <= f_wde_t)
    {
      double val = GiNaC::ex_to<GiNaC::numeric>(f_ex_ex.subs(symt==t)).to_double();
      PointFunction WDdf (val, f_wd_x);
      Plate* plate = Plate::this_plate(platelist, f_wd_x, true, length);
      if (plate) plate->forcing = WDdf; // NOTE THAT WE NEGLECT THE MASS OF THE PLATE HERE!
      if (fout) {fprintf(forcingfile, "%lf,%lf\n", t, val);fflush(forcingfile);}
    }
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
    {
      Plate *plate = *plit;
      if (forcingmode == 2)
        plate->forcing = GiNaCFunction(x, f_ex_ex.subs(symt==t));
      /* Evaluate pressure */
      Constant pf(0.0);
      /* Run plate solver with newly calculated pressure */
      plate->p = pf;
      plate->step();
      /* Adjust plate acceleration using alpha */
      plate->adjacc(alpha);
    }
    /* Update the log */
    char outchar = (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) ? '*' : ' ';
    logfile << outchar << "Time: " << t << " (" << titeration << ")";
    if (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) logfile << " [Frame " << titeration/outfreq << "]";
    logfile << std::endl;
    if (titeration%10 == 0) {
      logfile << "[T: " << T << " (" << T/Dt << ")] " << (int)100*t/T << "%";
      logfile << " PID: " << pid; // We need this to kill it.
      if ( dir_args ) logfile << " Run-name: " << runname;
      if ( default_args ) logfile << " [default]";
      time_t now, taken; time(&now); taken = now - then;
      struct tm * timeinfo = localtime(&now);
      logfile << " Real time: + " << taken/3600 << "h " << (taken/60)%60 << "m " << taken%60 << "s ";
      char sec[3], min[3]; sprintf(sec, "%2d", timeinfo->tm_sec); sprintf(min, "%2d", timeinfo->tm_min);
      logfile << "(" << timeinfo->tm_hour << ":" << min << ":" << sec << ")";
      logfile << std::endl;
    }

    /* Swap functions for next time-iteration */
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      (*plit)->timestep();

    /* Augment time variable */
    t += Dt;
    titeration++;
  }

  /* Tidy up */
  if (fout) fclose(forcingfile);
  logfile << "Successfully completed." << std::endl;
  return 0;
}
