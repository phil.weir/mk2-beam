#include "HEP.h"
#include "lTop.h"
#include "GeoMaker.h"
#include "vhTop.h"
#include "Top1.h"
#include "Top2.h"
#include <sys/stat.h>
#include "Predict.h"
#include "Standard.h"
#include "FSCond.h"
#include "Domain.h"
#include "Domain2.h"
#include "hDomain.h"
#include "hTop.h"
#include "dDomain.h"
#include "dTop.h"
#include "hDoubleTop.h"
#include "beam_dx.h"

#define BIGEPS 1e-12
#define DZ 1e-5

double inner = 0; // ALSO SET IN FLUIDMESH
double HEP::t = 0;
bool HEP::nlfluid = false;
bool HEP::tilted_fluid = false;

bool enable_refpress = false;
double refpress_trigger = 0.4;
 dolfin::Array<double> vtmp(1), vtmp2(2), xtmp(1), xtmp2(2);
class LinearFunction : public Expression
{
  public:
    LinearFunction(Function& ft) : f(ft) {};
    Function& f;
    void eval(dolfin::Array<double>& v, const dolfin::Array<double>& x) const
    { std::cout << v[0]; v[0] = eval_z(f, x[0]); }
};

void eval_beam_dx(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc)
{
  beam_dx::LinearForm Ldx(fsV); beam_dx::BilinearForm adx(fsV,fsV); Ldx.zeta = zetan1;
  Function zetaxtmp(fsV);
  Ldx.l = lc;
  solve ( adx == Ldx, zetaxtmp );
  zetax = zetaxtmp;
}

void linearize(Function& linear, Function& poly)
{
  LinearFunction l(poly); linear.interpolate(poly);
}

int mk2_exit (int code = 1)
{
  logfile << "Exit code: " << code << std::endl;
  logfile.close();
  exit(code);
}

int output_plane_csv (double length, double depth, int titeration, Function& zeta_linear, Function& zeta, Function& F)
{
  double leftend = length;
  int pc = 200;
  int vc = 20;
  int i; FILE* f;
  char fname[200];
  sprintf(fname, "body-%06d.csv", titeration);
  f = fopen(fname, "w");
  fprintf(f, "x,y\n");
  for ( i = 0 ; i < pc ; i++ ) {
   dolfin::Array<double> v(1), x(2);
   x[0] = leftend*i/(double)pc;
   double h = eval_z(zeta_linear, x[0]);
   for ( int j = 0 ; j <= vc; j++ ) {
    x[1] = 1e-5 + depth + (1-2e-5)*((h-depth)*j)/(double)vc;
    try {F.eval(v,x);} catch (std::runtime_error& str) { std::cout << "Error at " << x[0] << "," << x[1] << std::endl; mk2_exit(31); }
      
    fprintf(f, "%lf ", v[0]);
   }
   fprintf(f, "\n");
  }
  fclose(f);
  return 0;
}

// FOR DISCUSSION OF HOW SINGULARITY AFFECTS CONVERGENCE, SEE FEM BK PP369-370
class HEP::Beach_nuFunction : public Expression
{
  public:
    Beach_nuFunction (double lmdat) : lmda(lmdat) {}
    double lmda;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      double x0 = length-2*lmda, alpha = 0.2; // Bonnefoy et al., 2006b
      if ( x[0] < x0 ) { values[0] = 0.0; return; }
      double Lb = length - x0, u = (x[0] - x0)/Lb;
      values[0] = alpha*u*u*(3-2*u);
      return;
    }
};

class HEP::DzFunction : public Expression
{
  public:
  DzFunction ( Function& ut, Function& zeta_lineart, Function& zetat, Function& cut, PlateList& plt, double tt=0 ) :
  	u(ut), zeta_linear(zeta_lineart), zeta(zetat), cu(cut), pl(plt), t(tt) {}

  Function& u, &zeta_linear, &zeta, &cu; PlateList &pl; double t;
  void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    dolfin::Array<double> p1(2), p2(2); p1[0] = x[0]; p2[0] = x[0]; double dz = DZ, dx = dz*0.2;
    p1[1] = eval_z(zeta_linear, x[0]) - dz*1e-2; p2[1] = eval_z(zeta_linear, x[0]) - dz*1e-2 - dz;

    double o = 0;
    if (x[0] < 0.5*dx || Plate::this_plate(pl, x[0]-0.5*dx, true, length))        o = 0.5*dx;
    if (x[0] > length-0.5*dx || Plate::this_plate(pl, x[0]+0.5*dx, true, length)) o = -0.5*dx;

    double zetax = ( eval_z(zeta, x[0]+0.5*dx+o) - eval_z(zeta, x[0]-0.5*dx+o) ) / dx;
    double cux = ( eval_z(cu, x[0]+0.5*dx+o) - eval_z(cu, x[0]-0.5*dx+o) ) / dx;
    dolfin::Array<double> v1(1), v2(1);
    try { u.eval(v1, p1); } catch (std::runtime_error& str) { 
	mk2_exit(20);
    }

    dolfin::Array<double> n(2); n[0] = -zetax / sqrt(1+zetax*zetax); n[1] = 1/sqrt(1+zetax*zetax);
    p2[0] = p1[0] - dz*n[0]; p2[1] = p1[1] - dz*n[1];
    bool underplate = false;
    for ( PlateList::iterator plit = pl.begin() ; plit != pl.end() && !underplate ; plit++ )
      underplate = underplate || ( p2[0] >= (*plit)->left - EPS && p2[0] <= (*plit)->right + EPS );

    double us = cux/sqrt(1+zetax*zetax);
    double un = 0;
    if (p2[0] < 0 || underplate || p2[0] > length)
    { p2[0] = p1[0]; p2[1] = p1[1] - dz/sqrt(1+zetax*zetax);
      try { u.eval(v2, p2); } catch (std::runtime_error& str) { 
          mk2_exit(20);
      }
      double uz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      underplate = false;
      un = ( (1+zetax*zetax)*uz - cux*zetax ) / sqrt(1+zetax*zetax);
      un = uz; us = 0; //RMV!!
    } else {
      try { u.eval(v2, p2); } catch (std::runtime_error& str) { 
	mk2_exit(20);
      }
      un = (v1[0] - v2[0])/dz;
    }
    values[0] = un*un + us*us;
  }
};

// You're responsible for making sure this only gets evaluated where it's supposed to!
// NOW UPDATED TO GIVE FS DOM (NOTE MOD NEEDED TO USE PLATE AS MUTUAL BDY PTS TREATED AS FS)
class HEP::MeshFromInterval : public Expression
{
  public:
    MeshFromInterval ( PlateList& platelistt, Function& intft, bool freesurfacet = true ) : intf(intft), freesurface(freesurfacet), platelist(platelistt) {}
    Function &intf;
    bool freesurface;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);

      double xt = x[0];
      if (plate) {
	double l = plate->left, r = plate->right;
        values[0] = ((xt-r)*eval_z(intf, l) + (l-xt)*eval_z(intf,r)) / (l-r); // ENSURE CONTINUITY ON FS/P INTERFACE!
      } else
	intf.eval(values, x);
        //values[0] = approx_z(intf, xt); // RMV true
    }
};
double sf_n = 0;
dolfin::Array<double> p1(2), p2(2);
dolfin::Array<double> n(2);
dolfin::Array<double> xvA(2), vA(1), v2A(1), vV2A(2);  
class HEP::SquareFunction : public Expression
{
  public:
  SquareFunction ( Function& ut, Function& zeta_lineart, Function& zetat, Function& cut, PlateList& plt, Function& gradut, Function& zetaxt, MeshFunction<uint>& cell_mapt, MeshFunction<uint>& cell_to_facett, MeshConnectivity& mconnt, double tt=0 ) :
  	u(ut), zeta_linear(zeta_lineart), zeta(zetat), cu(cut), pl(plt), gradu(gradut), zetax(zetaxt), cell_map(cell_mapt), cell_to_facet(cell_to_facett), mconn(mconnt), t(tt) {}
  
  Function& u, &zeta_linear, &zeta, &cu; PlateList &pl; Function& gradu, &zetax; MeshFunction<uint> &cell_map, &cell_to_facet; MeshConnectivity& mconn; double t;
  //void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x) const
  void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x, const ufc::cell &cell) const
  {
   int index = 0;
   index = cell_map[cell.index];
   dolfin::Cell cell2d(*gradu.function_space()->mesh(), mconn(index)[0]);
   dolfin::Point normal = cell2d.normal(cell_to_facet[cell.index]);
   dolfin::Array<double> &gp = vV2A, &xv = xvA, &z = vA;
   xv[0] = x[0];
    try { zeta_linear.eval(z, x); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << x[0]<<std::endl;
        mk2_exit(20);
    }
   xv[1] = z[0]-1e-6;
    try { gradu.eval(gp, xv); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << xv[0] << "," << xv[1] << std::endl;
        mk2_exit(20);
    }
    try { zetax.eval(z, x); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << x[0]<<std::endl;
        mk2_exit(20);
    }

   values[0] = (normal[0]*gp[0]+normal[1]*gp[1])*sqrt(1+z[0]*z[0]);
   double first_value = values[0];
   //values[0] = gp[1];
   //values[0] = normal[0];
   //values[0] = normal[0];
   //return;
   //gradu.function_space()->mesh()->init(1,0);
   //gradu.function_space()->mesh()->init(2,1);
   //const MeshConnectivity& mc = gradu.function_space()->mesh()->topology()(1,0);
   //const MeshConnectivity& mc2 = gradu.function_space()->mesh()->topology()(2,1);
   ////Facet c(*gradu.function_space()->mesh(), mc2(cell2d.index())[cell_to_facet[cell.index]]);
   //Cell c(*zeta.function_space()->mesh(), cell.index);
   //values[0] = c.midpoint().x();
   //std::cout << x[0] << "," << values[0] << "," << c.str(true) << std::endl;
   ////values[0] = gradu.function_space()->mesh()->geometry().point(mc(mc2(cell2d.index())[cell_to_facet[cell.index]])[0]).y();
   ////std::cout << 
   ////      gradu.function_space()->mesh()->coordinates()[(mc(cell2d.index())[0])]<< std::endl;
   //////values[0] = gradu.function_space()->mesh()->coordinates()[mc(cell2d.index())[0]][1];
   //return;

                                        p1[0] = x[0]; p2[0] = x[0]; double dz = DZ, dx = dz;
    p1[1] = eval_z(zeta_linear, x[0]) - dz*1e-2; p2[1] = eval_z(zeta_linear, x[0]) - dz*1e-2 - dz;

    double o = 0;
    if (x[0] < 0.5*dx || Plate::this_plate(pl, x[0]-0.5*dx, true, length))        o = 0.5*dx;
    if (x[0] > length-0.5*dx || Plate::this_plate(pl, x[0]+0.5*dx, true, length)) o = -0.5*dx;

    double zetax = ( eval_z(zeta, x[0]+0.5*dx+o) - eval_z(zeta, x[0]-0.5*dx+o) ) / dx;
    double cux = ( eval_z(cu, x[0]+0.5*dx+o) - eval_z(cu, x[0]-0.5*dx+o) ) / dx;
    dolfin::Array<double> &v1 = vA, &v2 = v2A;
    try { u.eval(v1, p1); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at " << p1[0] << "," << p1[1] << std::endl;
	mk2_exit(20);
    }

                                n[0] = -zetax / sqrt(1+zetax*zetax); n[1] = 1/sqrt(1+zetax*zetax);
                                //n[0] = normal[0]; n[1] = normal[1];
    p2[0] = p1[0] - dz*n[0]; p2[1] = p1[1] - dz*n[1];
    bool underplate = false;
    for ( PlateList::iterator plit = pl.begin() ; plit != pl.end() && !underplate ; plit++ )
      underplate = underplate || ( p2[0] >= (*plit)->left - EPS && p2[0] <= (*plit)->right + EPS );

    double uz = 0;
    if (p2[0] < 0 || underplate || p2[0] > length)
    { p2[0] = p1[0]; p2[1] = p1[1] - dz/sqrt(1+zetax*zetax);
      try { u.eval(v2, p2); } catch (std::runtime_error& str) { 
          std::cout << "SquareFunction failed at edge " << p2[0] << "," << p2[1] << std::endl;
          mk2_exit(20);
      }
      underplate = false;
      uz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      values[0] = (1+zetax*zetax)*uz - cux*zetax;

    } else {
      try { u.eval(v2, p2); } catch (std::runtime_error& str) { 
        std::cout << "SquareFunction failed at interior " << p2[0] << "," << p2[1] << std::endl;
	mk2_exit(20);
      }
      uz = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      values[0] = sqrt(1+zetax*zetax)*(v1[0] - v2[0])/dz;
      //values[0] = v2[0];
    }
   // double old_val = values[0];
   //values[0] = (normal[0]*gp[0]+normal[1]*uz)*sqrt(1+z[0]*z[0]);
    //if (sf_n > -.5) 
    //    std::cout << sf_n << ", " << x[0] << ", " << values[0] << ", " << first_value << ", " << 
    //      normal[0] << ", " << normal[1] << ", " << gp[0] << ", " << gp[1] << ", " << z[0] << ", " <<
    //      n[0] << ", " << n[1] << ", " << uz << ", " << cux << ", " << zetax << ", " << std::endl;
  }
};
class HEP::PlateMask : public Expression
{
  public:
   PlateMask ( Function& ft, PlateList& platelistt ) : f(ft), platelist(platelistt) {}
   Function &f; PlateList& platelist;
   void eval (dolfin::Array<double> &values, const dolfin::Array<double> &x) const
   {
     if (!nlfluid) { values[0] = 0; return; }
     //std::cout << x[0] << " " << x[1] << std::endl;
     f.eval(values, x);
     return;
     //values[0] = 0; return; //RMV
     // RMV INCLUDE CORNERS OR NOT (SEE (FAR) BELOW)?
     Plate* plate = Plate::this_plate(platelist, x[0], false, length);
     if (plate && fabs(x[1] - eval_z(plate->Wn1_linear,x[0]) + plate->draft) <=1e-2/*+mcl[0]*3*/ )
     {
       dolfin::Array<double> xb(2); xb[0] = x[0]; xb[1] = x[1] - EPS;//RMV
       dolfin::Array<double> test(1);
       if (plate && fabs(x[1] - eval_z(plate->Wn1_linear,x[0]) + plate->draft) <= 1e-8 ) {
        //std::cout << x[0] << ": " << x[1]+0.5 << " " << approx_z(plate->Wn1,x[0],true) << " " << approx_z(plate->Wn1,x[0],false)<< std::endl;
        xb[1] = eval_z(plate->Wn1_linear,x[0]) - plate->draft - EPS - 1e-11;//RMV
//	fflush(stdout);
       }
      try { f.eval(test, xb); } catch (std::runtime_error& str) { 
        std::cout << "PlateMask failed at " << x[0] << "," << x[1]+0.5 << " (,"<<xb[1]+0.5<<")"<<std::endl;
	mk2_exit(20);
      }
//       f.eval(test, xb);
       values[0] = test[0];
     }
     else values[0] = 0.0;
   }
};

    dolfin::Array<double> fsx(2);
class HEP::FSLineFromMesh : public Expression
{
  public:
   FSLineFromMesh (Function& ft, Function& zeta_lineart, bool incdraftt = false) : f(ft), zeta_linear(zeta_lineart), incdraft(incdraftt) {}
  Function &f, &zeta_linear;
  bool incdraft;
  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    fsx[0] = x[0]; fsx[1] = eval_z(zeta_linear, x[0])-BIGEPS; // NOTE OUR OWN EPS!!!!!!!!@!!!! RMV
    try { f.eval(values, fsx); } catch (std::runtime_error& str) {
     std::cout << "Failed FSLineFromMesh at " << fsx[0] << "," << fsx[1] << std::endl;
     std::cout << "zeta is " << eval_z(zeta_linear, x[0]) << " " << "x is " << fsx[0] << " mesh is " <<
      f.function_space()->mesh()->geometry().x(0,0) << ","<<
      f.function_space()->mesh()->geometry().x(0,1) << std::endl;
     mk2_exit(18);
    }
  }
};

void breakme() {std::cout << "STOPHERE" <<std::endl;}

double dist(double* x1, double* x2)
{
  return sqrt(pow(x1[0]-x2[0],2) + pow(x1[1]-x2[1],2));
}
dolfin::Array<double> x(2);
double evaluate_lagrange(double* x, double* zs, double* ps[], int n)
{
  int i, j;
  double total = 0.;
  for (i = 0; i < n; i++ )
  {
    double term = 1.0;
    for (j = 0 ; j < n ; j++)
    {
      if (i == j) continue;
      term *= dist(x, ps[j])/dist(ps[i], ps[j]);
    }
    total += zs[i]*term;
  }
  return total;
}

void HEP::Single_mesh_move (Mesh& mesh, PlateList& platelist, Function& zeta_linear, Function& zetaold_linear, Function& varbottom, Function& varbottomold)
{
  for ( VertexIterator vit(mesh) ; !vit.end() ; ++vit )
  {
    int ind; ind = vit->index();
    double x = vit->x(0), zold = vit->x(1), z = mesh.geometry().x(vit.pos(),1);

    double uold = eval_z(varbottomold, x), u = eval_z(varbottom, x);
    Plate* platel = Plate::find_plate(platelist, false, x, false, length), *plater = Plate::find_plate(platelist, true, x, false, length);
    bool underplate = (platel == plater) && platel!=NULL;

    double val = 0, valold = 0;
    if ( fabs(zold-uold) <= EPS ) {
      z = u;
    }
    else if ( underplate ) {
    	Plate* plate = platel;
	double fl[2], fr[2], up[2], down[2];
	up[0] = x; up[1] = eval_z(plate->Wn1old_linear, x)-plate->draft;
	down[0] = x; down[1] = uold;
	fl[0] = plate->left; fl[1] = eval_z(zetaold_linear, plate->left);
	fr[0] = plate->right; fr[1] = eval_z(zetaold_linear, plate->right);
	double zs[4] = { eval_z(zeta_linear, fl[0])-fl[1], eval_z(zeta_linear, fr[0])-fr[1],
			 eval_z(plate->Wn1_linear, x)-plate->draft-up[1], u-down[1] };

    	if (fabs(zold-up[1]) <= EPS)
		z += zs[2];
	else {
	    	double* ps[4] = { fl, fr, up, down };
		double xv[2]; xv[0] = x; xv[1] = zold;
		z = zold + evaluate_lagrange(xv, zs, ps, 4);
	}
    }
    else if (plater && fabs (x - plater->left) <= EPS && fabs(zold - eval_z(plater->Wn1old_linear, plater->left) + plater->draft) <= BIGEPS)
      z = eval_z(plater->Wn1_linear, x) - plater->draft;
    else if (platel && fabs (x - platel->right) <= EPS && fabs(zold - eval_z(platel->Wn1old_linear, platel->right) + platel->draft) <= BIGEPS)
      z = eval_z(platel->Wn1_linear, x) - platel->draft;
    else {
	double up[2], down[2];

	double zs[4];
	int points = 2;
	up[0] = x; up[1] = eval_z(zetaold_linear, x);
	down[0] = x; down[1] = uold;
    	double* ps[4]; ps[0] = up; ps[1] = down;
	zs[0] = eval_z(zeta_linear, x)-up[1]; zs[1] = u-uold;

	if (fabs(zold-up[1]) <= EPS)
		z += zs[0];
	else {
	      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	      {
	      	Plate* plate=*plit;
		ps[points] = new double[2];
		double *pl = ps[points];
		ps[points][0] = plate->right;
		ps[points][1] = eval_z(plate->Wn1old_linear, pl[0])-plate->draft;
		zs[points] = eval_z(plate->Wn1_linear, pl[0])-plate->draft-ps[points][1];
		points++;

		ps[points] = new double[2];
		double *pr = ps[points];
		ps[points][0] = plate->left;
		ps[points][1] = eval_z(plate->Wn1old_linear, pr[0])-plate->draft;
		zs[points] = eval_z(plate->Wn1_linear, pr[0])-plate->draft-ps[points][1];
		points++;
	      }
	      double xv[2]; xv[0] = x; xv[1] = zold;
	      z = zold + evaluate_lagrange(xv, zs, ps, points);
	      for ( int i = 2; i < points ; i++ )
	        free(ps[i]);
	}
    }
    if (isnan(z)) {
      std::cout << "[MK2] Computed nan for vertical component vertex " << ind << " at x = " << x << " and zold = " << zold << std::endl;
      mk2_exit(7);
    }
	mesh.geometry().set(vit.pos(), 1, z);
    //if (fabs(x - 201.534) < 4 && fabs(zold+6) < 2) std::cout << x << "," << zold+6 << "->" << z+6 << std::endl;
  }
  mesh.intersection_operator().clear();
  //mesh.intersection_operator().reset_kernel();
}

class HEP::WavedNeumann : public Expression
{
  public:
    WavedNeumann (PlateList& platelistt, Function& zetat) : Expression(2), platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS ) {
	values[0] = -wavemaker->velocity(t, x);
      }
      return;
    }
};

class HEP::WaveddNeumann : public Expression
{
  public:
    WaveddNeumann (PlateList& platelistt, Function& zetat) : Expression(2), platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS ) {
	values[0] = -wavemaker->acceleration(t, x);
      }
      return;
    }
};

class HEP::WdotNeumann : public Expression
{
  public:
    WdotNeumann (PlateList& platelistt, Function& zetat, bool updatedt) : Expression(2), platelist(platelistt), zeta(zetat), updated(updatedt) {}
    PlateList& platelist; Function& zeta; bool updated;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;

        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if ( fabs(x[1] - z) <= 0.1 ) {
		if (updated && plate->refpress_on) {
			values[1] = eval_z(plate->Wzdt, x[0]);
		} else
			values[1] = eval_z(updated?plate->Wdtn1:plate->Wdotn, x[0]);
	}
	//if ( tilted_fluid ) {
	//	values[1] = values[1]/(1+pow(eval_z(plate->Wx, x[0]), 2));
	//}
//	values[1]=0;//RMV
      }
    }
};

class HEP::WddotNeumann : public Expression
{
  public:
    WddotNeumann (PlateList& platelistt, Function& zetat, bool updatedt) : Expression(2), platelist(platelistt), zeta(zetat), updated(updatedt) {}
    PlateList& platelist; Function& zeta;
    bool updated;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;

        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if ( fabs(x[1] - z) < 0.1 ) {
		if (updated && plate->refpress_on) {
			values[1] = eval_z(plate->Wzddt, x[0]);
		} else
			values[1] = eval_z(updated?plate->Wddtn1:plate->Wddotn, x[0]);
	}
	//if ( tilted_fluid ) {
	//	values[1] = values[1]/(1+pow(eval_z(plate->Wx, x[0]), 2));
	//}
	//values[0] = 1e-10*x[0]*x[0];
	//if ( fabs(x[0] - 150) <= EPS && fabs(x[1] - z) <= .1) std::cout << x[1]-z << approx_z(plate->Wddtn1, x[0]) << std::endl;
//	values[1]=0;//RMV
      }
    }
};

class HEP::VelNeumann : public Expression
{
  public:
    VelNeumann (PlateList& platelistt, Function& zetat, Function& zdtt, Function& cuxt, Function& zxt, double uxlt) :
     Expression(2), platelist(platelistt), zeta(zetat), zdt(zdtt), cux(cuxt), zx(zxt), uxl(uxlt) {}
    PlateList& platelist; Function& zeta; Function& zdt, &cux, &zx;
    double uxl;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = 0;
      values[1] = 0;
//     if (fabs(x[1]+3) < EPS) {values[1] = cos(2*M_PI*x[0]/3/3);return;}
 //     if (fabs(x[1]) < EPS) values[1] = -cos(M_PI*x[0]/3);
//      values[1] *= t*1e2;
//      return;//RMV
//      values[0] = 1;//sin(M_PI*x[1]/3);
  //    return;
      /* Wall paddle */
      if ( fabs(x[0]) <= EPS )
      {
	values[0] = -wavemaker->velocity(t, x);
	return;
      }
   }
	/*

      {
	//double z = approx_z(zeta, x[0], true);
        *if ( fabs(x[1] - z) <= 1.0 )
	{
          // double zxi = approx_z(zx, x[0]), zdti = approx_z(zdt, x[0]);
	   //double ux = approx_z(cux, x[0]) - zxi*zdti;
	   values[0] = (-(x[1]-z)*wavemaker->velocity(t, x) + (x[1] + 1.0 - z)*uxl)/1.0; //0;//-ux*0.5;
	   return;
           //values[0] = -zxi*ux;
      	   //values[1] = zdti;
   	   //values[0] /= sqrt(1+zxi*zxi); values[1] /= sqrt(1+zxi*zxi);
	} else* values[0] = wavemaker->velocity(t, x);
    *  } else* //if ( fabs(x[0]) <= EPS && x[1] < approx_z(zeta, 0.0) - EPS) {//RMV
//    *  } else* if ( fabs(x[1]+3) <= EPS ){//&& x[1] < approx_z(zeta, 0.0) - EPS) {//RMV
 //   values[1] = sin(t*M_PI*2*1e2) * sin (M_PI*x[0]/30);
	return;
      }*/
/*      } else if ( (fabs(x[0]) <= EPS *|| fabs(x[0] - 10) <= EPS*) && x[1]  approx_z(zeta, 0.0) - EPS) {
        return;
      }
      Plate* platel = Plate::find_plate(platelist, false, x[0], true, length);
      Plate* plater = Plate::find_plate(platelist, true, x[0], true, length);
      bool underplate = (platel==plater)&&platel;
      if ( underplate )
      {
	Plate* plate = platel;
        double z = eval_z(plate->Wn1_linear, x[0])-plate->draft;
	if (fabs(x[1]-z) <= 0.1)// && x[0] > plate->left)
         values[1] = approx_z(plate->Wdtn1, x[0]);
	//else values[1] = 0;
      }
      return;
	if (x[1] < -2.0) {* std::cout << x[0] << " " << x[1] << " " << z << std::endl; *return; }
        double zxi = approx_z(zx, x[0]), zdti = approx_z(zdt, x[0]);
	//double ux = approx_z(cux, x[0]) - zxi*zdti;
	//if (fabs(x[0]) <=EPS) ux = wavemaker->velocity(t,x);
      //RMV/  values[0] = -zxi*ux;
	values[1] = zdti;
	values[0] /= sqrt(1+zxi*zxi); values[1] /= sqrt(1+zxi*zxi);
	
    //values[0] = 0;

    //values[1] = sin(t*M_PI*2*1e2) * sin (M_PI*x[0]/30);//RMV
	//values[0] = 0;
//	values[1] = 1000;
	//values[0] = 0; values[1] = zdti;
	*if (fabs(x[1]-z) <= 1) { values[1] = approx_z(zdt, x[0]);}
        else
	  values[1] = 0; // RMV change to be continuous at ends*/
      //}
};

class HEP::Single_FreeSurfaceFlat : public SubDomain
{
  public:
    Single_FreeSurfaceFlat(PlateList& platelistt) : platelist(platelistt) {}
    PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return !Plate::this_plate(platelist, x[0], false, length);
    }
};

class HEP::Single_TopSubDomain : public SubDomain
{
  public:
    Single_TopSubDomain(PlateList& platelistt, Expression* zetat=NULL) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist;
    Expression* zeta;
    // dolfin man
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      // Allow this to run through as constant
      // before we get a function space
      Plate* plate = Plate::this_plate(platelist, x[0], true, length);
      if (!plate && zeta)
      { dolfin::Array<double> val(1), xv(1); xv[0] = x[0];
        zeta->eval(val, xv); return fabs(x[1]-val[0]) <= EPS; }
      if (!plate) return fabs(x[1]) <= EPS;
      if ((fabs(x[0] - plate->left) <= EPS || fabs(x[0] - plate->right) <= EPS)
       && (fabs(x[1]+plate->draft) <= EPS || fabs(x[1])<= EPS)) return true;
      return fabs(x[1] + plate->draft) <= EPS;
    }
};

class HEP::Single_FreeSurfaceIns : public SubDomain // : public HEP::Single_TopSubDomain
{
  public:
    Single_FreeSurfaceIns(PlateList& platelistt, Function* zetat = NULL) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist;
    Function* zeta;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);
      if (plate) return false;
      double z = zeta? eval_z(*zeta, x[0]) : 0;
      return fabs(x[1] - z) <= EPS && x[0] > EPS;
    }
};

//class HEP::Single_FreeSurface : public SubDomain // : public HEP::Single_c);
class HEP::Single_Plates : public SubDomain // : public HEP::Single_TopSubDomain
{
  public:
    Single_Plates(PlateList& platelistt) : platelist(platelistt) {}
    PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], true, length);
      return plate && fabs(x[1]-eval_z(plate->Wn1_linear,x[0])+plate->draft) <= 1e-3; //RMV
    }
};

class HEP::Single_FreeSurface : public SubDomain // : public HEP::Single_TopSubDomain
{
  public:
    Single_FreeSurface(PlateList& platelistt, Function* zeta_lineart = NULL, bool endst = true) : platelist(platelistt), zeta_linear(zeta_lineart), zetae(NULL), Dx(0), ends(endst) {}
    Single_FreeSurface(PlateList& platelistt, Expression* zetat, double Dxt) : platelist(platelistt), zeta_linear(NULL), zetae(zetat), Dx(Dxt) {}
    PlateList& platelist;
    Function* zeta_linear;
    Expression* zetae;
    double Dx;
    bool ends;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], !ends, length);
      double z = 0;
      if (plate) return false;
      if (zeta_linear) z = eval_z(*zeta_linear, x[0]);
      else if (zetae) {
       double a, b, s, t;
       xvA[0] = Dx*(int)(x[0]/Dx); s = xvA[0];
       zetae->eval(vA, xvA);
       a = vA[0];
       xvA[0] += Dx; t = xvA[0];
       zetae->eval(vA, xvA);
       b = vA[0];
       z = a+(b-a)*(x[0]-s)/Dx;
      }
      return fabs(x[1] - z) <= BIGEPS;
    }
};

class HEP::WaveVelDBC : public Expression
{
  public:
    WaveVelDBC(double topt, double tt, double adt, double u0t) : top(topt), t(tt), ad(adt), u0(u0t)
    {
     L = sin (2*M_PI*t*5);
     a = L / (2*(top-ad)); b = L*ad/(ad-top); c = u0 - a*top*top - b*top;
    }
    double top, t, ad, u0, L, a, b, c;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    values[0] = a*x[1]*x[1] + b*x[1] + c;
  }
};

class HEP::WaveAccDBC : public Expression
{
  public:
    WaveAccDBC(double topt, double tt, double adt, double u0t) : top(topt), t(tt), ad(adt), u0(u0t)
    {
     L = 2*M_PI*5*cos (2*M_PI*t*5);
     a = L / (2*(top-ad)); b = L*ad/(ad-top); c = u0 - a*top*top - b*top;
    }
    double top, t, ad, u0, L, a, b, c;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
    values[0] = a*x[1]*x[1] + b*x[1] + c;
  }
};

class HEP::ApproxzFunction : public Expression
{
  public:
    ApproxzFunction(Function& gradut, Function& zetaxt, Function& zeta_lineart, Function& un1t) :
    	gradu(gradut), zetax(zetaxt), zeta_linear(zeta_lineart), un1(un1t)
    {
    }
  Function& gradu, &zetax, &zeta_linear, &un1;

  void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
  {
  	xvA[0] = x[0]; xvA[1] = eval_z(zeta_linear, x[0]) - BIGEPS;
        try { gradu.eval(vV2A, xvA); }
	catch (std::runtime_error& str) { std::cout << "GP: Failed gradu at " << xvA[0] << "," << xvA[1] << std::endl; mk2_exit(34); }
  	values[0] = -vV2A[0]*eval_z(zetax, x[0]) + vV2A[1];
  }
};

class PhiFunction : public Expression
{
  public:
    PhiFunction(double Dtt) : Dt(Dtt) {}
    double Dt;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = -9.81*Dt*0.25*(cos(M_PI*x[0]/4)+1);
    }
};

class PhixFS : public Expression
{
  public:
    PhixFS(Function& cuxt, Function& uzt, Function& zetaxt)
     : cux(cuxt), uz(uzt), zetax(zetaxt) {}
    Function &cux, &uz, &zetax;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> xb(2); xb = x; double cuxf, zetaxf; dolfin::Array<double> uzv(1);
      cuxf = eval_z(cux, x[0]); uz.eval(uzv, xb); zetaxf = eval_z(zetax, x[0]);
      values[0] = cuxf + uzv[0]*zetaxf;
    }
};

class HEP::BernoulliPressure : public Expression
{
  public:
    BernoulliPressure(double rhowt, double gt, Function& udtt, Function& gradsqt) :
      rhow(rhowt), g(gt), udt(udtt), gradsq(gradsqt) {}
    double rhow, g;
    Function &udt, &gradsq;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> xb(2); xb[0] = x[0]; xb[1] = x[1];
      dolfin::Array<double> udtv(1), gradsqv(1);
      try { udt.eval(udtv, xb); } catch (std::runtime_error& str) { std::cout << "BP: Failed udt at " << xb[0] << "," << xb[1] << std::endl; mk2_exit(34); }
      try { gradsq.eval(gradsqv, xb); } catch (std::runtime_error& str) { std::cout << "BP: Failed gradsq at " << xb[0] << "," << xb[1] << std::endl; mk2_exit(34); }
      values[0] = - rhow * ( udtv[0] + 0.5*gradsqv[0]);//RMV + g*x[1] );
    }
};

/*
void do_PressureFunction (Function& pressure, double rhow, Function& udtp, Function& gsup, Function& W, double draft)
{
  pressure.vector() = udtp.vector();
  for (uint i = 0 ; i < pressure.vector().size() ; i++)
  {
      double udt = udtp.vector().getitem(i), gsu = gsup.vector().getitem(i);
      pressure.vector().setitem(i, - rhow * (udt + 0.5 * gsu));
  }
}
*/
dolfin::Array<double> udtA(1), gsuA(1);
class HEP::PressureFunction : public Expression
{
  public:
    PressureFunction(Plate* platet, double rhowt, Function& udtpt, Function& gsupt, Function& W_lineart, double draftt, double gt) :
      plate(platet), rhow(rhowt), udtp(udtpt), gsup(gsupt), W_linear(W_lineart), draft(draftt), g(gt) {}

    Plate* plate;
    double rhow;
    Function &udtp, &gsup, &W_linear;
    double draft, g;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      double z = eval_z(W_linear, x[0]) - draft;
      xvA[0] = x[0]; xvA[1] = z - BIGEPS;//RMV
      //if (xv[1] > -6) xv[1] = -6 + 1e-10;
      //udtp.eval(udt, xv);
      if (true || (!plate->refpress_on) || x[0] > plate->refpress_x) {
        try { udtp.eval(udtA, xvA); } catch (std::runtime_error& str)
        { std::cout << "PressureFunction failed at " << xvA[0] << "," << xvA[1]<<std::endl; mk2_exit(13);}
        gsup.eval(gsuA, xvA);
        values[0] = - rhow * (udtA[0] + 0.5 * gsuA[0]);
      } else {
        values[0] = (x[0]-plate->left)*eval_z(plate->Wz_Wpress, plate->refpress_x)/(plate->refpress_x-plate->left);
      }
    }
};

class HEP::LukeBoundary : public Expression
{
  public:
    LukeBoundary(double cnrpresst, double rhowt, Function& udtpt, Function& gsupt, Function& W_lineart, double draftt, double gt) :
      cnrpress(cnrpresst), rhow(rhowt), udtp(udtpt), gsup(gsupt), W_linear(W_lineart), draft(draftt), g(gt) {}

    double cnrpress, rhow;
    Function &udtp, &gsup, &W_linear;
    double draft, g;

    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> udt(1), gsu(1); double z = eval_z(W_linear, x[0]) - draft;
      dolfin::Array<double> xv(2); xv[0] = x[0]; xv[1] = z - 1e-2;//RMV
      //if (xv[1] > -6) xv[1] = -6 + 1e-10;
      //udtp.eval(udt, xv);
      try { udtp.eval(udt, xv); } catch (std::runtime_error& str)
      { std::cout << "WzFunction failed at " << xv[0] << "," << xv[1]<<std::endl; mk2_exit(13);}
      gsup.eval(gsu, xv);
      values[0] = - (cnrpress/rhow + udt[0] + 0.5 * gsu[0])/g + draft;
    }
};

class HEP::Pdtn1Function : public Expression
{
  public:
    Pdtn1Function ( PlateList& platelistt, Function& gspt, double gt, Function& zt ) : gsp(gspt), g(gt), z(zt), platelist(platelistt) {}
    Function &gsp;
    double g;
    Function &z;
    PlateList& platelist;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      Plate* plate = Plate::this_plate(platelist, x[0], false, length);

      double xt = x[0];
      if (plate) {
	double l = plate->left, r = plate->right;
	double lv = -0.5*eval_z(gsp, l)-g*eval_z(z,l);
	double rv = -0.5*eval_z(gsp, r)-g*eval_z(z,r);
        values[0] = ((xt-r)*lv + (l-xt)*rv) / (l-r); // ENSURE CONTINUITY ON FS/P INTERFACE!
      } else
        values[0] = (nlfluid?-0.5*eval_z(gsp, x[0]):0) - g*eval_z(z, x[0]); // RMV true
    }
};

class TopLeftCorner: public SubDomain
{
  //bool inside (const dolfin::Array<double> &x, bool on_boundary) const
 // { return fabs(x[0]) <= 5e-1 && fabs(x[1]) <= EPS; }
    public:
    TopLeftCorner(PlateList &platelistt, Function& zetat) : platelist(platelistt), zeta(zetat) {}
    PlateList& platelist; Function& zeta;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	if (fabs(x[0] - (*plit)->left) <= EPS || fabs(x[0] - (*plit)->right) <= EPS)
	   return true;
      return false;
    }
};

class RightEnd : public SubDomain
{
  public:
    RightEnd(double lengtht) : length(lengtht) {}
    double length;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return fabs(x[0]-length) <= EPS && fabs(x[1]+3) <= 0.02;
    }
};

class AllBdy : public SubDomain
{
  public:
    AllBdy(double lengtht, PlateList &platelistt) : length(lengtht), platelist(platelistt) {}
    double length;
    PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      //Plate* plate = Plate::this_plate(platelist, x[0], false, length);
      return on_boundary;/*fabs(x[0]-length) <= EPS || fabs(x[0]) <= EPS || fabs(x[1]+50) <= EPS;*/
    }
};

class NearTop : public SubDomain
{
  public:
    NearTop(Function& zetat) : zeta(zetat) {}
    Function& zeta;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const
    {
      return fabs(x[0])<=EPS;// && fabs(approx_z(zeta,x[0], true) - x[1]) > 1e0;
    }
};

void test (File& file, Function& fn)
{ file << fn;
}
class line : public Expression
{
  public:
   void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x) const
   {
     values[0] = x[0];
   }
};

int HEP::Single_run (char* dirname, bool resume)
{
  bool platetest = false;
  bool leave = false;
  parameters["linear_algebra_backend"] = "uBLAS";

  dolfin::Array<double> bounds(4);
  bounds[0] = 0.0; bounds[1] = 0.0; bounds[2] = length; bounds[3] = 1.0;

  /* Initialize output files */
  char fnbuffer[500];
  int files1db = 0, files1da = 0, files2db = 0, files2da = 0;
  char test1d[500], test2d[500];
  std::map< std::string, File* > basicfiles;
  std::map< std::string, File* > addfiles;

  sprintf(fnbuffer, "u/model-%s-u.pvd", dirname);
  basicfiles["u"] = new File(fnbuffer); files2db++;
  sprintf(fnbuffer, "disp/model-%s-W.pvd", dirname);
  basicfiles["W"] = new File(fnbuffer); files1db++;
  sprintf(fnbuffer, "disp/model-%s-zeta.pvd", dirname);
  basicfiles["zeta"] = new File(fnbuffer); files1db++;
  sprintf(fnbuffer, "pressure/model-%s-pressure.pvd", dirname);
  basicfiles["pressure"] = new File(fnbuffer); files2db++;
  sprintf(fnbuffer, "mesh/model-%s-mesh.pvd", dirname);
  sprintf(test2d, "mesh/model-%s-mesh000000.vtu", dirname);
  sprintf(test1d, "mesh/model-%s-mesh000001.vtu", dirname);
  File* meshfileptr = new File(fnbuffer);
  basicfiles["mesh"] = meshfileptr;
  sprintf(fnbuffer, "add/misc/model-%s-misc.pvd", dirname);
  basicfiles["misc"] = new File(fnbuffer);
  sprintf(fnbuffer, "unonz-%s.pvd", dirname);
  basicfiles["unonz"] = new File(fnbuffer);
  sprintf(fnbuffer, "unonz-%s.pvd", dirname);
  basicfiles["unonz"] = new File(fnbuffer);

  addfiles["mesh"] = meshfileptr; files2da++;
  sprintf(fnbuffer, "add/acc/model-%s-Wdd.pvd", dirname);
  addfiles["Wdd"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/vel/model-%s-Wd.pvd", dirname);
  addfiles["Wd"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/gsp/model-%s-gsp.pvd", dirname);
  addfiles["gsp"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/gsp/model-%s-gspfs.pvd", dirname);
  addfiles["gspfs"] = new File(fnbuffer); files1da++;
  sprintf(fnbuffer, "add/udt/model-%s-udt.pvd", dirname);
  addfiles["udt"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/unonz/model-%s-unonz.pvd", dirname);
  addfiles["unonz"] = new File(fnbuffer); files2da++;
  sprintf(fnbuffer, "add/btm/model-%s-btm.pvd", dirname);
  addfiles["btm"] = new File(fnbuffer); files1da++;

  /* Make mesh */
  Mesh mesh;
  Mesh line;

  /* Calulate average depth */
  double avdepth = -1, avfalsebtm = -1;
  AltUnitInterval tmpline ( bounds[2], N, mcl[1] ); // Add 'inner' consideration
  Top::FunctionSpace tmpV(tmpline);
  GiNaCFunction tmpvarbottomE ( x, depth.subs(symt==0) );
  GiNaCFunction tmpfalsebtmE ( x, falsebtm.subs(symt==0) );
  Function tmpvarbottom(tmpV); tmpvarbottom = tmpvarbottomE;
  Function tmpfalsebtm(tmpV); tmpfalsebtm = tmpfalsebtmE;
  avdepth = -get_interval_norm(tmpvarbottom)/(bounds[2] - bounds[0]);
  avfalsebtm = -get_interval_norm(tmpfalsebtm)/(bounds[2] - bounds[0]);
  //std::cout << depth << avdepth << std::endl;
  logfile << "(Average depth: " << avdepth << ")"<< std::endl;

  /* Generate mesh */
  GeoMaker geomaker;
  char filename[50] = "automesh.geo";
  for ( std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  { PlateDescription* plated = *pdit;
    if (plated->draft<0) 
     {
       //plated->draft = (plated->h)*(plated->rhop/rhow);
       //logfile << "(Just calc'd archimedian draft for plate ["<<plated->left<<","<<plated->right<<"] at " <<plated->draft<< ")"<<std::endl;
     }
  }
  if (automesh)
   mesh = geomaker.simplePlateDescriptionList(bounds[0], bounds[2], avdepth, platedescriptions, mcl, mrat, ic, x, mode3d);
  else
   mesh = geomaker.generateGMSHMeshFromFile(filename, mrat, mode3d);
  //mesh = generate_mesh_by_gmsh(length,avdepth,0.3,3,5,mcl,mclmd,automesh);
  *addfiles["mesh"] << mesh;

  /* Generate free surface mesh function */
//  MeshFunction<uint> tlcdommf(mesh, 1);
//  MeshFunction<uint> pdommf(mesh, 1);

  /* Derive the top boundary submesh */
  BoundaryMesh bdy(mesh);
  *addfiles["mesh"] << bdy; // Used for 2d size test
  PlateList platelist;
  PlaterMap& platermap = Plate::get_platermap();
  k_guess = wavemaker->fre;
  int ptct = 1; char platename[200];
  MeshFunction<uint>* p_to_bdy;

  for ( std::list<PlateDescription*>::iterator pdit = platedescriptions.begin() ; pdit != platedescriptions.end() ; pdit++ )
  {
    //(*pdit)->draft = (*pdit)->h*((*pdit)->rhop-rhow)/(*pdit)->rhop;
    (*pdit)->h = (*pdit)->draft*(rhow/(*pdit)->rhop);
    logfile << "Calculated " << (*pdit)->name << " thickness to be : " << (*pdit)->draft << std::endl;
    //std::cout << (*pdit)->draft << std::endl;
    Plater* plater = platermap[(*pdit)->typestr];
    if (!plater) { std::cout << "[MK2] ERROR: UNAVAILABLE PLATE TYPE - " << (*pdit)->typestr << std::endl; mk2_exit(11); }

    sprintf(platename, "%s_plate_%d", runname, ptct);
    const Mesh& plinec = make_pline(bdy, (*pdit)->left, (*pdit)->right, (*pdit)->draft, &p_to_bdy);
    Mesh& pline = *(new Mesh(plinec));
    Plate* plate = new Plate(pline, **pdit, Dt, rhow, g, *plater, platename, !platetest);
    plate->problem->log = plate_output;
    double D = plate->problem->get_D(), Cs = sqrt((D*pow(k_guess,4)+rhow*g)/(rhop*h*0.8+rhow*k_guess*(1/tanh(k_guess*avdepth))));
    logfile << "Estimated value of Cs: " << Cs << std::endl;
    platelist.push_back(plate);
    //std::cout << plate->Wn.function_space()->mesh().geometry().x(0,0) << " "
    //          << plate->Wn.function_space()->mesh().geometry().x(0,1) << std::endl;
	dolfin::Array<double> v(1), xv(1); xv[0] = 18;
	  const double* _x = xv.data().get();
  const Point point(plate->Wn.function_space()->mesh()->geometry().dim(), _x);
//  int id = plate->Wn.function_space()->mesh().any_intersected_entity(point);
//std::cout << id << " " <<  plate->Wn.function_space()->mesh().geometry().dim() << " " << point << std::endl;
*addfiles["mesh"] << plate->Wn;

        try { plate->Wn.eval(v, xv); }
	catch (std::runtime_error& str)
	  { std::cout << "Failed Wn at " << xv[0] << "," << std::endl; }
        
        std::cout << v[0] << std::endl;
     //std::cout << eval_z((*plit)->Wn, x[0]) << std::endl;
  }
  //  (*plit)->initdomain.mark(pdommf, 1);
  //RightEnd tlcdom(length);
//  tlcdom.mark(tlcdommf, 1);

  /* Instantiate subdomains */
  Single_ZetaInitialConditions zetaicpre(x, ic, platelist, icmode, false);

  //Single_FreeSurface freesurface(platelist, &zetaic, 0.01); // RMV!
  Single_FreeSurface freesurface(platelist); // RMV!
  //double asdf[2]; asdf[0] = 0; asdf[1] = 0;
//  Single_TopSubDomain topsdom(platelist, &zetaic);
  Single_TopSubDomain topsdom(platelist);
//  Boundary FSEnds;
//  MeshFunction<uint> fsdommf(mesh, 1);
//  freesurface.mark(fsdommf, 1);

  /* Size test */
  struct stat res1d, res2d;
  std::cout << test1d << " " << test2d << std::endl;
  stat(test1d, &res1d); stat(test2d, &res2d);
  /* We ignore the handful of basic mesh files and the forcing file */
  int maxnesspts = res1d.st_size * (files1db + aout*files1da) +
                   res2d.st_size * (files2db + aout*files2da);
  int cfact = ciout? 5 : 1;
  int predsize = cfact*(1+(T-ostarttime)/Dt/outfreq-ostartframe)*maxnesspts/(1000*1000);
  int predsizepts = cfact*maxnesspts/1000;
  logfile << "Max anticipated disk space required: ";
  if (predsize > 500)
    logfile << predsize/(1000) << "." << (predsize%1000)/100 << "GB (";
  else
    logfile << predsize << "MB (";
  if (predsizepts > 500)
    logfile << predsizepts/1000 << "." << (predsizepts%1000)/100 << "MB)\n";
  else
    logfile << predsizepts << "KB)\n";
  logfile
   << " (threshold is " << maxfs << "MB; each 2D file is " << res2d.st_size/1000 << "KB and 1D is" << res1d.st_size/1000 << "KB)" << std::endl;
  if (ciout)
    logfile << 
    "WARNING: THIS ASSUMES AV. 5 CORRECTION ITERATIONS"
    << std::endl;
  if (predsize > maxfs) {
    std::cout << "[MK2] Prediction exceeds disk space threshold - alter parameter file settings" << std::endl;
    //mk2_exit (10);
  }
  /*
  MeshFunction<uint> nsdommf(mesh, 2);
  nearfreesurface.mark(nsdommf, 1);
  SubMesh nsmesh(mesh, nsdommf, 1);
  *addfiles["mesh"] << nsmesh;
  MeshFunction<uint> fsnsdommf(nsmesh, 1);
  freesurface.mark(fsnsdommf, 1);*/

  /* Define our delta functional */
  double Err, Err1, Err2, Err3, tol = pow ( 10, tolexp );
  int lkjh = 0;

  /* Create progress bar */
  Progress progress("Progress");

  /* Generate 1D meshes */
  Single_FreeSurfaceFlat fsflat(platelist);
  SubMesh fsbdy(bdy, freesurface);
  *addfiles["mesh"] << fsbdy;
  MeshFunction<uint> *mf_tmp;
  MeshFunction<uint>& fsbdy_to_bdy_v = *fsbdy.data().mesh_function("parent_vertex_indices");
  line = AltUnitInterval(fsbdy, platelist, &mf_tmp, fsbdy_to_bdy_v, bdy, *p_to_bdy);
  *addfiles["mesh"] << line;
  SubMesh fsline(line, fsflat);
  MeshFunction<uint> mf(fsline, 1);
  MeshFunction<uint>& fsline_to_line_v = *fsline.data().mesh_function("parent_vertex_indices");
  MeshFunction<uint>& bdy_to_mesh = bdy.cell_map();

  line.init(0,1);
  MeshConnectivity& fsline_conn = fsline.topology()(1,0);
  MeshConnectivity& line_conn = line.topology()(0,1);
  MeshConnectivity& mesh_conn = mesh.topology()(1,2);
  MeshConnectivity& mesh_rev_conn = mesh.topology()(2,1);
  MeshFunction<uint> cell_to_facet(fsline, 1);
  int k;
  for ( MeshEntityIterator mit(fsline, 1); !mit.end() ; ++mit) {
        k = (*fsline.data().mesh_function("parent_vertex_indices"))[fsline_conn(mit->index())[0]];
        if (fsline_conn.size(mit->index()) == 1)
                k = line_conn(fsline_to_line_v[fsline_conn(mit->index())[0]])[0];
        else {
                int n = fsline_conn(mit->index())[0], m = fsline_conn(mit->index())[1];
                n = fsline_to_line_v[n];
                m = fsline_to_line_v[m];
                int a = line_conn.size(n), b = line_conn.size(m);
                for ( int i = 0 ; i < a ; i++ )
                   for ( int j = 0 ; j < b ; j++ )
                       if (line_conn(n)[i] == line_conn(m)[j])
                           k = line_conn(n)[i];
        }
        mf[mit->index()] = bdy_to_mesh[(*mf_tmp)[k]];

        int mc = mesh_conn(mf[mit->index()])[0];
        for ( int ii = 0 ; ii < mesh_rev_conn.size(mc) ; ii++ )
                if ( mesh_rev_conn(mc)[ii] == mf[mit->index()] )
                        cell_to_facet[mit->index()] = ii;
        //std::cout << fsline.geometry().x(fsline_conn(mit->index())[0], 0) << " " << mesh.geometry().x(mesh_conn(mf[mit->index()])[0],0) << std::endl;
  }
  //std::cout << mf[0] << std::endl;
  //std::cout << mesh.geometry().x(mesh_conn(mf[0])[0],0) << std::endl;

  *addfiles["mesh"] << fsline;
  line.order();
  Top::FunctionSpace topV(line);
  Top::FunctionSpace toplV(line);
  //SquareFunction asdf; Function asdff(topV); asdff = asdf;
  //std::cout << approx_z(asdff, 40, true) << " " << approx_z(asdff, 40, false) << std::endl;
  //mk2_exit(1);
  Top::FunctionSpace fsV(fsline);
  Function zetaic(fsV);
  eval_flatten(zetaic, fsV, zetaicpre);
  hTop::FunctionSpace fsVh(fsline);
  lTop::FunctionSpace fsVl(fsline);
  dTop::FunctionSpace dgfsV(fsline);
  DoubleTop::FunctionSpace dfsV(fsline);
  hDoubleTop::FunctionSpace dfsVh(fsline);
  Predict::FunctionSpace dfsVh2(fsline);
  //Top::FunctionSpace pV1(plate.pline);
  //hTop::FunctionSpace pV1h(plate.pline);

  /* Treat FS ends */
//  MeshFunction<uint> bfsdommf(fsline, 0);
//  FSEnds.mark(bfsdommf, 1);

  /* Set constants */
  Constant zero(0.0);
  Constant gc(g), Dtc(Dt), rhowc(rhow), muc(musurf), lc(lscale);

  /* Declare our intertemporal variables */
   /* ...on free surface */
  Function pdtn(fsVh), pdtnm1(fsVh), pdtnm2(fsVh), pdtnm3(fsVh);
  pdtn = zero; pdtnm1 = zero; pdtnm2 = zero; pdtnm3 = zero;
  Function cux(fsV); cux = zero;
//  Function cu(fsV); cu = zero;
  Function pdtn1(fsVh), zdtn1(fsV); pdtn1 = zero; zdtn1 = zero;
  Function zetan1(fsVh), zetan(fsVh), zeta0(fsVh), utopn(fsVh), utopn1(fsVh),
    zdtn(fsV), zdtnm1(fsV), zdtnm2(fsV), zdtnm3(fsV);
  zdtn = zero; zdtnm1 = zero; zdtnm2 = zero; zdtnm3 = zero;
  utopn = zero; utopn1 = zero; zetan = zero; zeta0 = zero; zetan1 = zero;
  Function gsun1fs(fsV); gsun1fs = zero; Function gsunfs(fsV); gsunfs = zero; 
  Function zetan1old(fsVh); zetan1old = zero;
  Function zetan1old_linear(fsVl); zetan1old_linear = zero;
  Function zetan1_linear(fsVl); zetan1_linear = zero;
  Function zetax_linear(fsVl); zetax_linear = zero;
  Function zetan1old2(fsVh); zetan1old2 = zero;
  Function utopn1old(fsV); utopn1old = zero;

  Beach_nuFunction beach_nu(10); Function beach_nuf(fsV); beach_nuf = beach_nu;
//  beach_nuf = zero;//RMV

   /* ...in fluid */
  //Function gsun1(V); gsun1 = zero; Function un1(Vh); un1 = zero; Function udtn1(V); udtn1 = zero;

  if (forcingmode == 1)
  {
    double vel = -sqrt(2*g*f_wd_h);
    PointFunction WDdf (vel, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, bounds[2]);
    if (plate) plate->Wdotn = WDdf; // NOTE THAT WE NEGLECT THE MASS OF THE PLATE HERE!
  } else if (forcingmode == 2)
  {
    GiNaCFunction f_ex_f(x, f_ex_ex.subs(symt==0));
    (*platelist.begin())->forcing = f_ex_f;
  } else if (forcingmode == 3)
  {
    double val = GiNaC::ex_to<GiNaC::numeric>(f_ex_ex.subs(symt==0)).to_double();
    PointFunction WDdf (val, f_wd_x);
    Plate* plate = Plate::this_plate(platelist, f_wd_x, true, bounds[2]);
    if (plate) plate->forcing = WDdf;
    //if (fout) {fprintf(forcingfile, "%lf,%lf\n", 0.0, val);fflush(forcingfile);}
  }

  /* Initialize our zeta vector and W_(n+1) */
  zeta0 = zetaic;
  Function fszero(fsV); fszero = zero;
//  Function zeta0tmp(fsV); zeta0tmp.interpolate(zeta0); zeta0=fszero;//RMV
  zetan = zeta0;
  // Assume no motion to begin with!!
  //Wn = Wn1;
  Single_ZetaInitialConditions plic(x, ic, platelist, icmode, true);
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  {  (*plit)->Wn = plic;
     (*plit)->nonstep(); }

  /* Instantiate wavemaker */
  /* (amp, fre, lwr, btm, waves [0 for inf], attcoeff) */
  //wavemaker = new WaveMaker(0.1, 0.4, -0.18, -0.2, 2, 0.1);
  /* Set up by readparams */

  /* Set up the bottom */
  Constant constdepth(avdepth);
  Constant constfalsebtm(avfalsebtm);
  Function varbottomold(toplV); varbottomold = constdepth;
  Function varbottom(toplV); varbottom = constdepth;
  Function movebottomold(toplV); movebottomold = constfalsebtm;
  Function movebottom(toplV); movebottom = constfalsebtm;
  std::cout << eval_z(varbottomold, 1e-15) << std::endl;
  std::cout << eval_z(movebottom, 1e-15) << std::endl;

  /* Initial values of pdtn and zdtn */
  Function unonzfs(fsV); unonzfs = zero;
//  Function uzfs(fsV); uzfs = zero;
      /* Solve the potential problems */
      //Mesh newmesh(mesh);
      zetan1_linear.vector()->zero();
      linearize(zetan1_linear, zeta0);
      linearize(zetan1old_linear, zetan1old);
      Single_mesh_move(mesh, platelist, zetan1_linear, zetan1old_linear, movebottom, movebottomold); // BTM AT RIGHT TS
      *addfiles["mesh"] << mesh;
      //mesh.move(newmesh);
      Domain::FunctionSpace nV(mesh);//RMV
      Domain2::FunctionSpace V2(mesh);//RMV
      hDomain::FunctionSpace Vh(mesh);
      Domain::FunctionSpace V(mesh);
      dDomain::FunctionSpace dV(mesh);
      zetan1old = zeta0; zetan = zeta0;
      linearize(zetan1old_linear, zetan1old);
       /* 1. Declare the relevant utility functions */
      Function u0(V); //u0 = zero;
      Function zetax0(fsV);//, cudtx(fsV);
      eval_dx(zetax0, fsV, zeta0, lc);
      //if (tilted_fluid)
        for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
        {
          Plate* plate = (*plit);
          eval_beam_dx(plate->Wx, plate->pV, plate->Wn1, lc);
          eval_beam_dx(plate->Wxx, plate->pV, plate->Wx, lc);
        }
      WdotNeumann Wdote(platelist, zetan1_linear, false);
      WavedNeumann wavede(platelist, zetan1_linear);
      Standard::CoefficientSpace_Wdot cVWdot(mesh);
      Function veln(cVWdot); veln = Wdote;
       /* 3. Solve the velocity potential problem */
      Constant one(1.0);
      Function fsnonzero(fsV); fsnonzero = one;
      MeshFromInterval ubc0f(platelist, fszero);
      //DirichletBC bc(Vh, ubc0f, resubdom);
      //DirichletBC bc(Vh, fszero, tlcdommf, 1);
      RightEnd resubdom(bounds[2]);
      Single_FreeSurface freesurface0(platelist, &zetan1_linear);
      Single_Plates plates0(platelist);
      DirichletBC bc(V, fszero, freesurface0);
//      MeshFunction<uint> dommf(mesh, 1);
//      AllBdy allbdy(length); allbdy.mark(dommf, 1);
      MeshFunction<uint> p1dommf(mesh, 1);
      p1dommf.set_all(5);
      AllBdy everywhere(bounds[2], platelist); NearTop waveside(zetan1_linear); everywhere.mark(p1dommf, 4); waveside.mark(p1dommf, 1);
      TopLeftCorner plateedges(platelist, zetan1); plateedges.mark(p1dommf, 5);
      plates0.mark(p1dommf, 3); freesurface0.mark(p1dommf, 2);
      WaveVelDBC wavdirbc(eval_z(zetan1, 0), t, avdepth, 0.0);
      DirichletBC bcw(V, wavdirbc, waveside);
      eval_velpot ( u0, V, zero, bc, bcw, Wdote, wavede, p1dommf, lc ); //, dommf, true );//RMV
      fszero = zero;
//Function ux(V); eval_ux(ux, V, u0);//RMV
       /* 4. Evaluate u_{n}/n_{z} */
      Function utop0(fsV);
      utop0 = FSLineFromMesh(u0, zetan1_linear);
      utopn = utop0;
      eval_dx(cux, fsV, utop0, lc);
      MeshFromInterval zetax0A(platelist, zetax0);
      MeshFromInterval cuxA(platelist, cux); // DOES THIS INTRODUCE DISCONTINUITIES ON THE PLATE/FS INTERFACES?
      Function unonz(nV);
      //Function uz(nV);
//      DzFunction unonze(u0, zetan1); unonzfs = unonze;
//      DirichletBC bcpz(fsV, unonzfs, FSEnds);
      //eval_unonz(unonz, nV, zetax0A, u0, cuxA);
      //eval_uz(uz, nV, u0);
//      *addfiles["unonz"] << unonz;
      //unonzfs = FSLineFromMesh(unonz, zeta0);
      Function gradu(V2);
      eval_unonz(gradu, V2, zetax0A, u0, cuxA);
     Function zetaxn1(fsV);//, cudtx(fsV);
      eval_dx(zetaxn1, fsV, zetan1, lc);
      SquareFunction unonzfsest(u0, zetan1_linear, zetan1, utop0, platelist, gradu, zetaxn1, mf, cell_to_facet, mesh_conn, 0);
      unonzfs.interpolate(unonzfsest);
       //write_function(unonzfs, "unonzfs"); // RMV
      //uzfs = FSLineFromMesh(uz, zeta0);
       /* 5. Find |grad(u)|^2 - NOTE THAT WE DO THIS REGARDLESS OF nlfluid TO MAKE SURE OUTPUT AVAILABLE!! */
      Function gsu0(V), gsu0fs(fsV);
      eval_gradsq(gsu0, V, u0, lc); // BC DEIMPLEMENTED
      gsu0fs = FSLineFromMesh(gsu0, zetan1_linear);
      //gsu0fs = DzFunction(u0, zeta0, utop0, platelist, 0.0);//RMV
       /* 6. Solve the acceleration potential problem */
      Function gsu0fake(V), gsu0fsfake(fsV), gsfszero(V); gsfszero = zero;
      if (nlfluid) gsu0fake = gsu0; else gsu0fake = zero;
      if (nlfluid) gsu0fsfake = gsu0fs; else gsu0fsfake = zero;
//      *basicfiles["u"] << u0;
//	Function ux(V); eval_ux(ux, V, u0);
//        *basicfiles["misc"] << ux;//veln;//RMV
  //eval_fscond ( *addfiles["unonz"], pdtn, zdtn, fsVh, fsV, gc, zeta0, gsu0fsfake, unonzfs, beach_nuf, utopn1, muc, lc );
  //pdtn1 = pdtn; pdtnm1 = pdtn; pdtnm2 = pdtn; pdtnm3 = pdtn;
  ////zdtn.vector().zero();
  //zdtn1 = zdtn; zdtnm1 = zdtn; zdtnm2 = zdtn; zdtnm3 = zdtn;

  /* Output initial values */
//  *basicfiles["zeta"] << zetan;
  time_t then; time(&then);
  struct tm * timeinfo = localtime(&then);
  logfile << "Start time: " << asctime(timeinfo) << std::endl;

  /* TIME LOOP */
  int titeration = 0;
  int ct = 0;
  int cl_last = 0;
  double alfac = 1;
  Constant zal(0.0);
  zetan1old2 = zetan; utopn1old = utopn;

  struct stat fsinfo; int fsret = 0; fsret = stat("state.st", &fsinfo);

  char openmode[4] = "w";
  if (resume && fsret==0)
  {
    std::string name("fluid");
    /* Swap functions for next time-iteration */
    load_function_vector(pdtnm3, name, "pdtnm3");
    load_function_vector(pdtnm2, name, "pdtnm2");
    load_function_vector(pdtnm1, name, "pdtnm1");
    load_function_vector(pdtn, name, "pdtn");
    load_function_vector(utopn, name, "utopn");
    load_function_vector(zdtnm3, name, "zdtnm3");
    load_function_vector(zdtnm2, name, "zdtnm2");
    load_function_vector(zdtnm1, name, "zdtnm1");
    load_function_vector(zdtn, name, "zdtn");
    load_function_vector(zetan, name, "zetan");

    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      (*plit)->load_function_vectors();

    load_function_vector(gsunfs, name, "gsunfs");
    load_function_vector(varbottomold, name, "varbottomold");
    load_function_vector(movebottomold, name, "movebottomold");
    // SORT OUT PHIN FOR OVING BOTTOM AND PHIDOTN AS WELL AS NOT NESS BOTTOM!

    /* Augment time variable */
    FILE* fstate; fstate = fopen("state.st", "r");
    int fret = 0;
    fret = fscanf(fstate, "t:%lf", &t);
    fret = fscanf(fstate, "titeration:%d", &titeration);
    fret = fscanf(fstate, "cl_last:%d", &cl_last);
    fclose(fstate);
    progress = t/T;
    logfile << "Resuming from time " << t << " (" << titeration << ")" << std::endl << "..." << std::endl; 
    std::cout << "WARNING: Resuming; make sure the code and parameters match previous run" << std::endl;
    openmode[0] = 'a';
  } else if (resume) std::cout << "From beginning as no state.st file" << std::endl;
    else if (fsret == 0) std::cout << "Ignoring state.st file (break asap and use -R to resume!)" << std::endl;
  //Function testf((*platelist.begin())->Wn1.function_space());

  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) (*plit)->setup_diagnostics(openmode);
  if (fout) forcingfile = fopen("forcing.csv", openmode);

  while ( t <= T+1e-9 )
  {
    sf_n = t;
    /* Set bottom for this timestep */
    varbottom = GiNaCFunction(x, -depth.subs(symt==t)); // NEGATED.
    movebottom = GiNaCFunction(x, -falsebtm.subs(symt==t)); // NEGATED.
//    if (aout&&(outfreq==0||titeration%outfreq==0)) *addfiles["btm"] << varbottom;
  
    /* Execute predictor step */
       //RMV CHK THESE WORK
       utopn1old = utopn1;
       zetan1old2 = zetan1;
    eval_gn_predict(zetan1, utopn1, dfsVh2, fsVh, Dtc,
      zetan, zdtn, zdtnm1, zdtnm2, zdtnm3,
      utopn, pdtn, pdtnm1, pdtnm2, pdtn1, lc, muc);
    //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
    // Plate* plate = *plit;
    // if (plate->refpress_on)
    //   eval_predict(plate->Wz, plate->Wz_p, dpVh2, pVh, Dtc,
    //   	plate->Wzn, plate->Wzdtn, plate->Wzdtnm1, plate->Wzdtnm2, plate->Wzdtnm3,
    //    plate->Wz_pn, plate->Wz_pdtn, plate->Wz_pdtnm1, plate->Wz_pdtnm2, plate->Wz_pdt, lc, mu);
    //}
    linearize(zetan1_linear, zetan1);
    //eval_alpha(zetan, fsV, zal, zetan1old);
    //eval_alpha(utopn, fsV, zal, utopn1old);

     //dolfin::Array<double> xarr(2); xarr[0] = 0; xarr[1] = eval_z(zetan1, 0);
     //DzFunction slze(-wavemaker->acceleration(t, xarr)/g - approx_z(zetaxn1, 0), length/10);
     //Function slz(fsV); slz.interpolate(slze);
    eval_dx(cux, fsV, zetaic, lc);
    //double zxl = approx_z(zetaxn1, 0);RMV
    //double wcuxl = -wavemaker->velocity(t, xarr);// + zxl*approx_z(uzfs,0); // zdtn1 not evaluated!
    //double wcuxl = approx_z(cux, 0) - zxl*approx_z(uzfs,0); // zdtn1 not evaluated!
//    wcuxl /= sqrt(1+zxl*zxl);
    //DzFunction slpe(wcuxl - approx_z(cux, 0), length);//cux should be ux
    //Function slp(fsV); slp.interpolate(slpe);

    eval_predict(zetan1, utopn1, dfsVh2, fsVh, Dtc,
       zetan, zdtn, zdtnm1, zdtnm2, zdtnm3,
       utopn, pdtn, pdtnm1, pdtnm2, pdtn1, lc, muc);
    eval_alpha(zetan1, fsV, zal, zetan1old2);
    eval_alpha(utopn1, fsV, zal, utopn1old);
    linearize(zetan1_linear, zetan1);

     eval_dx(zetaxn1, fsV, zetan1, lc);
    linearize(zetax_linear, zetaxn1);

    /* Assume pressure from previous step (!) and use to predict Wn1, Wdtn1, Wddtn1 */
//    pressuren1p1 = zero;//RMV
//    plateproblem1.step(pressuren1p1, Wn1, Wdtn1, Wddtn1, Wn, Wdotn, Wddotn, forcing);
//    if (aout&&(outfreq==0||titeration%outfreq==0)) { *addfiles["Wdd"] << Wddtn1; *addfiles["Wd"] << Wdtn1; }
  
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
    	(*plit)->output_diagnostics(t);
        //(*plit)->nonstep();//RMV??????
    }

    /* Evaluate any transient forcing */
    if (forcingmode == 2)
      (*platelist.begin())->forcing = GiNaCFunction(x, f_ex_ex.subs(symt==t));
    else if (forcingmode == 3 && t <= f_wde_t)
    {
      double val = GiNaC::ex_to<GiNaC::numeric>(f_ex_ex.subs(symt==t)).to_double();
      PointFunction WDdf (val, f_wd_x);
      Plate* plate = Plate::this_plate(platelist, f_wd_x, true, bounds[2]);
      if (plate) plate->forcing = WDdf; // NOTE THAT WE NEGLECT THE MASS OF THE PLATE HERE!
      if (fout) {fprintf(forcingfile, "%lf,%lf\n", t, val);fflush(forcingfile);}
    }

    /* CORRECTION LOOP */
    int cl = 0; Err = 1; Err1 = 1; Err2 = 1; Err3 = 1; leave = false;
    bool startplate = true; int startresponse = 2;

    if (platetest)
     for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
     {
      (*plit)->p = zero;
      (*plit)->step();
     }
    else
    do
    {
      bool lognow = (outfreq==0||titeration%outfreq==0)&&(t>=ostarttime&&titeration/outfreq>=ostartframe);

      //if (cl == 10) { alpha *= 10; alfac *= 10; } //RMV
      /* Adjust plate acceleration using alpha */
      //if (cl > 1 && startplate)
       //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	//(*plit)->adjacc(alpha);
      /* Move the mesh */
      Single_mesh_move(mesh, platelist, zetan1, zetan1old, movebottom, movebottomold);
      //*basicfiles["unonz"] << (*platelist.begin())->Wn1_linear;
      zetan1old = zetan1;
      linearize(zetan1old_linear, zetan1old);
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      {
        (*plit)->clstep();
      }
      varbottomold = varbottom; // slow and repettitve
      movebottomold = movebottom; // slow and repettitve

      /* Declare a new function space for the adjusted mesh */
      Domain::FunctionSpace V(mesh);
      Domain2::FunctionSpace V2(mesh);
      Domain::FunctionSpace nV(mesh);//RMV
      hDomain::FunctionSpace Vh(mesh);
      dDomain::FunctionSpace dV(mesh);

      /* Solve the potential problems */
       /* 1. Declare the relevant utility functions */
//	Function ux(V); eval_ux(ux, V, un1);//RMV
      //if (tilted_fluid)
        for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
        {
      	  Plate* plate = (*plit);
	  eval_beam_dx(plate->Wx, plate->pV, plate->Wn1, lc);
          eval_beam_dx(plate->Wxx, plate->pV, plate->Wx, lc);
        }
      WdotNeumann Wdote(platelist, zetan1, true); // cux is out of date!!
      WddotNeumann Wddote(platelist, zetan1, true);
      WavedNeumann wavede(platelist, zetan1);
      WaveddNeumann wavedde(platelist, zetan1);
      //Standard::CoefficientSpace_Wdot cVWdot(mesh);
      Function veln(cVWdot); veln = Wdote; Function accn(cVWdot); accn = Wddote;
       /* 2. Evaluate boundary conditions */
      MeshFromInterval utopn1f(platelist, utopn1);
      Constant one(1.00);
      RightEnd resubdom(bounds[2]);
      Single_FreeSurface freesurfacen1(platelist, &zetan1_linear, true);
      Single_Plates platesn1(platelist);
      MeshFunction<uint> p1dommf(mesh, 1);
      //AllBdy everywhere(length, platelist); everywhere.mark(p1dommf, 1); platesn1.mark(p1dommf, 3); freesurfacen1.mark(p1dommf, 2);
      p1dommf.set_all(5);
      AllBdy everywhere(bounds[2], platelist); NearTop waveside(zetan1); everywhere.mark(p1dommf, 4); waveside.mark(p1dommf, 1);
      TopLeftCorner plateedges(platelist, zetan1); plateedges.mark(p1dommf, 5);
      platesn1.mark(p1dommf, 3); freesurfacen1.mark(p1dommf, 2);
      DirichletBC bc(V, utopn1f, freesurfacen1);
      xtmp[0] = 0.0; utopn1.eval(vtmp, xtmp);
      WaveVelDBC wavdirbc(eval_z(zetan1, 0), t, avdepth, vtmp[0]);
      DirichletBC bcw(V, wavdirbc, waveside);
      //DirichletBC bc(Vh, fszero, resubdom);
      //DirichletBC bc(Vh, utopn1f, fsdommf, 1);
      //DirichletBC bc(Vh, fszero, tlcdommf, 1);
       /* 3. Solve the velocity potential problem */
      Function un1(V);
      MeshFromInterval utopn1A(platelist, utopn1);
      //eval_velpot ( un1, Vh, bc, veln );//RMV
      eval_velpot ( un1, V, utopn1A, bc, bcw, Wdote, wavede, p1dommf, lc );//RMV
       /* 4. Evaluate u_{n}/n_{z} */
      MeshFromInterval zetaxn1A(platelist, zetaxn1);
      //cu = FSLineFromMesh(un1, zetan1);
      eval_dx(cux, fsV, utopn1, lc);
      MeshFromInterval cuxA(platelist, cux); // DOES THIS INTRODUCE DISCONTINUITIES ON THE PLATE/FS INTERFACES?
      Function unonz(nV); unonz = zero;
      //Function uz(nV); uz.interpolate(zero);
//      DzFunction unonze(un1, zetan1); unonzfs = unonze;
//      DirichletBC bcpz(nV, unonzfs, FSEnds);
/*
      for ( VertexIterator vit(fsline); !vit.end() ; ++vit)
      {
        dolfin::Array<double> B(1), Ax(2); Ax[0] = vit->x(0);
	if (!Plate::this_plate(platelist, Ax[0], false, length)){
	 Ax[1] = approx_z(zetan1, vit->x(0), false);
	 double Axp[2]; Axp[0] = Ax[0]; Axp[1] = Ax[1];
	 Point point(2, Axp);
         if (mesh.any_intersected_entity(point) == -1) { std::cout << "Failed unonz at " << Ax[0] << "," << Ax[1] << std::endl; }
         //try { un1.eval(B, Ax); } catch (std::runtime_error& str) { std::cout << "Failed unonz at " << Ax[0] << "," << Ax[1] << std::endl; }
	}
      }*/
      //eval_unonz(unonz, nV, zetaxn1A, un1, cuxA);
      //eval_uz(uz, nV, un1);
      //unonzfs = FSLineFromMesh(unonz, zetan1);
      Function gradu(V2);
      eval_unonz(gradu, V2, zetaxn1A, un1, cuxA);
      eval_dx(zetaxn1, fsV, zetan1, lc);
      SquareFunction unonzfsest(un1, zetan1_linear, zetan1, utopn1, platelist, gradu, zetaxn1, mf, cell_to_facet, mesh_conn, t);
      //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      //{
      //  Plate *plate = *plit;
      //  SquareFunction unonzfse(un1, plate->Wz
      //}
	
      unonzfs.interpolate(unonzfsest);
      //if (sf_n > -.5)
      // write_function(unonzfs, "unonzfs"); // RMV
      sf_n = -1.;
      //uzfs = FSLineFromMesh(uz, zetan1);
       /* 5. Find |grad(u)|^2 - NOTE THAT WE DO THIS REGARDLESS OF nlfluid TO MAKE SURE OUTPUT AVAILABLE!! */
      Function gsun1(V);
      gsun1 = zero;
      gsun1fs = zero;
      if (nlfluid)
      {
      	eval_gradsq(gsun1, V, un1, lc); // BC DEIMPLEMENTED
      	//gsun1fs = DzFunction(un1, zetan1_linear, zetan1, utopn1, platelist, t);//RMV
      	gsun1fs = FSLineFromMesh(gsun1, zetan1_linear);
      }

      //ApproxzFunction unonzfs2(gradu, zetax_linear, zetan1_linear, un1);
      //unonzfs = unonzfs2;

       /* 6. Solve the acceleration potential problem */
      Function udtn1(dV);
      //std::cout << "[" << std::endl;
      //PlateMask gsun1mask(gsfszero, platelist);//RMV
      //PlateMask gsun1mask(gsun1, platelist);//RMV
      //Function gsun1maskf(V);
      //gsun1maskf = gsun1mask;
      //std::cout << "]" << std::endl;
      Pdtn1Function pdtn1f(platelist, gsun1fs, g, zetan1);
      //*basicfiles["zeta"] << zetan1;

      DirichletBC bcdt(dV, pdtn1f, freesurfacen1);
      //Function Wddotef(dV); Wddotef = Wddote;
      //*addfiles["gsp"] << Wddotef;
      //SquareFunction constant(1e-9);
      WaveAccDBC wavadirbc(eval_z(zetan1, 0), t, avdepth, vtmp[0]);

      DirichletBC bcwdt(V, wavadirbc, waveside);

      //*basicfiles["unonz"] << (*platelist.begin())->Wddtn1;
	eval_accpot ( udtn1, dV, pdtn1f, bcdt, bcwdt, Wddote, wavedde, gsun1, p1dommf, pdtn1f, lc ); // THIS STILL ISN'T QUITE RIGHT AS PLATEMASK HAS ONLY ONE (ZERO) VAL AT PLATE CORNERS
      //*basicfiles["unonz"] << udtn1;
      //pdtn1 = FSLineFromMesh(udtn1, zetan1);

      if (startplate)
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      {
        Plate *plate = *plit;
	
        /* Evaluate pressure */
	//*basicfiles["u"] << udtn1;
	//dolfin::Array<double> t1(1), t2(2); t2[0] = 15.061; t2[1] = eval_z(plate->Wn1_linear, t2[0]) - plate->draft - 1e-2;
	//udtn1.eval(t1, t2);
	//std::cout << t1[0] << ")" << std::endl;
        PressureFunction pf (plate, rhow, udtn1, gsun1, plate->Wn1_linear, plate->draft, g);
	plate->p = pf;
        //*basicfiles["unonz"] << (*platelist.begin())->p;
        //std::cout << "***\t" << vtmp[0] << "\n";
	bool redo_refpress = false;
	if (plate->refpress_now) {
		double Wl = eval_z(plate->Wn1, plate->left);
		plate->refpress = eval_z(plate->p, plate->left) - g*rhow*Wl;
		plate->refpress_on = true;
		redo_refpress = true;
		plate->Wz = plate->Wn1;
		plate->Wzn = plate->Wn1;
		plate->Wzdt = plate->Wdtn1;
		plate->Wzddt = plate->Wddtn1;
		plate->refpress_now = false;
		//std::cout << Wl << std::endl;
	}

	//dolfin::Array cv(1), cx(1); cx[0] = plate->left; pf.eval(cv, cx);
	if (plate->refpress_on)
	{
    		plate->Wz_Wpress = LukeBoundary(plate->refpress_on ? eval_z(plate->p, plate->left)+1e-3 - g*rhow*eval_z(plate->Wz, plate->left) : 0,
		rhow, udtn1, gsun1, plate->Wn1_linear, plate->draft, g);
  		write_function(plate->Wz_Wpress, "W_Wpress"); // RMV
  		write_function(plate->Wn1, "W_Wn1"); // RMV
		plate->refpress_zdtn1 = eval_z(zdtn1, plate->left);
		//std::cout << ":" << lkjh++ << " " << t << " " << plate->refpress_x << " " << plate->refpress_zdtn1 << std::endl;
		//std::cout << "A" << eval_z(plate->Wz_Wpress, plate->left) << "B" << eval_z(plate->Wn1, plate->left) << std::endl;
		double oldrefpress = plate->refpress_x;
 		double xt = plate->left;
    		while (xt < plate->right) {
    			if (eval_z(plate->Wz_Wpress, xt) > eval_z(plate->Wn1, xt)+PEPS) break;
    			xt += 0.1;
    		}
    		plate->refpress_x = redo_refpress ? xt : xt*0.1 + plate->refpress_x*0.9;
    		//std::cout << "!!" << plate->refpress_x << " || " << xt << std::endl;

	}
	//*addfiles["mesh"] << plate->Wn1_linear;
	//*addfiles["mesh"] << pf;

	//plate->forcing = Constant(1e3);
        //PressureFunction pf(rhow, udtn1, gsun1fake, plate->Wn1, plate->draft);
        /* Run plate solver with newly calculated pressure */
	plate->problem->log = lognow && plate_output && (ciout);// || cl>=0);

	plate->step();
	//plate->step(pf);
	if (plate->refpress_on) {
  		write_function(plate->Wn1_linear, "W_Wn1_linear"); // RMV
		//std::cout << "N" << eval_z(plate->Wn1_linear, plate->left) << std::endl;
		//std::cout << "M" << eval_z(plate->Wz, plate->left) << std::endl;
		//std::cout << "_" << eval_z(plate->Wzdt, plate->left) << std::endl;
		//std::cout << "_" << eval_z(plate->Wzddt, plate->left) << std::endl;
		//std::cout << "D" << eval_z(plate->Wn1_linear, 15.061) - plate->draft << std::endl;
  		write_function(plate->Wzdt, "W_Wzdt"); // RMV
  		write_function(plate->Wzddt, "W_Wzddt"); // RMV
  		write_function(plate->Wz, "W_Wz"); // RMV
  		write_function(zdtn1, "W_zdtn1"); // RMV
		if (eval_z(plate->Wn1_linear, plate->left) > eval_z(plate->Wn1, plate->left)) {
			plate->refpress_on = false;
			plate->Wn1_linear.interpolate(plate->Wn1);
			//std::cout << "%" << lkjh++ << " " << t << std::endl;
			//std::cout << "refpress off" << std::endl;
		}
	}
       // dolfin::Array<double> v(1), x(1); x[0] = 10.4; v[0] = approx_z(plate->Wn1, x[0]);
	//std::cout << v[0] << std::endl;
//	plate->Wn1 = plate->Wn;
//	if ( t < 2*Dt ) { plate->Wddtn1 = zero; plate->Wdtn1 = zero; }
      }

      /* Get several of these to make sure this isn't just a pair!
       * Even better, get the error rather than looking for a converged
       * delta */
      Err3 = Err2;
      Err2 = Err1;
      Err1 = Err;
      Err = 0;
      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
        //Err += (*plit)->evalerr();
        Err += eval_err((*plit)->Wddtkm1, (*plit)->Wddtn1);
      //Err += eval_err(zetan1, zetan1old);
      leave = /*(cl>1)&&*/Err<=Err1 && (Err<tol&&Err1<tol);//&&Err2<tol&&Err3<tol);

      //RMV!!! Put this back in at top of loop!
      if (startplate)
       for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	(*plit)->adjacc(alpha);

      /* Calculate zdtn1 and pdtn1 */
      //RMV CHK THIS WORKS
      utopn1old = pdtn1;
      Constant zero (0.0); Function zerof(fsV); zerof = zero;
      if (nlfluid)
       eval_fscond ( *addfiles["unonz"], pdtn1, zdtn1, fsVh, fsV, gc, zetan1, gsun1fs, unonzfs, beach_nuf, utopn1, muc, lc );//RMV
      else {
       //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
       // Plate* plate = *plit;
       // if (plate->refpress_on)
       //   eval_fscond ( *addfiles["unonz"], plate->Wz_pdt, plate->Wzdt, pVh, pV, gc, plate->Wz, zerof, plate->Wz_pnonz, beach_nuf, plate->Wz_p, muc, lc );
       //}
       eval_fscond ( *addfiles["unonz"], pdtn1, zdtn1, fsVh, fsV, gc, zetan1, zerof, unonzfs, beach_nuf, utopn1, muc, lc );//RMV
      }
      //SquareFunction zdtn1fsest(un1, zetan1_linear, zetan1, utopn1, platelist, t);
      //zdtn1 = zdtn1fsest;
      zetan1old2 = zetan1;
      utopn1old = utopn1;
      eval_correct(zetan1, utopn1, dfsVh2, Dtc,
        zetan, zdtn1, zdtn, zdtnm1, zdtnm2,
	utopn, pdtn1, pdtn, pdtnm1, pdtnm2);
      eval_alpha(zetan1, fsV, zal, zetan1old2);
      eval_alpha(utopn1, fsV, zal, utopn1old);
      if (nlfluid)
       eval_fscond ( *addfiles["unonz"], pdtn1, zdtn1, fsVh, fsV, gc, zetan1, gsun1fs, unonzfs, beach_nuf, utopn1, muc, lc );//RMV
      else {
       //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
       // Plate* plate = *plit;
       // if (plate->refpress_on)
       //   eval_fscond ( *addfiles["unonz"], plate->Wz_pdt, plate->Wzdt, pVh, pV, gc, plate->Wz, zerof, plate->Wz_pnonz, beach_nuf, plate->Wz_p, muc, lc );
       //}
       eval_fscond ( *addfiles["unonz"], pdtn1, zdtn1, fsVh, fsV, gc, zetan1, zerof, unonzfs, beach_nuf, utopn1, muc, lc );//RMV
      }
      //for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ ) {
      // Plate* plate = *plit;
      // if (plate->refpress_on)
      //   eval_correct(plate->Wz, plate->Wz_p, dpVh2, Dtc,
      //     plate->Wzn, plate->Wzdt, plate->Wzdtn, plate->Wzdtnm1, plate->Wzdtnm2,
      //     plate->Wz_pn, plate->Wz_p, plate->Wz_pdtn, plate->Wz_pdtnm1, plate->Wz_pdtnm2);
      //}
      linearize(zetan1_linear, zetan1);

      leave = leave && startplate && (startresponse==2);
      leave = cl >= clmax;
      //if (startresponse) startresponse = 2;
      /*if (leave) std::cout << "X ";
      std::cout << Err << ":(" << r++ << ")-" << startresponse << (startplate?".":" "); fflush(stdout);
      if (Err < tol) { if (!startplate) std::cout << "| "; else { if (startresponse==0) {startresponse = 1; std::cout << "; "; }} startplate = true; }*/
      //else if (startresponse==2) { startplate = false; std::cout << "< "; }
      //leave = (cl>2);

      //leave = cl>=50;
      /* Augment correction iteration counter */
      cl++;
      ct++;

      if (leave)
    	/* Calculate Wz */
    	for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
    	{
    	  Plate *plate = *plit;
    	  //*addfiles["mesh"] << plate->Wz;
    	  //*addfiles["mesh"] << plate->p;

	  double fs = eval_z(zetan1    , plate->left);
	  double ps = eval_z(plate->Wn1, plate->left);
	  double zs = eval_z(plate->Wz , plate->left);
	  double prs = eval_z(plate->p , plate->left);

	  if (plate->refpress_on && zs > ps + 1e-3) {
  		write_function(plate->Wz, "W_Wz"); // RMV
  		write_function(plate->Wn1, "W_Wn1"); // RMV
  		write_function(plate->Wn1_linear, "W_Wn1_linear"); // RMV
		//std::cout << "$" << lkjh++ << " " << t << std::endl;
	  	
	  	//plate->refpress_on = false;
		std::cout << "refpress not off" << std::endl;
	  } else if (enable_refpress && (!plate->refpress_on) && fs-ps+plate->draft < refpress_trigger) {
	  	plate->refpress = prs;
		plate->refpress_now = true;
		Function zerof(zdtn1.function_space()); zerof = zero;
  		write_function(zerof, "W_zdtn1"); // RMV
		//*basicfiles["u"] << zdtn1;
		std::cout << "refpress on" << std::endl;
	  }
	  //std::cout << t << " " << asdf++ << " " << fs << " " << ps << " " << zs << " " << fs-ps+plate->draft << " " << zs-(fs-ps+plate->draft) << " " << prs << std::endl;
    	}

      //leave = (leave||(cl>=5));

      /* Do any additional outputting */
      //*basicfiles["unonz"] << (*platelist.begin())->Wn1_linear;
      if (lognow&&(leave||ciout)) {
        *basicfiles["zeta"] << zetan1;
    	for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
	{
		(*plit)->problem->dispout << (*plit)->Wn1;
  		write_function((*plit)->Wn1_linear, "plate/W_Wn1"); // RMV
  		write_function((*plit)->Wxx, "plate/W_Wxx"); // RMV
  		write_function((*plit)->p, "plate/W_p"); // RMV
  		write_function((*plit)->Wddtn1, "plate/W_Wddtn1"); // RMV
	}
	*addfiles["gsp"] << gsun1;
        //*basicfiles["u"] << un1;
        //*addfiles["udt"] << udtn1;
	//*addfiles["gsp"] << gsun1;
	//*addfiles["gspfs"] << gsun1fs;
//	Function ux(V); eval_ux(ux, V, un1);
//        *basicfiles["misc"] << ux;//veln; //zetaxn1A;//RMV

        if (aout) {
          *addfiles["mesh"] << mesh;
	  *addfiles["btm"] << varbottom;
	  //eval_ux(unonzxfs, dV, udtn1);
	  //unonzxfs = Wddote;
	  Function ux(dV);
	  eval_ux(ux, dV, udtn1, lc);
          //*addfiles["unonz"] << ux;

          Top::FunctionSpace fsV1(fsline);
          Top::FunctionSpace fsV2(fsline);
/*	  Function udtn1x(fsV1);
	  eval_dx(udtn1x, fsV1, zdtn1);
	  Function udtn1xx(fsV2);
	  eval_dx(udtn1xx, fsV2, udtn1x);*/
          *addfiles["udt"] << udtn1;
	  //Function uz(V);
	  //eval_uz(uz, V, un1, p1dommf);
	  //Function uzxx(V);
	  //eval_ux(uzxx, V, uzx);
	  //*addfiles["gsp"] << uz;
	  Function udtn1xb(fsV);
	  //eval_dx(udtn1xb, fsV, utopn1old);
	  Function udtn1xxb(fsV);
	  //eval_dx(udtn1xxb, fsV, udtn1xb);
	  //*addfiles["gspfs"] << (*platelist.begin())->Wddtn1;
	}
	//BernoulliPressure p(rhow, g, udtn1, gsun1fake);
	//Function pf(V); pf = p; *basicfiles["pressure"] << pf;
	//output_plane_csv (length, avdepth, titeration, zetan1, unonz);
	/* Only do if exactly 1 or 0 plates */
	//if ((titeration/outfreq)%horizfm == 0 && platelist.size() <= 1) {
	//  double leftend = bounds[2], x;
	//  double ppu = 0.5; int pres = 2; int ph=30;
	//  int i; FILE* f, *f1, *f2, *f3, *f4, *fs, *fw, *fp, *g, *fmesh, *fz;
	//  char fname[200], fnameside[200], fname1[200], fnamewave[200], fname2[200], fname3[200], fname4[200], fnamep[200],
	//  	fnamez[200], fnamemesh[200];
	//  sprintf(fname3, "u-%06d.csv", titeration);
	//  //f3 = fopen(fname3, "w");
	//  x = 1e-3;
	//  sprintf(fnamewave, "wave-%06d.csv", titeration);
	//  fw = fopen(fnamewave, "w");
	//  fprintf(fw, "x,y\n");
	//  double y = eval_z(zetan1, x) - 1e-3, ybtm = eval_z(varbottom, 0) + 1e-3;
	//  while ( y >= ybtm )
	//  {
	//    xvA[0] = x; xvA[1] = y;

	//    un1.eval(vA, xvA);
	//    try {un1.eval(vA, xvA);} catch (std::runtime_error& str)
    	//        { std::cout << "waveneu Error at " << xvA[0] << "," << xvA[1] << std::endl; }
	//    xvA[0] = xvA[0] + 1e-3;
	//    try {un1.eval(v2A, xvA);} catch (std::runtime_error& str)
    	//        { std::cout << "waveneu Error at " << xvA[0] << "," << xvA[1] << std::endl; }
	//    fprintf(fw, "%lf,%lf\n", (v2A[0]-vA[0])/1e-3, xvA[1]);
	//    y -= ppu*1e-2;
	//  }
	//  fclose(fw);
	//  if (false && platelist.size()>0) {
	//   sprintf(fname, "plate1-%06d.csv", titeration);
	//   sprintf(fnamemesh, "plate1mesh-%06d.csv", titeration);
	//   sprintf(fnamez, "plate1z-%06d.csv", titeration);
	//   f = fopen(fname, "w");
	//   fz = fopen(fnamez, "w");
	//   fmesh = fopen(fnamemesh, "w");
	//   sprintf(fnameside, "plate1side-%06d.csv", titeration);
	//   fs = fopen(fnameside, "w");
	//   fprintf(f, "x,y\n");
	//   fprintf(fmesh, "x,y\n");
	//   fprintf(fz, "x,y\n");
	//   fprintf(fs, "x,y\n");
	//   Plate* pl = *(platelist.begin());
	//   leftend = pl->left;
	//   x = leftend;
	//   //for ( i = 0 ; i <= pc ; i++ ) {
	//   while ( x <= pl->right )
	//   {
	//    fprintf(f, "%lf,%lf\n", x, eval_z(pl->Wn1, x)-pl->draft);
	//    fprintf(fmesh, "%lf,%lf\n", x, eval_z(pl->Wn1_linear, x)-pl->draft);
	//    fprintf(fz, "%lf,%lf\n", x, eval_z(pl->Wz, x)-pl->draft);
	//    x += ppu; //pl->left + (pl->right-pl->left)*i/(double)pc;
	//    /*
	//    for ( int j = 0 ; j <= ph ; j++ )
	//    {
	//      dolfin::Array<double> xv(2), v(2); xv[0] = x;
	//      xv[1] = (j/(double)ph)*(approx_z(pl->Wn1, x, true)-pl->draft-1e-3) + (1-j/(double)ph)*avdepth*0.999;
	//      try {un1.eval(v, xv);} catch (std::runtime_error& str)
    	// 	 { std::cout << "plate1 Error at " << xv[0] << "," << xv[1] << " (" << i << "," << j << ")" << std::endl; mk2_exit(31); }
	//      fprintf(f3, "%lf %lf %lf\n", xv[0], xv[1], v[0]);
	//    }
	//    fprintf(f3, "\n");*/
	//   }
	//   fclose(f);
	//   x = leftend - 1e-3;
	//   Function& W = pl->Wn1_linear;
	//   double y = eval_z(zetan1old, x) - 1e-3, ybtm = eval_z(W, leftend+1e-3) - pl->draft + 1e-3;
	//   //std::cout << eval_z(zetan1old, x) << " " << ybtm << std::endl;
	//   while ( y >= ybtm )
	//   {
	//     xvA[0] = x; xvA[1] = y;
	//     try {un1.eval(vA, xvA);} catch (std::runtime_error& str)
    	// 	 { std::cout << "plate1side Error at " << xvA[0] << "," << xvA[1] << std::endl; }
	//     fprintf(fs, "%lf,%lf\n", vA[0], xvA[1]);
	//     y -= ppu*1e-2;
	//   }
	//   fclose(fs);
	//  }
	//  sprintf(fname, "left-%06d.csv", titeration);
	//  sprintf(fnamep, "leftp-%06d.csv", titeration);
	//  sprintf(fname1, "left1-%06d.csv", titeration);
	//  sprintf(fname2, "left2-%06d.csv", titeration);
	//  sprintf(fname4, "left4-%06d.csv", titeration);
	//  f = fopen(fname, "w");
	//  fp = fopen(fnamep, "w");
	//  f1 = fopen(fname1, "w");
	//  f2 = fopen(fname2, "w");
	//  f4 = fopen(fname4, "w");
	//  fprintf(f, "x,y\n");
	//  fprintf(f1, "x,y\n");
	//  fprintf(f2, "x,y\n");
	//  fprintf(f4, "x,y\n");
	//  fprintf(fp, "x,y\n");
	//  //for ( i = 0 ; i <= pc ; i++ ) {
	//  x = 0;
	//  double xold = 0, xint = 0; bool prevdone = false; int j;
	//  Plate* pl = *(platelist.begin());
	//  sprintf(fname, "right-%06d.csv", titeration);
	//  //fprintf(f, "x,y\n");
	//  double firstright = pl->right; bool rightnow = false;
	//  for ( VertexIterator vit(fsline) ; !vit.end() ; ++vit )
	//  { x = zetan1.function_space()->mesh()->geometry().x(vit->index(),0);
	//    double y = zetan1.vector().getitem(vit->index());
	//    if (x >= firstright-EPS && !rightnow) {
	//    	fclose(f);
	//  	f = fopen(fname, "w");
	//	fprintf(f, "x,y\n");
	//	rightnow = true;
	//    }
	//    if (prevdone) {
	//     for ( j = pres-1 ; j > 0 ; j-- ) {
	//      xint = x - j*(x - xold)/(double)pres;
    	//      bool underplate = false;
    	//      for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() && !underplate ; plit++ )
    	//	  underplate = underplate || ( xint >= (*plit)->left - EPS && xint <= (*plit)->right + EPS );
	//      if (!underplate) fprintf(f, "%lf,%lf\n", xint, eval_z(zetan1, xint));
	//     }
	//    } else prevdone = true;
	//    fprintf(f, "%lf,%lf\n", x, y);
	//    xold = x;
	//  }
	//  fclose(f);
	//  while (x <= leftend) {
	//    //double x = leftend*i/(double)pc;
	//    //fprintf(f, "%lf,%lf\n", x, approx_z(zetan1, x));
	//    fprintf(f1, "%lf,%lf\n", x, eval_z(zdtn1, x));
	//    xvA[0] = x; xvA[1] = eval_z(zetan1, x) - 0.1; vA[0] = 0.0;
	//    try {un1.eval(vA, xvA);} catch (std::runtime_error& str)
    	//    { std::cout << "left Error at " << xvA[0] << "," << xvA[1] << " (" << i << std::endl; }
	//    fprintf(f2, "%lf,%lf\n", x, eval_z(utopn1, x));
	//    fprintf(f4, "%lf,%lf\n", x, vA[0]);
	//    x += ppu;
	//    
	//    /*for ( int j = 0 ; j <= ph ; j++ )
	//    {
	//      dolfin::Array<double> xv(2), v(2); xv[0] = x;
	//      xv[1] = (j/(double)ph)*approx_z(zetan1, x, true) -1e-3 + (1-j/(double)ph)*avdepth*0.999;
	//      try {un1.eval(v, xv);} catch (std::runtime_error& str)
    	// 	 { std::cout << "left Error at " << xv[0] << "," << xv[1] << " (" << i << "," << j << ")" << std::endl; mk2_exit(31); }
	//      fprintf(f3, "%lf %lf %lf\n", xv[0], xv[1], v[0]);
	//    }*/
	//    //fprintf(f3, "\n");
	//  }
	//  //fclose(f);
	//  fclose(f1);
	//  fclose(f2);
	//  fclose(f4);
	//  if (platelist.size()>0) {
	//   Plate* pl = *(platelist.begin());
	//   sprintf(fname, "right-%06d.csv", titeration);
	//   //f = fopen(fname, "w");
	//   //fprintf(f, "x,y\n");
	//   x = pl->right;
	//   //for ( i = 0 ; i <= pc ; i++ ) {
	//   while ( x <= bounds[2] ) {
	//    //double x = pl->right + (length-pl->right)*i/(double)pc;
	//    //fprintf(f, "%lf,%lf\n", x, approx_z(zetan1, x));
	//    x += ppu;

	//    /*for ( int j = 0 ; j <= ph ; j++ )
	//    {
	//      dolfin::Array<double> xv(2), v(2); xv[0] = x;
	//      xv[1] = (j/(double)ph)*approx_z(zetan1, x, true) -1e-3 + (1-j/(double)ph)*avdepth*0.999;
	//      try {un1.eval(v, xv);} catch (std::runtime_error& str)
    	// 	 { std::cout << "right Error at " << xv[0] << "," << xv[1] << " (" << i << "," << j << ")" << std::endl; mk2_exit(31); }
	//      fprintf(f3, "%lf %lf %lf\n", xv[0], xv[1], v[0]);
	//    }*/
	//    //fprintf(f3, "\n");
	//   }
	//   //fclose(f);
	//  }
	//  //fclose(f3);
	//  //double xold = 0, xint = 0; bool prevdone = false; int j;
	//  for ( VertexIterator vit(*zetan1.function_space()->mesh()) ; !vit.end() ; ++vit )
	//  { x = zetan1.function_space()->mesh()->geometry().x(vit->index(),0);
	//    double y = zetan1.vector().getitem(vit->index());
	//    fprintf(fp, "%lf,%lf\n", x, y);
	//    xold = x;
	//  }
	//  fclose(fp);
	//    
	//  //std::cout << "[MK2] OUTPUTED HORIZ at frame " << horizfm << std::endl;
	//}
      }
 
      /* Check number of CIs - if too many, adjust alpha */
      /*if ( cl == clmax ) {
        logfile << "Reached maximum correction iterations " << clmax << " at time "
        << t << " (" << titeration << ")\n with our convergence variable still at " 
        << Err << " compared to\n a tolerance of " << tol << std::endl;
	if (alfac > 1e-2)
         logfile << "Continuing with alpha = (" << alpha << "/2) = " << alpha/2 << std::endl;
	else {
	 logfile << "Giving up with alpha at " << alpha << std::endl;
	 mk2_exit(8);
	}
        alpha *= 0.5; alfac *= 0.5; cl = 0;
      }*/
        //*basicfiles["unonz"] << gradu;
    } while ( !leave );
    //std::cout << "===" << t << std::endl;

    /* Return alpha to its original state */
    alpha /= alfac; alfac = 1;

    /* Update the log */
    char outchar = (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) ? '*' : ' ';
    logfile << outchar << "Time: " << t << " (" << titeration << ") #CI:" << cl << " Ddisp: " << Err << " Disp(k-1): " << Err1;
    if (outfreq>1&&titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) logfile << " [Frame " << titeration/outfreq << "]";
    logfile << std::endl;
    if (titeration%10 == 0) {
      logfile << "[T: " << T << " (" << T/Dt << ")] " << (int)(10000*t/T)/100.0 << "%";
      logfile << " PID: " << pid; // We need this to kill it.
      if ( dir_args ) logfile << " Run-name: " << runname;
      if ( default_args ) logfile << " [default]";
      time_t now, taken; time(&now); taken = now - then;
      struct tm * timeinfo = localtime(&now);
      logfile << std::endl << "  Real time: + " << taken/3600 << "h " << (taken/60)%60 << "m " << taken%60 << "s ";
      //proc_t p;
      //look_up_our_self(&p);
      //logfile << std::endl << " Memory: " << p.rss << "KB" << std::endl;
      char sec[3], min[3], mon[3], day[3], year[5]; sprintf(sec, "%2d", timeinfo->tm_sec); sprintf(min, "%2d", timeinfo->tm_min);
      logfile << "(" << timeinfo->tm_hour << ":" << min << ":" << sec << ")";
      if (t > 0) {
       //time_t end; end = then + T*taken/t;
       time_t end; end = now + (taken/ct)*cl_last*(T-t)/Dt;
       struct tm * endinfo = localtime(&end);
       sprintf(sec, "%2d", endinfo->tm_sec); sprintf(min, "%2d", endinfo->tm_min);
       sprintf(day, "%2d", endinfo->tm_mday); sprintf(mon, "%2d", endinfo->tm_mon);
       sprintf(year, "%4d", endinfo->tm_year);
       // Earliest Estimated Finish
       // Current Corrections Finish
       logfile << " CCF: " << endinfo->tm_hour << ":" << min << ":" << sec << " " <<
       	day << "/" << mon << "/" << year;
      }
      logfile << std::endl;
    }

    /* Swap functions for next time-iteration */
    pdtnm3 = pdtnm2;
    pdtnm2 = pdtnm1;
    pdtnm1 = pdtn;
    pdtn   = pdtn1;
    utopn= utopn1;
    zdtnm3 = zdtnm2;
    zdtnm2 = zdtnm1;
    zdtnm1 = zdtn;
    zdtn   = zdtn1;
    zetan  = zetan1;
    for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
      (*plit)->timestep();
    gsunfs = gsun1fs;
    varbottomold = varbottom;
    movebottomold = movebottom;
    // SORT OUT PHIN FOR OVING BOTTOM AND PHIDOTN AS WELL AS NOT NESS BOTTOM!

    if (//t<= 3.651 && // RMV!!!
    titeration%outfreq==0&&(t>=ostarttime&&titeration/outfreq>=ostartframe)) {
        // Save state
         std::string name("fluid");
         /* Swap functions for next time-iteration */
         save_function_vector(pdtnm3, name, "pdtnm3");
         save_function_vector(pdtnm2, name, "pdtnm2");
         save_function_vector(pdtnm1, name, "pdtnm1");
         save_function_vector(pdtn, name, "pdtn");
         save_function_vector(utopn, name, "utopn");
         save_function_vector(zdtnm3, name, "zdtnm3");
         save_function_vector(zdtnm2, name, "zdtnm2");
         save_function_vector(zdtnm1, name, "zdtnm1");
         save_function_vector(zdtn, name, "zdtn");
         save_function_vector(zetan, name, "zetan");

         for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
           (*plit)->save_function_vectors();

         save_function_vector(gsunfs, name, "gsunfs");
         save_function_vector(varbottomold, name, "varbottomold");
         save_function_vector(movebottomold, name, "movebottomold");
         // SORT OUT PHIN FOR OVING BOTTOM AND PHIDOTN AS WELL AS NOT NESS BOTTOM!

         FILE* fstate; fstate = fopen("state.st", "w");
         fprintf(fstate, "t:%lf\n", t);
         fprintf(fstate, "titeration:%d\n", titeration);
         fprintf(fstate, "cl_last:%d\n", cl_last);
         progress = t/T;
	 fclose(fstate);
    }
    /* Augment time variable */
    t += Dt;
    titeration++;
    progress = t/T;
    cl_last = cl;

  }
  for ( PlateList::iterator plit = platelist.begin() ; plit != platelist.end() ; plit++ )
  	(*plit)->shutdown_diagnostics();

  /* Tidy up */
  if (fout) fclose(forcingfile);
  logfile << "Successfully completed." << std::endl;
  return 0;
}
