// THIS FILE IS HEAVILY, HEAVILY BASED ON UnitSquare.cpp FROM THE FENICS PROJECT!!!!
//
// Copyright (C) 2005-2008 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// Modified by Garth N. Wells 2007.
//
// First added:  2005-12-02
// Last changed: 2008-11-13

#define GMSH_PATH "gmsh"
#include <dolfin.h>
#include "FluidMesh.h"
#include "logfile.h"

// Use our own eps, above the compiler's zero!
#define EPS 0.0000000001

using namespace dolfin;

Mesh& generate_mesh_by_gmsh(double width, double height, double draft, double left, double right, double cl[3], double clmd, bool automesh)
{
  double clenfar = cl[1], clennear = cl[0], clenmid = cl[2];
  double inner = 0;//draft*5;
  if (automesh) {
    std::ofstream gmshfile("automesh.geo");
    gmshfile << "Point(0) = { 0, 0, 0, " << clenfar << " };\n";
    gmshfile << "Point(1) = { " << left-inner << ", 0, 0, " << clenfar << " };\n";
    gmshfile << "Point(2) = { " << left << "," << -draft << ", 0, " << clennear << " };\n";
    gmshfile << "Point(3) = { " << left+clmd<< "," << -draft << ", 0, " << clenmid << " };\n";
    gmshfile << "Point(4) = { " << right-clmd << "," << -draft << ", 0, " << clenmid << " };\n";
    gmshfile << "Point(5) = { " << right << "," << -draft << ", 0, " << clennear << " };\n";
    gmshfile << "Point(6) = { " << right+inner << ", 0, 0, " << clenfar << " };\n";
    gmshfile << "Point(7) = { " << width << ", 0, 0, " << clenfar << " };\n";
    gmshfile << "Point(8) = { " << width << ", " << -height << ", 0, " << clenfar <<" };\n";
    gmshfile << "Point(9) = { 0, " << -height << ", 0, " << clenfar << " };\n";
    gmshfile << "Line(0) = {0,1};\n";
    gmshfile << "Line(1) = {1,2};\n";
    gmshfile << "Line(2) = {2,3};\n";
    gmshfile << "Line(3) = {3,4};\n";
    gmshfile << "Line(4) = {4,5};\n";
    gmshfile << "Line(5) = {5,6};\n";
    gmshfile << "Line(6) = {6,7};\n";
    gmshfile << "Line(7) = {7,8};\n";
    gmshfile << "Line(8) = {8,9};\n";
    gmshfile << "Line(9) = {9,0};\n";
    gmshfile << "Line Loop(10) = {0,1,2,3,4,5,6,7,8,9};\n";
    gmshfile << "Plane Surface(0) = { 10 };\n";
    gmshfile.close();
  }
  if (system ( "rm -f tmp.automesh" )) std::cout << "[MK2] FluidMesh: could not remove temporary mesh generation file" << std::endl;
  std::ostringstream gmshcommand; gmshcommand << GMSH_PATH << " -2 automesh.geo -algo del2d >> tmp.automesh";
  if (system ( gmshcommand.str().c_str() ) ||
      system ( "dolfin-convert automesh.msh automesh.xml >> tmp.automesh") ||
      system ( "dolfin-order automesh.xml >> tmp.automesh") )
      std::cout << "[MK2] ERROR IN MESH GENERATION!! ANY FUTURE OUTPUT COULD BE INCORRECT!!" << std::endl;
  logfile << "[Put gmsh & dolfin-convert output in tmp.automesh]" << std::endl;
  Mesh *mesh = new Mesh("automesh.xml");
  for (VertexIterator vit(*mesh) ; !vit.end() ; ++vit)
  {
    mesh->geometry().x(vit->index(),0) *= 5;
  }
//  mesh->order();
  return *mesh;
}

//DUMMY
//-----------------------------------------------------------------------------
FluidMesh::FluidMesh(double width, double height, uint nx, uint ny, double draft, double left, double right, Type type) : Mesh()
{
/*
  double rdraft = height - draft;
  uint nodescx[nx+1][ny+1];
  double cols[nx+1], rows[ny+1];
  uint lcs = (uint)(nx*left/width+0.5), rcs = nx - (uint)(nx*(1-right/width)+0.5), drs = (uint)(ny*rdraft/height);
  if (lcs == rcs) rcs++;

  for ( uint ix = 0 ; ix <= lcs ; ix++ )
  {
    cols[ix] = left*ix/lcs;
  }
  for ( uint ix = lcs+1 ; ix <= rcs ; ix++ )
  {
    cols[ix] = left + (right-left)*(ix-lcs)/(rcs-lcs);
  }
  for ( uint ix = rcs+1 ; ix <= nx ; ix++ )
  {
    cols[ix] = right + (width-right)*(ix-rcs)/(nx-rcs);
  }

  for ( uint iy = 0 ; iy <= drs ; iy++ )
  {
    rows[iy] = rdraft*iy/drs;
  }
  for ( uint iy = drs+1 ; iy <= ny ; iy++ )
  {
    rows[iy] = rdraft + (height-rdraft)*(iy-drs)/(ny-drs);
  }

  // JUST WORK OUT THE FORMULA. nodes_left = lcs+1, nodes_right = nx-rcs
  uint nodes_below = 0, nodes_left = 0, nodes_right = 0;
  bool nbf = false, nlf = false, nrf = false;
  for ( uint iy = 0 ; iy <= ny ; iy++ )
  {
    const double y = rows[iy];
    if (y > rdraft) { nodes_below = iy; nbf = true; break; }
  } if (!nbf) nodes_below = ny+1;
  for (uint ix = 0; ix <= nx; ix++) 
  {
    const double x = cols[ix];
    if (x > left+EPS && !nlf) { nodes_left = ix; nlf = true; }
    if (x >= right-EPS && !nrf) { nodes_right = ix; nrf = true; }
  }
  nodes_right = nx+1-nodes_right;
  logfile << "[FluidMesh] ROWS_BELOW: " << nodes_below << " COLS_LEFT: " << nodes_left << " COLS_RIGHT: " << nodes_right << std::endl;

  // Receive mesh according to parallel policy
//  if (MPI::receive()) { MPIMeshCommunicator::receive(*this); return; }
  
  if ( nx < 1 || ny < 1 )
    error("Size of unit square must be at least 1 in each dimension.");

  rename("mesh", "Mesh of the unit square (0,1) x (0,1) (RESCALED WITH BIT REMOVED!)");

  // Open mesh for editing
  MeshEditor editor;
  editor.open(*this, CellType::triangle, 2, 2);

  // Create vertices and cells:
  if (type == crisscross) 
  {
    editor.init_vertices((nx+1)*(ny+1) + nx*ny);
    editor.init_cells(4*nx*ny);
  } 
  else 
  {
    editor.init_vertices((nx+1)*nodes_below + (nodes_left+nodes_right)*(ny+1-nodes_below));
    editor.init_cells(2*(nx*ny - (nx + 2 - nodes_left - nodes_right)*(ny + 1 - nodes_below)));
//editor.initVertices((nx+1)*ny);//RMV
//editor.initCells(2*nx*(ny-1));//RMV
  }
  
  // Create main vertices:
  uint vertex = 0;
  for (uint iy = 0; iy <= ny; iy++) 
  {
    const double y = rows[iy];
    for (uint ix = 0; ix <= nx; ix++) 
    {
      const double x = cols[ix];
      if ( x <= left+EPS || x >= right-EPS || y <= rdraft+EPS )
      { nodescx[ix][iy] = vertex; editor.addVertex(vertex++, x, y); }
    }
  }
  // Create midpoint vertices if the mesh type is crisscross
  if (type == crisscross) 
  {
  / *
    for (uint iy = 0; iy < ny; iy++) 
    {
      const double y = rows[iy];
      for (uint ix = 0; ix < nx; ix++) 
      {
        const double x = cols[ix];
        if ( x < left+EPS || x > right-EPS || y < rdraft+EPS )
        { nodescx[ix][iy] = vertex; editor.addVertex(vertex++, x, y); }
      }
    }
    * /
  }

  // Create triangles
  uint cell = 0;
  if (type == crisscross) 
  {
    / *for (uint iy = 0; iy < ny; iy++) 
    {
      for (uint ix = 0; ix < nx; ix++) 
      {
        const uint v0 = iy*(nx + 1) + ix;
        const uint v1 = v0 + 1;
        const uint v2 = v0 + (nx + 1);
        const uint v3 = v1 + (nx + 1);
        const uint vmid = (nx + 1)*(ny + 1) + iy*nx + ix;
        
        // Note that v0 < v1 < v2 < v3 < vmid.
        editor.addCell(cell++, v0, v1, vmid);
        editor.addCell(cell++, v0, v2, vmid);
        editor.addCell(cell++, v1, v3, vmid);
        editor.addCell(cell++, v2, v3, vmid);
      }
    }* /
  } 
  else if (type == left ) 
  {
    / *for (uint iy = 0; iy < ny; iy++) 
    {
      for (uint ix = 0; ix < nx; ix++) 
      {
        const uint v0 = iy*(nx + 1) + ix;
        const uint v1 = v0 + 1;
        const uint v2 = v0 + (nx + 1);
        const uint v3 = v1 + (nx + 1);

        editor.addCell(cell++, v0, v1, v2);
        editor.addCell(cell++, v1, v2, v3);
      }
    }* /
  } 
  else 
  { 
    for (uint iy = 0; iy < ny; iy++) 
    {
      for (uint ix = 0; ix < nx; ix++) 
      {
//        if ( (int)iy >= nodes_below-1 ) continue; // RMV
	if ( iy >= nodes_below && ix >= nodes_left && ix < nx+1-nodes_right ) continue;
	if ( iy+1 >= nodes_below && ix+1 >= nodes_left && ix < nx+1-nodes_right ) continue; // not ix+1 to avoid add. triangle

        // Work out the constant to add to the index for the node up one row
//	uint nru = (iy >= nodes_below || (iy==nodes_below-1&&ix>=nodes_left)) ?
//	  (nodes_left+nodes_right) : nx+1;
        const uint v0 = nodescx[ix][iy];
        const uint v1 = nodescx[ix+1][iy];//v0 + 1;
        const uint v2 = nodescx[ix][iy+1];//v0 + nru;
        const uint v3 = nodescx[ix+1][iy+1];//v1 + nru;

        editor.addCell(cell++, v0, v1, v3);
        editor.addCell(cell++, v0, v2, v3);
      }
    }
  }

  // Close mesh editor
  editor.close();

  // Broadcast mesh according to parallel policy
//  if (MPI::broadcast()) { MPIMeshCommunicator::broadcast(*this); }
*/
}
//-----------------------------------------------------------------------------

