#!/bin/bash
source outroot.sh

#watch "tail -n 50 $outroot/output.$1/mk2.$1.log > paste1 && tail -n 50 $outroot/output.$2/mk2.$2.log >paste2 && paste -d \| paste1 paste2 | sed 's/|/  \t\t/g'"
watch "tail -n 50 $outroot/output.$1/mk2.$1.log > paste-$1 && tail -n 50 $outroot/output.$2/mk2.$2.log >paste-$2 && paste -d \| paste-$1 paste-$2 | column -s '|' -t"
