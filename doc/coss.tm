<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  <with|color|red|Antman, 2???> presents a thorough and highly generalized
  exposition of the deformation of elastic rods, based on Cosserat theory. In
  particular, he discusses the geometrically-exact, planar motion of a
  special Cosserat rod; all of our preceding beam theories may be described
  as subsets of this theory for given material constituitive relations and
  simplifications. We introduce some basic principles to illustrate the
  additional generality that may be achieved. However, we contend that the
  models shown thus far are sufficient for our problems, without the
  additional machinery added by this broader formulation. For a more complete
  and formal explanation of this general theory of rod dynamics, the reader
  is directed to Antman's book.

  __

  Antman introduces the notion of a reference configuration, <math|\<cal-B\>>
  and a base curve, <math|<wide*|r|\<wide-bar\>><rsub|0>>, from which
  deformations are considered. For simplicity, we take the vertical
  coordinate of our reference frame to be the beam centre-line in the
  hydrostatic case, that is, our origin is set to
  <math|<around*|(|0,h/2-d|)>>. We then have
  <math|\<cal-B\>=<around*|(|l,r|)>\<times\><around*|(|-h/2,h/2|)>> and
  <math|<wide*|r|\<wide-bar\>><rsub|0>=<around*|(|x,0|)>>. We call a vertical
  slice, <math|\<cal-A\><around*|(|x|)>=<around*|{|x|}>\<times\><around*|(|-h/2,h/2|)>>,
  a cross-section of the reference configuration.

  At a given time <math|t>, the displacement of the centreline is defined by
  <math|<wide*|r|\<wide-bar\>><around*|(|x,t|)>=<around*|(|x+u,w|)>>, using
  the notation of our geometric Euler-Bernoulli exposition. More generally,
  we require a function,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|p|\<wide-bar\>> :
    \<cal-B\>\<times\><around*|[|0,T|]>>|<cell|\<longrightarrow\>>|<cell|\<bbb-R\><rsup|2>,>>|<row|<cell|<around*|(|x,z,t|)>>|<cell|\<longmapsto\>>|<cell|<around*|(|x+<wide|u|^><around*|(|*x,t|)>,<wide|w|^><around|(|x,t|)>|)>,>>>>
  </eqnarray*>

  such that <with|mode|math|<wide*|p|\<wide-bar\>><around*|(|*<wide|\<cal-B\>|\<wide-bar\>>|)>>
  is the closure of our deformed beam and,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|p|\<wide-bar\>><around*|(|<wide*|r|\<wide-bar\>><rsub|0><around*|(|x|)>,t|)>>|<cell|=>|<cell|<wide*|r|\<wide-bar\>><around*|(|x,t|)>.>>>>
  </eqnarray*>

  We may additionally define a set of vectors
  <math|<wide*|d|\<wide-bar\>><rsub|i><around*|(|x,t|)>>, called directors,
  that allow us to describe the deformation of cross-sections through
  <math|<wide*|r|\<wide-bar\>><rsub|0><around*|(|x|)>> via a function
  <math|<wide*|g|\<wide-bar\>>>, such that,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|p|\<wide-bar\>><around*|(|x,z,t|)>>|<cell|=>|<cell|<wide*|r|\<wide-bar\>><around*|(|x,t|)>+<wide*|g|\<wide-bar\>><around*|(|x,z,t,<wide*|d|\<wide-bar\>><rsub|i><around*|(|x,t|)>|)>.>>>>
  </eqnarray*>

  The <math|<around*|(|i+1|)>>-tuple, <math|<around*|(|<wide*|r|\<wide-bar\>>,<wide*|d|\<wide-bar\>><rsub|i>|)>>,
  is referred to as a configuration. To complement the base curve, we define
  some reference directors, <math|<wide*|d|\<wide-bar\>><rsub|i><rsup|\<circ\>>>.
  We then have a configuration, the reference configuration,
  <math|<around*|(|<wide*|r|\<wide-bar\>><rsup|\<circ\>>,<wide*|d|\<wide-bar\>><rsub|i><rsup|\<circ\>>|)>>.
  More exotic forms of Cosserat rod theory than we intend to employ may
  encode higher order terms in the directors, such as the curvature of the
  deformed cross-section.

  For (planar) special Cosserat rod theory we take two orthonormal directors,
  <math|<wide*|d|\<wide-bar\>><rsub|1>> and
  <math|<wide*|d|\<wide-bar\>><rsub|2>>, though we do not yet define them
  explicitly. For simplicity, we introduce the assumption that <math|g> takes
  the form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|g<around*|(|x,z,t,<wide*|d|\<wide-bar\>><rsub|i>|)>>|<cell|=>|<cell|\<varphi\><rsub|1><around*|(|x,z|)>*<wide*|d|\<wide-bar\>><rsub|1><around*|(|x,t|)>+\<varphi\><rsub|2><around*|(|x,z|)>*<wide*|d|\<wide-bar\>><rsub|2>*<around*|(|x,t|)>.>>>>
  </eqnarray*>

  As we expect the directors to somehow describe the cross-sections, it is
  convenient to let the directors in the reference configuration be defined
  as, <math|<wide*|d|\<wide-bar\>><rsup|0><rsub|1>=<around*|(|1,0|)>> and
  <math|<wide*|d|\<wide-bar\>><rsub|2><rsup|\<circ\>>=<around*|(|0,1|)>>,
  such that,

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<cal-A\><around*|(|x|)>>|<cell|=>|<cell|<wide*|r|\<wide-bar\>><rsup|\<circ\>><around*|(|x,t|)>+z*<wide*|d|\<wide-bar\>><rsub|2><rsup|\<circ\>>.>>>>
  </eqnarray*>

  <\with|color|light grey>
    \ and let <math|<wide*|g|\<wide-bar\>><around*|(|x,z,t|)>=z*\<xi\><around*|(|x|)>*<wide*|d|\<wide-bar\>><rsub|2><around*|(|x,t|)>>.
    Thus,

    <\eqnarray*>
      <tformat|<table|<row|<cell|<wide*|p|\<wide-bar\>><around*|(|x,z,t|)>>|<cell|=>|<cell|<wide*|r|\<wide-bar\>><around*|(|x,t|)>+z*<wide*|d<rsub|2>|\<wide-bar\>>.>>>>
    </eqnarray*>

    In particular, this means we expect <math|<wide*|d|\<wide-bar\>><rsub|2><around*|(|x,t|)>>
    to span the deformation at time <math|t> of the cross-section
    <math|\<cal-A\><around*|(|x|)>>. <math|>
  </with>

  By the basic properties of orthonormal vectors in <math|\<bbb-R\><rsup|2>>,
  there must be some <math|\<theta\>>, such that,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|d|\<wide-bar\>><rsub|1>>|<cell|=>|<cell|<around*|(|cos
    \<theta\>,sin \<theta\>|)>,>>|<row|<cell|<wide*|d|\<wide-bar\>><rsub|2>>|<cell|=>|<cell|<around*|(|-sin
    \<theta\>,cos \<theta\>|)>.>>>>
  </eqnarray*>

  Moreover, as any two non-colinear vectors in <math|\<bbb-R\><rsup|2>> form
  a basis, we can write any other vector in <math|\<bbb-R\><rsup|2>> as
  <math|x<rsub|1>*<wide*|d|\<wide-bar\>><rsub|1>+x<rsub|2>*<wide*|d|\<wide-bar\>><rsub|2>>
  for some <math|x<rsub|1>,x<rsub|2>\<in\>\<bbb-R\>>. In particular, let
  <math|\<nu\>,\<eta\>> be such that <math|<wide*|r|\<wide-bar\>><rsub|x>=\<nu\>*<wide*|d|\<wide-bar\>><rsub|1>+\<eta\>*<wide*|d|\<wide-bar\>><rsub|2>>.
  We further define <math|\<mu\>=\<theta\><rsub|x>>. In the reference
  configuration, <math|\<nu\><rsup|\<circ\>>\<equiv\>0>,
  <math|\<eta\><rsup|\<circ\>>\<equiv\>1> and we may correspondingly define
  <math|\<theta\><rsup|\<circ\>>\<equiv\>0>.

  Following a similar process as we did in the Drozdov formulation, we now
  consider the forces and moments acting on a deformed beam segment. At
  present, it is convenient to leave these expressions in vector form,
  without choosing bases. We let <math|<wide*|n|\<wide-bar\>><rsup|+>> be the
  force exerted on a segment by the material to its right and
  <math|-<wide*|n|\<wide-bar\>><rsup|->> be the force exerted by the material
  to its left. Calling the body force <math|<wide*|f|\<wide-bar\>>>, we may
  equate the impulse due to those forces over time with the consequent change
  in momentum,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|t<rsub|a>><rsup|t<rsub|b>><around*|[|<wide*|n|\<wide-bar\>><rsup|+>-<wide*|n|\<wide-bar\>><rsup|->+<big-around|\<int\>|<rsub|seg>><wide*|f|\<wide-bar\>>
    d x|]>> d t>|<cell|=>|<cell|<big-around|\<int\>|<rsub|seg>>\<rho\>*A*<around*|(|<wide*|r|\<wide-bar\>><rsub|t><around*|(|t<rsub|b>|)>-<wide*|r|\<wide-bar\>><rsub|t><around*|(|t<rsub|a>|)>|)>
    d x.>>>>
  </eqnarray*>

  That the expression for the change in momentum for a beam involves only the
  centre of mass of each cross-section, is logical, but in the general case,
  the justification and, in particular, the origin of the <math|\<rho\>*A>
  coefficient is more subtle. We have made some implicit modelling decisions,
  such as the centroidal nature of <math|r<rsup|\<circ\>><around*|(|x|)>> and
  its straightness. Antman provides a fuller, abstract investigation of
  modelling choices affecting this physical equation and its component terms.

  If we may assume the smoothness of these functions, we may divide by the
  segment length (in the reference configuration) and the time interval,
  taking the limit as they go to zero to obtain,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|n|\<wide-bar\>><rsub|x>+<wide*|f|\<wide-bar\>>>|<cell|=>|<cell|\<rho\>*A*<wide*|r|\<wide-bar\>><rsub|t
    t>.>>>>
  </eqnarray*>

  We can apply a similar process to the moments about the origin, acting on a
  segment. If we call the (resultant) external moment
  <math|<wide*|l|\<wide-bar\>>=<around*|(|0,0,l|)>>, we obtain,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|m|\<wide-bar\>><rsub|x>+<around*|(|<wide*|r|\<wide-bar\>>\<times\><wide*|n|\<wide-bar\>>|)><rsub|x>+<wide*|r|\<wide-bar\>>\<times\><wide*|f|\<wide-bar\>>+<wide*|l|\<wide-bar\>>>|<cell|=>|<cell|\<rho\>*A*<wide*|r|\<wide-bar\>>\<times\><wide*|r|\<wide-bar\>><rsub|t
    t>+\<rho\>*J*<wide*|d|\<wide-bar\>><rsub|2>\<times\><wide*|d|\<wide-bar\>><rsub|2,t
    t>,>>>>
  </eqnarray*>

  where <math|J> is the second moment of area. The precise derivation of the
  second term on the right-hand side is again subtle, but it is worth noting
  that this equation is much simplified by the assumption of uniformity in
  the beam. Note that <math|<wide*|d|\<wide-bar\>><rsub|2>\<times\><wide*|d|\<wide-bar\>><rsub|2,t
  t>=\<theta\><rsub|t t>*<wide*|k|\<wide-bar\>>>, where
  <math|<wide*|k|\<wide-bar\>>> is the unit vector in the third dimension.

  Reducing the second of these two equations using the first, we may state
  the special Cosserat rod theory in the dynamic, planar case as,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|n|\<wide-bar\>><rsub|x>+<wide*|f|\<wide-bar\>>>|<cell|=>|<cell|\<rho\>*A*<wide*|r|\<wide-bar\>><rsub|t
    t>,>>|<row|<cell|<wide*|m|\<wide-bar\>><rsub|x>+<wide*|r|\<wide-bar\>><rsub|x>\<times\><wide*|n|\<wide-bar\>>+<wide*|l|\<wide-bar\>>>|<cell|=>|<cell|\<rho\>*J*\<theta\><rsub|t
    t>*<wide*|k|\<wide-bar\>>.>>>>
  </eqnarray*>

  To derive scalar equations, we suppose <math|N>, <math|H> and <math|M> such
  that,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide*|n|\<wide-bar\>>>|<cell|=>|<cell|N*<wide*|d|\<wide-bar\>><rsub|1>+H*<wide*|d|\<wide-bar\>><rsub|2>>>|<row|<cell|<wide*|m|\<wide-bar\>>>|<cell|=>|<cell|M*<wide*|k|\<wide-bar\>>>>|<row|<cell|<wide*|n|\<wide-bar\>><rsub|x>>|<cell|=>|<cell|N<rsub|x>*<wide*|d|\<wide-bar\>><rsub|1>+N*\<theta\><rsub|x>*<wide*|d|\<wide-bar\>><rsub|1,\<theta\>>+H<rsub|x>*<wide*|d|\<wide-bar\>><rsub|2>+H*\<theta\><rsub|x>*<wide*|d|\<wide-bar\>><rsub|2,\<theta\>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|N<rsub|x>-H*\<theta\><rsub|x>|)>*<wide*|d|\<wide-bar\>><rsub|1>+<around*|(|H<rsub|x>+N*\<theta\><rsub|x>|)>*<wide*|d|\<wide-bar\>><rsub|2>>>|<row|<cell|<wide*|r|\<wide-bar\>><rsub|x>\<times\><wide*|n|\<wide-bar\>>>|<cell|=>|<cell|<around*|(|\<nu\>*<wide*|d|\<wide-bar\>><rsub|1>+\<mu\>*<wide*|d|\<wide-bar\>><rsub|2>|)>\<times\><around*|(|N*<wide*|d|\<wide-bar\>><rsub|1>+H*<wide*|d|\<wide-bar\>><rsub|2>|)>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|\<nu\>*H-\<mu\>*N|)>*<wide*|k|\<wide-bar\>>>>>>
  </eqnarray*>

  By our definitions of <math|\<nu\>>, <math|\<mu\>>, we then have,

  <\eqnarray*>
    <tformat|<table|<row|<cell|N<rsub|x>-H*\<theta\><rsub|x>+<wide*|f|\<wide-bar\>>\<cdot\><wide*|d|\<wide-bar\>><rsub|1>>|<cell|=>|<cell|\<rho\>*A*<wide*|r|\<wide-bar\>><rsub|t
    t>\<cdot\><wide*|d|\<wide-bar\>><rsub|1>>>|<row|<cell|H<rsub|x>+N*\<theta\><rsub|x>+<wide*|f|\<wide-bar\>>\<cdot\><wide*|d|\<wide-bar\>><rsub|2>>|<cell|=>|<cell|\<rho\>*A*<wide*|r|\<wide-bar\>><rsub|t
    t>\<cdot\><wide*|d|\<wide-bar\>><rsub|2>>>|<row|<cell|M<rsub|x>+\<nu\>*H-\<mu\>*N+l>|<cell|=>|<cell|\<rho\>*J*\<theta\><rsub|t
    t>.>>>>
  </eqnarray*>

  Note that, while we have made modelling assumptions, we have made no
  significance approximations. We have not yet given physical meaning to our
  directors, or to the majority of variables in the above expression, but to
  progress any further we must become more specific about our interpretation
  and, in particular, our elastic constituitive equations. This set of
  equations gives us a general framework into which the majority of beam
  theories we would wish to consider will fit. We will refer to it again in
  Chapter <inactive|<hybrid|ch-further-extensions>>
</body>