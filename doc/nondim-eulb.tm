<TeXmacs|1.0.7.3>

<style|generic>

<\body>
  Rearranging Timoshenko and introducing a term <math|M>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-(<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>)>|<cell|=>|<cell|<wide|M|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>>>|<row|<cell|<wide|M|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>>|<cell|=>|<cell|\<um\>(E*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>)<rsub|<wide|x|\<wide-bar\>>>,>>>>
  </eqnarray*>

  which gives, assuming zero constant of integration,

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-(<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>)>|<cell|=>|<cell|<wide|M|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>>>|<row|<cell|<wide|M|\<wide-bar\>>>|<cell|=>|<cell|\<um\>(E*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>),>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|M=<wide|M|\<wide-bar\>>>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<rho\>*A*L|T<rsup|2>>*<wide|w|\<ddot\>>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*L*w>|<cell|=>|<cell|<frac|1|L<rsup|2>>*M<rsub|x
    x>>>|<row|<cell|M>|<cell|=>|<cell|\<um\><frac|1|L>*E*I*w<rsub|x x>>>>>
  </eqnarray*>

  Let <math|q=L<rsup|2>*<wide|q|\<wide-bar\>>>,
  <math|c<rsub|1>=<frac|\<rho\>*A*L<rsup|3>|T<rsup|2>>>,
  <math|c<rsub|2>=<frac|1|L>*E*I>, <math|c<rsub|4>=\<rho\><rsub|w>*g*L<rsup|3>>,
  then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|M<rsub|x
    x>>>|<row|<cell|M>|<cell|=>|<cell|\<um\>c<rsub|2>*w<rsub|x x>>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big|int>v*<wide|w|\<ddot\>> d
    x-<big|int>v*q*d x+c<rsub|4><big|int>v*w*d
    x>|<cell|=>|<cell|\<um\><big|int>v<rsub|x>*M<rsub|x> d
    x+<left|[>v*M<rsub|x><right|]><rsup|1><rsub|0>>>|<row|<cell|<big|int>u*M
    d x>|<cell|=>|<cell|c<rsub|2>*<big|int>u<rsub|x>*w<rsub|x> d
    x-<left|[>c<rsub|2>*u*w<rsub|x><right|]><rsup|1><rsub|0>>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><left|{>c<rsub|1>*v*<wide|w|\<ddot\>>+v<rsub|x>*M<rsub|x>+c<rsub|4>*v*w<right|}>
    \ d x-<left|[>v*M<rsub|x><right|]><rsup|1><rsub|0>>|<cell|=>|<cell|<big|int>v*q
    d x>>|<row|<cell|<big|int><left|{>u*M-c<rsub|2>*u<rsub|x>*w<rsub|x><right|}>
    d x+<left|[>c<rsub|2>*u*w<rsub|x><right|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|page-type|letter>
  </collection>
</initial>