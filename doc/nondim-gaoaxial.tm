<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  Adapted from the Andrews et al (2010) Gao beam formulation, neglecting
  horizontal traction:

  <\eqnarray*>
    <tformat|<cwith|1|5|2|2|cell-halign|r>|<table|<row|<cell|<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-<around|(|1/\<rho\><rsub|b>*A|)>*<around|(|<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>|)>>|<cell|=>|<cell|k*<wide|\<mu\>|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>+a*<around*|[|<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>*<around*|(|<wide|u|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|)>|]><rsub|<wide|x|\<wide-bar\>>>>>|<row|<cell|<wide|\<mu\>|\<wide-bar\>>>|<cell|\<assign\>>|<cell|\<um\><wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|x|\<wide-bar\>>>,>>|<row|<cell|<around*|(|<wide|u|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>+<frac|1|2>*<around*|(|1+\<nu\>|)>*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|)><rsub|<wide|x|\<wide-bar\>>>>|<cell|=>|<cell|0>>|<row|<cell|k>|<cell|\<assign\>>|<cell|2*<around|(|h/2|)><rsup|3>*E/3*<around|(|1-\<nu\><rsup|2>|)>*\<rho\><rsub|b>*A>>|<row|<cell|a>|<cell|\<assign\>>|<cell|E*h/<around*|(|1-\<nu\>|)>*\<rho\><rsub|b>*A>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|\<theta\>=<wide|\<theta\>|\<wide-bar\>>>,
  <math|\<mu\>=L*<wide|\<mu\>|\<wide-bar\>>>, <math|u=<wide|u|\<wide-bar\>>>
  then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|L|T<rsup|2>>*<wide|w|\<ddot\>>-<frac|1|\<rho\><rsub|b>*A>*<wide|q|\<wide-bar\>>+<frac|\<rho\><rsub|w>*g*L|\<rho\><rsub|b>*A>*w>|<cell|=>|<cell|<frac|k|L<rsup|3>>*\<mu\><rsub|x
    x>+<frac|a|L>*<around*|[|w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>|]><rsub|x>>>|<row|<cell|\<mu\>>|<cell|=>|<cell|\<um\>w<rsub|x
    x>>>|<row|<cell|<around*|(|u<rsub|x>+<frac|1|2>*<around*|(|1+\<nu\>|)>*w<rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  Let <math|q=<frac|L<rsup|2>|\<rho\><rsub|b>*A>*<wide|q|\<wide-bar\>>>,
  <math|c<rsub|1>=<frac|L<rsup|3>|T<rsup|2>>>, <math|c<rsub|2>=<frac|k|L>>,
  <math|c<rsub|3>=<frac|1|2>*<around*|(|1+\<nu\>|)>>,
  <math|c<rsub|4>=<frac|\<rho\><rsub|w>*g*L<rsup|3>|\<rho\><rsub|b>*A>>,
  <math|c<rsub|5>=a*L>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|c<rsub|2>*\<mu\><rsub|x
    x>+c<rsub|5>*<around*|[|w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>|]><rsub|x>>>|<row|<cell|\<mu\>>|<cell|=>|<cell|\<um\>w<rsub|x
    x>>>|<row|<cell|<around*|(|u<rsub|x>+c<rsub|3>*w<rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big-around|\<int\>|v*<wide|w|\<ddot\>>
    d x>-<big-around|\<int\>|v*q*d x>+c<rsub|4><big-around|\<int\>|v*w*d
    x>>|<cell|=>|<cell|\<um\>c<rsub|2>*<big-around|\<int\>|v<rsub|x>*\<mu\><rsub|x>
    d x>-c<rsub|5>*<big-around|\<int\>|v<rsub|x>*w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>*d
    x>>>|<row|<cell|>|<cell|>|<cell|+c<rsub|2>*<around*|[|v*\<mu\><rsub|x>|]><rsup|1><rsub|0>+c<rsub|5>*<around*|[|v*w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>|]><rsup|1><rsub|0>>>|<row|<cell|<big-around|\<int\>|\<zeta\>*\<mu\>
    \ d x>>|<cell|=>|<cell|<big-around|\<int\>|\<zeta\><rsub|x>*w<rsub|x> d
    x>-<around*|[|\<zeta\>*w<rsub|x>|]><rsup|1><rsub|0>>>|<row|<cell|<big-around|\<int\>|W<rsub|x>*<around*|(|u<rsub|x>+c<rsub|3>*w<rsub|x><rsup|2>|)>
    \ d x>-<around*|[|W*<around*|(|u<rsub|x>+c<rsub|3>*w<rsub|x><rsup|2>|)>|]><rsub|0><rsup|1>>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|<around*|{|c<rsub|1>*v*<wide|w|\<ddot\>>+c<rsub|2>*v<rsub|x>*\<mu\><rsub|x>+c<rsub|4>*v*w+c<rsub|5>*v<rsub|x>*w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>|}>
    \ d x>-c<rsub|2>*<around*|[|v*\<mu\><rsub|x>|]><rsup|1><rsub|0>-c<rsub|5>*<around*|[|v*w<rsub|x>*<around*|(|u<rsub|x>+w<rsub|x><rsup|2>|)>|]><rsup|1><rsub|0>>|<cell|=>|<cell|<big-around|\<int\>|v*q
    d x>>>|<row|<cell|<big-around|\<int\>|<around*|{|\<zeta\>*\<mu\>-\<zeta\><rsub|x>*w<rsub|x>|}>
    d x>+<around*|[|\<zeta\>*w<rsub|x>|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>|<row|<cell|<big-around|\<int\>|W<rsub|x>*<around*|(|u<rsub|x>+c<rsub|3>*w<rsub|x><rsup|2>|)>
    \ d x>-<around*|[|W*<around*|(|u<rsub|x>+c<rsub|3>*w<rsub|x><rsup|2>|)>|]><rsub|0><rsup|1>>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  <\with|color|light grey>
    <emdash>

    <strong|Dropping overbars for simplicity and setting <math|h> as half
    <math|h> above!>

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h>\<sigma\><rsup|x>\|<rsub|x=0,L>*\<delta\>
      u d y>>|<cell|=>|<cell|0<eq-number><label|bc1>>>|<row|<cell|<big-around|\<int\>|<rsup|h><rsub|\<um\>h>
      y*\<sigma\><rsup|x>\|<rsub|x=0,L>*\<delta\> u d
      y>>|<cell|=>|<cell|0<eq-number><label|bc2>>>|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h><around|(|y*\<sigma\><rsup|x><rsub|x>+<around|(|\<sigma\><rsup|x>+\<sigma\><rsup|y>|)>*w<rsub|x>|)>\|<rsub|x=0,L>*\<delta\>
      w d y>>|<cell|=>|<cell|0<eq-number><label|bc3>>>>>
    </eqnarray*>

    Treating BC <reference|bc1>, (NB: we assume <math|\<lambda\>=0> from
    Gao's paper)

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h>u<rsub|x>>-y*w<rsub|x
      x>+<frac|1+\<nu\>|2>* w<rsub|x><rsup|2>*d y>|<cell|=>|<cell|0<text| at
      >x=0,L>>|<row|<cell|<around*|[|y*u<rsub|x>-<frac|1|2>*y<rsup|2>*w<rsub|x
      x>+<frac|1+\<nu\>|2>*y*w<rsub|x><rsup|2>|]><rsub|\<um\>h><rsup|h>>|<cell|=>|<cell|<text|<with|font-base-size|7|[divided
      through by <math|E/<around|(|1-\<nu\><rsup|2>|)>>]>>>>|<row|<cell|h*<around*|(|2*u<rsub|x>+w<rsub|x><rsup|2>|)>>|<cell|=>|<cell|>>|<row|<cell|0>|<cell|=>|<cell|>>>>
    </eqnarray*>

    Treating BC <reference|bc2>,

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h>y*u<rsub|x>>-y<rsup|2>*w<rsub|x
      x>+<frac|1+\<nu\>|2>*y*w<rsub|x><rsup|2> d y>|<cell|=>|<cell|0<text| at
      >x=0,L>>|<row|<cell|<around*|[|\<um\><frac|y<rsup|3>|3>*w<rsub|x
      x>|]><rsup|h><rsub|\<um\>h>>|<cell|=>|<cell|0>>|<row|<cell|w<rsub|x
      x>>|<cell|=>|<cell|>>>>
    </eqnarray*>

    Treating BC <reference|bc3>,

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h>y*<around*|[|u<rsub|x
      x>-y*w<rsub|x x x>+<around|(|1+\<nu\>|)>*w<rsub|x>*w<rsub|x
      x>|]>>>|<cell|>|<cell|>>|<row|<cell|+w<rsub|x>*<around*|(|<around*|[|u<rsub|x>-y*w<rsub|x
      x>+<frac|<around|(|1+\<nu\>|)>|2>*w<rsub|x><rsup|2>|]>+<around*|[|<frac|1|2>*w<rsub|x><rsup|2>+\<nu\>*u<rsub|x>-y*\<nu\>*w<rsub|x
      x>+<frac|\<nu\>|2>*w<rsub|x><rsup|2>|]>|)> d y>|<cell|=>|<cell|0<text|
      at >x=0,L>>|<row|<cell|<big-around|\<int\>|<rsub|\<um\>h><rsup|h>y*<around*|[|u<rsub|x
      x>-y*w<rsub|x x x>|]>>+w<rsub|x>*<around|(|1+\<nu\>|)>*<around*|[|u<rsub|x>+w<rsub|x><rsup|2>|]>*d
      y>|<cell|=>|<cell|>>|<row|<cell|<around*|[|\<um\><frac|y<rsup|3>|3>*w<rsub|x
      x x>+y*w<rsub|x>*<around|(|1+\<nu\>|)>*<around*|[|u<rsub|x>+w<rsub|x><rsup|2>|]>|]><rsub|\<um\>h><rsup|h>>|<cell|=>|<cell|>>|<row|<cell|\<um\><frac|1|3>*h<rsup|3>*w<rsub|x
      x x>+w<rsub|x>*<around|(|1+\<nu\>|)>*<around|(|u<rsub|x>+w<rsub|x><rsup|2>|)>>|<cell|=>|<cell|>>|<row|<cell|\<um\><frac|1|3>*h<rsup|3>*w<rsub|x
      x x>+<around|(|1-\<nu\><rsup|2>|)>*w<rsub|x><rsup|3>>|<cell|=>|<cell|>>|<row|<cell|w<rsub|x
      x x>>|<cell|=>|<cell|<with|font-base-size|7|<text| [w/o high order
      term>]>>>>>
    </eqnarray*>

    ____________________________

    <\eqnarray*>
      <tformat|<table|<row|<cell|U>|<cell|=>|<cell|<frac|1|2>*<big-around|\<int\>|<rsub|\<Beta\>>\<sigma\><rsub|i
      j>*\<epsilon\><rsub|i j>*d\<Beta\>>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<big-around|\<int\>|<rsub|0><rsup|L><big-around|\<int\>|<rsub|-h/2><rsup|h/2>>\<sigma\><rsub|11>*\<epsilon\><rsub|11>+\<sigma\><rsub|22>*\<epsilon\><rsub|22>*d
      z*d x>>>|<row|<cell|>|<cell|=>|<cell|<frac|E|2*<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L><big-around|\<int\>|<rsub|-h/2><rsup|h/2>>\<epsilon\><rsub|11><rsup|2>+2*\<nu\>*\<epsilon\><rsub|11>*\<epsilon\><rsub|22>+\<epsilon\><rsub|22><rsup|2>*d
      z*d x>>>|<row|<cell|>|<cell|=>|<cell|<frac|E|2*<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L><big-around|\<int\>|<rsub|-h/2><rsup|h/2>>\<epsilon\><rsub|1
      1>*<around*|(|\<epsilon\><rsub|11>+2*\<nu\>*\<epsilon\><rsub|2
      2>|)>+\<epsilon\><rsub|2 2><rsup|2>*d z*d
      x>>>|<row|<cell|>|<cell|=>|<cell|<frac|E|2*<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L><big-around|\<int\>|<rsub|-h/2><rsup|h/2>><around*|(|u<rsub|0,x>-z*w<rsub|0,x
      x>+<frac|1|2>*w<rsub|0,x><rsup|2>|)>*<around*|(|u<rsub|0,x>-z*w<rsub|0,x
      x>+<frac|1|2>*<around*|(|1+2*\<nu\>|)>*w<rsub|0,x><rsup|2>|)>+<frac|1|4>*w<rsub|0,x><rsup|2>*d
      z*d x>>>|<row|<cell|>|<cell|=>|<cell|<frac|E*h|2*<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L>u<rsup|2><rsub|0,x>+<frac|h<rsup|2>|12>*w<rsup|2><rsub|0,x
      x>+<frac|1|4>*<around*|(|1+2*\<nu\>|)>*w<rsub|0,x><rsup|4>+<around*|(|1+\<nu\>|)>*u<rsub|0,x>*w<rsub|0,x><rsup|2>+<frac|1|4>*w<rsub|0,x><rsup|4>*d
      x>>>|<row|<cell|>|<cell|=>|<cell|<frac|E*h|2*<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L>u<rsup|2><rsub|0,x>+<frac|h<rsup|2>|12>*w<rsub|0,x
      x><rsup|2>+<frac|1|2>*<around*|(|1+\<nu\>|)>*w<rsub|0,x><rsup|4>+<around*|(|1+\<nu\>|)>*u<rsub|0,x>*w<rsub|0,x><rsup|2>*d
      z*d x>>>|<row|<cell|\<Rightarrow\>>|<cell|>|<cell|>>|<row|<cell|\<delta\>
      U>|<cell|=>|<cell|<frac|E*h|<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L>u<rsub|0,x>*<around*|(|\<delta\>
      u<rsub|0>|)><rsub|x>+<frac|h<rsup|2>|12>*w<rsub|0,x
      x>*<around*|(|\<delta\> w<rsub|0>|)><rsub|x
      x>>+<around*|(|1+\<nu\>|)>*w<rsub|0,x><rsup|3>*<around*|(|\<delta\>w<rsub|0>|)><rsub|x>>>|<row|<cell|>|<cell|>|<cell|+<around*|(|1+\<nu\>|)>*<around*|(|<frac|1|2>*w<rsup|2><rsub|0,x>*<around*|(|\<delta\>
      u<rsub|0>|)><rsub|x>+u<rsub|0,x>*w<rsub|0,x>*<around*|(|\<delta\>
      w<rsub|0>|)><rsub|x>|)>*d x>>|<row|<cell|>|<cell|=>|<cell|<frac|E*h|<around*|(|1-\<nu\><rsup|2>|)>>*<big-around|\<int\>|<rsub|0><rsup|L>-u<rsub|0,x
      x>*\<delta\> u<rsub|0>+<frac|h<rsup|2>|12>*w<rsub|0,x x x x>>*\<delta\>
      w<rsub|0>-3*<around*|(|1+\<nu\>|)>*w<rsub|0,x><rsup|2>*w<rsub|0,x
      x>*\<delta\> w<rsub|0>>>|<row|<cell|>|<cell|>|<cell|-<around*|(|1+\<nu\>|)>*<around*|(|w<rsub|0,x
      x>*w<rsub|0,x>*\<delta\> u<rsub|0>+<around*|(|u<rsub|0,x
      x>*w<rsub|0,x>+u<rsub|0,x>*w<rsub|0,x x>|)>*\<delta\> w<rsub|0>|)>*d
      x>>>>
    </eqnarray*>
  </with>
</body>

<\initial>
  <\collection>
    <associate|par-hyphen|normal>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|bc1|<tuple|1|?>>
    <associate|bc2|<tuple|2|?>>
    <associate|bc3|<tuple|3|?>>
  </collection>
</references>