<TeXmacs|1.0.7.14>

<style|generic>

<\body>
  Adapted from the Lagnese & Leugering (1991) von K�rm�n beam formulation (p.
  361):

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|t
    t>-\<rho\>*I*<wide|w|\<wide-bar\>><rsub|t t x
    x>-<wide|\<mu\>|\<wide-bar\>><rsub|x x>-E*A*<around*|[|<wide|w|\<wide-bar\>><rsub|x>*<around*|(|<wide|u|\<wide-bar\>><rsub|x>+<frac|1|2>*<wide|w|\<wide-bar\>><rsub|x><rsup|2>|)>|]><rsub|x>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>>|<cell|=>|<cell|0>>|<row|<cell|\<rho\>*A*<wide|u|\<wide-bar\>><rsub|t
    t>-E*A*<around*|(|<wide|u|\<wide-bar\>><rsub|x>+<frac|1|2>*<wide|w|\<wide-bar\>><rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|0>>|<row|<cell|<wide|\<mu\>|\<wide-bar\>>>|<cell|=>|<cell|\<um\><around*|(|E*I*<wide|w|\<wide-bar\>><rsub|x
    x>|)>>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|u=<wide|u|\<wide-bar\>>/L>,
  <math|\<mu\>=<wide|\<mu\>|\<wide-bar\>>>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|L|T<rsup|2>>*<wide|w|\<ddot\>>-<frac|I|A*T<rsup|2>*L>*<wide|w|\<ddot\>><rsub|x
    x>-<frac|1|\<rho\><rsub|b>*A*L<rsup|2>>*\<mu\><rsub|x
    x>-<frac|E|\<rho\><rsub|b>*L>*<around|[|w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsub|x>-<frac|1|\<rho\><rsub|b>*A>*<wide|q|\<wide-bar\>>+<frac|\<rho\><rsub|w>*g*L|\<rho\><rsub|b>*A>*w>|<cell|=>|<cell|0>>|<row|<cell|<frac|1|T<rsup|2>>*<wide|u|\<ddot\>>-<frac|E|\<rho\><rsub|b>*L>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsup|2><rsub|x>|)><rsub|x>>|<cell|=>|<cell|0>>|<row|<cell|\<mu\>>|<cell|=>|<cell|-<frac|E*I|L>*w<rsub|x
    x>>>>>
  </eqnarray*>

  Let <math|q=<frac|L<rsup|2>|\<rho\><rsub|b>*A>*<wide|q|\<wide-bar\>>>,
  <math|c<rsub|1>=<frac|L<rsup|3>|T<rsup|2>>>,
  <math|c<rsub|2>=<frac|L*I|A*T<rsup|2>>>,
  <math|c<rsub|3>=<frac|1|\<rho\><rsub|b>*A>>,
  <math|c<rsub|4>=<frac|E*L|\<rho\><rsub|b>>>,
  <math|c<rsub|5>=<frac|\<rho\><rsub|w>*g*L<rsup|3>|\<rho\><rsub|b>*A>>,
  <math|c<rsub|6>=<frac|E*T<rsup|2>|\<rho\><rsub|b>*L>>,
  <math|c<rsub|7>=<frac|E*I|L>> then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-c<rsub|2>*<wide|w|\<ddot\>><rsub|x
    x>-c<rsub|3>*\<mu\><rsub|x x>-c<rsub|4>*<around*|[|w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsub|x>-q+c<rsub|5>*w>|<cell|=>|<cell|0>>|<row|<cell|<wide|u|\<ddot\>>-c<rsub|6>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x>|)><rsub|x>>|<cell|=>|<cell|0>>|<row|<cell|\<mu\>>|<cell|=>|<cell|-c<rsub|7>*w<rsub|x
    x>>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big|int>v*<wide|w|\<ddot\>> d
    x+c<rsub|2>*<big|int>v<rsub|x>*<wide|w|\<ddot\>><rsub|x> d
    x-<big|int>v*q*d x+c<rsub|5><big|int>v*w*d
    x-c<rsub|2>*<around*|[|v*<wide|w|\<ddot\>><rsub|x>|]><rsup|1><rsub|0>>|<cell|=>|<cell|-c<rsub|4>*<big|int>v<rsub|x>*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>
    d x>>|<row|<cell|>|<cell|>|<cell|+c<rsub|4>*<around*|[|v*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsub|0><rsup|1>>>|<row|<cell|>|<cell|>|<cell|-c<rsub|3>*<big|int>v*<rsub|x>\<mu\><rsub|x>*d
    x>>|<row|<cell|>|<cell|>|<cell|+c<rsub|3>*<around|[|v*\<mu\><rsub|x>|]><rsub|0><rsup|1>>>|<row|<cell|<big|int>t*<wide|u|\<ddot\>>
    \ d x+c<rsub|6>*<big|int>t<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x>|)>
    d x>|<cell|=>|<cell|<around*|[|t*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x>|)>|]><rsup|1><rsub|0>>>|<row|<cell|<big|int>\<lambda\>*\<mu\>*d
    x>|<cell|=>|<cell|c<rsub|7>*<big|int>\<lambda\>*<rsub|x>w<rsub|x>*d
    x-c<rsub|7>*<around|[|\<lambda\>*w<rsub|x>|]><rsub|0><rsup|1>>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><around*|{|c<rsub|1>*v*<wide|w|\<ddot\>>+c<rsub|2>*v<rsub|x>*<wide|w|\<ddot\>><rsub|x>+c<rsub|3>*v<rsub|x>*\<mu\><rsub|x>+c<rsub|5>*v*w+c<rsub|4>*v<rsub|x>*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|}>
    \ d x>|<cell|=>|<cell|<big|int>v*q d x>>|<row|<cell|-c<rsub|2>*<around|[|v*<wide|w|\<ddot\>><rsub|x>|]><rsub|0><rsup|1>-c<rsub|3>*<around|[|v*\<mu\><rsub|x>|]><rsub|0><rsup|1>-c<rsub|4>*<around*|[|v*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsub|0><rsup|1>>|<cell|>|<cell|>>|<row|<cell|<big|int><around*|{|t*<wide|u|\<ddot\>>+c<rsub|6>*t<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x>|)>|}>
    d x-<around*|[|t*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>|<row|<cell|<big|int>\<lambda\>*\<mu\>-c<rsub|7>*\<lambda\>*<rsub|x>w<rsub|x>*d
    x+c<rsub|7>*<around|[|\<lambda\>*w<rsub|x>|]><rsub|0><rsup|1>>|<cell|=>|<cell|0.>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|par-hyphen|normal>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|bc1|<tuple|1|?>>
    <associate|bc2|<tuple|2|?>>
    <associate|bc3|<tuple|3|?>>
  </collection>
</references>