<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  Adapted from the Euler-Bernoulli formulation:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-<around|(|<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>|)>>|<cell|=>|<cell|M<rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>>>|<row|<cell|M>|<cell|=>|<cell|\<um\><frac|2*E*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>|2*<around|[|1+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|]><rsup|3/2>-h*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|x|\<wide-bar\>>>>,>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|\<theta\>=<wide|\<theta\>|\<wide-bar\>>>,
  then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<rho\>*A*L|T<rsup|2>>*<wide|w|\<ddot\>>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*L*w>|<cell|=>|<cell|<frac|1|L<rsup|2>>*M<rsub|x
    x>>>|<row|<cell|M>|<cell|=>|<cell|\<um\><frac|2*E*I*w<rsub|x
    x>/L|2*<around|[|1+w<rsub|x><rsup|2>|]><rsup|3/2>-h*w<rsub|x x>/L>>>>>
  </eqnarray*>

  Let <math|q=L<rsup|2>*<wide|q|\<wide-bar\>>>,
  <math|c<rsub|1>=<frac|\<rho\>*A*L<rsup|3>|T<rsup|2>>>,
  <math|c<rsub|2>=<frac|1|L>*E*I>, <math|c<rsub|3>=h/2*L>,
  <math|c<rsub|4>=\<rho\><rsub|w>*g*L<rsup|3>>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|-c<rsub|2>*<around*|[|w<rsub|x
    x>/R|]><rsub|x x>>>|<row|<cell|R>|<cell|=>|<cell|<around|[|1+w<rsub|x><rsup|2>|]><rsup|3/2>-c<rsub|3>*w<rsub|x
    x>>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big-around|\<int\>|v*<wide|w|\<ddot\>>
    d x>-<big-around|\<int\>|v*q*d x>+c<rsub|4><big-around|\<int\>|v*w*d
    x>>|<cell|=>|<cell|c<rsub|2>*<big-around|\<int\>|v<rsub|x>*<around*|(|w<rsub|x
    x>/R|)><rsub|x> d x>+<around*|[|v*<around*|(|w<rsub|x
    x>/R|)><rsub|x>|]><rsup|1><rsub|0>>>|<row|<cell|<big-around|\<int\>|u*R d
    x>>|<cell|=>|<cell|-c<rsub|3>*<big-around|\<int\>|u<rsub|x>*w<rsub|x> d
    x>+<big-around|\<int\>|u*<around*|(|1+w<rsub|x><rsup|2>|)><rsup|3/2> d
    x>+<around*|[|c<rsub|3>*u*w<rsub|x>|]><rsup|1><rsub|0>>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|<around*|{|c<rsub|1>*v*<wide|w|\<ddot\>>-v<rsub|x
    x>*<around*|(|w<rsub|x x>/R|)>+c<rsub|4>*v*w|}> \ d
    x>-<around*|[|v*<around*|(|w<rsub|x x>/R|)><rsub|x>|]><rsup|1><rsub|0>+<around*|[|v<rsub|x>*<around*|(|w<rsub|x
    x>/R|)>|]><rsup|1><rsub|0>>|<cell|=>|<cell|<big-around|\<int\>|v*q d
    x>>>|<row|<cell|<big-around|\<int\>|<around*|{|u*R+c<rsub|3>*u<rsub|x>*w<rsub|x>-u*<around*|(|1+w<rsub|x><rsup|2>|)><rsup|3/2>|}>
    d x>-<around*|[|c<rsub|3>*u*w<rsub|x>|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|page-type|letter>
    <associate|par-hyphen|normal>
  </collection>
</initial>