<TeXmacs|1.0.7.3>

<style|generic>

<\body>
  From wiki:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-(<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>)>|<cell|=>|<cell|\<kappa\>*A*G*(<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>-<wide|\<theta\>|\<wide-bar\>>)<rsub|<wide|x|\<wide-bar\>>>>>|<row|<cell|\<rho\>*I<wide|\<theta\>|\<wide-bar\>>*<rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>>|<cell|=>|<cell|(E*I*<wide|\<theta\>|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>)<rsub|<wide|x|\<wide-bar\>>>+\<kappa\>*A*G*(<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>-<wide|\<theta\>|\<wide-bar\>>)>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|\<theta\>=<wide|\<theta\>|\<wide-bar\>>>,
  then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<rho\>*A*L|T<rsup|2>>*<wide|w|\<ddot\>>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*L*w>|<cell|=>|<cell|<frac|\<kappa\>*A*G|L>*(w<rsub|x>-\<theta\>)<rsub|x>>>|<row|<cell|<frac|\<rho\>*I*|T<rsup|2>>*<wide|\<theta\>|\<ddot\>>>|<cell|=>|<cell|<frac|1|L<rsup|2>>*E*I*\<theta\><rsub|x
    x>+\<kappa\>*A*G*(w<rsub|x>-\<theta\>)>>>>
  </eqnarray*>

  Let <math|q=<frac|L|\<kappa\>*A*G>*<wide|q|\<wide-bar\>>>,
  <math|c<rsub|1>=<frac|\<rho\>*L<rsup|2>|\<kappa\>*G*T<rsup|2>>>,
  <math|c<rsub|2>=<frac|\<rho\>*I|\<kappa\>*A*G*T<rsup|2>>>,
  <math|c<rsub|3>=<frac|E*I|\<kappa\>*A*G*L<rsup|2>>>,
  <math|c<rsub|4>=<frac|\<rho\><rsub|w>*g*L<rsup|2>|\<kappa\>*A*G>>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|(w<rsub|x>-\<theta\>)<rsub|x>>>|<row|<cell|c<rsub|2>*<wide|\<theta\>|\<ddot\>>>|<cell|=>|<cell|c<rsub|3>*\<theta\><rsub|x
    x>+(w<rsub|x>-\<theta\>)>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big|int>v*<wide|w|\<ddot\>> d
    x-<big|int>v*q*d x+c<rsub|4><big|int>v*w*d
    x>|<cell|=>|<cell|\<um\><big|int>v<rsub|x>*(w<rsub|x>-\<theta\>) d
    x+<left|[>v*(w<rsub|x>-\<theta\>)<right|]><rsup|1><rsub|0>>>|<row|<cell|c<rsub|2>*<big|int>u*<wide|\<theta\>|\<ddot\>>
    d x>|<cell|=>|<cell|\<um\>c<rsub|3>*<big|int>u<rsub|x>*\<theta\><rsub|x>
    d x+<big|int>u*(w<rsub|x>-\<theta\>) d
    x+<left|[>c<rsub|3>*u*\<theta\><rsub|x><right|]><rsup|1><rsub|0>>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><left|{>c<rsub|1>*v*<wide|w|\<ddot\>>+v<rsub|x>*(w<rsub|x>-\<theta\>)+c<rsub|4>*v*w<right|}>
    \ d x-<left|[>v*(w<rsub|x>-\<theta\>)<right|]><rsup|1><rsub|0>>|<cell|=>|<cell|<big|int>v*q
    d x>>|<row|<cell|<big|int><left|{>c<rsub|2>*u*<wide|\<theta\>|\<ddot\>>+c<rsub|3>*u<rsub|x>*\<theta\><rsub|x>-u*(w<rsub|x>-\<theta\>)<right|}>
    d x-<left|[>c<rsub|3>*u*\<theta\><rsub|x><right|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|page-type|letter>
  </collection>
</initial>