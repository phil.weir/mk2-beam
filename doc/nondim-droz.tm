<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  Adapted from Drozdov (p. 196) beam formulation, neglecting horizontal
  traction:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-<around|(|<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>|)>>|<cell|=>|<cell|<around|(|<wide|N|\<wide-bar\>>*sin
    \<alpha\>-<around|(|1-sin<rsup|2> \<alpha\>|)>*<wide|M|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>|)><rsub|<wide|x|\<wide-bar\>>>>>|<row|<cell|<wide|N|\<wide-bar\>>>|<cell|=>|<cell|E*b*h*<around*|(|<around|[|1+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|]><rsup|1/2>-1|)>>>|<row|<cell|<wide|M|\<wide-bar\>>>|<cell|=>|<cell|<frac|E*b*h<rsup|3>|12>*<around*|[|1+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|]><rsup|\<um\>3/2>*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|x|\<wide-bar\>>>>>|<row|<cell|sin
    \<alpha\>>|<cell|=>|<cell|<around*|[|1+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|]><rsup|\<um\>1/2>*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>>>|<row|<cell|b>|<cell|=>|<cell|1>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|N=<wide|N|\<wide-bar\>>>,
  <math|M=<wide|M|\<wide-bar\>>/L> then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<rho\>*A*L|T<rsup|2>>*<wide|w|\<ddot\>>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*L*w>|<cell|=>|<cell|<frac|1|L>*<around|(|N*s-M<rsub|x>+s<rsup|2>*M<rsub|x>|)><rsub|x>>>|<row|<cell|M>|<cell|=>|<cell|<frac|E*h<rsup|3>|12*L<rsup|2>>*<around|[|1+w<rsub|x><rsup|2>|]><rsup|\<um\>3/2>*w<rsub|x
    x>>>|<row|<cell|N>|<cell|=>|<cell|E*h*<around*|(|<around|[|1+w<rsub|x><rsup|2>|]><rsup|1/2>-1|)>>>|<row|<cell|s>|<cell|=>|<cell|<around|[|1+w<rsub|x><rsup|2>|]><rsup|\<um\>1/2>*w<rsub|x>>>>>
  </eqnarray*>

  Let <math|q=L*<wide|q|\<wide-bar\>>>, <math|c<rsub|1>=<frac|\<rho\>*A*L<rsup|2>|T<rsup|2>>>,
  <math|c<rsub|2>=<frac|E*h<rsup|3>|12*L<rsup|2>>>, <math|c<rsub|3>=E*h>,
  <math|c<rsub|4>=\<rho\><rsub|w>*g*L<rsup|2>>. Further set
  <math|\<omega\>=<around|[|1+w<rsub|x><rsup|2>|]><rsup|\<um\>1/2>>,
  <math|s=\<omega\>*w<rsub|x>> and <math|N=c<rsub|3>*<around|(|\<omega\><rsup|\<um\>1>-1|)>>.
  While we should use <math|\<xi\>=N*s-M<rsub|x>-s<rsup|2>*M<rsub|x>>, we
  follow Drozdov in taking <with|mode|math|\<xi\>=N*s-M<rsub|x>>. Then,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|\<xi\><rsub|x>>>|<row|<cell|M>|<cell|=>|<cell|c<rsub|2>*\<omega\><rsup|3>*w<rsub|x
    x>>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big-around|\<int\>|v*<wide|w|\<ddot\>>
    d x>-<big-around|\<int\>|v*q*d x>+c<rsub|4><big-around|\<int\>|v*w*d
    x>>|<cell|=>|<cell|\<um\><big-around|\<int\>|v<rsub|x>*\<xi\> d
    x>+<around*|[|v*\<xi\>|]><rsup|1><rsub|0>>>|<row|<cell|<big-around|\<int\>|u*M
    \ d x>>|<cell|=>|<cell|\<um\>c<rsub|2><big-around|\<int\>|<around*|(|u*\<omega\><rsup|3>|)><rsub|x>*w<rsub|x>
    d x>+c<rsub|2>*<around*|[|u*\<omega\><rsup|3>*w<rsub|x>|]><rsup|1><rsub|0>>>>>
  </eqnarray*>

  Rearranged for test and trial,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|<around*|{|c<rsub|1>*v*<wide|w|\<ddot\>>+v<rsub|x>*\<xi\>+c<rsub|4>*v*w|}>
    \ d x>-<around*|[|v*\<xi\>|]><rsup|1><rsub|0>>|<cell|=>|<cell|<big-around|\<int\>|v*q
    d x>>>|<row|<cell|<big-around|\<int\>|<around*|{|u*M+c<rsub|2>*<around*|(|u*\<omega\><rsup|3>|)><rsub|x>*w<rsub|x>|}>
    d x>-c<rsub|2>*<around*|[|u*\<omega\><rsup|3>*w<rsub|x>|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|par-hyphen|normal>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|bc1|<tuple|1|?>>
    <associate|bc2|<tuple|2|?>>
    <associate|bc3|<tuple|3|?>>
  </collection>
</references>