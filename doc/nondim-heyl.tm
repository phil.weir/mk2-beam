<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  Adapted from the Heyliger-Reddy, 2004 formulation (<math|f> set to
  <math|0>)

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<rho\>*A*<wide|w|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>>
    <wide|t|\<wide-bar\>>>-<around*|(|<frac|1|21>*\<rho\>*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>
    <wide|t|\<wide-bar\>> <wide|t|\<wide-bar\>>>-<frac|16|105>*\<rho\>*I*<wide|\<psi\>|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>><wide|t|\<wide-bar\>>>|)><rsub|<wide|x|\<wide-bar\>>>-<around|(|<wide|q|\<wide-bar\>>-\<rho\><rsub|w>*g*<wide|w|\<wide-bar\>>|)>>|<cell|=>|<cell|<eq-number>>>|<row|<cell|<around*|(|A*E*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>*<around*|(|<wide|u|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>+<frac|1|2>*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|)>+<frac|8|15>*G*A*<around*|(|<wide|\<psi\>|\<wide-bar\>>+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>|)>|)><rsub|<wide|x|\<wide-bar\>>>-<around*|(|<frac|1|21>*E*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|x|\<wide-bar\>>>-<frac|16|105>*E*I*<wide|\<psi\>|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>|)><rsub|<wide|x|\<wide-bar\>>
    <wide|x|\<wide-bar\>>>>|<cell|>|<cell|>>|<row|<cell|<around*|(|A*E*<around*|(|<wide|u|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>+<frac|1|2>*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>><rsup|2>|)>|)><rsub|<wide|x|\<wide-bar\>>>>|<cell|=>|<cell|A*\<rho\>*<wide|u|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>><wide|t|\<wide-bar\>>><eq-number>>>|<row|<cell|\<um\><around*|(|<frac|68|105>*E*I*<wide|\<psi\>|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>-<frac|16|105>*E*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|x|\<wide-bar\>>>|)><rsub|<wide|x|\<wide-bar\>>>+<frac|8|15>*G*A*<around*|(|\<psi\>+<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>>>|)>>|<cell|=>|<cell|<eq-number>>>|<row|<cell|\<um\><frac|68|105>*\<rho\>*I*<wide|\<psi\>|\<wide-bar\>><rsub|<wide|t|\<wide-bar\>><wide|t|\<wide-bar\>>>+<frac|16|105>*\<rho\>*I*<wide|w|\<wide-bar\>><rsub|<wide|x|\<wide-bar\>><wide|t|\<wide-bar\>><wide|t|\<wide-bar\>>>>|<cell|>|<cell|>>>>
  </eqnarray*>

  Set <math|x=<wide|x|\<wide-bar\>>/L>, <math|t=<wide|t|\<wide-bar\>>/T>,
  <math|w=<wide|w|\<wide-bar\>>/L>, <math|u=<wide|u|\<wide-bar\>>/L>,
  <math|\<psi\>=<wide|\<psi\>|\<wide-bar\>>> then,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<rho\>*A*L|T<rsup|2>>*<wide|w|\<ddot\>>-<around*|(|<frac|\<rho\>*I|21*L*T<rsup|2>>*w<rsub|x
    t t>-<frac|16*\<rho\>*I|105*L*T<rsup|2>>*\<psi\><rsub|t
    t>|)><rsub|x>-<wide|q|\<wide-bar\>>+\<rho\><rsub|w>*g*L*w>|<cell|=>|<cell|>>|<row|<cell|<around*|(|<frac|A*E*|L>w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>+<frac|8*G*A|15*L>*<around*|(|\<psi\>+w<rsub|x>|)>|)><rsub|x>-<around*|(|<frac|E*I|21*L<rsup|3>>*w<rsub|x
    x>-<frac|16*E*I|105*L<rsup|3>>*\<psi\><rsub|x>|)><rsub|x
    x>>|<cell|>|<cell|>>|<row|<cell|<frac|A*E|L>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|<frac|A*\<rho\>|T<rsup|2>>*u<rsub|t
    t>>>|<row|<cell|\<um\><around*|(|<frac|68*E*I|105*L<rsup|2>>*\<psi\><rsub|x>-<frac|16*E*I|105*L<rsup|2>>*w<rsub|x
    x>|)><rsub|x>+<frac|8*G*A|15>*<around*|(|\<psi\>+w<rsub|x>|)>>|<cell|=>|<cell|>>|<row|<cell|\<um\><frac|68*\<rho\>*I|105*T<rsup|2>>*\<psi\><rsub|t
    t>+<frac|16*\<rho\>*I|105*T<rsup|2>>*w<rsub|x t t>>|<cell|>|<cell|>>>>
  </eqnarray*>

  Let <math|q=<wide|q|\<wide-bar\>>>, <math|c<rsub|1>=<frac|\<rho\>*A*L|T<rsup|2>>>,
  <math|c<rsub|2>=<frac|\<rho\>*I|21*L*T<rsup|2>>>,
  <math|c<rsub|3>=<frac|16*\<rho\>*I|105*L*T<rsup|2>>>,
  <math|c<rsub|4>=\<rho\><rsub|w>*g*L>, <math|c<rsub|5>=<frac|A*E|L>>,
  <math|c<rsub|6>=<frac|8*G*A|15*L>>, <math|c<rsub|7>=<frac|E*I|21*L<rsup|3>>>,
  <math|c<rsub|8>=<frac|16*E*I|105*L<rsup|3>>>,
  <math|c<rsub|9>=<frac|A*\<rho\>|T<rsup|2>>>,
  <math|c<rsub|10>=<frac|68*E*I|105*L<rsup|2>>>,
  <math|c<rsub|11>=<frac|16*E*I|105*L<rsup|2>>>,
  <math|c<rsub|12>=<frac|8*G*A|15>>, <math|c<rsub|13>=<frac|68*\<rho\>*I|105*T<rsup|2>>>,
  <math|c<rsub|14>=<frac|16*\<rho\>*I|105*T<rsup|2>>>, then

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<wide|w|\<ddot\>>-<around|(|c<rsub|2>*w<rsub|x
    t t>-c<rsub|3>*\<psi\><rsub|t t>|)><rsub|x>-q+c<rsub|4>*w>|<cell|=>|<cell|<around*|(|c<rsub|5>*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>+c<rsub|6>*<around*|(|\<psi\>+w<rsub|x>|)>|)><rsub|x>-<around*|(|c<rsub|7>*w<rsub|x
    x>-c<rsub|8>*\<psi\><rsub|x>|)><rsub|x
    x>>>|<row|<cell|c<rsub|5>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|c<rsub|9>*u<rsub|t
    t>>>|<row|<cell|\<um\><around*|(|c<rsub|10>*\<psi\><rsub|x>-c<rsub|1
    1>*w<rsub|x x>|)><rsub|x>+c<rsub|12>*<around|(|\<psi\>+w<rsub|x>|)>>|<cell|=>|<cell|\<um\>c<rsub|13>*\<psi\><rsub|t
    t>+c<rsub|14>*w<rsub|x t t>>>>>
  </eqnarray*>

  In variational form,

  <\eqnarray*>
    <tformat|<table|<row|<cell|c<rsub|1>*<big-around|\<int\>|v*<wide|w|\<ddot\>>
    d x>+<big-around|\<int\>|v<rsub|x>*<around|(|c<rsub|2>*w<rsub|x t
    t>-c<rsub|3>*\<psi\><rsub|t t>|)>*d x>-<big-around|\<int\>|v*q*d
    x>+c<rsub|4><big-around|\<int\>|v*w*d
    x>>|<cell|>|<cell|>>|<row|<cell|+<big-around|\<int\>|v<rsub|x>*<around*|[|c<rsub|5>*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>+c<rsub|6>*<around*|(|\<psi\>+w<rsub|x>|)>|]>*d
    x>+<big-around|\<int\>|v<rsub|x x>*<around|(|c<rsub|7>*w<rsub|x
    x>-c<rsub|8>*\<psi\><rsub|x>|)>*d x>>|<cell|>|<cell|>>|<row|<cell|-<around*|[|v*<around*|(|c<rsub|5>*w<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>+c<rsub|6>*<around*|(|\<psi\>+w<rsub|x>|)>|)>|]><rsub|0><rsup|1>+<around*|[|v*<around|(|c<rsub|7>*w<rsub|x
    x>-c<rsub|8>*\<psi\><rsub|x>|)><rsub|x>|]><rsub|0><rsup|1>>|<cell|>|<cell|>>|<row|<cell|-<around*|[|v*<around|(|c<rsub|2>*w<rsub|x
    t t>-c<rsub|3>*\<psi\><rsub|t t>|)>|]><rsub|0><rsup|1>-<around*|[|v<rsub|x>*<around|(|c<rsub|7>*w<rsub|x
    x>-c<rsub|8>*\<psi\><rsub|x>|)>|]><rsup|1><rsub|0>>|<cell|=>|<cell|0>>|<row|<cell|c<rsub|9><big-around|\<int\>|t*u<rsub|t
    t>*d x>+c<rsub|5>*<big-around|\<int\>|t<rsub|x>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>*d
    x>-<around*|[|c<rsub|5>*t*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>|]><rsub|0><rsup|1>>|<cell|=>|<cell|0>>|<row|<cell|<big-around|\<int\>|\<chi\><rsub|x>*<around|(|c<rsub|10>*\<psi\><rsub|x>-c<rsub|11>*w<rsub|x
    x>|)>*d x>+<big-around|\<int\>|c<rsub|12>*\<chi\>*<around|(|\<psi\>+w<rsub|x>|)>*d
    x>>|<cell|>|<cell|>>|<row|<cell|+<big-around|\<int\>|c<rsub|13>*\<chi\>*\<psi\><rsub|t
    t>*d x>-<big-around|\<int\>|c<rsub|14>*\<chi\>*w<rsub|x t t>*d
    x>-<around*|[|\<chi\>*<around|(|c<rsub|10>*\<psi\><rsub|x>-c<rsub|11>*w<rsub|x
    x>|)>|]><rsub|0><rsup|1>>|<cell|=>|<cell|0>>>>
  </eqnarray*>

  To reduce to Timo:

  <math|c<rsub|5>\<rightarrow\>0>

  <math|<around|(|c<rsub|7>,c<rsub|8>|)>\<rightarrow\><around|(|c<rsub|7>+c<rsub|8>,
  0|)>>

  <math|<around|(|c<rsub|10>,c<rsub|11>|)>\<rightarrow\><around|(|c<rsub|10>+c<rsub|11>,
  0|)>>

  <math|<around|(|c<rsub|13>,c<rsub|14>|)>\<rightarrow\><around|(|c<rsub|13>+c<rsub|14>,
  0|)>>

  <\eqnarray*>
    <tformat|<table|<row|<cell|105*a<rsub|1>*<wide|w|\<ddot\>>-<around|(|5*a<rsub|2>*w<rsub|x
    t t>-16*a<rsub|3>*\<psi\><rsub|t t>|)><rsub|x>-105*q+105*c<rsub|4>*w>|<cell|=>|<cell|56*a<rsub|6>*<around*|(|\<psi\>+w<rsub|x>|)><rsub|x>-21*a<rsub|7>*w<rsub|x
    x x x>>>|<row|<cell|c<rsub|5>*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)><rsub|x>>|<cell|=>|<cell|c<rsub|9>*u<rsub|t
    t>>>|<row|<cell|a<rsub|10>*\<psi\><rsub|x
    x>-<frac|14|21>*a<rsub|12>*<around|(|\<psi\>+w<rsub|x>|)>>|<cell|=>|<cell|a<rsub|13>*\<psi\><rsub|t
    t>>>>>
  </eqnarray*>

  Then, taking <math|w<rsub|x x x x>\<approx\>\<um\>\<psi\><rsub|x x x>> and
  <math|w<rsub|t t x>\<approx\>\<um\>\<psi\><rsub|t t>>, assuming matching
  <math|a>'s,

  <\eqnarray*>
    <tformat|<table|<row|<cell|105*a<rsub|1>*<wide|w|\<ddot\>>-105*q+105*c<rsub|4>*w>|<cell|=>|<cell|<around*|(|56*+14|)>*r*<around*|(|\<psi\>+w<rsub|x>|)><rsub|x>>>|<row|<cell|a<rsub|10>*\<psi\><rsub|x
    x>-<frac|2|3>*a<rsub|12>*<around|(|\<psi\>+w<rsub|x>|)>>|<cell|=>|<cell|a<rsub|13>*\<psi\><rsub|t
    t>>>>>
  </eqnarray*>

  So,

  <\eqnarray*>
    <tformat|<table|<row|<cell|a<rsub|1>*<wide|w|\<ddot\>>-q+c<rsub|4>*w>|<cell|=>|<cell|<frac|2|3>*r*<around*|(|\<psi\>+w<rsub|x>|)><rsub|x>>>|<row|<cell|a<rsub|10>*\<psi\><rsub|x
    x>-<frac|2|3>*a<rsub|12>*<around|(|\<psi\>+w<rsub|x>|)>>|<cell|=>|<cell|a<rsub|13>*\<psi\><rsub|t
    t>>>>>
  </eqnarray*>

  <emdash><emdash><emdash>

  Bending moment:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|z*\<sigma\><rsub|x x> d
    A>>|<cell|=>|<cell|<big-around|\<int\>|<rsub|\<um\>h/2><rsup|h/2><big-around|\<int\>|<rsub|0><rsup|b>E*z*<around*|(|u<rsub|x>+z*<around*|(|\<psi\><rsub|x
    x>-<frac|4|3>*<around*|(|<frac|z|h>|)><rsup|2>*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>|)>+<frac|1|2>*w<rsub|x><rsup|2>|)>*d y*d
    z>>>>|<row|<cell|>|<cell|=>|<cell|E*<big-around|\<int\>|<rsub|\<um\>h/2><rsup|h/2>z<rsup|2>*\<psi\><rsub|x>-<frac|4|3>*<frac|z<rsup|4>|h<rsup|2>>*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)> d z>>>|<row|<cell|>|<cell|=>|<cell|<frac|E*h<rsup|3>|12>*\<psi\><rsub|x>-<frac|1|60>*E*h<rsup|3>*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>>>|<row|<cell|>|<cell|=>|<cell|E*I*\<psi\><rsub|x>-<frac|1|5>*E*I*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>>>>>
  </eqnarray*>

  Shear:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|\<sigma\><rsub|x x> d
    A>>|<cell|=>|<cell|<big-around|\<int\>|<rsub|\<um\>h/2><rsup|h/2><big-around|\<int\>|<rsub|0><rsup|b>E*<around*|(|u<rsub|x>+z*<around*|(|\<psi\><rsub|x
    x>-<frac|4|3>*<around*|(|<frac|z|h>|)><rsup|2>*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>|)>+<frac|1|2>*w<rsub|x><rsup|2>|)>*d y*d
    z>>>>|<row|<cell|>|<cell|=>|<cell|E*h*<around*|(|u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|E*h<rsup|3>|12>*\<psi\><rsub|x>-<frac|1|60>*E*h<rsup|3>*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>>>|<row|<cell|>|<cell|=>|<cell|E*I*\<psi\><rsub|x>-<frac|1|5>*E*I*<around*|(|\<psi\><rsub|x>+w<rsub|x
    x>|)>>>>>
  </eqnarray*>
</body>

<\initial>
  <\collection>
    <associate|par-hyphen|normal>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|bc1|<tuple|4|?>>
    <associate|bc2|<tuple|5|?>>
    <associate|bc3|<tuple|6|?>>
  </collection>
</references>