<TeXmacs|1.0.7.3>

<style|generic>

<\body>
  As per Heyliger & Reddy, 1988:

  <\with|font-base-size|8>
    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|\<partial\>|\<partial\> x>
      <left|{>A*E*<left|[><frac|\<partial\> u|\<partial\>
      x>+<frac|1|2>*<left|(><frac|\<partial\> w|\<partial\>
      x><right|)><rsup|2><right|]><right|}>+f>|<cell|=>|<cell|A*\<rho\>*<frac|\<partial\><rsup|2>
      u|\<partial\> t<rsup|2>>,<eq-number>>>|<row|<cell|<frac|\<partial\>|\<partial\>
      x> <left|{>A*E*<frac|\<partial\> w|\<partial\>
      x>*<left|[><frac|\<partial\> u|\<partial\>
      x>+<frac|1|2>*<left|(><frac|\<partial\> w|\<partial\>
      x><right|)><rsup|2><right|]>+<frac|8|15>*G*A*<left|(>\<psi\>+<frac|\<partial\>
      w|\<partial\> x><right|)><right|}>>|<cell|>|<cell|<eq-number>>>|<row|<cell|-<frac|\<partial\><rsup|2>|\<partial\>
      x<rsup|2>>*<left|[><frac|1|21>*E*I*<frac|\<partial\><rsup|2>
      w|\<partial\> x<rsup|2>>-<frac|16|105>*E*I*<frac|\<partial\>
      \<psi\>|\<partial\> x><right|]>+q>|<cell|=>|<cell|\<rho\>*A*<frac|\<partial\><rsup|2>
      w|\<partial\> t<rsup|2>>-<frac|\<partial\>|\<partial\>
      x>*<left|[><frac|1|21>*\<rho\>*I-<frac|\<partial\><rsup|3>
      w|\<partial\> t<rsup|2>*\<partial\>
      x>-<frac|16|105>*\<rho\>*I*<frac|\<partial\><rsup|2>
      \<psi\>|\<partial\> t<rsup|2>><right|]>,>>|<row|<cell|\<um\><frac|\<partial\>|\<partial\>
      x>*<left|[><frac|68|105>*E*I*<frac|\<partial\> \<psi\>|\<partial\>
      x>-<frac|16|105>*E*I*<frac|\<partial\><rsup|2>w|\<partial\>
      x<rsup|2>><right|]>+<frac|8|15>*G*A*<left|(>\<psi\>+<frac|\<partial\>
      w|\<partial\> x><right|)>>|<cell|=>|<cell|\<um\><frac|68|105>*E*I*<frac|\<partial\><rsup|2>
      \<psi\>|\<partial\> t<rsup|2>>+<frac|16|105>*\<rho\>*I*<frac|\<partial\><rsup|3>
      w|\<partial\> x \<partial\> t<rsup|2>><eq-number>>>>>
    </eqnarray*>
  </with>

  In variational form,

  <\with|font-base-size|8>
    <\eqnarray*>
      <tformat|<table|<row|<cell|\<um\><big|int>{A*E*<left|[><frac|\<partial\>
      u|\<partial\> x>+<frac|1|2>*<left|(><frac|\<partial\> w|\<partial\>
      x><right|)><rsup|2><right|]>*<frac|\<partial\> U|\<partial\>
      x>+A*E*<left|[><frac|\<partial\> u|\<partial\>
      x>+<frac|1|2>*<left|(><frac|\<partial\> w|\<partial\>
      x><right|)><rsup|2><right|]>*<frac|\<partial\> w|\<partial\>
      x>*<frac|\<partial\> W|\<partial\> x>>|<cell|>|<cell|>>|<row|<cell|+E*I*<frac|\<partial\>
      \<psi\>|\<partial\> x>*<frac|\<partial\> \<Psi\>|\<partial\>
      x>-<frac|16|105>*E*I*<left|[><left|(><frac|\<partial\>
      \<psi\>|\<partial\> x>+<frac|\<partial\><rsup|2> w|\<partial\>
      x<rsup|2>><right|)>*<frac|\<partial\> \<Psi\>|\<partial\>
      x>+<left|(><frac|\<partial\> \<Psi\>|\<partial\>
      x>+<frac|\<partial\><rsup|2> W|\<partial\>
      x<rsup|2>><right|)>*<frac|\<partial\> \<psi\>|\<partial\>
      x><right|]>>|<cell|>|<cell|>>|<row|<cell|+<frac|E*I|21>*<with|color|red|<left|(>\<um\><frac|\<partial\>
      \<psi\>|\<partial\> x>*<frac|\<partial\> \<Psi\>|\<partial\>
      x>+<frac|\<partial\><rsup|2> w|\<partial\>
      x<rsup|2>>*<frac|\<partial\><rsup|2> W|\<partial\>
      x<rsup|2>><right|)>>+<frac|8|15>*G*A*<left|(>\<psi\>+<frac|\<partial\>
      w|\<partial\> x><right|)>*<left|(>\<Psi\>+<frac|\<partial\>
      W|\<partial\> x><right|)>>|<cell|>|<cell|>>|<row|<cell|-q*W-f*U}*\<mathd\>
      x>|<cell|>|<cell|>>|<row|<cell|<with|color|red|-><big|int>{\<rho\>*A*<wide|u|\<ddot\>>*U+\<rho\>*<left|[><frac|68|105>*I*<wide|\<psi\>|\<ddot\>>-<frac|16|105>*I*<frac|\<partial\>
      <wide|w|\<ddot\>>|\<partial\> x><right|]>*\<Psi\>>|<cell|>|<cell|>>|<row|<cell|+\<rho\>*<left|[><frac|I|21>*<frac|\<partial\>
      <wide|w|\<ddot\>>|\<partial\> x>-<frac|16|105>*I*<wide|\<psi\>|\<ddot\>><right|]>*<frac|\<partial\>
      W|\<partial\> x>+\<rho\>*A*<wide|w|\<ddot\>>*W}*\<mathd\>
      x>|<cell|>|<cell|>>>>
    </eqnarray*>
  </with>

  Note that the above includes the highlighted correction

  <\equation*>
    <left|(><frac|\<partial\> \<psi\>|\<partial\>
    x>+<frac|\<partial\><rsup|2> w|\<partial\>
    x<rsup|2>><right|)>*<left|(><frac|\<partial\> \<Psi\>|\<partial\>
    x>+<frac|\<partial\><rsup|2> W|\<partial\>
    x<rsup|2>><right|)>\<longrightarrow\><left|(>\<um\><frac|\<partial\>
    \<psi\>|\<partial\> x>*<frac|\<partial\> \<Psi\>|\<partial\>
    x>+<frac|\<partial\><rsup|2> w|\<partial\>
    x<rsup|2>>*<frac|\<partial\><rsup|2> W|\<partial\> x<rsup|2>><right|)>
  </equation*>

  Rewritten:

  <with|font-base-size|8|<\eqnarray*>
    <tformat|<table|<row|<cell|<big|int>U<rsub|x>*<left|{>A*E*<left|[>u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2><right|]><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W<rsub|x>*<left|{>A*E*<left|[>u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2><right|]>*w<rsub|x>+<frac|8|15>*G*A*<left|(>\<psi\>+w<rsub|x><right|)><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\><rsub|x>*<left|{>E*I*\<psi\><rsub|x>-<frac|16|105>*E*I*(2*\<psi\><rsub|x>+w<rsub|x
    x>)-<frac|E*I|21>*\<psi\><rsub|x><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W<rsub|x
    x>*<left|{>\<um\><frac|16|105>*E*I*\<psi\><rsub|x>+<frac|E*I|21>*w<rsub|x
    x>)<right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\>*G*A*<left|{><frac|8|15>*(\<psi\>+w<rsub|x>)<right|}>>|<cell|>|<cell|>>|<row|<cell|=<big|int>W*<left|{>q-\<rho\>*A*<wide|w|\<ddot\>><right|}>+<big|int>U*<left|{>f-\<rho\>*A*<wide|u|\<ddot\>><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\>*<left|{>\<um\>\<rho\>*<left|[><frac|68|105>*I*<wide|\<psi\>|\<ddot\>>-<frac|16|105>*I*<wide|w|\<ddot\>><rsub|x><right|]><right|}>+<big|int>W<rsub|x>*<left|{>\<um\>\<rho\>*<left|[><frac|I|21>*<wide|w|\<ddot\>><rsub|x>-<frac|16|105>*I*<wide|\<psi\>|\<ddot\>><right|]><right|}>>|<cell|>|<cell|>>>>
  </eqnarray*>>

  Let <math|\<xi\>=A*E*<left|[>u<rsub|x>+<frac|1|2>*w<rsub|x><rsup|2><right|]>,
  \<eta\>=\<psi\>+w<rsub|x>>;

  <with|font-base-size|8|<\eqnarray*>
    <tformat|<table|<row|<cell|<big|int>U<rsub|x>*\<xi\>+<big|int>W<rsub|x>*<left|{>\<xi\>*w<rsub|x>+<frac|8|15>*G*A*\<eta\><right|}>+<big|int>\<Psi\><rsub|x>*E*I*<left|{>\<um\><frac|16|105>*w<rsub|x
    x>+<frac|68|105>*\<psi\><rsub|x><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W<rsub|x
    x>*<left|{>\<um\><frac|16|105>*E*I*\<psi\><rsub|x>+<frac|1|21>*E*I*w<rsub|x
    x><right|}>+<frac|8|15>*<big|int>G*A*\<Psi\>*\<eta\>>|<cell|>|<cell|>>|<row|<cell|=<big|int>W*<left|{>q-\<rho\>*A*<wide|w|\<ddot\>><right|}>+<big|int>U*<left|{>f-\<rho\>*A*<wide|u|\<ddot\>><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\>*<left|{>\<um\>\<rho\>*<left|[><frac|68|105>*I*<wide|\<psi\>|\<ddot\>>-<frac|16|105>*I*<wide|w|\<ddot\>><rsub|x><right|]><right|}>+<big|int>W<rsub|x>*<left|{>\<um\>\<rho\>*<left|[><frac|I|21>*<wide|w|\<ddot\>><rsub|x>-<frac|16|105>*I*<wide|\<psi\>|\<ddot\>><right|]><right|}>>|<cell|>|<cell|>>>>
  </eqnarray*>>

  Splitting <math|<wide|F|\<ddot\>>=<wide|F|\<ddot\>><rsub|a>(F<rsup|n+1>)+<wide|F|\<ddot\>><rsub|b>
  (F<rsup|n>, <wide|F|\<dot\>><rsup|n>, <wide|F|\<ddot\>><rsup|n>)>,

  <with|font-base-size|8|<\eqnarray*>
    <tformat|<cwith|6|6|1|1|cell-valign|c>|<table|<row|<cell|<big|int>U<rsub|x>*\<xi\>+<big|int>W<rsub|x>*<left|{>\<xi\>*w<rsub|x>+<frac|8|15>*G*A*\<eta\><right|}>+<big|int>\<Psi\><rsub|x>*E*I*<left|{><frac|84|105>*\<psi\><rsub|x>-<frac|16|105>*\<eta\><rsub|x><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W<rsub|x
    x>*E*I*<left|{>\<um\><frac|16|105>*\<eta\><rsub|x>+<frac|21|105>*w<rsub|x
    x><right|}>+<frac|8|15>*<big|int>G*A*\<Psi\>*\<eta\>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W*\<rho\>*A*<wide|w|\<ddot\>><rsub|a>+<big|int>U*\<rho\>*A*<wide|u|\<ddot\>><rsub|a>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\>*\<rho\>*<left|[><frac|68|105>*I*<wide|\<psi\>|\<ddot\>><rsub|a>+<frac|16|105>*I*[<wide|w|\<ddot\>><rsub|a>]<rsub|x
    x><right|]>+<big|int>W<rsub|x>*\<rho\>*<left|[><frac|I|21>*[<wide|w|\<ddot\>><rsub|a>]<rsub|x>-<frac|16|105>*I*<wide|\<psi\>|\<ddot\>><rsub|a><right|]>>|<cell|>|<cell|>>|<row|<cell|=<big|int>W*<left|{>q-\<rho\>*A*<wide|w|\<ddot\>><rsub|b><right|}>+<big|int>U*<left|{>f-\<rho\>*A*<wide|u|\<ddot\>><rsub|b><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\>*<left|{>\<um\>\<rho\>*<left|[><frac|68|105>*I*<wide|\<psi\>|\<ddot\>><rsub|b>-<frac|16|105>*I*[<wide|w|\<ddot\>><rsub|b>]<rsub|x><right|]><right|}>+<big|int>W<rsub|x>*<left|{>\<um\>\<rho\>*<left|[><frac|I|21>*[<wide|w|\<ddot\>><rsub|b>]<rsub|x>-<frac|16|105>*I*<wide|\<psi\>|\<ddot\>><rsub|b><right|]><right|}>>|<cell|>|<cell|>>>>
  </eqnarray*>>

  Simplified:

  <math|a<rsup|3>>

  <with|font-base-size|8|<\eqnarray*>
    <tformat|<cwith|6|6|1|1|cell-valign|c>|<table|<row|<cell|<big|int>U<rsub|x>*u+<big|int>W<rsub|x>*u*w<rsub|x>+<big|int>\<Psi\><rsub|x>*E*I*<frac|84|105>*\<psi\><rsub|x>>|<cell|>|<cell|>>|<row|<cell|-<big|int>W<rsub|x
    x>*E*I*<frac|21|105>*w<rsub|x x>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W*\<rho\>*A*<wide|w|\<ddot\>><rsub|a>+<big|int>U*\<rho\>*A*<wide|u|\<ddot\>><rsub|a>>|<cell|>|<cell|>>|<row|<cell|+<big|int>W<rsub|x>*\<rho\>*<frac|I|21>*[<wide|w|\<ddot\>><rsub|a>]<rsub|x>>|<cell|>|<cell|>>|<row|<cell|=<big|int>W*<left|{>q-\<rho\>*A*<wide|w|\<ddot\>><rsub|b><right|}>+<big|int>U*<left|{>f-\<rho\>*A*<wide|u|\<ddot\>><rsub|b><right|}>>|<cell|>|<cell|>>|<row|<cell|-<big|int>W<rsub|x>*\<rho\>*<frac|I|21>*[<wide|w|\<ddot\>><rsub|b>]<rsub|x>>|<cell|>|<cell|>>>>
  </eqnarray*>>

  Checking with the Appendix:

  <with|font-base-size|8|<\eqnarray*>
    <tformat|<cwith|1|3|1|1|cell-lborder|0px>|<cwith|1|3|1|1|cell-rborder|0px>|<cwith|1|3|1|1|cell-bborder|0px>|<cwith|1|3|1|1|cell-tborder|0px>|<cwith|3|3|1|1|cell-bborder|1px>|<cwith|6|6|1|1|cell-bborder|1px>|<table|<row|<cell|<big|int>A*E*U<rsub|x>*u<rsub|x>+<big|int><frac|A*E|2>*U<rsub|x>*w<rsub|x><rsup|2>>|<cell|>|<cell|>>|<row|<cell|<big|int>A*E*W<rsub|x>*u<rsub|x>*w<rsub|x>+<big|int><left|{><frac|1|2>*A*E*W<rsub|x>*w<rsub|x><rsup|3>+<frac|8|15>*G*A*W<rsub|x>*w<rsub|x>+<frac|E*I|21>W<rsub|x
    x>**w<rsub|x x><right|}>+<big|int><left|{><frac|8|15>*G*A*W<rsub|x>*\<psi\>-<frac|16|105>*E*I*W<rsub|x
    x>*\<psi\><rsub|x><right|}>>|<cell|>|<cell|>>|<row|<cell|<big|int><left|{><frac|8|15>*G*A*\<Psi\>*w<rsub|x>-<frac|16|105>*E*I*\<Psi\><rsub|x>*w<rsub|x
    x><right|}>+<big|int><left|{><frac|78|105>*E*I*\<Psi\><rsub|x>*\<psi\><rsub|x>+<frac|8|15>*G*A*\<Psi\>*\<psi\><right|}>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<rho\>*A*U*<wide|u|\<ddot\>>>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<rho\>*A*W*<wide|w|\<ddot\>>+<big|int>\<um\><frac|16|105>*\<rho\>*I*W<rsub|x>*<wide|\<psi\>|\<ddot\>>>|<cell|>|<cell|>>|<row|<cell|<big|int>\<um\><frac|16|105>*\<rho\>*I*\<Psi\>*<wide|w|\<ddot\>><rsub|x>+<big|int><frac|68|105>*I*\<Psi\>*<wide|\<Psi\>|\<ddot\>>>|<cell|>|<cell|>>|<row|<cell|=<big|int>W*q+<big|int>U*f>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|DISCREPANCY:>|<cell|>|<cell|>>|<row|<cell|+<big|int>\<Psi\><rsub|x>*<left|{>-<frac|10|105>*E*I*\<psi\><rsub|x>+<frac|E*I|21>*(w<rsub|x
    x>)<right|}>+<big|int>W<rsub|x x>*<left|{><frac|E*I|21>*(\<psi\><rsub|x>)<right|}><with|mode|text|
    [see notes on sheet; seems to be a mistake in H-R]>>|<cell|>|<cell|>>>>
  </eqnarray*>>
</body>

<\references>
  <\collection>
    <associate|ref-somethign|<tuple|4|?>>
    <associate|ref-something|<tuple|4|?>>
  </collection>
</references>