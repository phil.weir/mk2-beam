<TeXmacs|1.0.7.10>

<style|generic>

<\body>
  <section|Discontinuous Galerkin Product Rule>

  These following definitions hold.

  <\with|par-left|1cm>
    <\with|font-base-size|8>
      <math|<around|[|x<rsub|0>,\<cdots\>,x<rsub|n>|]>> is a partition of
      <math|<around|[|a,b|]>>

      <math|f<around|(|x<rsub|0><rsup|\<upl\>>|)>=f<around|(|a|)>>, sim.
      <math|x<rsub|n><rsup|->> and/or <math|g>.

      <math|jump<around|(|f,x<rsub|i>|)>=<around|[|<around|[|f|]>|]><rsub|i>=f<around|(|x<rsub|i><rsup|\<upl\>>|)>-f<around|(|x<rsub|i><rsup|\<um\>>|)>>

      <math|avg<around|(|f,x<rsub|i>|)>=<around|\<langle\>|f|\<rangle\>><rsub|i>=<frac|1|2>*<around|[|f<around|(|x<rsub|i><rsup|+>|)>-f<around|(|x<rsub|i><rsup|\<um\>>|)>|]>>
    </with>
  </with>

  Now consider,<math|\<llbracket\>>

  <\eqnarray*>
    <tformat|<cwith|1|5|2|2|cell-halign|l>|<table|<row|<cell|<big-around|\<int\>|<rsub|a><rsup|b>f*g<rsub|x>*d
    x>>|<cell|=<rsub|CG>>|<cell|<around*|[|f*g|]><rsup|b><rsub|a>-<big-around|\<int\>|<rsub|a><rsup|b>f<rsub|x>*g*d
    x>>>|<row|<cell|>|<cell|=<rsub|DG>>|<cell|<big-around|\<sum\>|<rsub|i=0><rsup|n-1><big-around|\<int\>|<rsub|x<rsub|i>><rsup|x<rsub|i+1>>f*g<rsub|x>*d
    x>>>>|<row|<cell|>|<cell|=>|<cell|<big-around|\<sum\>|<rsub|i=0><rsup|n-1><around*|{|<around*|[|f*g|]><rsup|x<rsup|\<um\>><rsub|i+1>><rsub|x<rsup|\<upl\>><rsub|i>>-<big-around|\<int\>|<rsub|x<rsub|i>><rsup|x<rsub|i+1>>f<rsub|x>*g*d
    x>|}>>>>|<row|<cell|>|<cell|=>|<cell|<around|[|f*g|]><rsup|b><rsub|a>-<big-around|\<sum\>|<rsub|i=1><rsup|n-1><around*|{|<around|(|f*g|)>
    <around|(|x<rsub|i><rsup|\<um\>>|)>-<around|(|f*g|)>
    <around|(|x<rsub|i><rsup|\<upl\>>|)>|}>>-<big-around|\<sum\>|<rsub|i=0><rsup|n-1><big-around|\<int\>|<rsub|x<rsub|i>><rsup|x<rsub|i+1>>f<rsub|x>*g*d
    x>>>>|<row|<cell|>|<cell|=>|<cell|<around|[|f*g|]><rsup|b><rsub|a>-<big-around|\<int\>|<rsub|a><rsup|b>f<rsub|x>*g*d
    x>+<big-around|\<sum\>|<rsub|i=1><rsup|n-1><around|\<langle\>|f*g|\<rangle\>><rsub|i>><eq-number><label|ref-init-simp>>>>>
  </eqnarray*>

  However, letting <math|<around|[|f<rsub|i<rsup|\<pm\>>>|]>\<assign\>f<around|(|x<rsub|i><rsup|\<pm\>>|)>>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<around|\<langle\>|f*g|\<rangle\>><rsub|i>>|<cell|=>|<cell|<around|[|f<rsub|i<rsup|+>>|]>*<around|[|g<rsub|i<rsup|\<upl\>>>|]>-<around|[|f<rsub|i<rsup|\<um\>>>|]>*<around|[|g<rsub|i<rsup|\<um\>>>|]>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<around*|{|2*<around|[|f<rsub|i<rsup|+>>|]>*<around|[|g<rsub|i<rsup|\<upl\>>>|]>-2*<around|[|f<rsub|i<rsup|\<um\>>>|]>*<around|[|g<rsub|i<rsup|\<um\>>>|]>+<around|[|f<rsub|i<rsup|\<um\>>>|]>*<around|[|g<rsub|i<rsup|\<upl\>>>|]>-<around|[|f<rsub|i<rsup|->>|]>*<around|[|g<rsub|i<rsup|+>>|]>+<around|[|f<rsub|i<rsup|+>>|]>*<around|[|g<rsub|i<rsup|->>|]>-<around|[|f<rsub|i<rsup|+>>|]>*<around|[|g<rsub|i<rsup|->>|]>|}>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<around*|{|<around*|(|<around|[|f<rsub|i<rsup|+>>|]>+<around|[|f<rsub|i<rsup|\<um\>>>|]>|)>*<around|[|g<rsub|i<rsup|\<upl\>>>|]>-<around|(|<around|[|f<rsub|i<rsup|\<upl\>>>|]>+<around|[|f<rsub|i<rsup|\<um\>>>|]>|)>*<around|[|g<rsub|i<rsup|\<um\>>>|]>+<around|(|<around|[|g<rsub|i<rsup|\<upl\>>>|]>+<around|[|g<rsub|i<rsup|\<um\>>>|]>|)>*<around|[|f<rsub|i<rsup|\<upl\>>>|]>-<around|(|<around|[|g<rsub|i<rsup|\<upl\>>>|]>+<around|[|g<rsub|i<rsup|\<um\>>>|]>|)><around|[|f<rsub|i<rsup|\<um\>>>|]>|}>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>*<around|(|<around|[|f<rsub|i<rsup|+>>|]>+<around|[|f<rsub|i<rsup|\<um\>>>|]>|)>*<around|(|<around|[|g<rsub|i<rsup|\<upl\>>>|]>-<around|[|g<rsub|i<rsup|\<um\>>>|]>|)>+<frac|1|2>*<around|(|<around|[|f<rsub|i<rsup|+>>|]>-<around|[|f<rsub|i<rsup|\<um\>>>|]>|)>*<around|(|<around|[|g<rsub|i<rsup|\<upl\>>>|]>+<around|[|g<rsub|i<rsup|\<um\>>>|]>|)>>>|<row|<cell|>|<cell|=>|<cell|<around|[|<around|[|f<rsub|i>|]>|]>*<around|\<langle\>|g<rsub|i>|\<rangle\>>+<around|\<langle\>|f<rsub|i>|\<rangle\>>*<around|[|<around|[|g<rsub|i>|]>|]>>>>>
  </eqnarray*>

  Substituting into (<reference|ref-init-simpl>),

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big-around|\<int\>|<rsub|a><rsup|b>f*g<rsub|x>*d
    x>>|<cell|=>|<cell|<around|[|f*g|]><rsup|b><rsub|a>-<big-around|\<int\>|<rsub|a><rsup|b>f<rsub|x>*g*d
    x>+<big-around|\<sum\>|<rsub|i=1><rsup|n-1><around|(|<around|[|<around|[|f<rsub|i>|]>|]>*<around|\<langle\>|g<rsub|i>|\<rangle\>>+<around|\<langle\>|f<rsub|i>|\<rangle\>>*<around|[|<around|[|g<rsub|i>|]>|]>|)>>,>>>>
  </eqnarray*>

  or, in UFL,

  <\code>
    f*Dx(g,0)*dx = f*g*n*ds - Dx(f,0)*g*dx + jump(f)*avg(g)*dS +
    avg(f)*jump(g)*dS,
  </code>

  where <verbatim|n><math|<around|(|x|)>=<choice|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|\<um\>1>|<cell|if>|<cell|x=a>>|<row|<cell|1>|<cell|if>|<cell|x=b>>>>>>.
</body>

<\initial>
  <\collection>
    <associate|par-hyphen|normal>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|ref-init-simp|<tuple|1|?>>
    <associate|ref-init-simpl|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Discontinuous
      Galerkin Product Rule> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>
