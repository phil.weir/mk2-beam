#!/bin/bash
source outroot.sh

let FROM=$2+5
sed "$FROM,\$d" $outroot/output.$1/pressure/model-$1-pressure.pvd > $outroot/output.$1/pressure/model-$1-pressure-$2.pvd
echo '</Collection></VTKFile>' >> $outroot/output.$1/pressure/model-$1-pressure-$2.pvd
