#include "HEP.h"
#include "flatten.h"
#include "LukeFunctional.h"
#include "GradSqfs.h"
#include "realdz.h"
#include "dz2.h"
#include "realdx.h"
#include "dx.h"
#include "fakedx1.h"
#include "fakedx2.h"
#include "dz.h"
#include "Pressure.h"
#include "Err.h"
#include "Standard.h"
#include "Standarddt.h"
#include "FEniCSBiharmonic.h"
#include "Alpha.h"
#include "GradSq.h"
#include "Predict.h"
#include "Correct.h"
#include "pdt.h"
#include "zdt.h"

//double l = 3;
class NormalExpression : public Expression
{
    public:
    NormalExpression(double lt, double rt) : l(lt), r(rt) {}
    double l, r;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      values[0] = (x[0] > 1e-1 ? r:l);
    }
};

void HEP::eval_luke (double& luke, FunctionSpace& V, Function& phi, Function& lukebdy, MeshFunction<uint>& fs)
{
   //if (mode3d) { eval_luke_3d (luke, V, phi, lukebdy, fs); return; }
   LukeFunctional::Functional ML(*V.mesh());
   ML.phi = phi; ML.bdyfn = lukebdy;
   luke = assemble(ML, 0, &fs, 0);
}

void HEP::eval_pressure (Function& pressure, FunctionSpace& pV, Constant& rhowc, Function& phidt, Function& gsphi)
{
   //if (mode3d) { eval_pressure_3d (pressure, pV, rhowc, phidt, gsphi); return; }
   Pressure::LinearForm LP(pV); Pressure::BilinearForm aP(pV,pV);
   LP.rhow = rhowc; LP.phidt = phidt; LP.gsphi = gsphi;
   Function pressuretmp(pV);
   solve(aP == LP, pressuretmp);
   pressure = pressuretmp;
}

class HEP::Boundary : public SubDomain
{
  public:
    //Boundary ( PlateList& platelistt ) : platelist(platelistt) {}
    //PlateList& platelist;
    bool inside (const dolfin::Array<double> &x, bool on_boundary) const { return on_boundary;}//fabs(x[0] - 150) < EPS || fabs(x[0] - 650) < EPS; }//Plate::this_plate(platelist, x[0], true, length); }
};

void HEP::eval_flatten (Function& zetaic, FunctionSpace& V, Expression& zetaicpre)
{
  //if (mode3d) { eval_flatten_3d(zetaic, V, zetaicpre); return; }
  NormalExpression n(-1, 1);
  flatten::BilinearForm af(V,V); flatten::LinearForm Lf(V);
  Lf.pre = zetaicpre; af.n = n;
  Function post(V);
  solve(af == Lf, post);
  zetaic = post;
}

void HEP::eval_fscond (File& file, Function& pdtf, Function& zdtf, FunctionSpace& dpV, FunctionSpace& dzV, Constant& gc, Function& zeta, Function& gsphi,
  Function& phinonz, Function& beach_nu, Function& cphi, Constant& muc, Constant& lc)
{
  //if (mode3d) { eval_fscond_3d(file, pdtf, zdtf, dpV, dzV, gc, zeta, gsphi, phinonz, beach_nu, cphi, muc, lc); return; }
  MeshFunction<uint> p1dommf(*dzV.mesh(), 0);
  p1dommf.set_all(2);
  Boundary plbdy; plbdy.mark(p1dommf, 1);
  std::vector< const BoundaryCondition* > bcv;
  pdt::BilinearForm apdt(dpV,dpV); pdt::LinearForm Lpdt(dpV);
  zdt::BilinearForm azdt(dzV,dzV); zdt::LinearForm Lzdt(dzV);
  NormalExpression npdt(0, -1);
  Lpdt.l = lc; Lzdt.l = lc; Lzdt.mu = muc; //Constant zero(0.0);//RMV
  Lpdt.g = gc; Lpdt.zeta = zeta; Lpdt.cphi = cphi; Lpdt.mu = muc; Lpdt.gsphi = gsphi;/* Lpdt.phinonz = phinonz; Lpdt.nu = beach_nu;*/
  Lzdt.phinonz = phinonz; Lzdt.zeta = zeta; //azdt.n = npdt;// apdt.n = npdt;
  azdt.ds = p1dommf;
  Lzdt.ds = p1dommf;
  //VariationalProblem zdtp (azdt, Lzdt);
  Function fsc0(dpV), fsc1(dzV);
  solve(apdt == Lpdt, fsc0);
  solve(azdt == Lzdt, fsc1);
  pdtf = fsc0; zdtf = fsc1;
}

void Fscale ( Function& F, double r );
void Fadd ( Function& F, Function& G );
void HEP::eval_predict (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, FunctionSpace& dpV, Constant& Dtc,
  Function& zetan, Function& zdtn, Function& zdtnm1, Function& zdtnm2, Function& zdtnm3,
  Function& phitopn, Function& pdtn, Function& pdtnm1, Function& pdtnm2, Function& pdtn1, Constant& lc, Constant& muc)
{
  zetan1.vector()->operator=(*zdtn.vector());
  zetan1.vector()->operator*=(55.);
  zetan1.vector()->axpy(-59., *zdtnm1.vector());
  zetan1.vector()->axpy(37., *zdtnm2.vector());
  zetan1.vector()->axpy(-9., *zdtnm3.vector());
  zetan1.vector()->operator*=(Dtc/24.);
  zetan1.vector()->operator+=(*zetan.vector());

  phitopn1.vector()->operator=(*zetan1.vector());
  phitopn1.vector()->operator*=(-9.81*9);
  phitopn1.vector()->axpy(19., *pdtn.vector());
  phitopn1.vector()->axpy(-5., *pdtnm1.vector());
  phitopn1.vector()->axpy(1., *pdtnm2.vector());
  phitopn1.vector()->operator*=(Dtc/24.);
  phitopn1.vector()->operator+=(*phitopn.vector());
  //Predict::LinearForm LPred(dfsV); Predict::BilinearForm aPred(dfsV,dfsV);
  //LPred.Dt = Dtc;
  Constant gc(9.81);
  //LPred.un = zetan; LPred.udtn = zdtn; LPred.udtnm1 = zdtnm1; LPred.udtnm2 = zdtnm2; LPred.udtnm3 = zdtnm3;
  //aPred.g = gc; aPred.Dt = Dtc; LPred.vn = phitopn; LPred.vdtn = pdtn; LPred.vdtnm1 = pdtnm1; LPred.vdtnm2 = pdtnm2; /*LPred.vdtnm3 = pdtnm3;*/
  //Function predsol(dfsV);
  //solve( aPred == LPred, predsol );
  //zetan1 = predsol[0]; phitopn1 = predsol[1];
  pdt::BilinearForm apdt(dpV,dpV); pdt::LinearForm Lpdt(dpV); Lpdt.mu = muc; Constant zero(0.0);
  Lpdt.g = gc; Lpdt.zeta = zetan1; Lpdt.cphi = phitopn1; Lpdt.l = lc; Lpdt.gsphi = zero; //RMV switch to gsp
  Function fsc0(dpV);
  solve( apdt == Lpdt, fsc0 );
  pdtn1 = fsc0;
}

void HEP::eval_correct (Function& zetan1, Function& phitopn1, FunctionSpace& dfsV, Constant& Dtc,
  Function& zetan, Function& zdtn1, Function& zdtn, Function& zdtnm1, Function& zdtnm2,
  Function& phitopn, Function& pdtn1, Function& pdtn, Function& pdtnm1, Function& pdtnm2 )
{
  //if (mode3d) { eval_correct_3d(zetan1, phitopn1, dfsV, Dtc, zetan, zdtn1, zdtn, zdtnm1, zdtnm2, phitopn, pdtn1, pdtn, pdtnm1, pdtnm2); return; }
  Correct::LinearForm LCorr(dfsV); Correct::BilinearForm aCorr(dfsV, dfsV);
  aCorr.Dt = Dtc;
  LCorr.Dt = Dtc;
  LCorr.un = zetan; LCorr.udtkm1 = zdtn1; LCorr.udtn = zdtn; LCorr.udtnm1 = zdtnm1; LCorr.udtnm2 = zdtnm2;
  LCorr.vn = phitopn; /*LCorr.vdtkm1 = pdtn1;*/ LCorr.vdtn = pdtn; LCorr.vdtnm1 = pdtnm1; LCorr.vdtnm2 = pdtnm2;
  Function corrsol(dfsV);
  solve( aCorr == LCorr, corrsol );
  zetan1 = corrsol[0]; phitopn1 = corrsol[1];
}

void HEP::eval_alpha (Function& Wddtn1, FunctionSpace& pV, Constant& alphac, Function& Wddtkm1)
{
  //if (mode3d) { eval_alpha_3d(Wddtn1, pV, alphac, Wddtkm1); return; }
  Alpha::LinearForm LA1(pV); Alpha::BilinearForm aA1(pV, pV);
  Function Wddtn1tmp1(pV); Wddtn1tmp1.interpolate(Wddtn1);
  LA1.al = alphac; LA1.unew = Wddtn1tmp1; LA1.uold = Wddtkm1;
  Function Wddtn1tmp(pV);
  solve( aA1 == LA1, Wddtn1tmp );
  Wddtn1 = Wddtn1tmp;
}

void HEP::eval_dx1(Function& zetax, FunctionSpace& fsV, Function& zetan1)
{
  //if (mode3d) { eval_dx1_3d(zetax, fsV, zetan1); return; }
  NormalExpression npdt(-1, 1);
  fakedx1::LinearForm Ldx(fsV);
  fakedx1::BilinearForm adx(fsV,fsV);
  adx.n = npdt; Ldx.zeta = zetan1; Ldx.n = npdt;
  // bczx ignored! (don't forget cphix uses this too)
  Function zetaxtmp(fsV);
  solve( adx == Ldx, zetaxtmp );
  zetax = zetaxtmp;
}

void HEP::eval_dx2(Function& zetax, FunctionSpace& fsV, Function& zetan1)
{
  //if (mode3d) { eval_dx2_3d(zetax, fsV, zetan1); return; }
  fakedx2::LinearForm Ldx(fsV); fakedx2::BilinearForm adx(fsV,fsV); Ldx.zeta = zetan1;
  // bczx ignored! (don't forget cphix uses this too)
  Function zetaxtmp(fsV);
  solve( adx == Ldx, zetaxtmp );
  zetax = zetaxtmp;
}

void HEP::eval_dx(Function& zetax, FunctionSpace& fsV, Function& zetan1, Constant& lc)
{
  //if (mode3d) { eval_dx_3d(zetax, fsV, zetan1, lc); return; }
  dx::LinearForm Ldx(fsV); dx::BilinearForm adx(fsV,fsV); Ldx.zeta = zetan1;
  // bczx ignored! (don't forget cphix uses this too)
  Function zetaxtmp(fsV);
  //Ldx.l = lc;
  solve ( adx == Ldx, zetaxtmp );
  zetax = zetaxtmp;
}

void HEP::eval_phinonz(Function& gradphi, FunctionSpace& V, Expression& zetax, Function& phi, Expression& cphix)
{
  //if (mode3d) { eval_phinonz_3d(phinonz, V, zetax, phi, cphix); return; }
  dz::LinearForm Ldz(V); dz::BilinearForm adz(V,V); Ldz.phi = phi;// Ldz.zetax = zetax; Ldz.cphix = cphix;
  Function gradphitmp(V);
  solve ( adz == Ldz, gradphitmp );
  gradphi = gradphitmp;
}

void HEP::eval_gradsq (Function& gsphin1, FunctionSpace& V, Function& phin1, Constant& lc)
{
  //if (mode3d) { eval_gradsq_3d(gsphin1, V, phin1, lc); return; }
  GradSq::BilinearForm ags(V,V); GradSq::LinearForm Lgs(V);
  Lgs.phi = phin1;
  Lgs.l = lc;
  Function gsphin1tmp(V);
  solve ( ags == Lgs, gsphin1tmp );
  gsphin1 = gsphin1tmp;
}

void HEP::eval_phix ( Function& phix, FunctionSpace& V, Function& phi, Constant& lc )
{
  //if (mode3d) { eval_phix_3d(phix, V, phi, lc); return; }
  realdx::LinearForm Ldx(V); realdx::BilinearForm adx(V,V); Ldx.phi = phi;
  Function tmp(V);
  Ldx.l = lc;
  solve ( adx == Ldx, tmp );
  phix = tmp;
}

void HEP::eval_phiz ( Function& phiz, FunctionSpace& V, Function& phi, MeshFunction<uint>& p1dommf )
{
  //if (mode3d) { eval_phiz_3d(phiz, V, phi, p1dommf); return; }
  dz2::LinearForm Ldz(V); dz2::BilinearForm adz(V,V); Ldz.phi = phi;
  Function phinonztmp(V);
  std::vector< const BoundaryCondition* > bcv(0);
  adz.ds = p1dommf;
  Ldz.ds = p1dommf;
  solve ( adz == Ldz, phinonztmp, bcv );
  phiz = phinonztmp;
/*
  realdz::LinearForm Ldz(V); realdz::BilinearForm adz(V,V); Ldz.phi = phi;
  Function phinonztmp(V);
  VariationalProblem realdzprob(adz, Ldz); realdzprob.solve(phinonztmp);
  phiz = phinonztmp;
  */
}

void HEP::eval_gradsqfs ( Function& gsphifs, FunctionSpace& fsV, Function& phiz, Function& cphi, Function& zetax, DirichletBC& bgsfs )
{
  //if (mode3d) { eval_gradsqfs_3d(gsphifs, fsV, phiz, cphi, zetax, bgsfs); return; }
  GradSqfs::BilinearForm agsfs(fsV, fsV); GradSqfs::LinearForm Lgsfs(fsV);
//  Lgsfs.phinonz = phinonz; Lgsfs.phitop = phitop; Lgsfs.zeta = zeta;
  Lgsfs.phiz = phiz; Lgsfs.cphi = cphi; Lgsfs.zetax = zetax;
  Function gsphifstmp(fsV);
  solve ( agsfs == Lgsfs, gsphifstmp );
  gsphifs = gsphifstmp;
}

void HEP::eval_velpot (Function& phi, FunctionSpace& V, Expression& phitop, DirichletBC& bc, DirichletBC& bcw,
  Expression& Wdt, Expression& wave, MeshFunction<uint>& p1dommf, Constant& lc)
{
  //if (mode3d) { eval_velpot_3d(phi, V, phitop, bc, bcw, Wdt, wave, p1dommf, lc); return; }
   Standard::BilinearForm a(V, V);
   Standard::LinearForm L(V); L.Wdot = Wdt; L.wave = wave;// a.h = h;
   // CHECK CHANGELOG
   Constant zero(0.0, 0.0);
   L.noflow = zero;
   a.l = lc;
   std::vector< const BoundaryCondition* > bcv(0);// bcv[0] = &bc;// bcv[1] = &bcw;
   L.phitop = phitop;
   a.ds = p1dommf;
   L.ds = p1dommf;
   //VariationalProblem velpotprob(a, L, bc);
   Function phitmp(V);
   //solve ( a == L, phitmp, bcv );
   Matrix a_;
   Vector L_;
   assemble(a_, a);
   assemble(L_, L);
   //bcv.apply(a_, L_);
   UmfpackLUSolver vel_solver;
   vel_solver.solve(a_, *phi.vector(), L_);
   //phi = phitmp;
}

/*
void HEP::eval_velpot (Function& phi, FunctionSpace& V, DirichletBC& bc, Function& Wdt, MeshFunction<uint>& p1dommf)
{
   Standard::BilinearForm a(V, V);
   Standard::LinearForm L(V); L.Wdot = Wdt;
//   std::vector<const BoundaryCondition*> bcv; bcv.push_back(&bc);
   VariationalProblem velpotprob(a, L, bc);//, bc);
//   VariationalProblem velpotprob(a, L, bcv, 0, &p1dommf, 0);
   Function phitmp(V);
   velpotprob.solve(phitmp);
   phi = phitmp;
}
*/

File testfile("test.pvd");
File testfile2("test2.pvd");

class FlatExpression : public Expression
{
  public:
    FlatExpression(Expression& expt) : Expression(1), exp(expt) {}
    Expression& exp;
    void eval ( dolfin::Array<double> &values, const dolfin::Array<double> &x ) const
    {
      dolfin::Array<double> val2(2);
      exp.eval(val2, x);
      values[0] = val2[1];
    }
};

void HEP::eval_accpot (Function& pdt, FunctionSpace& V, Expression& pdttop, DirichletBC& bcdt, DirichletBC& bcwdt, Expression& Wddt, Expression& wave, Function& phin1, MeshFunction<uint>& p1dommf, Expression& pdtbc, Constant& lc, bool nlfluid)
{
  //if (mode3d) { eval_accpot_3d(pdt, V, pdttop, bcdt, bcwdt, Wddt, wave, gsphin1, p1dommf, pdtbc, lc); return; }
   Standarddt::BilinearForm adt(V, V);
   
   Standarddt::LinearForm Ldt(V); Ldt.Wddot = Wddt; Ldt.wave = wave; //Ldt.pdt = pdtbc; Ldt.h = h; adt.h = h;
   Constant zero1(0.0);
   if (nlfluid)
   {
	   Ldt.phi = phin1;
   } else
   {
	   Ldt.phi = zero1;
   }
   adt.l = lc;
   //adt.h = h;
   Constant zero(0.0, 0.0);
   Ldt.noflow = zero;
   Ldt.pdt = pdttop;

   //Matrix A;
   //Vector b;
   //assemble(A, adt);
   //assemble(b, Ldt);
   //bcdt.apply(A, b);
   //solve(A, pdt.vector(), b);
   std::vector<const BoundaryCondition*> bcdtv(0);// bcdtv[0] = &bcdt; //bcdtv.push_back(&bcdt); //bcdtv.push_back(&bcwdt);
  // VariationalProblem accpotprob(adt, Ldt, bcdt);
  adt.ds = p1dommf;
  Ldt.ds = p1dommf;
   //testfile2 << phidttmp;
   Function phidttmp(V);
   Matrix adt_;
   Vector Ldt_;
   assemble(adt_, adt);
   assemble(Ldt_, Ldt);
   //solve ( adt == Ldt, phidttmp, bcdtv );
   UmfpackLUSolver acc_solver;
   acc_solver.solve(adt_, *phidttmp.vector(), Ldt_);
   pdt = phidttmp;
}
