#!/bin/bash
# ./makeanim DIR LAST STEP
source outroot.sh
padn=`printf %06d $2`
file="$outroot/output.$1/body-$padn.csv"
ofile="body.csv"
cp $file $file.tmp
sed '1d' $file.tmp > $ofile
octave 3d.m
