#include "matrix_solve.h"
#include <dolfin/la/PETScMatrix.h>
#include <dolfin/la/PETScVector.h>

using namespace dolfin;

//Mat* Zargs[] = { &(*C12.mat()), &(*B12m.mat()), &(*A1.mat()), &(*B12p.mat()), &(*A2.mat()) };
//Mat* Yargs[] = { C23.mat(), B23m.mat(), A2.mat(), B23p.mat(), A3.mat(), B12p.mat(), &Z };

struct Ctx {
        Mat** mats;
        KSP** ksps;
};
PetscErrorCode Zmult(Mat d, Vec x, Vec b)
{
        struct Ctx *ctx;
        MatShellGetContext(d, (void**)&ctx);

        KSP &kspA2 = *ctx->ksps[1], &kspA1 = *ctx->ksps[0];
        Mat** mats = ctx->mats;
        //PC pc;
        //KSP kspA1, kspA2;
        //KSPCreate(PETSC_COMM_SELF, &kspA1);
        //KSPSetOperators(kspA1, *mats[2], *mats[2], DIFFERENT_NONZERO_PATTERN);
        //KSPGetPC(kspA1, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA1);
        ////KSPSetType(kspA1, KSPCG);
        //KSPCreate(PETSC_COMM_SELF, &kspA2);
        //KSPSetOperators(kspA2, *mats[4], *mats[4], DIFFERENT_NONZERO_PATTERN);
        //KSPGetPC(kspA2, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA2);
        ////KSPSetType(kspA2, KSPCG);

        Vec Vm, Vp;

        VecCreate(PETSC_COMM_SELF, &Vm);
        VecCreate(PETSC_COMM_SELF, &Vp);
        PetscInt m, n;
        MatGetSize(*mats[1], &m, &n);
        VecSetSizes(Vm, m, PETSC_DECIDE);
        MatGetSize(*mats[3], &m, &n);
        VecSetSizes(Vp, m, PETSC_DECIDE);
        VecSetFromOptions(Vm);
        VecSetFromOptions(Vp);

        //C12
        MatMult(*mats[0], x, b);
        VecScale(b, -1.);

        // B12m
        MatMult(*mats[1], x, Vm);
        // A1I
        KSPSolve(kspA1, Vm, Vm);
        // B12mT
        MatMultTransposeAdd(*mats[1], Vm, b, b);

        // B12p
        MatMult(*mats[3], x, Vp);
        // A2I
        KSPSolve(kspA2, Vp, Vp);
        // B12pT
        MatMultTransposeAdd(*mats[3], Vp, b, b);

        VecScale(b, -1.);

        VecDestroy(&Vm);
        VecDestroy(&Vp);
        //KSPDestroy(&kspA1);
        //KSPDestroy(&kspA2);

        return 0;
}
// vargs[] = { &(*B23m.mat(), &(*A2.mat()), &(*B23p.mat()), &(*A3.mat()), &(*B12p.mat()), &Z, &(*B12m.mat()), &(*A1.mat())}
// vargs[] = { &(*G23.vec()), &(*G12.vec()), &(*L1.vec()), &(*L2.vec()), &(*L3.vec()) }
PetscErrorCode make_v(Vec& b, struct Ctx* ctx, Vec** vecs)
{
        KSP &kspA2 = *ctx->ksps[1], &kspA1 = *ctx->ksps[0], &kspA3 = *ctx->ksps[2], &kspZ = *ctx->ksps[3];
        Mat** mats = ctx->mats;
        //PC pc;
        //KSPCreate(PETSC_COMM_SELF, &kspA1);
        //KSPSetOperators(kspA1, *mats[7], *mats[7], DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA1, KSPPREONLY);
        //KSPGetPC(kspA1, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA1);
        ////KSPSetType(kspA1, KSPCG);
        //KSPCreate(PETSC_COMM_SELF, &kspA2);
        //KSPSetOperators(kspA2, *mats[1], *mats[1], DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA2, KSPPREONLY);
        //KSPGetPC(kspA2, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA2);
        //KSPCreate(PETSC_COMM_SELF, &kspA3);
        //KSPSetOperators(kspA3, *mats[3], *mats[3], DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA3, KSPPREONLY);
        //KSPGetPC(kspA3, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA3);
        //KSPSetType(kspA1, KSPCG);

        Vec V2, Vm, Vp, Vk;

        VecCreate(PETSC_COMM_SELF, &V2);
        VecCreate(PETSC_COMM_SELF, &Vm);
        VecCreate(PETSC_COMM_SELF, &Vp);
        VecCreate(PETSC_COMM_SELF, &Vk);
        PetscInt m, n;
        MatGetSize(*mats[5], &m, &n);
        VecSetSizes(V2, m, PETSC_DECIDE);
        MatGetSize(*mats[0], &m, &n);
        VecSetSizes(Vm, m, PETSC_DECIDE);
        MatGetSize(*mats[2], &m, &n);
        VecSetSizes(Vp, m, PETSC_DECIDE);
        MatGetSize(*mats[7], &m, &n);
        VecSetSizes(Vk, n, PETSC_DECIDE);
        VecSetFromOptions(V2);
        VecSetFromOptions(Vm);
        VecSetFromOptions(Vp);
        VecSetFromOptions(Vk);

        //// G23
        VecCopy(*vecs[0], b);

        // G12
        VecCopy(*vecs[1], V2);
        VecScale(V2, -1.);
        // A1I*L1
        KSPSolve(kspA1, *vecs[2], Vk);
        // B12mT
        MatMultTransposeAdd(*mats[6], Vk, V2, V2);
        // A2I*L2
        KSPSolve(kspA2, *vecs[3], Vm);
        // B12pT
        MatMultTransposeAdd(*mats[4], Vm, V2, V2);
        VecScale(V2, -1.);
        // Z
        KSPSolve(kspZ, V2, V2);
        // B12p
        MatMult(*mats[4], V2, Vm);
        // A2I
        KSPSolve(kspA2, Vm, Vm);
        // B23mT
        MatMultTransposeAdd(*mats[0], Vm, b, b);

        VecScale(b, -1.);
        
        // A3I*L3
        KSPSolve(kspA3, *vecs[4], Vp);
        // B23pT
        MatMultTransposeAdd(*mats[2], Vp, b, b);

        // A2I*L2
        KSPSolve(kspA2, *vecs[3], Vm);
        // B23mT
        MatMultTransposeAdd(*mats[0], Vm, b, b);

        VecScale(b, -1.);

        VecDestroy(&V2);
        VecDestroy(&Vm);
        VecDestroy(&Vp);
        VecDestroy(&Vk);

        return 0;
}
PetscErrorCode Ymult(Mat d, Vec x, Vec b)
{
        struct Ctx *ctx;
        MatShellGetContext(d, (void**)&ctx);
        KSP &kspA2 = *ctx->ksps[1], &kspA1 = *ctx->ksps[0], &kspA3 = *ctx->ksps[2], &kspZ = *ctx->ksps[3];
        Mat** mats = ctx->mats;

        //KSP kspA2, kspA3, kspZ;
        //PC pc;
        //KSPCreate(PETSC_COMM_SELF, &kspA2);
        //KSPSetOperators(kspA2, *mats[2], *mats[2], DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA2, KSPPREONLY);
        //KSPGetPC(kspA2, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA2);
        //KSPCreate(PETSC_COMM_SELF, &kspA3);
        //KSPSetOperators(kspA3, *mats[4], *mats[4], DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA3, KSPPREONLY);
        //KSPGetPC(kspA3, &pc);
        //PCSetType(pc, PCLU);
        //KSPSetFromOptions(kspA3);

        Vec V2, Vm, Vp;

        VecCreate(PETSC_COMM_SELF, &V2);
        VecCreate(PETSC_COMM_SELF, &Vm);
        VecCreate(PETSC_COMM_SELF, &Vp);
        PetscInt m, n;
        MatGetSize(*mats[6], &m, &n);
        VecSetSizes(V2, m, PETSC_DECIDE);
        MatGetSize(*mats[1], &m, &n);
        VecSetSizes(Vm, m, PETSC_DECIDE);
        MatGetSize(*mats[3], &m, &n);
        VecSetSizes(Vp, m, PETSC_DECIDE);
        VecSetFromOptions(V2);
        VecSetFromOptions(Vm);
        VecSetFromOptions(Vp);

        ////C23
        MatMult(*mats[0], x, b);

        VecScale(b, -1.);

        // B23m
        MatMult(*mats[1], x, Vm);
        // A2I
        KSPSolve(kspA2, Vm, Vm);
        // B12pT
        MatMultTranspose(*mats[5], Vm, V2);
        // Z
        KSPSolve(kspZ, V2, V2);
        // B12p
        MatMult(*mats[5], V2, Vm);
        // A2I
        KSPSolve(kspA2, Vm, Vm);
        // B23mT
        MatMultTransposeAdd(*mats[1], Vm, b, b);

        // B23p
        MatMult(*mats[3], x, Vp);
        // A3I
        KSPSolve(kspA3, Vp, Vp);
        // B23pT
        MatMultTransposeAdd(*mats[3], Vp, b, b);

        // B23m
        MatMult(*mats[1], x, Vm);
        // A2I
        KSPSolve(kspA2, Vm, Vm);
        // B23mT
        MatMultTransposeAdd(*mats[1], Vm, b, b);

        VecScale(b, -1.);

        VecDestroy(&V2);
        VecDestroy(&Vm);
        VecDestroy(&Vp);

        return 0;
}
//PetscErrorCode dImult(Mat d, Vec x, Vec b)
//{
//        Mat *A;
//        MatShellGetContext(d, (void**)A);
//        KSP ksp;
//        KSPCreate(PETSC_COMM_SELF, &ksp);
//        KSPSetOperators(ksp, *A, *A, DIFFERENT_NONZERO_PATTERN);
//        KSPSolve(ksp, b, x);
//        return 0; //solve
//}
//PetscErrorCode schur(Mat d, Vec x, Vec b)
//{
//        Mat **d12mats;
//        MatShellGetContext(d, (void**)d12mats);
//        return 0; //solve
//}
void matrix_solve (
        PETScMatrix& A1,
        PETScMatrix& B12m,
        PETScMatrix& C12,
        PETScMatrix& B12p,
        PETScMatrix& A2,
        PETScMatrix& B23m,
        PETScMatrix& C23,
        PETScMatrix& B23p,
        PETScMatrix& A3,
        PETScVector& U1,
        PETScVector& LA12,
        PETScVector& U2,
        PETScVector& LA23,
        PETScVector& U3,
        PETScVector& L1,
        PETScVector& G12m,
        PETScVector& G12p,
        PETScVector& L2,
        PETScVector& G23m,
        PETScVector& G23p,
        PETScVector& L3,
	double tol
        )
{
        Mat Y, Z;

        Mat* Zargs_mats[] = { &(*C12.mat()), &(*B12m.mat()), &(*A1.mat()), &(*B12p.mat()), &(*A2.mat()) };
        Ctx Zargs; Zargs.mats = Zargs_mats;
        Mat* Yargs_mats[] = { &(*C23.mat()), &(*B23m.mat()), &(*A2.mat()), &(*B23p.mat()), &(*A3.mat()), &(*B12p.mat()), &Z };
        Ctx Yargs; Yargs.mats = Yargs_mats;

        PC pcA1, pcA2, pcA3;
        KSP kspA1, kspA2, kspA3, kspZ;

        KSPCreate(PETSC_COMM_SELF, &kspA1);
        KSPSetOperators(kspA1, *A1.mat(), *A1.mat(), DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA1, KSPPREONLY);
        KSPGetPC(kspA1, &pcA1);
        PCSetType(pcA1, PCLU);
	KSPSetTolerances(kspA1, tol, tol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPSetFromOptions(kspA1);
        KSPSetUp(kspA1);

        KSPCreate(PETSC_COMM_SELF, &kspA2);
        KSPSetOperators(kspA2, *A2.mat(), *A2.mat(), DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA2, KSPPREONLY);
        KSPGetPC(kspA2, &pcA2);
        PCSetType(pcA2, PCLU);
	KSPSetTolerances(kspA2, tol, tol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPSetFromOptions(kspA2);
        KSPSetUp(kspA2);

        KSPCreate(PETSC_COMM_SELF, &kspA3);
        KSPSetOperators(kspA3, *A3.mat(), *A3.mat(), DIFFERENT_NONZERO_PATTERN);
        //KSPSetType(kspA3, KSPPREONLY);
        KSPGetPC(kspA3, &pcA3);
        PCSetType(pcA3, PCLU);
	KSPSetTolerances(kspA3, tol, tol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPSetFromOptions(kspA3);
        //KSPSetType(kspA1, KSPCG);
        KSPSetUp(kspA3);

        KSP* Zargs_ksps[] = { &kspA1, &kspA2, &kspA3 };
        Zargs.ksps = Zargs_ksps;

        PetscInt m, n;
        m = C12.size(0); n = C12.size(1);
        MatCreateShell(PETSC_COMM_SELF, m, n, m, n, &Zargs, &Z);
        MatShellSetOperation(Z, MATOP_MULT, (void(*)(void))Zmult);

        KSP* Yargs_ksps[] = { &kspA1, &kspA2, &kspA3, &kspZ };
        Yargs.ksps = Yargs_ksps;

        KSPCreate(PETSC_COMM_SELF, &kspZ);
        KSPSetOperators(kspZ, Z, Z, DIFFERENT_NONZERO_PATTERN);
        KSPSetType(kspZ, KSPCG);
	KSPSetTolerances(kspZ, tol, tol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPSetUp(kspZ);

        m = C23.size(0); n = C23.size(1);
        MatCreateShell(PETSC_COMM_SELF, m, n, m, n, &Yargs, &Y);
        MatShellSetOperation(Y, MATOP_MULT, (void(*)(void))Ymult);

        Vec& LA23v = *LA23.vec();
        //Vec ne;
        //VecSetSizes(ne, m, PETSC_DECIDE);

        Vec G12, G23;
        VecDuplicate(*G12m.vec(), &G12);
        VecAXPY(G12, 1., *G12p.vec());
        VecDuplicate(*G23m.vec(), &G23);
        VecAXPY(G23, 1., *G23p.vec());

        Mat* vargs_m[] = { &(*B23m.mat()), &(*A2.mat()), &(*B23p.mat()), &(*A3.mat()), &(*B12p.mat()), &Z, &(*B12m.mat()), &(*A1.mat())};
        struct Ctx vctx; vctx.mats = vargs_m; vctx.ksps = Yargs_ksps;
        Vec* vargs_v[] = { &G23, &G12, &(*L1.vec()), &(*L2.vec()), &(*L3.vec()) };
        make_v(LA23v, &vctx, vargs_v);

        Vec& LA12v = *LA12.vec();

        KSP kspY;
        KSPCreate(PETSC_COMM_SELF, &kspY);
        KSPSetOperators(kspY, Y, Y, DIFFERENT_NONZERO_PATTERN);
	KSPSetTolerances(kspY, tol, tol, PETSC_DEFAULT, PETSC_DEFAULT);
        KSPSetType(kspY, KSPCG);
        KSPSolve(kspY, LA23v, LA23v);

        // Work backwards
        Vec Vk, Vm, &Vp = *U3.vec();

        VecCreate(PETSC_COMM_SELF, &Vm);
        VecCreate(PETSC_COMM_SELF, &Vk);

        VecGetSize(*L2.vec(), &m);
        VecSetSizes(Vm, m, PETSC_DECIDE);

        VecGetSize(*L1.vec(), &m);
        VecSetSizes(Vk, m, PETSC_DECIDE);

        VecSetFromOptions(Vm);
        VecSetFromOptions(Vk);

        // G12
        VecCopy(G12, LA12v);
        VecScale(LA12v, -1.);
        // A1I*L1
        KSPSolve(kspA1, *L1.vec(), Vk);
        // B12mT
        MatMultTransposeAdd(*B12m.mat(), Vk, LA12v, LA12v);
        VecScale(LA12v, -1.);
        // L2
        VecCopy(*L2.vec(), Vm);
        VecScale(Vm, -1.);
        // B23m*x13
        MatMultAdd(*B23m.mat(), LA23v, Vm, Vm);
        // A2I
        KSPSolve(kspA2, Vm, Vm);
        // B12pT
        MatMultTransposeAdd(*B12p.mat(), Vm, LA12v, LA12v);
        // Z
        KSPSolve(kspZ, LA12v, LA12v);

        // L1
        VecCopy(*L1.vec(), Vk);
        VecScale(Vk, -1.);
        // B12m*LA12
        MatMultAdd(*B12m.mat(), LA12v, Vk, Vk);
        VecScale(Vk, -1.);
        // A1I
        KSPSolve(kspA1, Vk, *U1.vec());

        // L2
        VecCopy(*L2.vec(), Vm);
        VecScale(Vm, -1.);
        // B12p*LA12
        MatMultAdd(*B12p.mat(), LA12v, Vm, Vm);
        // B23m*LA23
        MatMultAdd(*B23m.mat(), LA23v, Vm, Vm);
        VecScale(Vm, -1.);
        // A2I
        KSPSolve(kspA2, Vm, *U2.vec());

        // L3
        VecCopy(*L3.vec(), Vp);
        VecScale(Vp, -1.);
        // B23p*LA23
        MatMultAdd(*B23p.mat(), LA23v, Vp, Vp);
        VecScale(Vp, -1.);
        // A3I
        KSPSolve(kspA3, Vp, *U3.vec());

        VecDestroy(&Vm);
        VecDestroy(&Vk);

        VecDestroy(&G12);
        VecDestroy(&G23);
        MatDestroy(&Z);
        MatDestroy(&Y);
        KSPDestroy(&kspA1);
        KSPDestroy(&kspA2);
        KSPDestroy(&kspA3);
        KSPDestroy(&kspY);
        KSPDestroy(&kspZ);
}
