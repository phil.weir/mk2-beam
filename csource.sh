source outroot.sh

mkdir -p /tmp/csource/cmp1
mkdir -p /tmp/csource/cmp2
cd /tmp/csource/cmp1
tar -xzf $outroot/output.$1/source/source.tgz
echo Unpacked $1
cd /tmp/csource/cmp2
tar -xzf $outroot/output.$2/source/source.tgz
echo Unpacked $2

if [ "$3" != "" ]
then
	files=$3
else
	files=`find ./* -type f`
fi

for file in $files
do
	diff $file ../cmp1/$file > /dev/null 2> /dev/null
	diffresp=$?
	if [ "$diffresp" -ne "0" ]
	then
		echo $file
		diff ../cmp1/$file $file
	fi
done

rm -rf /tmp/csource
