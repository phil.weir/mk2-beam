// ALMOST VERBATIM FROM FENICS PROJECT!!!
//
// Copyright (C) 2007 Kristian B. Oelgaard.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2007-11-23
// Last changed: 2007-11-23

#ifndef __ALT_UNIT_INTERVAL_H
#define __ALT_UNIT_INTERVAL_H

#include <dolfin.h>
#include "plateproblem.h"

namespace dolfin
{


  class AltUnitInterval : public Mesh
  {
  public:
    
    AltUnitInterval(double length, uint nx, double cl);
    AltUnitInterval(Mesh& fs, PlateList& p, dolfin::MeshFunction<uint> **mf, dolfin::MeshFunction<uint> &fs_to_bdy, Mesh& bdy,
        dolfin::MeshFunction<uint> &p_to_bdy);

  };
  
}

#endif
