#!/bin/bash
source outroot.sh
cd $outroot
DIR=output.$1
head -n 30 $outroot/output.$1/mk2.$1.log > $outroot/output.$1/log-end
tail -n 30 $outroot/output.$1/mk2.$1.log >> $outroot/output.$1/log-end
tar -czf sim-$1.tgz $DIR/rms-*.csv $DIR/mbm-*.csv $DIR/left-*.csv $DIR/right-*.csv $DIR/plate1-*.csv $DIR/leftp-*.csv $DIR/parameters $outroot/output.$1/log-end
