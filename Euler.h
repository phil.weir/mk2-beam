#include "HEP.h"

class Euler : public HEP
{
      class EulPressureFunction;
      class EulerDotFromInterval;
      void Single_mesh_move (Mesh& mesh, PlateList& plate, Function& zeta, Function& zetaold, Function& varbottom, Function& varbottomold);

      int Single_run(char* dirname, bool resume);
};
