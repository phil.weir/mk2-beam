// ALMOST VERBATIM FROM FENICS PROJECT!!!
//
// Copyright (C) 2007 Kristian B. Oelgaard.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2007-11-23
// Last changed: 2007-11-23

#ifndef __DOUBLE_ALT_UNIT_INTERVAL_H
#define __DOUBLE_ALT_UNIT_INTERVAL_H

#include <dolfin.h>

namespace dolfin
{


  class DoubleAltUnitInterval : public Mesh
  {
  public:
    
    DoubleAltUnitInterval(double length, uint nx, double left1, double right1, double left2, double right2);

  };
  
}

#endif
