#!/bin/bash
#Setup from arg1 to arg2
source outroot.sh
cd ..
./beamprocsetup.sh $2
#cp $outroot/beamrun.output.$1/params/$1_* $outroot/beamrun.output.$2/params
cp $outroot/beamrun.output.$1/params/* $outroot/beamrun.output.$2/params
sed -e "s/\\$1_/$2_/" $outroot/beamrun.output.$1/params/beamrun.conf > $outroot/beamrun.output.$2/params/beamrun.conf
echo $1 > $outroot/beamrun.output.$2/copied_from
